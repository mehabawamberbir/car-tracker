## iTransit Modules
# CMD
npm cache clean --force


# Install npm-check-updates
$ npm i -g npm-check-updates

# Run npm-check-updates with -u, will upgrade package.json
$ ncu -u

# Install updated packages
$ npm install
npm install --force

# run
- run angular
ng serve
- run laravel
php -S localhost:8000 -t public

- run json server
json-server --watch db.json
