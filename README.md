# iTransit

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

## DEV Commands
- ng new angular-pwa --routing true --style scss
- ng add @angular/pwa
- npm start
- ng serve --host 0.0.0.0
- ng serve --host your-local-ip-address:port-number

## SERVICE WORKER
- ng build
- npm install --global http-server
- http-server -p 8080 -c-1 dist/smis-pwa
- http://localhost:8080/