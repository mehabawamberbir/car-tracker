<?php
namespace App\Contracts;

interface ContainerContract extends CommonContract{    
    function toggleStatus($id);
}