<?php
namespace App\Contracts;

interface GatePassContract extends CommonContract{    
    function toggleStatus($id);
}