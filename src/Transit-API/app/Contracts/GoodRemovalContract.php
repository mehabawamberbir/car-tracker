<?php
namespace App\Contracts;

interface GoodRemovalContract extends CommonContract{    
    function toggleStatus($id);
}

