<?php
namespace App\Contracts;

interface InvoiceContract extends CommonContract{    
    function getInvoices($type);      
    function getAllByType($type, $limit, $skip);
}

