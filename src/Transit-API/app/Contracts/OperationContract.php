<?php
namespace App\Contracts;

interface OperationContract extends CommonContract{    
    function getAllByType($type, $limit, $skip);
    function toggleStatus($id, $status);
}
