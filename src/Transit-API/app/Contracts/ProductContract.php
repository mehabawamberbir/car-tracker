<?php
namespace App\Contracts;

interface ProductContract extends CommonContract{        
    function getAllByType($type, $limit, $skip);    
    function listByType($type);
    function toggleStatus($id);
}

