<?php
namespace App\Contracts;

interface ReconciliationContract extends CommonContract{    
    function toggleStatus($id);
}

