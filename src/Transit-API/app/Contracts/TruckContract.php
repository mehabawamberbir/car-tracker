<?php
namespace App\Contracts;

interface TruckContract extends CommonContract{    
    function toggleStatus($id);
}