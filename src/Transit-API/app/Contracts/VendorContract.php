<?php
namespace App\Contracts;

interface VendorContract extends CommonContract{    
    function toggleStatus($id);
}
