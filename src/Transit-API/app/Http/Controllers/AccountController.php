<?php
namespace App\Http\Controllers;

use App\Contracts\AccountContract;
use Illuminate\Http\Request;

class AccountController extends Controller{
    
    private $_modelRepository;
    function __construct(AccountContract $contract)
    {
        $this->_modelRepository = $contract;
    }

    function getAll(Request $request){
        return $this->_modelRepository->getAll($request->limit, $request->skip);
    }

    function index()
    {
        return $this->_modelRepository->all();
    }

    function create(Request $request){
        
        //return response()->json('Record updated', 200);
        // $this->validate($request, [
        //     'code' => 'required|string',
        //     'name' => 'required|string'
        //  ]);
        return $this->_modelRepository->create($request);
    }

    
    function update(Request $req, $id){
        return $this->_modelRepository->update($req, $id);
    }

    function get($id){
        return $this->_modelRepository->get($id);
    }

    function query($attribute,$value){
        return $this->_modelRepository->query($attribute,$value);
    }

    function delete($id){
        return $this->_modelRepository->delete($id);
    }

    function toggleStatus($id){
        return $this->_modelRepository->toggleStatus($id);
    }
}