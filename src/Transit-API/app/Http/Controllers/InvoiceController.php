<?php
namespace App\Http\Controllers;

use App\Contracts\InvoiceContract;
use Illuminate\Http\Request;

class InvoiceController extends Controller{
    
    private $_modelRepository;
    function __construct(InvoiceContract $contract)
    {
        $this->_modelRepository = $contract;
    }

    function getAll(Request $request){
        return $this->_modelRepository->getAll($request->limit, $request->skip);
    }

    function getAllByType(Request $request){
        return $this->_modelRepository->getAllByType($request->type, $request->limit, $request->skip);
    }

    function index()
    {
        return $this->_modelRepository->all();
    }

    function create(Request $request){
        // $this->validate($request, [
        //     'invoice_no' => 'required|string',
        //     'operation_id' => 'required|string'
        //  ]);
        return $this->_modelRepository->create($request);
    }

    
    function update(Request $req, $id){
        return $this->_modelRepository->update($req, $id);
    }

    function get($id){
        return $this->_modelRepository->get($id);
    }

    function query($attribute,$value){
        return $this->_modelRepository->query($attribute,$value);
    }

    function getInvoices($type){        
        return $this->_modelRepository->getInvoices($type);
    }

    function delete($id){
        return $this->_modelRepository->delete($id);
    }
}
