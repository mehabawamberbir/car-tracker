<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model{
    protected $table = "accounts";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];  

    // public $timestamps = false;

    public function AccountType()
    {
        return $this->belongsTo('App\Models\AccountType', 'account_type_id');
    }
}