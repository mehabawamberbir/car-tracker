<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountType extends Model{
    protected $table = "account_types";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];  

    // public $timestamps = false;
    
    public function Type()
    {
        return $this->belongsTo('App\Models\Lookup', 'coa_type_id');
    }
    public function children()
    {
        return $this->hasMany('App\Models\Account', 'account_type_id', 'id');
    }
}