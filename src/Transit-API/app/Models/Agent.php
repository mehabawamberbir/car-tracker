<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model{
    protected $table = "agents";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];  

    // public $timestamps = false;
}