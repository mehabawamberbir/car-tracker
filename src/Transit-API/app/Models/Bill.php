<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model{
    protected $table = "bills";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];
}