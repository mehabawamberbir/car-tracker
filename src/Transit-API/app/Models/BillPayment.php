<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillPayment extends Model{
    protected $table = "bill_payments";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];
}