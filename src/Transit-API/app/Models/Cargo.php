<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model{
    protected $table = "cargos";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];

    public function Operation()
    {
        return $this->belongsTo('App\Models\Operation', 'operation_id');
    }

    public function Location()
    {
        return $this->belongsTo('App\Models\Lookup', 'location_id');
    }
}