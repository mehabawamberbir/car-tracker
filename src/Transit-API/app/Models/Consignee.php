<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consignee extends Model{
    protected $table = "consignees";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];  

    // public $timestamps = false;
}