<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Container extends Model{
    protected $table = "containers";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];

    public function ContainerType()
    {
        return $this->belongsTo('App\Models\Lookup', 'container_type_id');
    }
    public function Operation()
    {
        return $this->belongsTo('App\Models\Operation', 'operation_id');
    }
    public function Location()
    {
        return $this->belongsTo('App\Models\Lookup', 'location_id');
    }
}