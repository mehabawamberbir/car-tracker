<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model{
    protected $table = "customers";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];  

    // public $timestamps = false;
    public function Group(){
        return $this->belongsTo('App\Models\Lookup','group_id');
    }
    public function AccountType(){
        return $this->belongsTo('App\Models\Lookup','account_id');   
    }
}