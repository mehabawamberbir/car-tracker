<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model{
    protected $table = "employees";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];  

    // public $timestamps = false;
    public function Group(){
        return $this->belongsTo('App\Models\Lookup','group_id');
    }
    public function Status(){
        return $this->belongsTo('App\Models\Lookup','status_id');   
    }
    public function JobTitle(){
        return $this->belongsTo('App\Models\Lookup','job_title_id');   
    }
    public function Department(){
        return $this->belongsTo('App\Models\Lookup','department_id');   
    }
}