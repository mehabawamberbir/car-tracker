<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model{
    protected $table = "expenses";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];
    

    public function Operation()
    {
        return $this->belongsTo('App\Models\Operation', 'operation_id');
    }    

    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}