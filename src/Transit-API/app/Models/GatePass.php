<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GatePass extends Model{
    protected $table = "gate_passes";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];

    public function Operation()
    {
        return $this->belongsTo('App\Models\Operation', 'operation_id');
    }
    
    public function GatePassContainers()
    {
        return $this->hasMany('App\Models\GatePassContainer', 'gate_pass_id', 'id');
    }
}