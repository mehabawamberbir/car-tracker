<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GatePassContainer extends Model{
    protected $table = "gate_pass_containers";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];    
    
    public function GatePass()
    {
        return $this->belongsTo('App\Models\GatePass', 'gate_pass_id');
    }
}