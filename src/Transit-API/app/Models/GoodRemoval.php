<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodRemoval extends Model{
    protected $table = "good_removals";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];

    
    
    public function Operation()
    {
        return $this->belongsTo('App\Models\Operation', 'operation_id');
    }
    
    public function GoodRemovalWaybills()
    {
        return $this->hasMany('App\Models\GoodRemovalWaybill', 'good_removal_id', 'id');
    }
}