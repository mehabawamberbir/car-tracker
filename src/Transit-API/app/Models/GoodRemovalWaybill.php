<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodRemovalWaybill extends Model{
    protected $table = "good_removal_waybills";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];    
    
    public function GoodRemoval()
    {
        return $this->belongsTo('App\Models\GoodRemoval', 'good_removal_id');
    }
}