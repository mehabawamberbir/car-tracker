<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model{
    protected $table = "invoices";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];

    public function Operation()
    {
        return $this->belongsTo('App\Models\Operation', 'operation_id');
    }

    public function Currency()
    {
        return $this->belongsTo('App\Models\Lookup', 'currency_id');
    }

    public function Customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function Consignee()
    {
        return $this->belongsTo('App\Models\Consignee', 'consignee_id');
    }
}