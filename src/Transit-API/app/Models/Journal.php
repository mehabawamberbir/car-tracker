<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model{
    protected $table = "journals";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];  

    // public $timestamps = false;
    
    public function JournalType()
    {
        return $this->belongsTo('App\Models\Lookup', 'journal_type_id');
    }
    public function Account()
    {
        return $this->belongsTo('App\Models\Account', 'account_id');
    }
}