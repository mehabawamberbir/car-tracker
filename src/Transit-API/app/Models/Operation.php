<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model{
    protected $table = "operations";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];

    public function Category()
    {
        return $this->belongsTo('App\Models\Lookup','category_id');
    }

    public function Consignee()
    {
        return $this->belongsTo('App\Models\Lookup','consignee_id');
    }

    public function Customer()
    {
        return $this->belongsTo('App\Models\Customer','freight_forwarder_id');
    }

    public function LoadingType()
    {
        return $this->belongsTo('App\Models\Lookup','loading_type_id');
    }

    public function Agent()
    {
        return $this->belongsTo('App\Models\Lookup','agent_id');
    }
}