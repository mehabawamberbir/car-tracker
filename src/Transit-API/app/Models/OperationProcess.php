<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OperationProcess extends Model{
    protected $table = "operation_processes";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];
    
    public function Operation()
    {
        return $this->belongsTo('App\Models\Operation', 'operation_id');
    }
}