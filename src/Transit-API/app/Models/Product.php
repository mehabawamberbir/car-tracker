<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{
    protected $table = "products";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];
 
    public function ProductType()
    {
        return $this->belongsTo('App\Models\Lookup', 'product_type_id');
    }

    public function IncomeAccount()
    {
        return $this->belongsTo('App\Models\Account', 'income_account_id');
    }

    public function BillableAccount()
    {
        return $this->belongsTo('App\Models\Account', 'billable_account_id');
    }

    public function ExpenseAccount()
    {
        return $this->belongsTo('App\Models\Account', 'exp_account_id');
    }
}