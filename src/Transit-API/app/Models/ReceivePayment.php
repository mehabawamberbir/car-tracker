<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReceivePayment extends Model{
    protected $table = "receive_payments";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];
}