<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reconciliation extends Model{
    protected $table = "reconciliations";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];   
    
    public function Account()
    {
        return $this->belongsTo('App\Models\Account', 'account_id');
    }
}