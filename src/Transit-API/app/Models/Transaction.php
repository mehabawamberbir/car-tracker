<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model{
    protected $table = "transactions";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];  
    
    public function TransactionType()
    {
        return $this->belongsTo('App\Models\Lookup', 'transaction_type_id');
    }  
    
    public function Account()
    {
        return $this->belongsTo('App\Models\Account', 'account_id');
    }
    
    public function Currency()
    {
        return $this->belongsTo('App\Models\Lookup', 'currency_id');
    }
    
    public function Journal()
    {
        return $this->belongsTo('App\Models\Journal', 'journal_id');
    }
    
    public function TransactionItems()
    {
        return $this->hasMany('App\Models\TransactionItem', 'transaction_id', 'id');
    }
}