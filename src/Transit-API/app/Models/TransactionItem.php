<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model{
    protected $table = "transaction_items";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];    
    
    public function Transaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'transaction_id');
    }
    
    public function Account()
    {
        return $this->belongsTo('App\Models\Account', 'account_id');
    }
}