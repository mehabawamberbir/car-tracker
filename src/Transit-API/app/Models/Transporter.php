<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transporter extends Model{
    protected $table = "transporters";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];
}