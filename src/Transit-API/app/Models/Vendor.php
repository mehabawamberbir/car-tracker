<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model{
    protected $table = "vendors";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];  

    // public $timestamps = false;
    public function Group(){
        return $this->belongsTo('App\Models\Lookup','group_id');
    }
    public function Account(){
        return $this->belongsTo('App\Models\Account','account_id');   
    }
}