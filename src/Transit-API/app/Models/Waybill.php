<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Waybill extends Model{
    protected $table = "waybills";
    
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $fillable = [];
    protected $guarded = [];
    
    public function Operation()
    {
        return $this->belongsTo('App\Models\Operation', 'operation_id');
    }
    public function Transporter()
    {
        return $this->belongsTo('App\Models\Transporter', 'transporter_id');
    }
    public function LoadingType()
    {
        return $this->belongsTo('App\Models\Lookup', 'loading_type_id');
    }
}