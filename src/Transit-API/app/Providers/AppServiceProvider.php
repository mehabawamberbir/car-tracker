<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //$this->app->bind('App\Contracts\UserContract', 'App\Services\UserService');
        $this->app->bind('App\Contracts\LookupContract', 'App\Services\LookupService');
        $this->app->bind('App\Contracts\LookupTypeContract', 'App\Services\LookupTypeService');
        $this->app->bind('App\Contracts\CustomerContract', 'App\Services\CustomerService');
        $this->app->bind('App\Contracts\VendorContract', 'App\Services\VendorService');
        $this->app->bind('App\Contracts\AgentContract', 'App\Services\AgentService');
        $this->app->bind('App\Contracts\ConsigneeContract', 'App\Services\ConsigneeService');
        
        $this->app->bind('App\Contracts\OperationContract', 'App\Services\OperationService');
        $this->app->bind('App\Contracts\OperationProcessContract', 'App\Services\OperationProcessService');
        $this->app->bind('App\Contracts\AccountTypeContract', 'App\Services\AccountTypeService');
        $this->app->bind('App\Contracts\AccountContract', 'App\Services\AccountService');
        $this->app->bind('App\Contracts\EmployeeContract', 'App\Services\EmployeeService');
        $this->app->bind('App\Contracts\ProductContract', 'App\Services\ProductService');
        $this->app->bind('App\Contracts\InvoiceContract', 'App\Services\InvoiceService');
        $this->app->bind('App\Contracts\ExpenseContract', 'App\Services\ExpenseService');
        $this->app->bind('App\Contracts\ContainerContract', 'App\Services\ContainerService');

        
        $this->app->bind('App\Contracts\JournalContract', 'App\Services\JournalService');
        $this->app->bind('App\Contracts\TransactionContract', 'App\Services\TransactionService');
        $this->app->bind('App\Contracts\TransactionItemContract', 'App\Services\TransactionItemService');
        $this->app->bind('App\Contracts\ReconciliationContract', 'App\Services\ReconciliationService');

        $this->app->bind('App\Contracts\CargoContract', 'App\Services\CargoService');
        $this->app->bind('App\Contracts\TruckContract', 'App\Services\TruckService');
        $this->app->bind('App\Contracts\GatePassContract', 'App\Services\GatePassService');
        $this->app->bind('App\Contracts\GoodRemovalContract', 'App\Services\GoodRemovalService');
        $this->app->bind('App\Contracts\WaybillContract', 'App\Services\WaybillService');
        $this->app->bind('App\Contracts\TransporterContract', 'App\Services\TransporterService');

    }

    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
