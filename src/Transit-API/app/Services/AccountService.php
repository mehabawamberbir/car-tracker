<?php
namespace App\Services;

use App\Contracts\AccountContract;
use App\Models\Account;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
class AccountService implements AccountContract {

    protected $_model = "App\\Models\\Account";
    protected $_intermediaries = ['AccountType'];

    function create($request){

        $model = new $this->_model();

        $model->id = (string) Str::uuid();  

        $model->name = $request->name;
        $model->code = $request->code;
        $model->description = $request->description;
        $model->account_type_id = $request->account_type_id;
        $model->class_id = $request->class_id;
        $model->sub_account_id = $request->sub_account_id;
        $model->reconcileable = $request->reconcileable;
        $model->status = 1;
        $model->save();
            return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id); 

            $model->name = $request->name;
            $model->code = $request->code;
            $model->description = $request->description;
            $model->account_type_id = $request->account_type_id;
            $model->class_id = $request->class_id;
            $model->sub_account_id = $request->sub_account_id;
            $model->reconcileable = $request->reconcileable;
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('order')->get();      
        return response()->json($resultList);
    }

    function toggleStatus($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => ($obj->status ^ 1))); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}