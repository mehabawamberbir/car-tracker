<?php
namespace App\Services;

use App\Contracts\AgentContract;
use App\Models\Agent;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
class AgentService implements AgentContract {

    protected $_model = "App\\Models\\Agent";
    protected $_intermediaries = [];

    function create($request){

        $model = new $this->_model();

        $model->id = (string) Str::uuid();  

        $model->name = $request->name;
        $model->code = $request->code;
        $model->telephone = $request->telephone;
        $model->email = $request->email;
        $model->address = $request->address;
        $model->address2 = $request->address2;
        $model->bp = $request->bp;
        $model->city = $request->city;
        $model->country = $request->country;
        $model->terms = $request->terms;
        $model->remarks = $request->remarks;
        $model->start_date = $request->start_date;
        $model->end_date = $request->end_date;
        $model->usd_rate = $request->usd_rate;
        $model->status = 1;
        $model->save();
            return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id);  

            $model->name = $request->name;
            $model->code = $request->code;
            $model->telephone = $request->telephone;
            $model->email = $request->email;
            $model->address = $request->address;
            $model->address2 = $request->address2;
            $model->bp = $request->bp;
            $model->city = $request->city;
            $model->country = $request->country;
            $model->terms = $request->terms;
            $model->remarks = $request->remarks;
            $model->start_date = $request->start_date;
            $model->end_date = $request->end_date;
            $model->usd_rate = $request->usd_rate;
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('order')->get();      
        return response()->json($resultList);
    }

    function toggleStatus($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => ($obj->status ^ 1))); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}