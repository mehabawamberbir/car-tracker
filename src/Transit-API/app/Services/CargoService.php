<?php
namespace App\Services;

use App\Contracts\CargoContract;
use App\Models\Cargo;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
class CargoService implements CargoContract {

    protected $_model = "App\\Models\\Cargo";
    protected $_intermediaries = ['Operation','Location'];

    function create($request){

        $model = new $this->_model();

        $model->id = (string) Str::uuid();  
        
        $model->operation_id = $request->operation_id;
        $model->chasis_no = $request->chasis_no;
        $model->quantity = $request->quantity;
        $model->weight = $request->weight;
        $model->description = $request->description;
        $model->way_bill_no = $request->way_bill_no;
        $model->truck_no = $request->truck_no;
        $model->gate_pass = $request->gate_pass;
        $model->sydonia_ref = $request->sydonia_ref;
        $model->location_id = $request->location_id;

        $model->status = 1;
        $model->save();
            return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id);   
        
            $model->operation_id = $request->operation_id;
            $model->chasis_no = $request->chasis_no;
            $model->quantity = $request->quantity;
            $model->weight = $request->weight;
            $model->description = $request->description;
            $model->way_bill_no = $request->way_bill_no;
            $model->truck_no = $request->truck_no;
            $model->gate_pass = $request->gate_pass;
            $model->sydonia_ref = $request->sydonia_ref;
            $model->location_id = $request->location_id;
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('order')->get();      
        return response()->json($resultList);
    }

    function toggleStatus($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => ($obj->status ^ 1))); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}