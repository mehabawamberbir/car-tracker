<?php
namespace App\Services;

use App\Contracts\ContainerContract;
use App\Models\Container;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
class ContainerService implements ContainerContract {

    protected $_model = "App\\Models\\Container";
    protected $_intermediaries = ['Operation','ContainerType','Location'];

    function create($request){

        $model = new $this->_model();

        $model->id = (string) Str::uuid();  

        $model->operation_id = $request->operation_id;
        $model->container_type_id = $request->container_type_id;
        $model->container_no = $request->container_no;
        $model->seal_no = $request->seal_no;
        $model->voyage_no = $request->voyage_no;
        $model->description = $request->description;
        $model->quantity = $request->quantity;
        $model->gross_weight = $request->gross_weight;
        $model->port_out_date = $request->port_out_date;
        $model->empty_date = $request->empty_date;
        $model->return_date = $request->return_date;
        $model->truck_no = $request->truck_no;
        $model->refund = $request->refund;
        $model->deposit = $request->deposit;
        $model->gate_pass_no = $request->gate_pass_no;
        $model->way_bill_no = $request->way_bill_no;
        $model->declaration_no = $request->declaration_no;
        $model->declaration_no4 = $request->declaration_no4;
        $model->declaration_date = $request->declaration_date;
        $model->destination = $request->destination;
        $model->certification_no = $request->certification_no;
        $model->vgm_form = $request->vgm_form;
        $model->location_id = $request->location_id;
        $model->status = 1;

        $model->save();
            return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id);  

            $model->operation_id = $request->operation_id;
            $model->container_type_id = $request->container_type_id;
            $model->container_no = $request->container_no;
            $model->seal_no = $request->seal_no;
            $model->voyage_no = $request->voyage_no;
            $model->description = $request->description;
            $model->quantity = $request->quantity;
            $model->gross_weight = $request->gross_weight;
            $model->port_out_date = $request->port_out_date;
            $model->empty_date = $request->empty_date;
            $model->return_date = $request->return_date;
            $model->truck_no = $request->truck_no;
            $model->refund = $request->refund;
            $model->deposit = $request->deposit;
            $model->gate_pass_no = $request->gate_pass_no;
            $model->way_bill_no = $request->way_bill_no;
            $model->declaration_no = $request->declaration_no;
            $model->declaration_no4 = $request->declaration_no4;
            $model->declaration_date = $request->declaration_date;
            $model->destination = $request->destination;
            $model->certification_no = $request->certification_no;
            $model->vgm_form = $request->vgm_form;
            $model->location_id = $request->location_id;
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('created_at')->get();      
        return response()->json($resultList);
    }

    function toggleStatus($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => ($obj->status ^ 1))); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}