<?php
namespace App\Services;

use App\Contracts\EmployeeContract;
use App\Models\Employee;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
class EmployeeService implements EmployeeContract {

    protected $_model = "App\\Models\\Employee";
    protected $_intermediaries = ['Group','JobTitle','Department','Status'];

    function create($request){

        $model = new $this->_model();

        $model->id = (string) Str::uuid();  

        $model->name = $request->name;
        $model->department_id = $request->department_id;
        $model->telephone = $request->telephone;
        $model->gender = $request->gender;
        $model->picture = $request->picture;
        $model->group_id = $request->group_id;
        $model->home_tel = $request->home_tel;
        $model->work_tel = $request->work_tel;
        $model->work_email = $request->work_email;
        $model->other_email = $request->other_email;
        $model->address = $request->address;
        $model->job_title_id = $request->job_title_id;
        $model->status_id = $request->status_id;
        $model->contract_details = $request->contract_details;
        $model->start_date = $request->start_date;
        $model->end_date = $request->end_date;
        $model->end_reason = $request->end_reason;

        $model->save();
            return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id);  

            $model->name = $request->name;
            $model->department_id = $request->department_id;
            $model->telephone = $request->telephone;
            $model->gender = $request->gender;
            $model->picture = $request->picture;
            $model->group_id = $request->group_id;
            $model->home_tel = $request->home_tel;
            $model->work_tel = $request->work_tel;
            $model->work_email = $request->work_email;
            $model->other_email = $request->other_email;
            $model->address = $request->address;
            $model->job_title_id = $request->job_title_id;
            $model->status_id = $request->status_id;
            $model->contract_details = $request->contract_details;
            $model->start_date = $request->start_date;
            $model->end_date = $request->end_date;
            $model->end_reason = $request->end_reason;
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('order')->get();      
        return response()->json($resultList);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}