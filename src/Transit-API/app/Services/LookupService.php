<?php
namespace App\Services;

use App\Contracts\LookupContract;
use App\Models\Lookup;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use App\Models\LookupType;
class LookupService implements LookupContract {

    protected $_model = "App\\Models\\Lookup";
    protected $_intermediaries = ['LookupType'];

    function create($request){

        $model = new $this->_model();

        $model->id = (string) Str::uuid();  

        $model->code = $request->code;    
        $model->lookup_type_id = $request->lookup_type_id;  
        $model->name = $request->name;   
        $model->local_name = $request->local_name;  
        $model->order = $request->order;
        $model->status = 1;
        $model->save();
        
        return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id);  

            $model->code = $request->code;              
            $model->lookup_type_id = $request->lookup_type_id;   
            $model->name = $request->name;   
            $model->local_name = $request->local_name;  
            $model->order = $request->order;
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->orderBy('lookup_type_id')->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        // $resultList = $this->_model::with($this->_intermediaries)->orderBy('name')->paginate($limit);
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->orderBy('lookup_type_id')->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('lookup_type_id')->orderBy('order')->get();      
        return response()->json($resultList);
    }

    public function getLookups($type)
    {     
        $lookupType = LookupType::where('code',$type)->first();
        if($lookupType != null){            
            $resultList = $this->_model::where('lookup_type_id', $lookupType->id)->orderBy('order')->get();
            return response()->json($resultList);
        }
        else 
            return response()->json('DATA NOT FOUND', 404);

    }

    function toggleStatus($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => ($obj->status ^ 1))); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}