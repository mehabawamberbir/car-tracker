<?php
namespace App\Services;

use App\Contracts\OperationProcessContract;
use App\Models\OperationProcess;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use App\Models\Operation;
class OperationProcessService implements OperationProcessContract {

    protected $_model = "App\\Models\\OperationProcess";
    protected $_intermediaries = ['Operation'];

    // create or update method
    function create($request){

        $model = new $this->_model();

        $operationProcess = $this->_model::where('operation_id',$request->operation_id)->first();  
        if($operationProcess) $model = $operationProcess;
        else 
            $model->id = (string) Str::uuid();  

        $model->operation_id = $request->operation_id;
        $model->declaration_no = $request->declaration_no;
        $model->declaration_date = $request->declaration_date;
        $model->vessel_name = $request->vessel_name;
        $model->voyage_no = $request->voyage_no;
        $model->partial_shipment = $request->partial_shipment;
        $model->container_status = $request->container_status;
        $model->stuffing_date = $request->stuffing_date;
        $model->gross_weight = $request->gross_weight;
        $model->net_weight = $request->net_weight;
        $model->description = $request->description;
        $model->declaration_no_eth = $request->declaration_no_eth;
        $model->assigned_no_declarant = $request->assigned_no_declarant;
        $model->assigned_no_importer = $request->assigned_no_importer;
        $model->border_office = $request->border_office;
        $model->clearance_office = $request->clearance_office;
        $model->fumigation = $request->fumigation;
        $model->weight_inspection = $request->weight_inspection;
        $model->inspection = $request->inspection;
        $model->craft_paper = $request->craft_paper;

        $model->save();

        $operation = Operation::find($request->operation_id);
        if($operation && $operation->staus != $request->staus){
            $operation->status = $request->staus;
            $operation->save();
        }
        return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id); 

            $model->operation_id = $request->operation_id;
            $model->declaration_no = $request->declaration_no;
            $model->declaration_date = $request->declaration_date;
            $model->vessel_name = $request->vessel_name;
            $model->voyage_no = $request->voyage_no;
            $model->partial_shipment = $request->partial_shipment;
            $model->container_status = $request->container_status;
            $model->stuffing_date = $request->stuffing_date;
            $model->gross_weight = $request->gross_weight;
            $model->net_weight = $request->net_weight;
            $model->description = $request->description;
            $model->declaration_no_eth = $request->declaration_no_eth;
            $model->assigned_no_declarant = $request->assigned_no_declarant;
            $model->assigned_no_importer = $request->assigned_no_importer;
            $model->border_office = $request->border_office;
            $model->clearance_office = $request->clearance_office;
            $model->fumigation = $request->fumigation;
            $model->weight_inspection = $request->weight_inspection;
            $model->inspection = $request->inspection;
            $model->craft_paper = $request->craft_paper;
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->get();      
        return response()->json($resultList);
    }

    function toggleStatus($id, $status)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => $status)); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}