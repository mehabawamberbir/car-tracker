<?php
namespace App\Services;

use App\Contracts\OperationContract;
use App\Models\Operation;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
class OperationService implements OperationContract {

    protected $_model = "App\\Models\\Operation";
    protected $_intermediaries = ['Customer','Consignee','Category','Agent','LoadingType'];

    function create($request){

        $model = new $this->_model();

        $model->id = (string) Str::uuid();  

        $model->category_id = $request->category_id;
        $model->operation_no = $request->operation_no;
        $model->reference_no = $request->reference_no;
        $model->consignee_id = $request->consignee_id;
        $model->freight_forwarder_id = $request->freight_forwarder_id;
        $model->reception_date = $request->reception_date;
        $model->registration_date = $request->registration_date;
        $model->atd_operation = $request->atd_operation;
        $model->esl_operation = $request->esl_operation;
        $model->bol = $request->bol;
        $model->quantity = $request->quantity;
        $model->gross_weight = $request->gross_weight;
        $model->loading_type_id = $request->loading_type_id;
        $model->total_vehicle = $request->total_vehicle;
        $model->total_cbm = $request->total_cbm;
        $model->partial_shipment = $request->partial_shipment;
        $model->container_status = $request->container_status;
        $model->destination = $request->destination;
        $model->agent_id = $request->agent_id;
        $model->arrival_date = $request->arrival_date;
        $model->description = $request->description;
        $model->attachement_file = $request->attachement_file;
        $model->created_by = $request->created_by;
        $model->status = 2;
        $model->save();
            return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id); 

            $model->category_id = $request->category_id;
            $model->operation_no = $request->operation_no;
            $model->reference_no = $request->reference_no;
            $model->consignee_id = $request->consignee_id;
            $model->freight_forwarder_id = $request->freight_forwarder_id;
            $model->reception_date = $request->reception_date;
            $model->registration_date = $request->registration_date;
            $model->booking_no = $request->booking_no; 
            $model->atd_operation = $request->atd_operation;
            $model->esl_operation = $request->esl_operation;
            $model->bol = $request->bol;
            $model->quantity = $request->quantity;
            $model->gross_weight = $request->gross_weight;
            $model->loading_type_id = $request->loading_type_id;
            $model->total_vehicle = $request->total_vehicle;
            $model->total_cbm = $request->total_cbm;
            $model->partial_shipment = $request->partial_shipment;
            $model->container_status = $request->container_status;
            $model->destination = $request->destination;
            $model->agent_id = $request->agent_id;
            $model->arrival_date = $request->arrival_date;
            $model->description = $request->description;
            $model->attachement_file = $request->attachement_file;
            $model->created_by = $request->created_by; 
            $model->status = $request->status; 
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    function getAllByType($type, $limit, $skip){

        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->where('category_id',$type)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('order')->get();      
        return response()->json($resultList);
    }

    function toggleStatus($id, $status)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => $status)); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}