<?php
namespace App\Services;

use App\Contracts\ProductContract;
use App\Models\Product;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
class ProductService implements ProductContract {

    protected $_model = "App\\Models\\Product";
    protected $_intermediaries = ['ProductType','IncomeAccount', 'BillableAccount','ExpenseAccount'];

    function create($request){

        $model = new $this->_model();

        $model->id = (string) Str::uuid();  

        $model->is_sales_product = $request->is_sales_product;
        $model->product_type_id = $request->product_type_id;
        $model->name = $request->name;
        $model->description = $request->description;
        $model->unit_price = $request->unit_price;
        $model->is_billable = $request->is_billable;
        $model->is_sellable = $request->is_sellable;
        $model->invoice_account_id = $request->invoice_account_id;
        $model->income_account_id = $request->income_account_id;
        $model->purchasable = $request->purchasable;
        $model->exp_account_id = $request->exp_account_id;
        $model->view_at_invoice_detail = $request->view_at_invoice_detail;
        $model->trackable = $request->trackable;
        $model->disp_at_operation_exp = $request->disp_at_operation_exp;
        $model->status = 1;
        $model->save();
            return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id); 

            $model->is_sales_product = $request->is_sales_product;
            $model->product_type_id = $request->product_type_id;
            $model->name = $request->name;
            $model->description = $request->description;
            $model->unit_price = $request->unit_price;
            $model->is_billable = $request->is_billable;
            $model->is_sellable = $request->is_sellable;
            $model->invoice_account_id = $request->invoice_account_id;
            $model->income_account_id = $request->income_account_id;
            $model->purchasable = $request->purchasable;
            $model->exp_account_id = $request->exp_account_id;
            $model->view_at_invoice_detail = $request->view_at_invoice_detail;
            $model->trackable = $request->trackable;
            $model->disp_at_operation_exp = $request->disp_at_operation_exp;  
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function listByType($type){        
        $sellable = $type == 'sellable' || $type == 'all';
        $resultList = $this->_model::with($this->_intermediaries)->where('is_sellable',$sellable)->get();
        return response()->json($resultList, 200);        
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    function getAllByType($type, $limit, $skip){
        $sellable = $type == 'sellable' || $type == 'all';
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->where('is_sellable',$sellable)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('order')->get();      
        return response()->json($resultList);
    }

    function toggleStatus($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => ($obj->status ^ 1))); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}