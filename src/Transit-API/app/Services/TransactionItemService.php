<?php
namespace App\Services;

use App\Contracts\TransactionItemContract;
use App\Models\TransactionItem;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
class TransactionItemService implements TransactionItemContract {

    protected $_model = "App\\Models\\TransactionItem";
    protected $_intermediaries = ['Transaction','Account'];

    function create($request){

        $model = new $this->_model();

        $model->id = (string) Str::uuid();  
        $model->transaction_id = $request->transaction_id;
        $model->account_id = $request->account_id;
        $model->debit = $request->debit;
        $model->credit = $request->credit;
        $model->description = $request->description;
        
        $model->status = 1;
        $model->save();
            return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id); 

            $model->transaction_id = $request->transaction_id;
            $model->account_id = $request->account_id;
            $model->debit = $request->debit;
            $model->credit = $request->credit;
            $model->description = $request->description;
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('order')->get();      
        return response()->json($resultList);
    }

    function toggleStatus($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => ($obj->status ^ 1))); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}