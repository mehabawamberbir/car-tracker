<?php
namespace App\Services;

use App\Contracts\TransactionContract;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

class TransactionService implements TransactionContract {

    protected $_model = "App\\Models\\Transaction";
    protected $_transactionItem = "App\\Models\\TransactionItem";
    protected $_intermediaries = ['TransactionType', 'Account', 'Currency', 'Journal', 'TransactionItems'];

    function create($request){

        \DB::beginTransaction();
        try{
            $model = new $this->_model();

            $id = (string) Str::uuid();
            $model->id = $id;  
    
            $model->transaction_date = $request->transaction_date;
            $model->amount = $request->amount;
            $model->reference_no = $request->reference_no;
            $model->description = $request->description;
            $model->currency_id = $request->currency_id;
            $model->journal_id = $request->journal_id;
            $model->created_by = $request->created_by;
            
            $model->status = 1;
            $model->save();

            $items = collect(new $this->_transactionItem());

            foreach ($request->transaction_items as $item) {
                $obj = new $this->_transactionItem();
                $obj->id = (string) Str::uuid();
    
                $obj->transaction_id = $id;
                $obj->account_id = $item["account_id"];
                $obj->debit = $item["debit"];
                $obj->credit = $item["credit"];
                $obj->description = $item["description"];
                
                $obj->status = 1;
                $items->push($obj);
                //$obj->save();
            }
            $model->TransactionItems()->saveMany($items);
        }
        catch (\Exception $e) {
            dd($e);
            \DB::rollback();
            return response()->json('Record not found', 404);
        }    

        \DB::commit();

        return response()->json($model, 201);

    }
    
    function update($request, $id){
        
        \DB::beginTransaction();
        try
        {
            $model = $this->_model::findOrFail($id); 
    
            $model->transaction_date = $request->transaction_date;
            $model->amount = $request->amount;
            $model->reference_no = $request->reference_no;
            $model->description = $request->description;
            $model->currency_id = $request->currency_id;
            $model->journal_id = $request->journal_id;
            $model->created_by = $request->created_by;

            $model->save();

            $items = collect(new $this->_transactionItem());
            
            $this->_transactionItem::where('transaction_id',$id)->delete();

            foreach ($request->transaction_items as $item) {
                $obj = new $this->_transactionItem();
                $obj->id = (string) Str::uuid();
    
                $obj->transaction_id = $id;
                $obj->account_id = $item["account_id"];
                $obj->debit = $item["debit"];
                $obj->credit = $item["credit"];
                $obj->description = $item["description"];
                
                $obj->status = 1;
                $items->push($obj);
            }
            $model->TransactionItems()->saveMany($items);
        }
        catch (\Exception $e) {
            dd($e);
            \DB::rollback();
            return response()->json('Record not found', 404);
        }    

        \DB::commit();

        return response()->json($model, 200);
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('order')->get();      
        return response()->json($resultList);
    }

    function toggleStatus($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => ($obj->status ^ 1))); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}