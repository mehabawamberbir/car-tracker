<?php
namespace App\Services;

use App\Contracts\WaybillContract;
use App\Models\Waybill;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
class WaybillService implements WaybillContract {

    protected $_model = "App\\Models\\Waybill";
    protected $_intermediaries = ['Operation','Transporter','LoadingType'];

    function create($request){

        $model = new $this->_model();

        $model->id = (string) Str::uuid();  

        $model->operation_id = $request->operation_id;
        $model->transporter_id = $request->transporter_id;
        $model->departure_date = $request->departure_date;
        $model->driver_name = $request->driver_name;
        $model->driver_tel = $request->driver_tel;
        $model->truck_no = $request->truck_no;
        $model->final_destination = $request->final_destination;
        $model->rate_agree = $request->rate_agree;
        $model->clearance_office = $request->clearance_office;
        $model->loading_type_id = $request->loading_type_id;
        $model->remarks = $request->remarks;

        $model->status = 1;

        $model->save();
            return response()->json($model, 201);

    }
    function update($request, $id){
        try{
            $model = $this->_model::findOrFail($id);  

            $model->operation_id = $request->operation_id;
            $model->transporter_id = $request->transporter_id;
            $model->departure_date = $request->departure_date;
            $model->driver_name = $request->driver_name;
            $model->driver_tel = $request->driver_tel;
            $model->truck_no = $request->truck_no;
            $model->final_destination = $request->final_destination;
            $model->rate_agree = $request->rate_agree;
            $model->clearance_office = $request->clearance_office;
            $model->loading_type_id = $request->loading_type_id;
            $model->remarks = $request->remarks;
            
            $model->save();
            return response()->json('Record updated', 200);
        }catch(ModelNotFoundException $e){
            return response()->json('Record not found', 404);
        }
    }

    function get($id)
    {
        $obj = $this->_model::with($this->_intermediaries)->findOrFail($id);
        return response()->json($obj, 200);
    }

    function all(){
        $resultList = $this->_model::with($this->_intermediaries)->get();
        return response()->json($resultList, 200);
    }

    function getAll($limit, $skip){
        $currentPage = $limit != 0 ? ($skip/$limit)+1 : 1;
        $resultList = $this->_model::with($this->_intermediaries)->paginate(
            $perPage = $limit, $columns = ['*'], $currentPage = $currentPage
        );
        return response()->json($resultList, 200);
    }

    public function query($attribute,$value)
    {                                
        $resultList = $this->_model::with($this->_intermediaries)->where($attribute,$value)->orderBy('order')->get();      
        return response()->json($resultList);
    }

    function toggleStatus($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->update(array('status' => ($obj->status ^ 1))); 
        return response()->json('Record status toggled', 200);
    }

    function delete($id)
    {
        $obj = $this->_model::findOrFail($id);
        $obj->delete();
        return response()->json('Record deleted successfuly', 200);
    }

}