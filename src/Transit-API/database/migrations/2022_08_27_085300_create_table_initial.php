<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInitial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('roles', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('user_roles', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');
            $table->uuid('role_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('roles');

        });

        Schema::create('lookup_types',function(Blueprint $table){
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('code');
            $table->integer('order')->default(0);
            $table->string('local_name')->nullable(); 
            $table->boolean('status');

            $table->timestamps();        
         });

        Schema::create('lookups',function(Blueprint $table){
            $table->uuid('id')->primary();
            $table->uuid('lookup_type_id');
            $table->string('name');
            $table->string('code');
            $table->integer('order')->default(0);
            $table->string('local_name')->nullable();
 
            $table->boolean('status');
            $table->timestamps();   
            
            $table->foreign('lookup_type_id')->references('id')->on('lookup_types');
         });
         
        Schema::create('customers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('code');
            $table->string('telephone')->nullable(); 
            $table->string('email')->nullable(); 
            $table->string('address')->nullable(); 
            $table->string('address2')->nullable(); 
            $table->string('bp')->nullable(); 
            $table->string('city')->nullable(); 
            $table->string('country')->nullable(); 
            $table->uuid('account_id')->nullable(); 
            $table->string('terms')->nullable(); 
            $table->double('usd_rate', 18,2)->nullable(); 
            $table->uuid('group_id')->nullable(); 
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('group_id')->references('id')->on('lookups');
        });
        
        Schema::create('vendors', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('code');
            $table->string('telephone')->nullable(); 
            $table->string('email')->nullable(); 
            $table->string('address')->nullable(); 
            $table->string('address2')->nullable(); 
            $table->string('bp')->nullable(); 
            $table->string('city')->nullable(); 
            $table->string('country')->nullable(); 
            $table->uuid('account_id')->nullable(); 
            $table->string('terms')->nullable(); 
            $table->string('balance')->nullable(); 
            $table->double('usd_rate', 18,2)->nullable(); 
            $table->uuid('group_id')->nullable(); 
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('group_id')->references('id')->on('lookups');
        });

        Schema::create('products', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('is_sales_product')->nullable();
            $table->string('product_type_id')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('unit_price')->nullable();
            $table->string('is_billable')->nullable();
            $table->string('is_sellable')->nullable();
            $table->string('invoice_account_id')->nullable();
            $table->string('income_account_id')->nullable();
            $table->string('purchasable')->nullable();
            $table->string('exp_account_id')->nullable();
            $table->string('view_at_invoice_detail')->nullable();
            $table->string('trackable')->nullable();
            $table->string('disp_at_operation_exp')->nullable();
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('product_type_id')->references('id')->on('lookups');
        });

        Schema::create('agents', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('code');
            $table->string('telephone')->nullable(); 
            $table->string('email')->nullable(); 
            $table->string('address')->nullable(); 
            $table->string('address2')->nullable(); 
            $table->string('bp')->nullable(); 
            $table->string('city')->nullable(); 
            $table->string('country')->nullable(); 
            $table->string('terms')->nullable();  
            $table->date('start_date')->nullable(); 
            $table->date('end_date')->nullable(); 
            $table->string('remarks')->nullable(); 
            $table->double('usd_rate', 18,2)->nullable(); 
            $table->boolean('status');
            $table->timestamps();

        });
        
        Schema::create('consignees', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('code');
            $table->string('telephone')->nullable(); 
            $table->string('email')->nullable(); 
            $table->string('address')->nullable(); 
            $table->string('address2')->nullable(); 
            $table->string('bp')->nullable(); 
            $table->string('city')->nullable(); 
            $table->string('country')->nullable();
            $table->string('terms')->nullable(); 
            $table->double('usd_rate', 18,2)->nullable(); 
            $table->boolean('status');
            $table->timestamps();

        });

        Schema::create('transporters', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('code');
            $table->string('telephone')->nullable(); 
            $table->string('email')->nullable(); 
            $table->string('address')->nullable(); 
            $table->string('address2')->nullable(); 
            $table->string('bp')->nullable(); 
            $table->string('city')->nullable(); 
            $table->string('country')->nullable();
            $table->string('remarks')->nullable(); 
            $table->boolean('status');
            $table->timestamps();

        });
        Schema::create('operations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('category_id')->nullable();
            $table->string('operation_no')->nullable();
            $table->string('reference_no')->nullable();
            $table->uuid('consignee_id')->nullable();
            $table->uuid('freight_forwarder_id')->nullable();
            $table->date('reception_date')->nullable();
            $table->date('registration_date')->nullable();
            $table->string('atd_operation')->nullable();
            $table->string('esl_operation')->nullable();
            $table->string('bol')->nullable();
            $table->integer('quantity')->default(0);
            $table->double('gross_weight', 18, 2)->default(0);
            $table->uuid('loading_type_id')->nullable();
            $table->string('partial_shipment')->nullable();
            $table->string('container_status')->nullable();
            $table->string('destination')->nullable();
            $table->string('agent_id')->nullable();
            $table->string('arrival_date')->nullable();
            $table->string('description')->nullable();
            $table->string('created_by')->nullable();
            $table->integer('status');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('lookups');
            $table->foreign('consignee_id')->references('id')->on('consignees');
            $table->foreign('freight_forwarder_id')->references('id')->on('customers');
            $table->foreign('loading_type_id')->references('id')->on('lookups');
            $table->foreign('agent_id')->references('id')->on('agents');
        });

        Schema::create('expenses', function (Blueprint $table){
            $table->uuid('id')->primary();
            $table->uuid('product_id')->nullable();
            $table->string('reference_no')->nullable();
            $table->double('amount', 18, 2)->default(0);
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('trucks', function (Blueprint $table){
            $table->uuid('id')->primary();
            $table->uuid('operation_id')->nullable();
            $table->string('chasis_no')->nullable();
            $table->double('quantity', 18, 2)->default(0);
            $table->double('weight', 18, 2)->default(0);
            $table->string('description')->nullable();
            $table->string('way_bill_no')->nullable();
            $table->string('truck_no')->nullable();
            $table->string('gate_pass')->nullable();
            $table->string('sydonia_ref')->nullable();
            $table->uuid('location_id')->nullable();
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('location_id')->references('id')->on('lookups');
        });

        Schema::create('cargos', function (Blueprint $table){
            $table->uuid('id')->primary();
            $table->uuid('operation_id')->nullable();
            $table->string('chasis_no')->nullable();
            $table->double('quantity', 18, 2)->default(0);
            $table->double('weight', 18, 2)->default(0);
            $table->string('description')->nullable();
            $table->string('way_bill_no')->nullable();
            $table->string('truck_no')->nullable();
            $table->string('gate_pass')->nullable();
            $table->string('sydonia_ref')->nullable();
            $table->uuid('location_id')->nullable();
            $table->boolean('status');
            $table->timestamps();
            
            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('location_id')->references('id')->on('lookups');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('user_roles');
        
        Schema::dropIfExists('lookup_types');
        Schema::dropIfExists('lookups');
        Schema::dropIfExists('customers');   

        Schema::dropIfExists('vendors');
        Schema::dropIfExists('products');
        Schema::dropIfExists('agents');
        Schema::dropIfExists('consignees');
        Schema::dropIfExists('transporters');
        Schema::dropIfExists('operations');  
        Schema::dropIfExists('expenses');   
        Schema::dropIfExists('trucks');   
        Schema::dropIfExists('cargos');   
        Schema::dropIfExists('containers');  
        Schema::dropIfExists('gate_passes');    
        Schema::dropIfExists('way_bills');   
        Schema::dropIfExists('good_removals');  
    }
}
