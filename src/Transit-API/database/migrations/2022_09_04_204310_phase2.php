<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Phase2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::create('account_types', function (Blueprint $table) {
            $table->uuid('id')->primary();            
            $table->uuid('coa_type_id')->nullable(); 
            $table->string('code');
            $table->string('name');
            $table->string('description')->nullabel();
            $table->boolean('status');

            $table->timestamps();
            $table->foreign('coa_type_id')->references('id')->on('lookups');
        });        

        Schema::create('accounts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('account_type_id'); 
            $table->uuid('class_id')->nullable(); 
            $table->string('name');
            $table->string('code');
            $table->string('description')->nullable(); 
            $table->uuid('sub_account_id')->nullable(); 
            $table->boolean('status');
            $table->boolean('reconcileable');
            $table->timestamps();

            $table->foreign('account_type_id')->references('id')->on('account_types');
            $table->foreign('class_id')->references('id')->on('lookups');
        });
        
        // Schema::create('journals', function (Blueprint $table) {
        //     $table->uuid('id')->primary();            
        //     $table->uuid('journal_type_id'); 
        //     $table->string('code');
        //     $table->string('name');
        //     $table->string('description')->nullabel();        
        //     $table->uuid('account_id'); 
        //     $table->string('created_by')->nullable();
        //     $table->boolean('status');

        //     $table->timestamps();
        //     $table->foreign('journal_type_id')->references('id')->on('lookups');
        //     $table->foreign('account_id')->references('id')->on('accounts');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::dropIfExists('account_types');
        Schema::dropIfExists('accounts');
        Schema::dropIfExists('journals');
    }
}
