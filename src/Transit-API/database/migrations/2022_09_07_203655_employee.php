<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Employee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->uuid('id')->primary;
            $table->string('name');
            $table->uuid('department_id')->nullable(); 
            $table->string('telephone')->nullable(); 
            $table->string('gender')->nullable(); 
            $table->string('picture')->nullable(); 
            $table->uuid('group_id')->nullable(); 
            $table->string('home_tel')->nullable(); 
            $table->string('work_tel')->nullable();
            $table->string('work_email')->nullable(); 
            $table->string('other_email');
            $table->string('address')->nullable();
            $table->uuid('job_title_id')->nullable(); 
            $table->uuid('status_id')->nullable();
            $table->string('contract_details')->nullable();
            $table->string('start_date')->date(); 
            $table->string('end_date')->date();
            $table->string('end_reason')->nullable();  
            
            $table->timestamps();

            $table->foreign('department_id')->references('id')->on('lookups');
            $table->foreign('group_id')->references('id')->on('lookups');
            $table->foreign('status_id')->references('id')->on('lookups');
            $table->foreign('job_title_id')->references('id')->on('lookups');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {         
        Schema::dropIfExists('employees');    
    }
}
