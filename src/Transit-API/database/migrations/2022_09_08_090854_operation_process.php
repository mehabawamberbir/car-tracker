<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OperationProcess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operation_processes', function (Blueprint $table) {
            $table->uuid('id')->primary;
            $table->uuid('operation_id')->nullable();
            $table->string('declaration_no')->nullable();
            $table->date('declaration_date')->nullable();
            $table->string('vessel_name')->nullable();
            $table->string('voyage_no')->nullable();
            $table->string('partial_shipment')->nullable();
            $table->string('container_status')->nullable();
            $table->date('stuffing_date')->nullable();
            $table->integer('gross_weight')->nullable();
            $table->integer('net_weight')->nullable();
            $table->string('description')->nullable();
            $table->string('declaration_no_eth')->nullable();
            $table->string('assigned_no_declarant')->nullable();
            $table->string('assigned_no_importer')->nullable();
            $table->string('border_office')->nullable();
            $table->string('clearance_office')->nullable();
            $table->boolean('fumigation')->nullable();
            $table->boolean('weight_inspection')->nullable();
            $table->boolean('inspection')->nullable();
            $table->boolean('craft_paper')->nullable();  
            
            $table->timestamps();

            $table->foreign('operation_id')->references('id')->on('operations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operation_processes');   
    }
}
