<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Invoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->uuid('id')->primary;
            $table->string('type');
            $table->uuid('operation_id')->nullable(); 
            $table->string('customer_id')->nullable(); 
            $table->string('consignee_id')->nullable(); 
            $table->string('invoice_no')->nullable();
            $table->date('invoice_date')->nullable(); 
            $table->date('invoice_due_date')->nullable(); 
            $table->uuid('currency_id')->nullable();
            $table->double('amount', 18,2);
            $table->string('remarks')->nullable();
            $table->integer('status');
            
            $table->timestamps();

            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('consignee_id')->references('id')->on('consignees');
            $table->foreign('currency_id')->references('id')->on('lookups');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {         
        Schema::dropIfExists('invoices');    
    }
}
