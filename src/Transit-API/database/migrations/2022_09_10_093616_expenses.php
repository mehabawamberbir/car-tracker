<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Expenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::create('expenses', function (Blueprint $table){
            $table->uuid('id')->primary();
            $table->uuid('operation_id')->nullable();
            $table->uuid('product_id')->nullable();
            $table->string('reference_no')->nullable();
            $table->integer('quantity')->default(0);
            $table->double('unit_cost', 18, 2)->default(0);
            $table->double('amount', 18, 2)->default(0);
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('operation_id')->references('id')->on('operations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses'); 
    }
}
