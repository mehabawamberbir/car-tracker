<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Containers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    
        Schema::create('containers', function (Blueprint $table){
            $table->uuid('id')->primary();
            $table->uuid('operation_id')->nullable();
            $table->uuid('container_type_id')->nullable();
            $table->string('container_no')->nullable();
            $table->string('seal_no')->nullable();
            $table->string('voyage_no')->nullable();
            $table->string('description')->nullable();
            $table->integer('quantity')->nullable();
            $table->double('gross_weight',18,2)->nullable();
            $table->date('port_out_date')->nullable();
            $table->date('empty_date')->nullable();
            $table->date('return_date')->nullable();
            $table->string('truck_no')->nullable();
            $table->integer('refund')->nullable();
            $table->string('deposit')->nullable();
            $table->string('gate_pass_no')->nullable();
            $table->string('way_bill_no')->nullable();
            $table->string('declaration_no')->nullable();
            $table->string('declaration_no4')->nullable();
            $table->date('declaration_date')->nullable();
            $table->string('destination')->nullable();
            $table->string('certification_no')->nullable();
            $table->string('vgm_form')->nullable();
            $table->uuid('location_id')->nullable();
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('location_id')->references('id')->on('lookups');
            $table->foreign('container_type_id')->references('id')->on('lookups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('containers'); 
    }
}
