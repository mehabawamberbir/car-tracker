<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Journals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->uuid('id')->primary();            
            $table->uuid('journal_type_id'); 
            $table->string('code');
            $table->string('name');
            $table->string('description')->nullable();        
            $table->uuid('account_id'); 
            $table->string('created_by')->nullable();
            $table->boolean('status');

            $table->timestamps();
            $table->foreign('journal_type_id')->references('id')->on('lookups');
            $table->foreign('account_id')->references('id')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('journals');
    }
}
