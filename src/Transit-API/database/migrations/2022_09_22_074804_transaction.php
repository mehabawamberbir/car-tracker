<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();            
            $table->uuid('transaction_type_id');     
            $table->date('transaction_date'); 
            $table->decimal('amount', 18, 2);
            $table->string('reference_no')->nullable();     
            $table->string('description')->nullable();      
            $table->uuid('account_id')->nullable();       
            $table->uuid('currency_id')->nullable();       
            $table->uuid('journal_id')->nullable(); 
            $table->string('created_by')->nullable();
            $table->boolean('status');

            $table->timestamps();
            $table->foreign('transaction_type_id')->references('id')->on('lookups');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('currency_id')->references('id')->on('lookups');
            $table->foreign('journal_id')->references('id')->on('journals');
        });

        Schema::create('transaction_items', function (Blueprint $table) {
            $table->uuid('id')->primary();            
            $table->uuid('transaction_id');     
            $table->uuid('account_id')->nullable();    
            $table->decimal('debit', 18, 2); 
            $table->decimal('credit', 18, 2);   
            $table->string('description')->nullable();   
            $table->boolean('status');

            $table->timestamps();
            $table->foreign('transaction_id')->references('id')->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('transaction_items');
    }
}
