<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PurchaseManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->uuid('id')->primary();               
            $table->uuid('vendor_id')->nullable();     
            $table->uuid('currency_id')->nullable();  
            $table->string('invoice_no')->nullable(); 
            $table->decimal('amount', 18, 2);
            $table->date('bill_date')->nullable();
            $table->date('due_date')->nullable();
            $table->string('remark')->nullable();
            $table->integer('status');

            $table->timestamps();
            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->foreign('currency_id')->references('id')->on('lookups');
        });
        Schema::create('bill_payments', function (Blueprint $table) {
            $table->uuid('id')->primary();               
            $table->uuid('vendor_id')->nullable();     
            $table->uuid('currency_id')->nullable();  
            $table->string('invoice_no')->nullable(); 
            $table->decimal('amount', 18, 2);
            $table->date('payment_date')->nullable();
            $table->date('due_date')->nullable();
            $table->string('remark')->nullable();
            $table->integer('status');

            $table->timestamps();
            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->foreign('currency_id')->references('id')->on('lookups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
        Schema::dropIfExists('bill_payments');
    }
}
