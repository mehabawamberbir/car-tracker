<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GatePass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gate_passes', function (Blueprint $table) {
            $table->uuid('id')->primary();               
            $table->uuid('operation_id')->nullable();     
            $table->string('truck_no')->nullable(); 
            $table->string('destination')->nullable();
            $table->integer('status');

            $table->timestamps();
            $table->foreign('operation_id')->references('id')->on('operations');
        });
        Schema::create('good_removals', function (Blueprint $table) {
            $table->uuid('id')->primary();            
            $table->uuid('operation_id')->nullable();  
            $table->date('good_removal_date')->nullable();
            $table->integer('status');

            $table->timestamps();
            $table->foreign('operation_id')->references('id')->on('operations');
        });
        Schema::create('gate_pass_containers', function (Blueprint $table) {
            $table->uuid('id')->primary();            
            $table->uuid('gate_pass_id')->nullable();  
            $table->uuid('container_id')->nullable();  
            $table->integer('status');

            $table->timestamps();
            $table->foreign('gate_pass_id')->references('id')->on('gate_passes');
            $table->foreign('container_id')->references('id')->on('containers');
        });
        Schema::create('good_removal_waybills', function (Blueprint $table) {
            $table->uuid('id')->primary();            
            $table->uuid('good_removal_id')->nullable();  
            $table->uuid('waybill_id')->nullable();  
            $table->integer('status');

            $table->timestamps();
            $table->foreign('gate_pass_id')->references('id')->on('good_removals');
            $table->foreign('container_id')->references('id')->on('waybills');
        });
        

        Schema::create('waybills', function (Blueprint $table){
            $table->uuid('id')->primary();
            $table->uuid('transporter_id')->nullable();
            $table->date('departure_date')->nullable();
            $table->string('driver_name')->nullable();
            $table->string('driver_tel')->nullable();
            $table->string('truck_no')->nullable();
            $table->string('final_destination')->nullable();
            $table->string('rate_agree')->nullable();
            $table->string('clearance_office')->nullable();
            $table->uuid('loading_type_id')->nullable();
            $table->string('remarks')->nullable();
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('transporter_id')->references('id')->on('transporters');
            $table->foreign('loading_type_id')->references('id')->on('lookups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('gatepasses');
        Schema::dropIfExists('good_removals');
        Schema::dropIfExists('waybills');
    }
}
