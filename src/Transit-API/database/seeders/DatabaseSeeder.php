<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'id' => 'S002',
            'name' => 'Admin User', 
            'email' => 'admin@gmail.com',
            'password' => Hash::make('transit123')
        ]);
        $user2 = User::create([
            'id' => 'S001',
            'name' => 'First User', 
            'email' => 'user1@gmail.com',
            'password' => Hash::make('transit123')
        ]);
        
        $roles = Role::create([
            'id' => '1QWSD',
            'name' => 'Admin'
        ]);
        $roles = Role::create([
            'id' => '23DF',
            'name' => 'User'
        ]);
        // $userRole = UserRole::create([
        //     'id' => 'S002',
        //     'user_id' => 'A001',
        //     'role_id' => '1QWSD'
        // ]);
        // $userRole2 = UserRole::create([
        //     'id' => '1SCV',
        //     'user_id' => 'S001',
        //     'role_id' => '1QWSD'
        // ]);
    }
}
