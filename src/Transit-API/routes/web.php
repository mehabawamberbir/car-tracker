<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/




$router->get('/', function () use ($router) {
    echo "<center> Welcome </center>";
});

$router->get('/version', function () use ($router) {
    return $router->app->version();
});

Route::group([

    'prefix' => 'api'

], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('user-profile', 'AuthController@me');

});


$router->group(['prefix' => 'api'], function ($router) {        
    //lookups
    $router->post('/lookups', 'LookupController@create');
    $router->put('/lookups/{id}', 'LookupController@update');
    $router->get('/lookups/{id}', 'LookupController@get');
    $router->get('/lookups/{type}/lookups-by-type', 'LookupController@getLookups');
    $router->get('/lookups', 'LookupController@getAll');
    $router->get('/lookups-index', 'LookupController@index');
    $router->get('/lookups/query/{attribute}/{value}', 'LookupController@query');
    $router->delete('/lookups/{id}', 'LookupController@delete');
    $router->patch('/lookups/{id}/toggle-status', 'LookupController@toggleStatus');

     //lookup types
     $router->post('/lookup-types', 'LookupTypeController@create');
     $router->put('/lookup-types/{id}', 'LookupTypeController@update');
     $router->get('/lookup-types/{id}', 'LookupTypeController@get');
     $router->get('/lookup-types/{type}/lookups', 'LookupTypeController@getLookups');
     $router->get('/lookup-types', 'LookupTypeController@getAll');     
     $router->get('/lookup-types-index', 'LookupTypeController@index');
     $router->get('/lookup-types/query/{attribute}/{value}', 'LookupTypeController@query');
     $router->delete('/lookup-types/{id}', 'LookupTypeController@delete');
     $router->patch('/lookup-types/{id}/toggle-status', 'LookupTypeController@toggleStatus');
     
     //customers
     $router->post('/contact-manager/customers', 'CustomerController@create');
     $router->put('/contact-manager/customers/{id}', 'CustomerController@update');
     $router->get('/contact-manager/customers/{id}', 'CustomerController@get');
     $router->get('/contact-manager/customers', 'CustomerController@getAll');
     $router->get('/contact-manager/customers-index', 'CustomerController@index');
     $router->get('/contact-manager/customers/query/{attribute}/{value}', 'CustomerController@query');
     $router->delete('/contact-manager/customers/{id}', 'CustomerController@delete');
     $router->patch('/contact-manager/customers/{id}/toggle-status', 'CustomerController@toggleStatus');
     
     //vendors
     $router->post('/contact-manager/vendors', 'VendorController@create');
     $router->put('/contact-manager/vendors/{id}', 'VendorController@update');
     $router->get('/contact-manager/vendors/{id}', 'VendorController@get');
     $router->get('/contact-manager/vendors', 'VendorController@getAll');
     $router->get('/contact-manager/vendors-index', 'VendorController@index');
     $router->get('/contact-manager/vendors/query/{attribute}/{value}', 'VendorController@query');
     $router->delete('/contact-manager/vendors/{id}', 'VendorController@delete');
     $router->patch('/contact-manager/vendors/{id}/toggle-status', 'VendorController@toggleStatus');
     
     //agents
     $router->post('/contact-manager/agents', 'AgentController@create');
     $router->put('/contact-manager/agents/{id}', 'AgentController@update');
     $router->get('/contact-manager/agents/{id}', 'AgentController@get');
     $router->get('/contact-manager/agents', 'AgentController@getAll');
     $router->get('/contact-manager/agents-index', 'AgentController@index');
     $router->get('/contact-manager/agents/query/{attribute}/{value}', 'AgentController@query');
     $router->delete('/contact-manager/agents/{id}', 'AgentController@delete');
     $router->patch('/contact-manager/agents/{id}/toggle-status', 'AgentController@toggleStatus');
     
     //consignees
     $router->post('/contact-manager/consignees', 'ConsigneeController@create');
     $router->put('/contact-manager/consignees/{id}', 'ConsigneeController@update');
     $router->get('/contact-manager/consignees/{id}', 'ConsigneeController@get');
     $router->get('/contact-manager/consignees', 'ConsigneeController@getAll');
     $router->get('/contact-manager/consignees-index', 'ConsigneeController@index');
     $router->get('/contact-manager/consignees/query/{attribute}/{value}', 'ConsigneeController@query');
     $router->delete('/contact-manager/consignees/{id}', 'ConsigneeController@delete');
     $router->patch('/contact-manager/consignees/{id}/toggle-status', 'ConsigneeController@toggleStatus');
     
     //operations
     $router->post('/operation-manager/operations', 'OperationController@create');
     $router->put('/operation-manager/operations/{id}', 'OperationController@update');
     $router->get('/operation-manager/operations/{id}', 'OperationController@get');
     $router->get('/operation-manager/operations', 'OperationController@getAllByType');
     $router->get('/operation-manager/operations-index', 'OperationController@index');
     $router->get('/operation-manager/operations/query/{attribute}/{value}', 'OperationController@query');
     $router->delete('/operation-manager/operations/{id}', 'OperationController@delete');
     $router->patch('/operation-manager/operations/{id}/toggle-status/{status}', 'OperationController@toggleStatus');
     
     //operation-processes
     $router->post('/operation-manager/operation-processes', 'OperationProcessController@create');
     $router->put('/operation-manager/operation-processes/{id}', 'OperationProcessController@update');
     $router->get('/operation-manager/operation-processes/{id}', 'OperationProcessController@get');
     $router->get('/operation-manager/operation-processes', 'OperationProcessController@getAll');
     $router->get('/operation-manager/operation-processes-index', 'OperationProcessController@index');
     $router->get('/operation-manager/operation-processes/query/{attribute}/{value}', 'OperationProcessController@query');
     $router->delete('/operation-manager/operation-processes/{id}', 'OperationProcessController@delete');
    
     //containers
    $router->post('/operation-manager/containers', 'ContainerController@create');
    $router->put('/operation-manager/containers/{id}', 'ContainerController@update');
    $router->get('/operation-manager/containers/{id}', 'ContainerController@get');
    $router->get('/operation-manager/containers', 'ContainerController@getAll');
    $router->get('/operation-manager/containers-index', 'ContainerController@index');
    $router->get('/operation-manager/containers/query/{attribute}/{value}', 'ContainerController@query');
    $router->delete('/operation-manager/containers/{id}', 'ContainerController@delete');
    $router->patch('/operation-manager/containers/{id}/toggle-status', 'ContainerController@toggleStatus');
    
    //cargos
    $router->post('/operation-manager/cargos', 'CargoController@create');
    $router->put('/operation-manager/cargos/{id}', 'CargoController@update');
    $router->get('/operation-manager/cargos/{id}', 'CargoController@get');
    $router->get('/operation-manager/cargos', 'CargoController@getAll');
    $router->get('/operation-manager/cargos-index', 'CargoController@index');
    $router->get('/operation-manager/cargos/query/{attribute}/{value}', 'CargoController@query');
    $router->delete('/operation-manager/cargos/{id}', 'CargoController@delete');
    $router->patch('/operation-manager/cargos/{id}/toggle-status', 'CargoController@toggleStatus');

    //trucks
    $router->post('/operation-manager/trucks', 'TruckController@create');
    $router->put('/operation-manager/trucks/{id}', 'TruckController@update');
    $router->get('/operation-manager/trucks/{id}', 'TruckController@get');
    $router->get('/operation-manager/trucks', 'TruckController@getAll');
    $router->get('/operation-manager/trucks-index', 'TruckController@index');
    $router->get('/operation-manager/trucks/query/{attribute}/{value}', 'TruckController@query');
    $router->delete('/operation-manager/trucks/{id}', 'TruckController@delete');
    $router->patch('/operation-manager/trucks/{id}/toggle-status', 'TruckController@toggleStatus');
    
    //waybills
    $router->post('/operation-manager/waybills', 'WaybillController@create');
    $router->put('/operation-manager/waybills/{id}', 'WaybillController@update');
    $router->get('/operation-manager/waybills/{id}', 'WaybillController@get');
    $router->get('/operation-manager/waybills', 'WaybillController@getAll');
    $router->get('/operation-manager/waybills-index', 'WaybillController@index');
    $router->get('/operation-manager/waybills/query/{attribute}/{value}', 'WaybillController@query');
    $router->delete('/operation-manager/waybills/{id}', 'WaybillController@delete');
    $router->patch('/operation-manager/waybills/{id}/toggle-status', 'WaybillController@toggleStatus');
    
    //gate-passes
    $router->post('/operation-manager/gate-passes', 'GatePassController@create');
    $router->put('/operation-manager/gate-passes/{id}', 'GatePassController@update');
    $router->get('/operation-manager/gate-passes/{id}', 'GatePassController@get');
    $router->get('/operation-manager/gate-passes', 'GatePassController@getAll');
    $router->get('/operation-manager/gate-passes-index', 'GatePassController@index');
    $router->get('/operation-manager/gate-passes/query/{attribute}/{value}', 'GatePassController@query');
    $router->delete('/operation-manager/gate-passes/{id}', 'GatePassController@delete');
    $router->patch('/operation-manager/gate-passes/{id}/toggle-status', 'GatePassController@toggleStatus');
    
    //loading-requests
    $router->post('/operation-manager/loading-requests', 'LoadingRequestController@create');
    $router->put('/operation-manager/loading-requests/{id}', 'LoadingRequestController@update');
    $router->get('/operation-manager/loading-requests/{id}', 'LoadingRequestController@get');
    $router->get('/operation-manager/loading-requests', 'LoadingRequestController@getAll');
    $router->get('/operation-manager/loading-requests-index', 'LoadingRequestController@index');
    $router->get('/operation-manager/loading-requests/query/{attribute}/{value}', 'LoadingRequestController@query');
    $router->delete('/operation-manager/loading-requests/{id}', 'LoadingRequestController@delete');
    $router->patch('/operation-manager/loading-requests/{id}/toggle-status', 'LoadingRequestController@toggleStatus');
    
    //account-types
    $router->post('/accounting-manager/account-types', 'AccountTypeController@create');
    $router->put('/accounting-manager/account-types/{id}', 'AccountTypeController@update');
    $router->get('/accounting-manager/account-types/{id}', 'AccountTypeController@get');
    $router->get('/accounting-manager/account-types', 'AccountTypeController@getAll');
    $router->get('/accounting-manager/account-types-index', 'AccountTypeController@index');
    $router->get('/accounting-manager/account-types/query/{attribute}/{value}', 'AccountTypeController@query');
    $router->delete('/accounting-manager/account-types/{id}', 'AccountTypeController@delete');
    $router->patch('/accounting-manager/account-types/{id}/toggle-status', 'AccountTypeController@toggleStatus');
    
    //accounts
    $router->post('/accounting-manager/accounts', 'AccountController@create');
    $router->put('/accounting-manager/accounts/{id}', 'AccountController@update');
    $router->get('/accounting-manager/accounts/{id}', 'AccountController@get');
    $router->get('/accounting-manager/accounts', 'AccountController@getAll');
    $router->get('/accounting-manager/accounts-index', 'AccountController@index');
    $router->get('/accounting-manager/accounts/query/{attribute}/{value}', 'AccountController@query');
    $router->delete('/accounting-manager/accounts/{id}', 'AccountController@delete');
    $router->patch('/accounting-manager/accounts/{id}/toggle-status', 'AccountController@toggleStatus');
     
    //journals
    $router->post('/accounting-manager/journals', 'JournalController@create');
    $router->put('/accounting-manager/journals/{id}', 'JournalController@update');
    $router->get('/accounting-manager/journals/{id}', 'JournalController@get');
    $router->get('/accounting-manager/journals', 'JournalController@getAll');
    $router->get('/accounting-manager/journals-index', 'JournalController@index');
    $router->get('/accounting-manager/journals/query/{attribute}/{value}', 'JournalController@query');
    $router->delete('/accounting-manager/journals/{id}', 'JournalController@delete');
    $router->patch('/accounting-manager/journals/{id}/toggle-status', 'JournalController@toggleStatus');
     
    //transactions
    $router->post('/accounting-manager/transactions', 'TransactionController@create');
    $router->put('/accounting-manager/transactions/{id}', 'TransactionController@update');
    $router->get('/accounting-manager/transactions/{id}', 'TransactionController@get');
    $router->get('/accounting-manager/transactions', 'TransactionController@getAll');
    $router->get('/accounting-manager/transactions-index', 'TransactionController@index');
    $router->get('/accounting-manager/transactions/query/{attribute}/{value}', 'TransactionController@query');
    $router->delete('/accounting-manager/transactions/{id}', 'TransactionController@delete');
    $router->patch('/accounting-manager/transactions/{id}/toggle-status', 'TransactionController@toggleStatus');
     
    //transaction-items
    $router->post('/accounting-manager/transaction-items', 'TransactionItemController@create');
    $router->put('/accounting-manager/transaction-items/{id}', 'TransactionItemController@update');
    $router->get('/accounting-manager/transaction-items/{id}', 'TransactionItemController@get');
    $router->get('/accounting-manager/transaction-items', 'TransactionItemController@getAll');
    $router->get('/accounting-manager/transaction-items-index', 'TransactionItemController@index');
    $router->get('/accounting-manager/transaction-items/query/{attribute}/{value}', 'TransactionItemController@query');
    $router->delete('/accounting-manager/transaction-items/{id}', 'TransactionItemController@delete');
    $router->patch('/accounting-manager/transaction-items/{id}/toggle-status', 'TransactionItemController@toggleStatus');

     
    //reconciliations
    $router->post('/accounting-manager/reconciliations', 'ReconciliationController@create');
    $router->put('/accounting-manager/reconciliations/{id}', 'ReconciliationController@update');
    $router->get('/accounting-manager/reconciliations/{id}', 'ReconciliationController@get');
    $router->get('/accounting-manager/reconciliations', 'ReconciliationController@getAll');
    $router->get('/accounting-manager/reconciliations-index', 'ReconciliationController@index');
    $router->get('/accounting-manager/reconciliations/query/{attribute}/{value}', 'ReconciliationController@query');
    $router->delete('/accounting-manager/reconciliations/{id}', 'ReconciliationController@delete');
    $router->patch('/accounting-manager/reconciliations/{id}/toggle-status', 'ReconciliationController@toggleStatus');

    //employees
    $router->post('/employee-manager/employees', 'EmployeeController@create');
    $router->put('/employee-manager/employees/{id}', 'EmployeeController@update');
    $router->get('/employee-manager/employees/{id}', 'EmployeeController@get');
    $router->get('/employee-manager/employees', 'EmployeeController@getAll');
    $router->get('/employee-manager/employees-index', 'EmployeeController@index');
    $router->get('/employee-manager/employees/query/{attribute}/{value}', 'EmployeeController@query');
    $router->delete('/employee-manager/employees/{id}', 'EmployeeController@delete');
     
    //products
    $router->post('/product-manager/products', 'ProductController@create');
    $router->put('/product-manager/products/{id}', 'ProductController@update');
    $router->get('/product-manager/products/{id}', 'ProductController@get');
    $router->get('/product-manager/products-sellable', 'ProductController@getAll');
    $router->get('/product-manager/products-index/{type}', 'ProductController@index');
    $router->get('/product-manager/products-purchase', 'ProductController@getAll');
    $router->get('/product-manager/products-index', 'ProductController@index');
    $router->get('/product-manager/products/query/{attribute}/{value}', 'ProductController@query');
    $router->delete('/product-manager/products/{id}', 'ProductController@delete');
     
    //Invoices
    $router->post('/invoice-manager/invoices', 'InvoiceController@create');
    $router->put('/invoice-manager/invoices/{id}', 'InvoiceController@update');
    $router->get('/invoice-manager/invoices/{id}', 'InvoiceController@get');
    $router->get('/invoice-manager/invoices', 'InvoiceController@getAllByType');
    $router->get('/invoice-manager/invoices-index', 'InvoiceController@index');
    $router->get('/invoice-manager/invoices/query/{attribute}/{value}', 'InvoiceController@query');
    $router->delete('/invoice-manager/invoices/{id}', 'InvoiceController@delete');
     
    //expenses
    $router->post('/operation-manager/expenses', 'ExpenseController@create');
    $router->put('/operation-manager/expenses/{id}', 'ExpenseController@update');
    $router->get('/operation-manager/expenses/{id}', 'ExpenseController@get');
    $router->get('/operation-manager/expenses', 'ExpenseController@getAll');
    $router->get('/operation-manager/expenses-index', 'ExpenseController@index');
    $router->get('/operation-manager/expenses/query/{attribute}/{value}', 'ExpenseController@query');
    $router->delete('/operation-manager/expenses/{id}', 'ExpenseController@delete');
    
});