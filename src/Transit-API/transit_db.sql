-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2022 at 11:04 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `transit_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_account_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `reconcileable` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `account_type_id`, `class_id`, `name`, `code`, `description`, `sub_account_id`, `status`, `reconcileable`, `created_at`, `updated_at`) VALUES
('00f2f500-7625-407d-9849-2ec5ee21ab19', 'c9d6c9a5-2c9e-11ed-b575-00090ffe0001', '453e7ae7-50de-4799-9473-961e3a2b3cb3', 'Test 2001', '2001', 'desc', '', 1, 0, '2022-09-26 20:07:45', '2022-09-26 20:07:45'),
('ec22b909-2c9e-11ed-b575-00090ffe0001', 'c9d6c9a5-2c9e-11ed-b575-00090ffe0001', '81a9c6c6-7e00-4f8a-82b9-8c33a8faa14b', 'Test acc', 'Test acc', 'Test acc', NULL, 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `account_types`
--

CREATE TABLE `account_types` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coa_type_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `account_types`
--

INSERT INTO `account_types` (`id`, `coa_type_id`, `code`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
('c9d6c9a5-2c9e-11ed-b575-00090ffe0001', 'b74061d6-a541-4da8-ad66-ceb07cfbe60f', 'Test1', 'Test', 'test', 1, NULL, '2022-09-14 09:28:22');

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usd_rate` double(18,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`id`, `name`, `code`, `telephone`, `email`, `address`, `address2`, `bp`, `city`, `country`, `terms`, `start_date`, `end_date`, `remarks`, `usd_rate`, `status`, `created_at`, `updated_at`) VALUES
('5ab84210-5f98-4e54-92ae-4faeb226ca53', 'Agent Test1', 'agt1', '', '', '', '', '', '', 'Ethiopia', '', '2022-09-05', '2022-09-16', NULL, 1.00, 1, '2022-09-04 22:00:25', '2022-09-04 22:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(18,2) NOT NULL,
  `bill_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `remark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bill_payments`
--

CREATE TABLE `bill_payments` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(18,2) NOT NULL,
  `payment_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `remark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cargos`
--

CREATE TABLE `cargos` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operation_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chasis_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` double(18,2) NOT NULL DEFAULT 0.00,
  `weight` double(18,2) NOT NULL DEFAULT 0.00,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `way_bill_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gate_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sydonia_ref` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `consignees`
--

CREATE TABLE `consignees` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usd_rate` double(18,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consignees`
--

INSERT INTO `consignees` (`id`, `name`, `code`, `telephone`, `email`, `address`, `address2`, `bp`, `city`, `country`, `terms`, `usd_rate`, `status`, `created_at`, `updated_at`) VALUES
('eb0af234-e436-4033-bcd5-cdac50fdabbc', 'Consigne Test', 'CT1', '', '', '', '', '', '', 'Ethiopia', '', 1.00, 1, '2022-09-04 21:54:35', '2022-09-04 21:54:35');

-- --------------------------------------------------------

--
-- Table structure for table `containers`
--

CREATE TABLE `containers` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operation_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `container_type_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `container_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seal_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voyage_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `gross_weight` double(18,2) DEFAULT NULL,
  `port_out_date` date DEFAULT NULL,
  `empty_date` date DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `truck_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refund` int(11) DEFAULT NULL,
  `deposit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gate_pass_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `way_bill_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `declaration_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `declaration_no4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `declaration_date` date DEFAULT NULL,
  `destination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certification_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vgm_form` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `containers`
--

INSERT INTO `containers` (`id`, `operation_id`, `container_type_id`, `container_no`, `seal_no`, `voyage_no`, `description`, `quantity`, `gross_weight`, `port_out_date`, `empty_date`, `return_date`, `truck_no`, `refund`, `deposit`, `gate_pass_no`, `way_bill_no`, `declaration_no`, `declaration_no4`, `declaration_date`, `destination`, `certification_no`, `vgm_form`, `location_id`, `status`, `created_at`, `updated_at`) VALUES
('72ecfa78-ab30-429e-bce9-7ec71c1b4167', '62e87d68-84b0-4b55-ba8e-28834bf933c1', '9db6982f-5e08-4f42-b006-b6bb0fc57674', 'CONT101', 'SEAL091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2022-09-13 10:58:40', '2022-09-13 10:58:40');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usd_rate` double(18,2) DEFAULT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `code`, `telephone`, `email`, `address`, `address2`, `bp`, `city`, `country`, `account_id`, `terms`, `usd_rate`, `group_id`, `status`, `created_at`, `updated_at`) VALUES
('1c3f1c3b-75c3-4ae5-9086-c83721af7a51', 'Transit DEV', 'KAH', '', '', '', '', '', '', 'Ethiopia', NULL, '', 1.00, 'b33f9011-2924-4b02-bb47-a5c470405958', 1, '2022-09-04 21:54:09', '2022-09-09 16:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_tel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_tel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_title_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `department_id`, `telephone`, `gender`, `picture`, `group_id`, `home_tel`, `work_tel`, `work_email`, `other_email`, `address`, `job_title_id`, `status_id`, `contract_details`, `start_date`, `end_date`, `end_reason`, `created_at`, `updated_at`) VALUES
('2c9ec588-1ff4-4803-acab-7fbf117f05ef', 'Abebe', '4eb41669-23ab-4916-8157-9dc6f668540e', NULL, 'male', '', 'b48e439f-1b0d-4392-b1ed-6f37b2642c7a', '0911', '89', '', '', 'ADD', NULL, 'c4caea23-01d8-4858-b71a-6a82a4016a77', '123', '2022-09-22', '', 'REAS', '2022-09-23 10:13:42', '2022-09-23 10:13:42');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operation_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(18,2) NOT NULL DEFAULT 0.00,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `operation_id`, `product_id`, `reference_no`, `amount`, `status`, `created_at`, `updated_at`) VALUES
('2b657d11-d322-4ecf-8d89-af2bbfa12b83', '5b14c22b-d437-402a-85a1-001aeb01be0f', 'e8ea3dc4-3289-4a2c-9360-454765f21c2b', 'ref3', 50.00, 1, '2022-09-10 13:31:26', '2022-09-10 13:31:26'),
('5627a6bf-21cc-4f79-b9ff-4466ddde693a', '5b14c22b-d437-402a-85a1-001aeb01be0f', 'e8ea3dc4-3289-4a2c-9360-454765f21c2b', 'ref12', 30.00, 1, '2022-09-10 13:32:41', '2022-09-10 13:32:41'),
('7199ff3c-930a-46ca-811f-ba5498f049c2', '6daf7672-c683-4889-b138-d319e244bca1', 'e8ea3dc4-3289-4a2c-9360-454765f21c2b', 'EXP12', 13.00, 1, '2022-09-10 13:19:25', '2022-09-10 13:42:13'),
('8732e9ea-d703-45f7-8575-da6e6794be26', '6daf7672-c683-4889-b138-d319e244bca1', 'e8ea3dc4-3289-4a2c-9360-454765f21c2b', 'ref56', 0.00, 1, '2022-09-10 13:36:16', '2022-09-10 13:36:16'),
('a73d1b68-3311-40fb-b6fd-bf60d2346c7b', '5b14c22b-d437-402a-85a1-001aeb01be0f', 'e8ea3dc4-3289-4a2c-9360-454765f21c2b', 'resfs', 40.00, 1, '2022-09-10 13:33:49', '2022-09-10 13:33:49'),
('a78f13a8-8402-4163-9b36-97b634ea67b0', '5b14c22b-d437-402a-85a1-001aeb01be0f', 'e8ea3dc4-3289-4a2c-9360-454765f21c2b', 'refp', 50.00, 1, '2022-09-10 13:30:19', '2022-09-10 13:30:19'),
('afcea6f4-16f5-4f95-87ad-a6a332f65183', '6daf7672-c683-4889-b138-d319e244bca1', 'e8ea3dc4-3289-4a2c-9360-454765f21c2b', 'ref67', 0.00, 1, '2022-09-10 13:35:41', '2022-09-10 13:35:41'),
('b6adc7c7-a615-4de5-84e3-49b677166e8c', '6daf7672-c683-4889-b138-d319e244bca1', 'e8ea3dc4-3289-4a2c-9360-454765f21c2b', 're67', 0.00, 1, '2022-09-10 13:36:21', '2022-09-10 13:36:21'),
('c4c1ef91-c2c9-461e-939b-d119d82c4222', '6daf7672-c683-4889-b138-d319e244bca1', 'e8ea3dc4-3289-4a2c-9360-454765f21c2b', 'ref13', 15.00, 1, '2022-09-10 13:25:38', '2022-09-10 13:42:19'),
('f606a31a-5423-45a6-acad-2cb6044a8da6', '5b14c22b-d437-402a-85a1-001aeb01be0f', 'e8ea3dc4-3289-4a2c-9360-454765f21c2b', 'ref', 60.00, 1, '2022-09-10 13:31:50', '2022-09-10 13:31:50');

-- --------------------------------------------------------

--
-- Table structure for table `gate_passes`
--

CREATE TABLE `gate_passes` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operation_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gate_pass_containers`
--

CREATE TABLE `gate_pass_containers` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gate_pass_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `container_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `good_removals`
--

CREATE TABLE `good_removals` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operation_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `good_removal_date` date DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `good_removal_waybills`
--

CREATE TABLE `good_removal_waybills` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `good_removal_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waybill_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operation_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consignee_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `invoice_due_date` date DEFAULT NULL,
  `currency_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(18,2) NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `type`, `operation_id`, `customer_id`, `consignee_id`, `invoice_no`, `invoice_date`, `invoice_due_date`, `currency_id`, `amount`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
('b036970c-4eea-49b7-a269-8a984477a6e9', 'transit', '5b14c22b-d437-402a-85a1-001aeb01be0f', '1c3f1c3b-75c3-4ae5-9086-c83721af7a51', 'eb0af234-e436-4033-bcd5-cdac50fdabbc', 'INV001/12', '2022-09-09', NULL, 'ca17bd85-2ac1-4e52-9bee-db8e66e085fc', 10.00, 'REM', 1, '2022-09-09 15:32:36', '2022-09-09 15:53:55');

-- --------------------------------------------------------

--
-- Table structure for table `journals`
--

CREATE TABLE `journals` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `journal_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `journals`
--

INSERT INTO `journals` (`id`, `journal_type_id`, `code`, `name`, `description`, `account_id`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
('06068062-1246-4a65-8fc6-0afa34f33bb1', '4199542f-1cc2-4664-8735-4cea3d69a84a', 'Test Journal', 'TJ1', 'DESC', 'ec22b909-2c9e-11ed-b575-00090ffe0001', NULL, 1, '2022-09-26 12:51:47', '2022-09-26 12:51:47');

-- --------------------------------------------------------

--
-- Table structure for table `lookups`
--

CREATE TABLE `lookups` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lookup_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `local_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lookups`
--

INSERT INTO `lookups` (`id`, `lookup_type_id`, `name`, `code`, `order`, `local_name`, `status`, `created_at`, `updated_at`) VALUES
('26918383-6abb-42fb-8b52-005d0b3bd92f', 'fed96d9f-550f-4a62-89fe-d3ca53f85d9f', 'Default', 'DEF', 1, '', 1, '2022-09-23 10:03:23', '2022-09-23 10:03:23'),
('27717007-4dae-4910-9748-a483d5e92975', '14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Import Unimodal', 'IUM', 3, '', 1, '2022-09-04 21:15:21', '2022-09-04 21:16:25'),
('380d44fb-7f62-482e-9c5f-a1f5537be1d5', '8468c1ce-3f1d-4b7f-b5c2-577595b31a5b', 'Simple', 'SIMPLE', 1, '', 1, '2022-09-26 07:45:24', '2022-09-26 07:45:24'),
('3ac73894-0d86-4fc3-a604-7c9dfa692b18', '1aa7bfe2-6345-485a-be5a-9d336fa5baa1', 'Equity', '3', 1, '', 1, '2022-09-14 09:29:55', '2022-09-14 09:29:55'),
('3c5b4160-f182-4b3b-9d79-863b204b7ee0', 'c5d748ef-66d0-46b9-9e24-512a4d8ccc93', 'Invoice', 'INV', 1, '', 1, '2022-09-08 20:34:06', '2022-09-08 20:34:06'),
('3f395775-6647-4e73-a425-abf33098f65e', '3f575f41-28d1-43b1-b850-f43b0677439c', 'Default', 'DEF', 1, '', 1, '2022-09-05 21:37:06', '2022-09-05 21:37:06'),
('4199542f-1cc2-4664-8735-4cea3d69a84a', '78adf718-ca6d-4412-87ed-dc72b1f0b014', 'Journal des Achats et frais', '2', 1, '', 1, '2022-09-14 10:39:23', '2022-09-14 10:39:23'),
('453e7ae7-50de-4799-9473-961e3a2b3cb3', '3ca40e34-2083-4ab7-88e1-ec7193beea70', '1-Class', '1', 1, '', 1, '2022-09-04 22:12:15', '2022-09-04 22:12:15'),
('4e2ccec3-5927-4603-9ef4-6550c5600d89', 'ca1526a2-eba1-4d71-ab03-407716284065', 'Default', 'DEF', 1, '', 1, '2022-09-04 21:18:57', '2022-09-04 21:18:57'),
('4eb41669-23ab-4916-8157-9dc6f668540e', 'dd97c575-ed2f-44ef-bd16-f43a32f5635e', 'Default', 'DEF', 1, '', 1, '2022-09-23 10:03:55', '2022-09-23 10:03:55'),
('56cca088-9c08-4eb0-a052-162081b61a62', 'c5d748ef-66d0-46b9-9e24-512a4d8ccc93', 'Commission', 'COMM', 1, '', 1, '2022-09-08 20:34:17', '2022-09-08 20:34:17'),
('585134ca-c9d9-4b72-937d-cd576e7227ed', '1aa7bfe2-6345-485a-be5a-9d336fa5baa1', 'Liability', '2', 1, '', 1, '2022-09-14 09:29:36', '2022-09-14 09:29:36'),
('66fa5273-ddf7-41d1-99f8-4a8b74c4e814', '30ec5c8e-f61f-4fa6-a2f4-2399a1a30fba', 'Cargo', 'CARGO', 1, '', 1, '2022-09-04 22:32:16', '2022-09-04 22:32:16'),
('81a9c6c6-7e00-4f8a-82b9-8c33a8faa14b', '14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Transport', 'TRN', 4, '', 1, '2022-09-04 21:15:39', '2022-09-04 21:16:34'),
('914f0d7b-78ef-4afa-be14-2ae5a1746b1a', 'fed96d9f-550f-4a62-89fe-d3ca53f85d9f', 'Clerk', 'CLR', 1, '', 1, '2022-09-23 10:03:16', '2022-09-23 10:03:16'),
('949d5340-c79b-4f45-b2e3-638d5127dfb2', '13e0d7f2-c84f-493c-ad22-8b347de32067', 'Free Zone', 'FZO', 1, '', 1, '2022-09-13 11:31:32', '2022-09-13 11:31:32'),
('9846238b-c179-4762-bb7e-5154941ac5ca', '78adf718-ca6d-4412-87ed-dc72b1f0b014', 'Journal des À-Nouveaux', '1', 1, '', 1, '2022-09-14 10:39:07', '2022-09-14 10:39:07'),
('9db6982f-5e08-4f42-b006-b6bb0fc57674', 'ed531bde-2acb-4e91-ab9b-eaf6ad31052f', '20 Feet', '20F', 1, '', 1, '2022-09-13 10:47:06', '2022-09-13 10:47:06'),
('b33f9011-2924-4b02-bb47-a5c470405958', 'abef8b9f-7d44-4032-aa6d-f6e7748079ae', 'Default', 'DEF', 1, '', 1, '2022-09-04 21:18:46', '2022-09-04 21:18:46'),
('b48e439f-1b0d-4392-b1ed-6f37b2642c7a', '0367d689-a9ef-4d4c-802a-d61226eed123', 'Default', 'DEF', 1, '', 1, '2022-09-23 10:04:27', '2022-09-23 10:04:27'),
('b74061d6-a541-4da8-ad66-ceb07cfbe60f', '1aa7bfe2-6345-485a-be5a-9d336fa5baa1', 'Assets', '1', 1, '', 1, '2022-09-04 22:12:02', '2022-09-14 09:31:51'),
('b7f702b0-e911-442c-9f51-2d89a21e2144', '8468c1ce-3f1d-4b7f-b5c2-577595b31a5b', 'Journal', 'JOURNAL', 1, '', 1, '2022-09-26 07:45:42', '2022-09-26 07:45:42'),
('bc0d3ff2-f16e-461a-b5e5-8e99de58984c', 'e4805806-cf44-426f-ad87-fe4b881dab5d', 'Transit Invoice', 'transit', 1, '', 1, '2022-09-09 15:53:19', '2022-09-09 15:53:19'),
('c4caea23-01d8-4858-b71a-6a82a4016a77', '5349654a-b011-4252-bb9f-426401a70be4', 'Active', 'ACTIVE', 1, '', 1, '2022-09-23 10:02:41', '2022-09-23 10:02:41'),
('c5cefd40-c38e-4dd6-9fef-9a20534d7542', '1aa7bfe2-6345-485a-be5a-9d336fa5baa1', 'Income', '4', 1, '', 1, '2022-09-14 09:30:02', '2022-09-14 09:30:02'),
('ca17bd85-2ac1-4e52-9bee-db8e66e085fc', 'a5ab1721-868c-43f9-82e0-806b72148dd5', 'DJF', 'DJF', 1, '', 1, '2022-09-08 22:09:10', '2022-09-08 22:09:10'),
('d00be1e5-4d27-4029-af43-da0e27d65258', '13e0d7f2-c84f-493c-ad22-8b347de32067', 'DCT', 'DCT', 1, '', 1, '2022-09-13 11:31:23', '2022-09-13 11:31:23'),
('e0fa84e1-8c36-4f78-8b4f-a32826033481', '13e0d7f2-c84f-493c-ad22-8b347de32067', 'Port De Djibouti', 'PDDJ', 1, '', 1, '2022-09-13 11:31:06', '2022-09-13 11:31:06'),
('e3022b68-7ccc-4e60-97b6-bcf6667a34d4', '1aa7bfe2-6345-485a-be5a-9d336fa5baa1', 'Expenses', '5', 1, '', 1, '2022-09-14 09:30:13', '2022-09-14 09:30:13'),
('e490e762-d959-4dd3-b8bf-57691b886b7b', 'a5ab1721-868c-43f9-82e0-806b72148dd5', 'USD', 'USD', 1, '', 1, '2022-09-08 22:09:01', '2022-09-08 22:09:01'),
('e5fb653f-fbfe-4f83-8538-afd86c127788', '14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Import Multimodal', 'IMM', 1, '', 1, '2022-09-04 21:14:53', '2022-09-04 21:14:53'),
('eb830dbc-88fd-4ef6-b77b-31685e9bea99', '14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Import Local', 'ILO', 5, '', 1, '2022-09-04 21:16:04', '2022-09-04 21:16:44'),
('fd8fa21e-086f-4832-9a92-85ce2bb301f1', 'ed531bde-2acb-4e91-ab9b-eaf6ad31052f', '40 Feet', '40F', 1, '', 1, '2022-09-13 10:47:17', '2022-09-13 10:47:17'),
('fdc45465-b741-49b2-b5d0-7278c8ee703f', '14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Export', 'EXP', 2, '', 1, '2022-09-04 21:15:00', '2022-09-04 21:16:17');

-- --------------------------------------------------------

--
-- Table structure for table `lookup_types`
--

CREATE TABLE `lookup_types` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `local_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lookup_types`
--

INSERT INTO `lookup_types` (`id`, `name`, `code`, `order`, `local_name`, `status`, `created_at`, `updated_at`) VALUES
('0367d689-a9ef-4d4c-802a-d61226eed123', 'Employee Group', 'EMP_GROUP', 1, '', 1, '2022-09-08 23:05:32', '2022-09-08 23:05:32'),
('13e0d7f2-c84f-493c-ad22-8b347de32067', 'Container Location', 'CONT_LOCATION', 1, '', 1, '2022-09-13 11:30:37', '2022-09-13 11:30:37'),
('14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Operation Category', 'OPCAT', 1, '', 1, '2022-09-04 21:13:01', '2022-09-04 21:13:01'),
('1aa7bfe2-6345-485a-be5a-9d336fa5baa1', 'COA Type', 'COA_TYPE', 1, '', 1, '2022-09-04 22:11:42', '2022-09-04 22:11:42'),
('30ec5c8e-f61f-4fa6-a2f4-2399a1a30fba', 'Loading Type', 'LOAD_TYPE', 1, '', 1, '2022-09-04 21:17:20', '2022-09-04 21:17:20'),
('3ca40e34-2083-4ab7-88e1-ec7193beea70', 'Account Class', 'ACC_CLASS', 1, '', 1, '2022-09-04 21:43:51', '2022-09-04 21:43:51'),
('3f575f41-28d1-43b1-b850-f43b0677439c', 'Account Type', 'ACC_TYPE', 1, '', 1, '2022-09-05 21:36:53', '2022-09-05 21:36:53'),
('5349654a-b011-4252-bb9f-426401a70be4', 'Employee Status', 'EMP_STATUS', 1, '', 1, '2022-09-23 10:02:24', '2022-09-23 10:02:24'),
('78adf718-ca6d-4412-87ed-dc72b1f0b014', 'Journal Type', 'JOURNAL_TYPE', 1, '', 1, '2022-09-14 10:38:18', '2022-09-14 10:38:18'),
('8468c1ce-3f1d-4b7f-b5c2-577595b31a5b', 'Transaction Type', 'TRAN_TYPE', 1, '', 1, '2022-09-26 07:45:11', '2022-09-26 07:45:11'),
('a5ab1721-868c-43f9-82e0-806b72148dd5', 'Currency', 'CURR', 1, '', 1, '2022-09-08 22:08:47', '2022-09-08 22:08:47'),
('abef8b9f-7d44-4032-aa6d-f6e7748079ae', 'Customer Group', 'CG', 1, '', 1, '2022-09-04 21:18:15', '2022-09-04 21:18:15'),
('c5d748ef-66d0-46b9-9e24-512a4d8ccc93', 'Product Type', 'PROD_TYPE', 1, '', 1, '2022-09-08 20:32:25', '2022-09-08 20:32:25'),
('ca1526a2-eba1-4d71-ab03-407716284065', 'Vendor Group', 'VG', 1, '', 1, '2022-09-04 21:18:24', '2022-09-04 21:18:24'),
('dd97c575-ed2f-44ef-bd16-f43a32f5635e', 'Department', 'DEPT', 1, '', 1, '2022-09-08 23:05:10', '2022-09-08 23:05:10'),
('e4805806-cf44-426f-ad87-fe4b881dab5d', 'Invoice Type', 'INV_TYPE', 1, '', 1, '2022-09-09 15:52:57', '2022-09-09 15:52:57'),
('ed531bde-2acb-4e91-ab9b-eaf6ad31052f', 'Container Type', 'CONT_TYPE', 1, '', 1, '2022-09-13 10:45:49', '2022-09-13 10:45:49'),
('fed96d9f-550f-4a62-89fe-d3ca53f85d9f', 'Job Title', 'JOB_TIT', 1, '', 1, '2022-09-08 23:05:19', '2022-09-08 23:05:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2022_08_27_085300_create_table_initial', 1),
(2, '2022_09_04_204310_phase2', 2),
(3, '2022_09_07_203655_employee', 3),
(4, '2022_09_08_090854_operation_process', 4),
(6, '2022_09_09_142656_invoices', 5),
(7, '2022_09_10_093616_expenses', 6),
(8, '2022_09_13_103310_containers', 7),
(9, '2022_09_14_103657_journals', 8),
(10, '2022_09_22_074804_transaction', 9),
(11, '2022_09_22_083537_reconciliation', 10),
(12, '2022_09_29_141850_purchase_manager', 11),
(13, '2022_10_03_142742_gate_pass', 12);

-- --------------------------------------------------------

--
-- Table structure for table `operations`
--

CREATE TABLE `operations` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operation_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consignee_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `freight_forwarder_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reception_date` date DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  `atd_operation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `esl_operation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bol` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `gross_weight` double(18,2) NOT NULL DEFAULT 0.00,
  `loading_type_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_vehicle` int(11) DEFAULT NULL,
  `total_cbm` int(11) DEFAULT NULL,
  `total_containers_20` int(11) DEFAULT NULL,
  `total_containers_40` int(11) DEFAULT NULL,
  `partial_shipment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `container_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrival_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachement_file` blob DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `operations`
--

INSERT INTO `operations` (`id`, `category_id`, `operation_no`, `reference_no`, `consignee_id`, `freight_forwarder_id`, `reception_date`, `registration_date`, `atd_operation`, `esl_operation`, `bol`, `booking_no`, `quantity`, `gross_weight`, `loading_type_id`, `total_vehicle`, `total_cbm`, `total_containers_20`, `total_containers_40`, `partial_shipment`, `container_status`, `destination`, `agent_id`, `arrival_date`, `description`, `attachement_file`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
('3ae3151a-d7e5-44d6-bb34-f89d07e41abb', '81a9c6c6-7e00-4f8a-82b9-8c33a8faa14b', 'TR/001', 'REF', 'eb0af234-e436-4033-bcd5-cdac50fdabbc', '1c3f1c3b-75c3-4ae5-9086-c83721af7a51', '2022-09-09', '2022-09-09', '', '', '', NULL, 1, 0.00, '66fa5273-ddf7-41d1-99f8-4a8b74c4e814', 0, 0, NULL, NULL, '', '', '', '5ab84210-5f98-4e54-92ae-4faeb226ca53', '2022-09-09', '', '', NULL, 2, '2022-09-09 12:24:26', '2022-09-09 12:24:26'),
('5b14c22b-d437-402a-85a1-001aeb01be0f', '27717007-4dae-4910-9748-a483d5e92975', 'OP/001', 'REF001', 'eb0af234-e436-4033-bcd5-cdac50fdabbc', '1c3f1c3b-75c3-4ae5-9086-c83721af7a51', '2022-09-06', '2022-08-30', 'ATD1', 'ESL', '90', NULL, 1, 0.00, '66fa5273-ddf7-41d1-99f8-4a8b74c4e814', 0, 0, 0, 0, NULL, NULL, 'DES', '5ab84210-5f98-4e54-92ae-4faeb226ca53', '2022-09-05', 'DESC', NULL, NULL, 6, '2022-09-05 20:23:28', '2022-09-10 21:50:30'),
('62e87d68-84b0-4b55-ba8e-28834bf933c1', '27717007-4dae-4910-9748-a483d5e92975', 'TR009/09', 'REF', 'eb0af234-e436-4033-bcd5-cdac50fdabbc', '1c3f1c3b-75c3-4ae5-9086-c83721af7a51', '2022-09-23', '2022-09-09', '', '', '', NULL, 1, 40.00, '66fa5273-ddf7-41d1-99f8-4a8b74c4e814', 0, 0, NULL, NULL, '', '', 'DES', '5ab84210-5f98-4e54-92ae-4faeb226ca53', '2022-09-22', '', '', NULL, 3, '2022-09-10 13:54:06', '2022-09-13 10:45:15'),
('6daf7672-c683-4889-b138-d319e244bca1', '27717007-4dae-4910-9748-a483d5e92975', 'op09', 'ref', 'eb0af234-e436-4033-bcd5-cdac50fdabbc', '1c3f1c3b-75c3-4ae5-9086-c83721af7a51', '2022-09-09', '2022-09-15', '', '', '', 'book', 1, 0.00, '66fa5273-ddf7-41d1-99f8-4a8b74c4e814', 0, 0, NULL, NULL, '', '', '', '5ab84210-5f98-4e54-92ae-4faeb226ca53', '2022-09-09', '', NULL, NULL, 5, '2022-09-08 22:57:59', '2022-09-10 13:02:33'),
('7c2d1467-eb7d-41ff-9150-6ae5a1c98262', '27717007-4dae-4910-9748-a483d5e92975', 'OP017', '', 'eb0af234-e436-4033-bcd5-cdac50fdabbc', '1c3f1c3b-75c3-4ae5-9086-c83721af7a51', '2022-09-15', '2022-09-08', '', '', '', NULL, 1, 0.00, '66fa5273-ddf7-41d1-99f8-4a8b74c4e814', 0, 0, NULL, NULL, '', '', 'DES', '5ab84210-5f98-4e54-92ae-4faeb226ca53', '2022-09-21', '', 0x433a5c66616b65706174685c5061796d656e745f4f75747075745f32303232303930373135343231322e747874, NULL, 6, '2022-09-10 13:58:02', '2022-09-10 21:33:29'),
('c77ee67b-9c6c-4b45-a2d4-f990d3857641', '81a9c6c6-7e00-4f8a-82b9-8c33a8faa14b', 'TR009', 'REF12', 'eb0af234-e436-4033-bcd5-cdac50fdabbc', '1c3f1c3b-75c3-4ae5-9086-c83721af7a51', '2022-09-22', '2022-09-09', 'd3', '', 'bol12', NULL, 1, 35.00, '66fa5273-ddf7-41d1-99f8-4a8b74c4e814', 0, 0, NULL, NULL, '', '', 'des', '5ab84210-5f98-4e54-92ae-4faeb226ca53', '2022-09-21', '', '', NULL, 2, '2022-09-10 13:53:15', '2022-09-10 13:53:15');

-- --------------------------------------------------------

--
-- Table structure for table `operation_processes`
--

CREATE TABLE `operation_processes` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operation_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `declaration_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `declaration_date` date DEFAULT NULL,
  `vessel_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voyage_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partial_shipment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `container_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stuffing_date` date DEFAULT NULL,
  `gross_weight` int(11) DEFAULT NULL,
  `net_weight` int(11) DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `declaration_no_eth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assigned_no_declarant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assigned_no_importer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `border_office` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clearance_office` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fumigation` tinyint(1) DEFAULT NULL,
  `weight_inspection` tinyint(1) DEFAULT NULL,
  `inspection` tinyint(1) DEFAULT NULL,
  `craft_paper` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `operation_processes`
--

INSERT INTO `operation_processes` (`id`, `operation_id`, `declaration_no`, `declaration_date`, `vessel_name`, `voyage_no`, `partial_shipment`, `container_status`, `stuffing_date`, `gross_weight`, `net_weight`, `description`, `declaration_no_eth`, `assigned_no_declarant`, `assigned_no_importer`, `border_office`, `clearance_office`, `fumigation`, `weight_inspection`, `inspection`, `craft_paper`, `created_at`, `updated_at`) VALUES
('3578cae1-c49d-43a9-be06-d07b22a329ce', '5b14c22b-d437-402a-85a1-001aeb01be0f', 'dec12', '2022-09-08', 'v12', '12', '', '', '2022-09-15', 0, 0, '', '', '', '', '', 'clear off', 0, 0, 1, 1, '2022-09-08 20:19:26', '2022-09-08 20:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_sales_product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_type_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_billable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_sellable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_account_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `income_account_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchasable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exp_account_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_at_invoice_detail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trackable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disp_at_operation_exp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `is_sales_product`, `product_type_id`, `name`, `description`, `unit_price`, `is_billable`, `is_sellable`, `invoice_account_id`, `income_account_id`, `purchasable`, `exp_account_id`, `view_at_invoice_detail`, `trackable`, `disp_at_operation_exp`, `status`, `created_at`, `updated_at`) VALUES
('e8ea3dc4-3289-4a2c-9360-454765f21c2b', NULL, '3c5b4160-f182-4b3b-9d79-863b204b7ee0', 'Prod1', 'Desc', '12', NULL, '1', 'ec22b909-2c9e-11ed-b575-00090ffe0001', 'ec22b909-2c9e-11ed-b575-00090ffe0001', '', 'ec22b909-2c9e-11ed-b575-00090ffe0001', '', '1', '', 1, '2022-09-08 21:09:00', '2022-09-08 21:43:05');

-- --------------------------------------------------------

--
-- Table structure for table `reconciliations`
--

CREATE TABLE `reconciliations` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statement_ref` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `beg_balance` decimal(18,2) NOT NULL,
  `end_balance` decimal(18,2) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reconciliations`
--

INSERT INTO `reconciliations` (`id`, `statement_ref`, `account_id`, `beg_balance`, `end_balance`, `start_date`, `end_date`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
('aacb7090-80d7-4d17-874b-eecf31fb5f9d', 'REF101', 'ec22b909-2c9e-11ed-b575-00090ffe0001', '100.00', '0.00', '2022-09-26', '2022-09-26', 'REMARK', 1, '2022-09-26 19:48:24', '2022-09-26 19:48:24');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
('1QWSD', 'Admin', '2022-09-04 20:08:41', '2022-09-04 20:08:41'),
('23DF', 'User', '2022-09-04 20:08:41', '2022-09-04 20:08:41');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_type_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `amount` decimal(18,2) NOT NULL,
  `reference_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `journal_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `transaction_type_id`, `transaction_date`, `amount`, `reference_no`, `description`, `account_id`, `currency_id`, `journal_id`, `created_by`, `status`, `created_at`, `updated_at`) VALUES
('0219b57e-5448-4f16-b513-4b0fde0fe9eb', 'b7f702b0-e911-442c-9f51-2d89a21e2144', '2022-09-26', '5000.00', 'multiple', '5000 multiple', NULL, 'ca17bd85-2ac1-4e52-9bee-db8e66e085fc', '06068062-1246-4a65-8fc6-0afa34f33bb1', '', 1, '2022-09-26 20:15:16', '2022-09-26 20:15:16'),
('b850e92b-aa36-4538-b7a3-4ea1849e02e1', 'b7f702b0-e911-442c-9f51-2d89a21e2144', '2022-09-26', '100.00', 'test', 'desc', NULL, 'ca17bd85-2ac1-4e52-9bee-db8e66e085fc', '06068062-1246-4a65-8fc6-0afa34f33bb1', '', 1, '2022-09-26 15:29:39', '2022-09-26 15:29:39'),
('e78bdefb-1483-43d8-9c8c-96c278f4a336', 'b7f702b0-e911-442c-9f51-2d89a21e2144', '2022-09-26', '0.00', 'ref', 'desc', NULL, 'ca17bd85-2ac1-4e52-9bee-db8e66e085fc', '06068062-1246-4a65-8fc6-0afa34f33bb1', '', 1, '2022-09-26 15:30:34', '2022-09-26 15:30:34');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_items`
--

CREATE TABLE `transaction_items` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `debit` decimal(18,2) NOT NULL,
  `credit` decimal(18,2) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaction_items`
--

INSERT INTO `transaction_items` (`id`, `transaction_id`, `account_id`, `debit`, `credit`, `description`, `status`, `created_at`, `updated_at`) VALUES
('1b043838-f74a-4ad5-a673-0dab77605616', 'b850e92b-aa36-4538-b7a3-4ea1849e02e1', 'ec22b909-2c9e-11ed-b575-00090ffe0001', '100.00', '0.00', 'test item', 1, '2022-09-26 15:29:39', '2022-09-26 15:29:39'),
('3ef2cb0a-e160-41e0-b14c-f068ac09c60b', 'e78bdefb-1483-43d8-9c8c-96c278f4a336', 'ec22b909-2c9e-11ed-b575-00090ffe0001', '100.00', '0.00', '', 1, '2022-09-26 15:30:34', '2022-09-26 15:30:34'),
('c26d0b20-b62d-4e2f-a079-1acff497ec90', '0219b57e-5448-4f16-b513-4b0fde0fe9eb', 'ec22b909-2c9e-11ed-b575-00090ffe0001', '2000.00', '0.00', 'test2', 1, '2022-09-26 20:15:16', '2022-09-26 20:15:16'),
('ee2600aa-df08-4a3f-a1fd-358409731ea6', '0219b57e-5448-4f16-b513-4b0fde0fe9eb', '00f2f500-7625-407d-9849-2ec5ee21ab19', '3000.00', '0.00', 'test1', 1, '2022-09-26 20:15:16', '2022-09-26 20:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `transporters`
--

CREATE TABLE `transporters` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trucks`
--

CREATE TABLE `trucks` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operation_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chasis_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` double(18,2) NOT NULL DEFAULT 0.00,
  `weight` double(18,2) NOT NULL DEFAULT 0.00,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `way_bill_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gate_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sydonia_ref` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
('S001', 'First User', 'user1@gmail.com', NULL, '$2y$10$kK/E4gUTzQMOH1aPXhPnReqr339SKWQp.YqBSvQC6CiKVInQFCmzS', NULL, '2022-09-04 20:08:41', '2022-09-04 20:08:41'),
('S002', 'Admin User', 'admin@gmail.com', NULL, '$2y$10$EFt33Epgf8cem/jC.g.1EuBVcAENotbclrU/ZDPP.LeYNifCZQi2e', NULL, '2022-09-04 20:08:40', '2022-09-04 20:08:40');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usd_rate` double(18,2) DEFAULT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `name`, `code`, `telephone`, `email`, `address`, `address2`, `bp`, `city`, `country`, `account_id`, `terms`, `balance`, `usd_rate`, `group_id`, `status`, `created_at`, `updated_at`) VALUES
('af78883f-6a28-4c8a-896d-b6cfcfd7d7e6', 'Vendor Test1', 'VT1', '098', 'test@gmail.com', '', '', '', '', '', 'ec22b909-2c9e-11ed-b575-00090ffe0001', '', '0', 1.00, '4e2ccec3-5927-4603-9ef4-6550c5600d89', 1, '2022-09-04 22:25:09', '2022-09-04 22:25:09');

-- --------------------------------------------------------

--
-- Table structure for table `waybills`
--

CREATE TABLE `waybills` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transporter_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departure_date` date DEFAULT NULL,
  `driver_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driver_tel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `final_destination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate_agree` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clearance_office` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loading_type_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accounts_account_type_id_foreign` (`account_type_id`),
  ADD KEY `accounts_class_id_foreign` (`class_id`);

--
-- Indexes for table `account_types`
--
ALTER TABLE `account_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_types_coa_type_id_foreign` (`coa_type_id`);

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bills_vendor_id_foreign` (`vendor_id`),
  ADD KEY `bills_currency_id_foreign` (`currency_id`);

--
-- Indexes for table `bill_payments`
--
ALTER TABLE `bill_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_payments_vendor_id_foreign` (`vendor_id`),
  ADD KEY `bill_payments_currency_id_foreign` (`currency_id`);

--
-- Indexes for table `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cargos_location_id_foreign` (`location_id`),
  ADD KEY `cargos_operation_id_foreign` (`operation_id`);

--
-- Indexes for table `consignees`
--
ALTER TABLE `consignees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `containers`
--
ALTER TABLE `containers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `containers_operation_id_foreign` (`operation_id`),
  ADD KEY `containers_location_id_foreign` (`location_id`),
  ADD KEY `containers_container_type_id_foreign` (`container_type_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customers_account_id_foreign` (`account_id`),
  ADD KEY `customers_group_id_foreign` (`group_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD KEY `employees_department_id_foreign` (`department_id`),
  ADD KEY `employees_group_id_foreign` (`group_id`),
  ADD KEY `employees_status_id_foreign` (`status_id`),
  ADD KEY `employees_job_title_id_foreign` (`job_title_id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_product_id_foreign` (`product_id`),
  ADD KEY `expenses_operation_id_foreign` (`operation_id`);

--
-- Indexes for table `gate_passes`
--
ALTER TABLE `gate_passes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gate_passes_operation_id_foreign` (`operation_id`);

--
-- Indexes for table `gate_pass_containers`
--
ALTER TABLE `gate_pass_containers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gate_pass_containers_gate_pass_id_foreign` (`gate_pass_id`),
  ADD KEY `gate_pass_containers_container_id_foreign` (`container_id`);

--
-- Indexes for table `good_removals`
--
ALTER TABLE `good_removals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `good_removals_operation_id_foreign` (`operation_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD KEY `invoices_operation_id_foreign` (`operation_id`),
  ADD KEY `invoices_customer_id_foreign` (`customer_id`),
  ADD KEY `invoices_consignee_id_foreign` (`consignee_id`),
  ADD KEY `invoices_currency_id_foreign` (`currency_id`);

--
-- Indexes for table `journals`
--
ALTER TABLE `journals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `journals_journal_type_id_foreign` (`journal_type_id`),
  ADD KEY `journals_account_id_foreign` (`account_id`);

--
-- Indexes for table `lookups`
--
ALTER TABLE `lookups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lookups_lookup_type_id_foreign` (`lookup_type_id`);

--
-- Indexes for table `lookup_types`
--
ALTER TABLE `lookup_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operations`
--
ALTER TABLE `operations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operations_category_id_foreign` (`category_id`),
  ADD KEY `operations_consignee_id_foreign` (`consignee_id`),
  ADD KEY `operations_freight_forwarder_id_foreign` (`freight_forwarder_id`),
  ADD KEY `operations_loading_type_id_foreign` (`loading_type_id`),
  ADD KEY `operations_agent_id_foreign` (`agent_id`);

--
-- Indexes for table `operation_processes`
--
ALTER TABLE `operation_processes`
  ADD KEY `operation_processes_operation_id_foreign` (`operation_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_product_type_id_foreign` (`product_type_id`),
  ADD KEY `products_income_account_id_foreign` (`income_account_id`);

--
-- Indexes for table `reconciliations`
--
ALTER TABLE `reconciliations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reconciliations_account_id_foreign` (`account_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_transaction_type_id_foreign` (`transaction_type_id`),
  ADD KEY `transactions_currency_id_foreign` (`currency_id`),
  ADD KEY `transactions_journal_id_foreign` (`journal_id`);

--
-- Indexes for table `transaction_items`
--
ALTER TABLE `transaction_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transporters`
--
ALTER TABLE `transporters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trucks`
--
ALTER TABLE `trucks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trucks_location_id_foreign` (`location_id`),
  ADD KEY `trucks_operation_id_foreign` (`operation_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_roles_user_id_foreign` (`user_id`),
  ADD KEY `user_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendors_group_id_foreign` (`group_id`),
  ADD KEY `vendors_account_id_foreign` (`account_id`);

--
-- Indexes for table `waybills`
--
ALTER TABLE `waybills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `waybills_transporter_id_foreign` (`transporter_id`),
  ADD KEY `waybills_loading_type_id_foreign` (`loading_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_account_type_id_foreign` FOREIGN KEY (`account_type_id`) REFERENCES `account_types` (`id`),
  ADD CONSTRAINT `accounts_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `account_types`
--
ALTER TABLE `account_types`
  ADD CONSTRAINT `account_types_coa_type_id_foreign` FOREIGN KEY (`coa_type_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `bills`
--
ALTER TABLE `bills`
  ADD CONSTRAINT `bills_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `bills_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`);

--
-- Constraints for table `bill_payments`
--
ALTER TABLE `bill_payments`
  ADD CONSTRAINT `bill_payments_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `bill_payments_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`);

--
-- Constraints for table `cargos`
--
ALTER TABLE `cargos`
  ADD CONSTRAINT `cargos_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `cargos_operation_id_foreign` FOREIGN KEY (`operation_id`) REFERENCES `operations` (`id`);

--
-- Constraints for table `containers`
--
ALTER TABLE `containers`
  ADD CONSTRAINT `containers_container_type_id_foreign` FOREIGN KEY (`container_type_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `containers_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `containers_operation_id_foreign` FOREIGN KEY (`operation_id`) REFERENCES `operations` (`id`);

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `customers_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `employees_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `employees_job_title_id_foreign` FOREIGN KEY (`job_title_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `employees_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_operation_id_foreign` FOREIGN KEY (`operation_id`) REFERENCES `operations` (`id`),
  ADD CONSTRAINT `expenses_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `gate_passes`
--
ALTER TABLE `gate_passes`
  ADD CONSTRAINT `gate_passes_operation_id_foreign` FOREIGN KEY (`operation_id`) REFERENCES `operations` (`id`);

--
-- Constraints for table `gate_pass_containers`
--
ALTER TABLE `gate_pass_containers`
  ADD CONSTRAINT `gate_pass_containers_container_id_foreign` FOREIGN KEY (`container_id`) REFERENCES `containers` (`id`),
  ADD CONSTRAINT `gate_pass_containers_gate_pass_id_foreign` FOREIGN KEY (`gate_pass_id`) REFERENCES `gate_passes` (`id`);

--
-- Constraints for table `good_removals`
--
ALTER TABLE `good_removals`
  ADD CONSTRAINT `good_removals_operation_id_foreign` FOREIGN KEY (`operation_id`) REFERENCES `operations` (`id`);

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_consignee_id_foreign` FOREIGN KEY (`consignee_id`) REFERENCES `consignees` (`id`),
  ADD CONSTRAINT `invoices_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `invoices_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `invoices_operation_id_foreign` FOREIGN KEY (`operation_id`) REFERENCES `operations` (`id`);

--
-- Constraints for table `journals`
--
ALTER TABLE `journals`
  ADD CONSTRAINT `journals_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `journals_journal_type_id_foreign` FOREIGN KEY (`journal_type_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `lookups`
--
ALTER TABLE `lookups`
  ADD CONSTRAINT `lookups_lookup_type_id_foreign` FOREIGN KEY (`lookup_type_id`) REFERENCES `lookup_types` (`id`);

--
-- Constraints for table `operations`
--
ALTER TABLE `operations`
  ADD CONSTRAINT `operations_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`),
  ADD CONSTRAINT `operations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `operations_consignee_id_foreign` FOREIGN KEY (`consignee_id`) REFERENCES `consignees` (`id`),
  ADD CONSTRAINT `operations_freight_forwarder_id_foreign` FOREIGN KEY (`freight_forwarder_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `operations_loading_type_id_foreign` FOREIGN KEY (`loading_type_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `operation_processes`
--
ALTER TABLE `operation_processes`
  ADD CONSTRAINT `operation_processes_operation_id_foreign` FOREIGN KEY (`operation_id`) REFERENCES `operations` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_income_account_id_foreign` FOREIGN KEY (`income_account_id`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `products_product_type_id_foreign` FOREIGN KEY (`product_type_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `reconciliations`
--
ALTER TABLE `reconciliations`
  ADD CONSTRAINT `reconciliations_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `transactions_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `transactions_journal_id_foreign` FOREIGN KEY (`journal_id`) REFERENCES `journals` (`id`),
  ADD CONSTRAINT `transactions_transaction_type_id_foreign` FOREIGN KEY (`transaction_type_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `transaction_items`
--
ALTER TABLE `transaction_items`
  ADD CONSTRAINT `transaction_items_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`);

--
-- Constraints for table `trucks`
--
ALTER TABLE `trucks`
  ADD CONSTRAINT `trucks_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `trucks_operation_id_foreign` FOREIGN KEY (`operation_id`) REFERENCES `operations` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `vendors`
--
ALTER TABLE `vendors`
  ADD CONSTRAINT `vendors_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `vendors_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `waybills`
--
ALTER TABLE `waybills`
  ADD CONSTRAINT `waybills_loading_type_id_foreign` FOREIGN KEY (`loading_type_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `waybills_transporter_id_foreign` FOREIGN KEY (`transporter_id`) REFERENCES `transporters` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
