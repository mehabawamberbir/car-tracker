import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { CONSTANT_CONFIG } from '../config/constants';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../user-manager/model/user';
import { StorageService } from './storage.service';

import  axios  from 'axios';

const AUTH_API = CONSTANT_CONFIG.AUTH_API + 'login';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  responseCode = 0;

  constructor(
    private http: HttpClient,
    private storageService : StorageService,    
    public jwtHelper: JwtHelperService) 
    {
      //var user = storageService.getCurrUser() || '{}';
      //let jsonObj = JSON.parse(user || '{}');
      //let item: User = <User>jsonObj;

      // console.log("CURRENT USER22:" + item);
      // this.currentUserSubject = new BehaviorSubject(item);
      // this.currentUser = this.currentUserSubject.asObservable();
      // this.currentUser.pipe().subscribe(res => {        
      // console.log("CURRENT USER23:" + this.currentUserSubject.value);
      // });
    }

    public get currentUserValue(): User {
      return this.currentUserSubject.value;
    }

  
  login(user_name : string,password : string){
    
    var result =  axios.post(AUTH_API, {
      email: user_name,
      password: password
    });
    result.then((res) => 
    {
      if(res.status == 200)
      {
        this.storageService.clean();
        this.storageService.saveUserKey(res.data.access_token);
        this.storageService.saveCurrUser(res.data.user.email);
        this.storageService.saveUserRoles(res.data.user.roles);
        this.currentUserSubject.next(res.data.user.email);
      } 
      else
      {
        window.localStorage.removeItem(CONSTANT_CONFIG.API_TOKEN);
        window.localStorage.removeItem(CONSTANT_CONFIG.CURR_USER);
        window.localStorage.removeItem(CONSTANT_CONFIG.ROLES);
      }
    })
    .catch((err) => {
    })
    .finally(() => {
      //console.log('Login completed');
    })
    return result;
  }
  
  register(username: string, email: string, password: string): Observable<any> {
    return this.http.post(
      AUTH_API + 'signup',
      {
        username,
        email,
        password,
      },
      httpOptions
    );
  }
  logout(): Observable<any> {
    window.localStorage.removeItem(CONSTANT_CONFIG.API_TOKEN);
    window.localStorage.removeItem(CONSTANT_CONFIG.CURR_USER);
    window.localStorage.removeItem(CONSTANT_CONFIG.ROLES);
    //this.currentUserSubject.next(new User());
    return this.http.post(AUTH_API + 'signout', { }, httpOptions);
  }

  public isAuthenticated(): boolean {
    
    console.log("VALIDATE LOGIN");
    var item = this.storageService.getUserKey();
    const token = item ||  '{}';
    // Check whether the token is expired and return
    // true or false
    if(token == '{}') return false;
    return !this.jwtHelper.isTokenExpired(token);
  }

}