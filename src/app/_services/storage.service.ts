import { Injectable } from '@angular/core';
import { CONSTANT_CONFIG } from '../config/constants';
const API_TOKEN = CONSTANT_CONFIG.API_TOKEN;
@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() {}

  clean(): void {
    window.localStorage.clear();
  }

  public saveUserKey(data: any): void {
      window.localStorage.removeItem(API_TOKEN);
      window.localStorage.setItem(API_TOKEN, JSON.stringify(data));
  }

  public saveCurrUser(data: any): void {
      window.localStorage.removeItem(CONSTANT_CONFIG.CURR_USER);
      window.localStorage.setItem(CONSTANT_CONFIG.CURR_USER, JSON.stringify(data));
  }
  public saveUserRoles(data: any): void {
    window.localStorage.removeItem(CONSTANT_CONFIG.ROLES);
    window.localStorage.setItem(CONSTANT_CONFIG.ROLES, JSON.stringify(data));
}

  public getUserKey(){
    const user = window.localStorage.getItem(CONSTANT_CONFIG.API_TOKEN);        
    return user;
  }

  public getCurrUser(){
    const user = window.localStorage.getItem(CONSTANT_CONFIG.CURR_USER);        
    return user;
  }

  public getCurrUserRoles(){
    const res = window.localStorage.getItem(CONSTANT_CONFIG.ROLES);        
    return res;
  }

  public isLoggedIn(): boolean {
    const user = window.localStorage.getItem(CONSTANT_CONFIG.CURR_USER);
    if (user) {
      return true;
    }
    return false;
  }
}