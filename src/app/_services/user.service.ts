import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONSTANT_CONFIG } from '../config/constants';
import { RepositoryService } from '../shared/services/repository.service';
const API_URL = CONSTANT_CONFIG.API_URL + 'user/';
const AUTH_API = CONSTANT_CONFIG.AUTH_API + 'login';
var responseCode = 0;
// const axios = require('axios').default;
import  axios  from 'axios';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient,
    private repository: RepositoryService) {}
    
  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }
  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', { responseType: 'text' });
  }
  
  getModeratorBoard(): Observable<any> {
    return this.http.get(API_URL + 'mod', { responseType: 'text' });
  }
  getAdminBoard(): Observable<any> {
    return this.http.get(API_URL + 'admin', { responseType: 'text' });
  }
  
  login(user_name : string,password : string){
    var response = axios.post(AUTH_API, {
      email: user_name,
      password: password
    });
    return response;

    // axios.post(AUTH_API, {
    //   user_name: user_name,
    //   password: password
    // })
    // .then(function (response) {
    //   responseCode = response.status;
    //   if(response.status == 200)
    //   {
    //     window.sessionStorage.removeItem(USER_KEY);
    //     console.log(JSON.stringify(response.data.remember_token));
    //     window.sessionStorage.setItem(USER_KEY, JSON.stringify(response.data.remember_token));
    //   }
    //   else{
    //     window.sessionStorage.removeItem(USER_KEY);
    //   }
    //   return response.status;
    // })
    // .catch(function (error) {
    //   console.log(error);
    // });
  }
  
  login2(user_name : string,password : string){
    axios.post(AUTH_API, {
      user_name: user_name,
      password: password
    })
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      return error;
    });
    //return this.http.post(AUTH_API, JSON.stringify({user_name: user_name, password:password}));
    //return this.repository.sendRequest("POST", AUTH_API, { body: {user_name: user_name, password:password} });
  }
}