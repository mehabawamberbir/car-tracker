import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/shared/services/search.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SortService } from 'src/app/shared/services/sort.service';
import { PaginationComponent } from 'src/app/shared/templates/pagination/pagination.component';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { AccountTypeService } from '../services/account-type.service';
import { CONSTANT_CONFIG } from 'src/app/config/constants';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { AccountType } from '../accounting-manager.model';

@Component({
  selector: 'app-account-type',
  templateUrl: './account-type.component.html',
  styleUrls: ['./account-type.component.scss']
})
export class AccountTypeComponent implements OnInit {
  types: Lookup[];
  url:string; 
  id:string;
  submitted: boolean = false;
  disabled: boolean = false;
  createMode: boolean = false;
  editMode: boolean = false;
  model: FormGroup;
  models:AccountType[];

  constructor(
    private notificationService: NotificationService,
    private lookupService: LookupService,
    private modelService:AccountTypeService,
    private sortService: SortService,
    private searchService: SearchService,
    private repositoryService: RepositoryService,
    private fb: FormBuilder,
    private pagination: PaginationComponent) {

  }

  ngOnInit(): void {
    this.createMode = false;
    this.editMode = false;
    this.submitted = false;
    this.disabled = false;
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath  + "?";
    
    this.LoadData();
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.COA_TYPE).then(res => {
      this.types = res.data
    })
  }
  
  LoadData(){
    
    this.modelService.GetAll()
    .then((res) => {
      this.models = res.data;
      this.searchService.searchData = res.data;
    })
  }
  ClearForm(){
    this.model = this.fb.group({
      coa_type_id: ['', Validators.required],
      name: ['', Validators.required],
      code: ['', Validators.required],
      status: 1,
    });
  }
  createClicked() {
    this.createMode = true;
    this.editMode = false;
    this.model = this.fb.group({
      coa_type_id: ['', Validators.required],
      name: ['', Validators.required],
      code: ['', Validators.required],
      status: 1,
    });
  }

  editClicked(obj:AccountType) {
    this.createMode = false;
    this.editMode = true;
    this.id = obj.id;
    this.model = this.fb.group({
      id: [obj.id],
      coa_type_id: [obj.coa_type_id, Validators.required],
      name: [obj.name, Validators.required],
      code: [obj.code, Validators.required],
      status : [obj.status]
    })
  }

  closeModes() {
    this.createMode = this.editMode = this.submitted = false;
  }
  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    }
    else {
      this.submitted = true
    }
  }
  edit() {
    if (this.model.valid) {
      this.modelService.Edit(this.id, this.model.value)
      .then((res) => 
      {
        if(res.status == 200)
        {
          this.notificationService.showToastr("success", "Record Updated successfully!");
          this.editMode = false;
          this.LoadData();
        } 
        else
        {
          this.notificationService.showToastr("danger", "Some error occured!")
        }
      })
      .catch((err) => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
      .finally(() => {
      });
    }
    else {
      this.submitted = true
    }
  }

  newData(data:any) {
    this.models = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.models = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.models = data
  }

  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.modelService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
}
