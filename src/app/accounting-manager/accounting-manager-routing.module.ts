import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountTypeComponent } from './account-type/account-type.component';
import { CreateChartOfAccountsComponent } from './chart-of-accounts/create-chart-of-accounts/create-chart-of-accounts.component';
import { EditChartOfAccountsComponent } from './chart-of-accounts/edit-chart-of-accounts/edit-chart-of-accounts.component';
import { IndexChartOfAccountsComponent } from './chart-of-accounts/index-chart-of-accounts/index-chart-of-accounts.component';
import { IndexAccountingManagerComponent } from './index-accounting-manager/index-accounting-manager.component';
import { CreateJournalTransactionComponent } from './journal-transaction/create-journal-transaction/create-journal-transaction.component';
import { EditJournalTransactionComponent } from './journal-transaction/edit-journal-transaction/edit-journal-transaction.component';
import { IndexJournalTransactionComponent } from './journal-transaction/index-journal-transaction/index-journal-transaction.component';
import { CreateJournalComponent } from './journal/create-journal/create-journal.component';
import { EditJournalComponent } from './journal/edit-journal/edit-journal.component';
import { IndexJournalComponent } from './journal/index-journal/index-journal.component';
import { IndexReconcilationComponent } from './reconcilation/index-reconcilation/index-reconcilation.component';
import { CreateReconcilationComponent } from './reconcilation/create-reconcilation/create-reconcilation.component';
import { EditReconcilationComponent } from './reconcilation/edit-reconcilation/edit-reconcilation.component';
import { CreateSimpleTransactionComponent } from './simple-transaction/create-simple-transaction/create-simple-transaction.component';
const routes: Routes = [
  {path: '', component:IndexAccountingManagerComponent},

  {path: 'chart-of-accounts', component:IndexChartOfAccountsComponent},
  {path: 'chart-of-accounts/create', component:CreateChartOfAccountsComponent},
  {path: 'chart-of-accounts/edit/:id', component:EditChartOfAccountsComponent},

  {path: 'account-types', component:AccountTypeComponent},
  {path: 'simple-transactions', component:CreateSimpleTransactionComponent},
  

  {path: 'journals', component:IndexJournalComponent},
  {path: 'journals/create', component:CreateJournalComponent},
  {path: 'journals/edit/:id', component:EditJournalComponent},

  {path: 'journal-transactions', component:IndexJournalTransactionComponent},
  {path: 'journal-transactions/create', component:CreateJournalTransactionComponent},
  {path: 'journal-transactions/edit/:id', component:EditJournalTransactionComponent},

  {path: 'reconciliations', component:IndexReconcilationComponent},
  {path: 'reconciliations/create', component:CreateReconcilationComponent},
  {path: 'reconciliations/edit/:id', component:EditReconcilationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingManagerRoutingModule { }
