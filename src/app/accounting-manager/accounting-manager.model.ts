import { Lookup } from "../master-data/master.model";

export class AccountType{
    id:string;
    code:string;
    name:string;
    description:string;
    coa_type_id:string;
    type:Lookup;
    status:boolean;
}
export class Account{    
    id:string;
    account_type_id:string;
    account_type:AccountType;
    class_id:string;
    code:string;
    name:string;
    description:string;
    sub_account_id:string;
    reconcileable:boolean;
    status:boolean;
}
export class Journal{    
    id:string;
    journal_type_id:string;
    journal_type:Lookup;
    code:string;
    name:string;
    description:string;
    account_id:string;
    account:Account;
    created_by:string;
    status:boolean;
}
export class Reconcilation{    
    id:string;
    statement_ref:string;
    account_id:string;
    account:Account;
    beg_balance:number;
    end_balance:number;
    start_date:Date;
    end_date:Date;
    remarks:string;
    status:boolean;
}

export class Transaction{    
    id:string;
    transaction_type_id:string;
    transaction_date:Date;
    account_id:string;
    currency_id:string;
    journal_id:string;
    amount:number;
    reference_no:string;
    description:string;
    created_by:string;
    status:boolean;
    transaction_items:TransactionItem[]
}
export class TransactionItem{
    id:string;
    transaction_id:string;
    account_id:string;
    debit:number;
    credit:number;
    description:string;
    status:boolean; 
}