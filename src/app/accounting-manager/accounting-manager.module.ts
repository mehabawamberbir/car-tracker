import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbIconModule, NbMenuModule, NbSelectModule } from '@nebular/theme';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountingManagerRoutingModule } from './accounting-manager-routing.module';
import { IndexAccountingManagerComponent } from './index-accounting-manager/index-accounting-manager.component';
import { IndexChartOfAccountsComponent } from './chart-of-accounts/index-chart-of-accounts/index-chart-of-accounts.component';
import { CreateChartOfAccountsComponent } from './chart-of-accounts/create-chart-of-accounts/create-chart-of-accounts.component';
import { EditChartOfAccountsComponent } from './chart-of-accounts/edit-chart-of-accounts/edit-chart-of-accounts.component';
import { AccountTypeComponent } from './account-type/account-type.component';
import { CreateJournalComponent } from './journal/create-journal/create-journal.component';
import { EditJournalComponent } from './journal/edit-journal/edit-journal.component';
import { IndexJournalComponent } from './journal/index-journal/index-journal.component';
import { IndexJournalTransactionComponent } from './journal-transaction/index-journal-transaction/index-journal-transaction.component';
import { EditJournalTransactionComponent } from './journal-transaction/edit-journal-transaction/edit-journal-transaction.component';
import { CreateJournalTransactionComponent } from './journal-transaction/create-journal-transaction/create-journal-transaction.component';
import { CreateReconcilationComponent } from './reconcilation/create-reconcilation/create-reconcilation.component';
import { IndexReconcilationComponent } from './reconcilation/index-reconcilation/index-reconcilation.component';
import { EditReconcilationComponent } from './reconcilation/edit-reconcilation/edit-reconcilation.component';
import { CreateSimpleTransactionComponent } from './simple-transaction/create-simple-transaction/create-simple-transaction.component';
// import { IndexJournalComponent } from './journal/index-journal/index-journal.component';


@NgModule({
  declarations: [
    IndexAccountingManagerComponent,
    IndexChartOfAccountsComponent,
    CreateChartOfAccountsComponent,
    EditChartOfAccountsComponent,
    AccountTypeComponent,
    CreateJournalComponent,
    EditJournalComponent,
    IndexJournalComponent,
    IndexJournalTransactionComponent,
    EditJournalTransactionComponent,
    CreateJournalTransactionComponent,
    CreateReconcilationComponent,
    IndexReconcilationComponent,
    EditReconcilationComponent,
    CreateSimpleTransactionComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,    
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    ReactiveFormsModule,
    NbMenuModule,
    AccountingManagerRoutingModule,
  ]
})
export class AccountingManagerModule { }
