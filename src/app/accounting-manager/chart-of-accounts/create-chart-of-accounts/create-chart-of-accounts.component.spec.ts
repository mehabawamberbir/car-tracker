import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateChartOfAccountsComponent } from './create-chart-of-accounts.component';

describe('CreateChartOfAccountsComponent', () => {
  let component: CreateChartOfAccountsComponent;
  let fixture: ComponentFixture<CreateChartOfAccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateChartOfAccountsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateChartOfAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
