import { Component, OnInit } from '@angular/core';
import { Account, Journal } from '../../accounting-manager.model';
import { AccountService } from '../../services/account.service';
import { AccountType } from '../../accounting-manager.model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AccountTypeService } from '../../services/account-type.service';
import { JournalService } from '../../services/journal-service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
@Component({
  selector: 'app-create-chart-of-accounts',
  templateUrl: './create-chart-of-accounts.component.html',
  styleUrls: ['./create-chart-of-accounts.component.scss']
})
export class CreateChartOfAccountsComponent implements OnInit {
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  //types: AccountType[];
  types: Lookup[];
  accounts: Account[];
  //journals:Journal[];
  // journals:Lookup[];
  classes:Lookup[];
    constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: AccountService,
    private accountTypeService: AccountTypeService,
    private accountService: AccountService,
    private journalService: JournalService,
    private lookUpService: LookupService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
    id:[''],
    account_type_id:['', [Validators.required]],
    class_id:[''],
    code:['',[Validators.required]],
    name:['', [Validators.required]],
    description:[''],
    sub_account_id:[''],
    reconcileable:0,
    status: 1
    });
    // this.lookUpService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.COA_TYPE).then((res)=>
    // {
    //   this.types = res.data;
    // });
    this.accountTypeService.List().then((res)=>
    {
      this.types = res.data;
    });
    this.accountService.GetAll().then((res)=>
    {
      this.accounts = res.data;
    });
    this.lookUpService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.ACCOUNT_CLASS).then((res)=>
    {
      this.classes = res.data;
    });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  ClearForm(){
    this.model = this.fb.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      account_type_id:['', Validators.required],
      status: 1,
    });
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
}
