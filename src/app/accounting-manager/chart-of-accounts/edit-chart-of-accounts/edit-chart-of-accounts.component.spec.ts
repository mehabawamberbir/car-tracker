import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChartOfAccountsComponent } from './edit-chart-of-accounts.component';

describe('EditChartOfAccountsComponent', () => {
  let component: EditChartOfAccountsComponent;
  let fixture: ComponentFixture<EditChartOfAccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditChartOfAccountsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditChartOfAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
