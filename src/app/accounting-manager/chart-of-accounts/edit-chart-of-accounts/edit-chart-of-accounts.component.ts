import { Component, OnInit } from '@angular/core';
import { Account, Journal } from '../../accounting-manager.model';
import { AccountService } from '../../services/account.service';
import { AccountType } from '../../accounting-manager.model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AccountTypeService } from '../../services/account-type.service';
import { JournalService } from '../../services/journal-service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-edit-chart-of-accounts',
  templateUrl: './edit-chart-of-accounts.component.html',
  styleUrls: ['./edit-chart-of-accounts.component.scss']
})
export class EditChartOfAccountsComponent implements OnInit {

  id:string;
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;
  model: FormGroup;
  types: AccountType[];
  // types: Lookup[];
  accounts: Account[];
  //journals:Journal[];
  journals:Lookup[];
    constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: AccountService,
    private accountTypeService: AccountTypeService,
    private accountService: AccountService,
    private journalService: JournalService,
    private lookUpService: LookupService,
    private route:ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false

    this.id = this.route.snapshot.paramMap.get("id")!;

    this.model = this.fb.group({
    id:[''],
    account_type_id:['', [Validators.required]],
    class_id:[''],
    code:['',[Validators.required]],
    name:['', [Validators.required]],
    description:[''],
    sub_account_id:[''],
    reconcileable:0,
    status: 1
    });
    // this.accountTypeService.GetAll().then((res)=>
    // {
    //   this.types = res.data;
    // });
    this.lookUpService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.ACCOUNT_TYPE).then((res)=>
    {
      this.types = res.data;
    });
    this.accountService.GetAll().then((res)=>
    {
      this.accounts = res.data;
    });
    // this.journalService.GetAll().then((res)=>
    // {
    //   this.journals = res.data;
    // });
    this.lookUpService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.JOURNAL_TYPE).then((res)=>
    {
      this.journals = res.data;
    });
    this.populateData(this.id);
  }

  populateData(id:string){

    this.modelService.GetById(id).subscribe((data)=>
    {
      this.model.setValue({
        id:this.id,
        account_type_id:data.account_type_id,
        class_id:data.class_id,
        code:data.code,
        name:data.name,
        description:data.description,
        sub_account_id:data.sub_account_id,
        reconcileable:data.reconcileable,
        status: data.status,
      });
    });
   
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  update() {
    var isFormValid = true;
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
}