import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexChartOfAccountsComponent } from './index-chart-of-accounts.component';

describe('IndexChartOfAccountsComponent', () => {
  let component: IndexChartOfAccountsComponent;
  let fixture: ComponentFixture<IndexChartOfAccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexChartOfAccountsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexChartOfAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
