import { Component, OnInit, Type } from '@angular/core';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { AccountTypeService } from '../../services/account-type.service';
import { AccountService } from '../../services/account.service';
import { Account } from '../../accounting-manager.model';
import { SearchService } from 'src/app/shared/services/search.service';
import { Router } from '@angular/router';
import { RepositoryService } from 'src/app/shared/services/repository.service';
@Component({
  selector: 'app-index-chart-of-accounts',
  templateUrl: './index-chart-of-accounts.component.html',
  styleUrls: ['./index-chart-of-accounts.component.scss']
})
export class IndexChartOfAccountsComponent implements OnInit {

  url:string;
 model: Account[];
  constructor(
    private notificationService:NotificationService,
    private sortService:SortService,
    private modelService:AccountService,
    private searchService:SearchService,
    private router:Router,
    private accountTypeService:AccountTypeService,
    private repositoryService:RepositoryService
  ) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath  + "?";
    this.LoadData();
  }
  LoadData(){
    this.modelService.GetAll()
    .then((res) => {
      this.model = res.data;
      this.searchService.searchData = res.data;
    });
  }
  edit(obj:Account) {
    
    this.router.navigate(['/accounting-manager/chart-of-accounts/edit/' + obj.id]);
  }
  newData(data:any) {
    this.model = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.modelService = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.modelService = data
  }

  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.modelService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }

}
