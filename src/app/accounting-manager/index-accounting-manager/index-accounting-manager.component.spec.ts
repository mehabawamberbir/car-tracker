import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexAccountingManagerComponent } from './index-accounting-manager.component';

describe('IndexAccountingManagerComponent', () => {
  let component: IndexAccountingManagerComponent;
  let fixture: ComponentFixture<IndexAccountingManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexAccountingManagerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexAccountingManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
