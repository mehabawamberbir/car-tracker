import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { CONSTANT_CONFIG } from 'src/app/config/constants';
@Component({
  selector: 'app-index-accounting-manager',
  templateUrl: './index-accounting-manager.component.html',
  styleUrls: ['./index-accounting-manager.component.scss']
})
export class IndexAccountingManagerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
items: NbMenuItem[] = [
  {
    title:'Simple Transactions',
    link: 'simple-transactions',
    icon:'checkmark'
  },
  {
    title:'Journal Transactions',
    link: 'journal-transactions',
    icon:'checkmark'
  },
  {
    title:'Journals',
    link: 'journals',
    icon:'checkmark'
  },
  {
    title:'Chart of Accounts',
    link: 'chart-of-accounts',
    icon:'checkmark'
  },
  {
    title:'Reconciliation',
    link: 'reconciliations',
    icon:'checkmark'
  },
  {
    title:'Account Types',
    link: 'account-types',
    icon:'checkmark'
  }
]

}
