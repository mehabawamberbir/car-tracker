import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateJournalTransactionComponent } from './create-journal-transaction.component';

describe('CreateJournalTransactionComponent', () => {
  let component: CreateJournalTransactionComponent;
  let fixture: ComponentFixture<CreateJournalTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateJournalTransactionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateJournalTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
