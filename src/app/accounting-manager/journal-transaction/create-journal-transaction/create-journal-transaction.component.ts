import { Component, OnInit } from '@angular/core';
import { Account, Journal, TransactionItem } from '../../accounting-manager.model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormArray, FormBuilder,FormGroup,Validators } from '@angular/forms';
import { JournalTransactionService } from '../../services/journal-transaction.service';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { AccountService } from '../../services/account.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';
import { NbCardModule } from '@nebular/theme';
import { SaveButtonComponent } from 'src/app/shared/templates/save-button.component';
import { JournalService } from '../../services/journal-service';
@Component({
  selector: 'app-create-journal-transaction',
  templateUrl: './create-journal-transaction.component.html',
  styleUrls: ['./create-journal-transaction.component.scss']
})
export class CreateJournalTransactionComponent implements OnInit {

  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  currency_types: Lookup[];
  types: Lookup[];
  accounts: Account[];
  journals: Journal[];
  //transaction_items: TransactionItem[];
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: JournalTransactionService,
    private lookupService:LookupService,
    private accountService:AccountService,
    private journalService:JournalService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false;

    

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CURRENCY).then((res)=>
    {
      this.currency_types = res.data;
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.TRANSACTION_TYPE).then((res)=>
    {
      this.types = res.data;
    });
    
    this.journalService.List().then((res)=> {
      this.journals = res.data;
    });
    this.accountService.GetAll().then((res)=> {
      this.accounts = res.data;
    });

    this.model = this.fb.group({
    id:[''],
    //transaction_type_id:['', [Validators.required]],
    transaction_date:[''],
    currency_id:['', [Validators.required]],
    journal_id:['', [Validators.required]],
    amount:0,
    reference_no:[''],
    description:[''],
    created_date:[''],
    created_by:[''],
    status:1,
    transaction_items:this.fb.array([])
      });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  getFromArray() {
    return this.model.get("transaction_items") as FormArray;
	}
  addTransactionItem(): FormGroup{
		return this.fb.group({
      id:[''],
      transaction_id:[''],
      account_id:['',[Validators.required]],
      debit:[0,[Validators.required]],
      credit:[0,[Validators.required]],
      description:['']
		});
	}
  addButtonClick(): void {
    this.formArr.push(this.addTransactionItem());
  }
  deleteTransactionItem(index: number) {
		this.formArr.removeAt(index);
	}
  get formArr() : FormArray  {
    return this.model.get("transaction_items") as FormArray;
  }

  ClearForm(){
    this.model = this.fb.group({
    transaction_type_id:['', [Validators.required]],
    currency_id:['', [Validators.required]],
    transaction_date:[''],
    journal_id:['', [Validators.required]],
    account_id:['', [Validators.required]],
    amount:0,
    description:[''],
    reference_no:[''],
    created_date:[''],
    created_by:[''],
    status:1
    });
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }

}
