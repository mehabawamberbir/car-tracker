import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditJournalTransactionComponent } from './edit-journal-transaction.component';

describe('EditJournalTransactionComponent', () => {
  let component: EditJournalTransactionComponent;
  let fixture: ComponentFixture<EditJournalTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditJournalTransactionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditJournalTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
