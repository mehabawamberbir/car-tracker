import { Component, OnInit } from '@angular/core';
import { Account, Journal, Transaction, TransactionItem } from '../../accounting-manager.model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormArray, FormBuilder,FormGroup,Validators } from '@angular/forms';
import { JournalTransactionService } from '../../services/journal-transaction.service';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { AccountService } from '../../services/account.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';
import { NbCardModule } from '@nebular/theme';
import { SaveButtonComponent } from 'src/app/shared/templates/save-button.component';
import { JournalService } from '../../services/journal-service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-journal-transaction',
  templateUrl: './edit-journal-transaction.component.html',
  styleUrls: ['./edit-journal-transaction.component.scss']
})
export class EditJournalTransactionComponent implements OnInit {
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  id: string;
  transaction : Transaction;
  model: FormGroup;
  currency_types: Lookup[];
  types: Lookup[];
  accounts: Account[];
  journals: Journal[];
  transaction_items : TransactionItem[];

  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: JournalTransactionService,
    private lookupService:LookupService,
    private accountService:AccountService,
    private journalService:JournalService,
    private route:ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.id = this.route.snapshot.paramMap.get("id") !;

    this.model = this.fb.group({
        id:[''],
        transaction_type_id:['', [Validators.required]],
        transaction_date:[''],
        currency_id:['', [Validators.required]],
        journal_id:['', [Validators.required]],
        amount:0,
        reference_no:[''],
        description:[''],
        transaction_items:this.fb.array([])
      });

      this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CURRENCY).then((res)=>
      {
        this.currency_types = res.data;
      });
  
      this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.TRANSACTION_TYPE).then((res)=>
      {
        this.types = res.data;
      });
      
      this.journalService.List().then((res)=> {
        this.journals = res.data;
      });
      this.accountService.GetAll().then((res)=> {
        this.accounts = res.data;
      });

      this.populateData(this.id);
  }
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      
      console.log(data.transaction_items); 

      data.transaction_items.map(
        (obj: TransactionItem) => {    
          console.log(obj.debit);      
          this.formArr.push(this.addItemWithValue(obj));
        }
      );
      this.model.setValue({
        id:this.id,
        transaction_type_id:data.transaction_type_id,
        transaction_date:data.transaction_date,
        currency_id:data.currency_id,
        journal_id:data.journal_id,
        amount:data.amount,
        reference_no:data.reference_no,
        description:data.description,
        transaction_items:this.formArr
      });
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  

  getFormArray() {
    return this.model.get("transaction_items") as FormArray;
	}
  addItemWithValue(obj:TransactionItem): FormGroup{
		return this.fb.group({
      id:obj.id,
      transaction_id:obj.transaction_id,
      account_id:obj.account_id,
      debit:obj.debit,
      credit:obj.credit,
      description:obj.description
		});
	}
  addTransactionItem(): FormGroup{
		return this.fb.group({
      id:[''],
      transaction_id:[''],
      account_id:['',[Validators.required]],
      debit:[0,[Validators.required]],
      credit:[0,[Validators.required]],
      description:['']
		});
	}
  addButtonClick(): void {
    this.formArr.push(this.addTransactionItem());
  }
  deleteTransactionItem(index: number) {
		this.formArr.removeAt(index);
	}
  get formArr() : FormArray  {
    return this.model.get("transaction_items") as FormArray;
  }

  

  update() {
    var isFormValid = true;
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
}
