import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexJournalTransactionComponent } from './index-journal-transaction.component';

describe('IndexJournalTransactionComponent', () => {
  let component: IndexJournalTransactionComponent;
  let fixture: ComponentFixture<IndexJournalTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexJournalTransactionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexJournalTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
