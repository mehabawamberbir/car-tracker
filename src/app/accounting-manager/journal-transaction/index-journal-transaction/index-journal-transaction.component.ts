import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { PaginationComponent } from 'src/app/shared/templates/pagination/pagination.component';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Account, Journal ,Transaction,TransactionItem} from '../../accounting-manager.model';
import { JournalTransactionService } from '../../services/journal-transaction.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-index-journal-transaction',
  templateUrl: './index-journal-transaction.component.html',
  styleUrls: ['./index-journal-transaction.component.scss']
})
export class IndexJournalTransactionComponent implements OnInit {
  url:string; 
  transactions: any;
  today: Date = new Date();
  pipe = new DatePipe('en-US');
  todayWithPipe = null;

  constructor(private notificationService: NotificationService,
    private modelService:JournalTransactionService,
    private sortService: SortService,
    private searchService: SearchService,
    private repositoryService: RepositoryService,
    private router: Router,
    private pagination: PaginationComponent) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath  + "?";
    
    //this.LoadData();
  }
  LoadData(){
    this.modelService.GetAll()
    .then((res) => {
      this.transactions = res.data;
      this.searchService.searchData = res.data;
    });
  }
  formatDate(input:string){
    return this.pipe.transform(input, 'yyyy/MM/dd');
  }
  edit(obj:Transaction) {
    
    this.router.navigate(['/accounting-manager/journal-transactions/edit/' + obj.id]);
  }

  newData(data:any) {
    this.transactions = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.transactions = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.transactions = data
  }

  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.modelService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }

}
