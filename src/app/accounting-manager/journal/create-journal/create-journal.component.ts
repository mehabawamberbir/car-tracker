import { Component, OnInit } from '@angular/core';
import { Account } from '../../accounting-manager.model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JournalService } from '../../services/journal-service';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { AccountService } from '../../services/account.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';

@Component({
  selector: 'app-create-journal',
  templateUrl: './create-journal.component.html',
  styleUrls: ['./create-journal.component.scss']
})
export class CreateJournalComponent implements OnInit {
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  accounts: Account[];
  types: Lookup[];
  
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: JournalService,
    private lookupService:LookupService,
    private accountService :AccountService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
    id:[''],
    journal_type_id:['', [Validators.required]],
    account_id:[''],
    code:['',[Validators.required]],
    name:['', [Validators.required]],
    description:[''],
    status: 1
    });

    this.accountService.GetAll().then((res)=>
    {
      this.accounts = res.data;
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.JOURNAL_TYPE).then((res)=>
    {
      this.types = res.data;
    });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  ClearForm(){
    this.model = this.fb.group({
      journal_type_id:['', [Validators.required]],
      account_id:[''],
      code:['',[Validators.required]],
      name:['', [Validators.required]],
      description:[''],
      status: 1,
    });
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
}
