import { Component, OnInit } from '@angular/core';
import { Account } from '../../accounting-manager.model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JournalService } from '../../services/journal-service';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { AccountService } from '../../services/account.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';
import { ActivatedRoute, Route } from '@angular/router';

@Component({
  selector: 'app-edit-journal',
  templateUrl: './edit-journal.component.html',
  styleUrls: ['./edit-journal.component.scss']
})
export class EditJournalComponent implements OnInit {
  id:string;
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  accounts: Account[];
  types: Lookup[];
  
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: JournalService,
    private lookupService:LookupService,
    private accountService :AccountService,
    private route:ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false;
    
    this.id = this.route.snapshot.paramMap.get("id") !;

    this.model = this.fb.group({
    id:[''],
    journal_type_id:['', [Validators.required]],
    account_id:[''],
    code:['',[Validators.required]],
    name:['', [Validators.required]],
    description:[''],
    status: 1
    });

    this.accountService.GetAll().then((res)=>
    {
      this.accounts = res.data;
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.JOURNAL_TYPE).then((res)=>
    {
      this.types = res.data;
    });

    this.populateData(this.id);
  }

  
  
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        id : this.id,
        name:  data.name,
        code:  data.code,
        description : data.description,
        journal_type_id : data.journal_type_id,
        account_id : data.account_id,
        status : data.status,
      });
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  

  update() {
    var isFormValid = true;
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
}
