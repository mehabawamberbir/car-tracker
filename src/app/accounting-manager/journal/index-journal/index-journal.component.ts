import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/shared/services/search.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SortService } from 'src/app/shared/services/sort.service';
import { PaginationComponent } from 'src/app/shared/templates/pagination/pagination.component';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { CONSTANT_CONFIG } from 'src/app/config/constants';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Account, Journal } from '../../accounting-manager.model';
import { JournalService } from '../../services/journal-service';
import { AccountService } from '../../services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index-journal',
  templateUrl: './index-journal.component.html',
  styleUrls: ['./index-journal.component.scss']
})
export class IndexJournalComponent implements OnInit {
  types: Lookup[];
  accounts: Account[];
  url:string; 
  id:string;
  submitted: boolean = false;
  disabled: boolean = false;
  createMode: boolean = false;
  editMode: boolean = false;
  model: FormGroup;
  models:Journal[];

  constructor(
    private notificationService: NotificationService,
    private lookupService: LookupService,
    private modelService:JournalService,
    private accountService : AccountService,
    private sortService: SortService,
    private searchService: SearchService,
    private repositoryService: RepositoryService,
    private fb: FormBuilder,
    private router: Router,
    private pagination: PaginationComponent) {

  }

  ngOnInit(): void {
    this.createMode = false;
    this.editMode = false;
    this.submitted = false;
    this.disabled = false;
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath  + "?";
    
    //  this.LoadData();
  }
  LoadData(){
    this.modelService.GetAll()
    .then((res) => {
      this.models = res.data;
      this.searchService.searchData = res.data;
    });
  }
  edit(obj:Journal) {
    
    this.router.navigate(['/accounting-manager/journals/edit/' + obj.id]);
  }

  closeModes() {
    this.createMode = this.editMode = this.submitted = false;
  }
  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    }
    else {
      this.submitted = true
    }
  }

  newData(data:any) {
    this.models = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.models = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.models = data
  }

  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.modelService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
}
