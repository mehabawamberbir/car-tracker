import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateReconcilationComponent } from './create-reconcilation.component';

describe('CreateReconcilationComponent', () => {
  let component: CreateReconcilationComponent;
  let fixture: ComponentFixture<CreateReconcilationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateReconcilationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateReconcilationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
