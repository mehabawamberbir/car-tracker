import { Component, OnInit } from '@angular/core';
import { Account } from '../../accounting-manager.model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReconcilationService } from '../../services/reconcilation.service';
import { AccountService } from '../../services/account.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-create-reconcilation',
  templateUrl: './create-reconcilation.component.html',
  styleUrls: ['./create-reconcilation.component.scss']
})
export class CreateReconcilationComponent implements OnInit {
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  accounts: Account[];
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: ReconcilationService,
    private accountService:AccountService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
    id:[''],
    statement_ref:['', [Validators.required]],
    account_id:['', [Validators.required]],
    beg_balance:[0, [Validators.required]],
    end_balance:[0, [Validators.required]],
    start_date:['', [Validators.required]],
    end_date:['', [Validators.required]],
    remarks:[''],
    status:1
    });
  this.accountService.GetAll().then((res)=> {
    this.accounts = res.data;
  });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  ClearForm(){
    this.model = this.fb.group({
      id:[''],
      statement_ref:['', [Validators.required]],
      account_id:['', [Validators.required]],
      beg_balance:[0, [Validators.required]],
      end_balance:[0, [Validators.required]],
      start_date:['', [Validators.required]],
      end_date:['', [Validators.required]],
      remarks:[''],
      status:1
      });
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.router.navigate(['../'], { relativeTo: this.route });
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }


}
