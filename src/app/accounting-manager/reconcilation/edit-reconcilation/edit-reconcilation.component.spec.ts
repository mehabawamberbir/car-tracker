import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditReconcilationComponent } from './edit-reconcilation.component';

describe('EditReconcilationComponent', () => {
  let component: EditReconcilationComponent;
  let fixture: ComponentFixture<EditReconcilationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditReconcilationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditReconcilationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
