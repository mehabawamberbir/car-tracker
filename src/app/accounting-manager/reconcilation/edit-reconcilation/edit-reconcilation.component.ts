import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Account } from '../../accounting-manager.model';
import { AccountService } from '../../services/account.service';
import { ReconcilationService } from '../../services/reconcilation.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-edit-reconcilation',
  templateUrl: './edit-reconcilation.component.html',
  styleUrls: ['./edit-reconcilation.component.scss']
})
export class EditReconcilationComponent implements OnInit {

  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;
  id: string;
  model: FormGroup;
  accounts: Account[];
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: ReconcilationService,
    private accountService:AccountService,
    private route:ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false

    this.id = this.route.snapshot.paramMap.get("id") !;
    this.model = this.fb.group({
    id:[''],
    statement_ref:['', [Validators.required]],
    account_id:['', [Validators.required]],
    beg_balance:['', [Validators.required]],
    end_balance:['', [Validators.required]],
    start_date:['', [Validators.required]],
    end_date:['', [Validators.required]],
    remarks:[''],
    status:1
    });
  this.accountService.GetAll().then((res)=> {
    this.accounts = res.data;
  });
  this.populateData(this.id);
  }
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        id:this.id,
        statement_ref:data.statement_ref,
        account_id:data.account_id,
        beg_balance:data.beg_balance,
        end_balance:data.end_balance,
        start_date:data.start_date,
        end_date:data.end_date,
        remarks:data.remarks,
        status:data.status,
      });
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  

  update() {
    var isFormValid = true;
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
        this.router.navigate(['../../'], { relativeTo: this.route });
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
}
