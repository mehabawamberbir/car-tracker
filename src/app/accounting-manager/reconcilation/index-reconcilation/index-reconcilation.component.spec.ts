import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexReconcilationComponent } from './index-reconcilation.component';

describe('IndexReconcilationComponent', () => {
  let component: IndexReconcilationComponent;
  let fixture: ComponentFixture<IndexReconcilationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexReconcilationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexReconcilationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
