import { Component, OnInit } from '@angular/core';
import { Reconcilation } from '../../accounting-manager.model';
import { ReconcilationService } from '../../services/reconcilation.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { SearchService } from 'src/app/shared/services/search.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-index-reconcilation',
  templateUrl: './index-reconcilation.component.html',
  styleUrls: ['./index-reconcilation.component.scss']
})
export class IndexReconcilationComponent implements OnInit {

  url:string; 
  reconcilations: any;
  constructor(private notificationService: NotificationService,
    private modelService:ReconcilationService,
    private sortService: SortService,
    private searchService: SearchService,
    private repositoryService: RepositoryService,
    private router: Router) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath  + "?";
    
    this.LoadData();
  }
  LoadData(){
    this.modelService.GetAll()
    .then((res) => {
      this.reconcilations = res.data;
      this.searchService.searchData = res.data;
    });
  }
  edit(obj:Reconcilation) {
    
    this.router.navigate(['/accounting-manager/reconciliations/edit/' + obj.id]);
  }

  newData(data:any) {
    this.reconcilations = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.reconcilations = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.reconcilations = data
  }

  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.modelService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }

}
