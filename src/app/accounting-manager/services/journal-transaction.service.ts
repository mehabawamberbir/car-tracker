import { Injectable } from '@angular/core';
import { RepositoryService } from 'src/app/shared/services/repository.service';

import  axios  from 'axios';
import { Transaction } from '../accounting-manager.model';
@Injectable({
  providedIn: 'root'
})
export class JournalTransactionService {

  model : Transaction;
  modelPath : string = "accounting-manager/transactions";
  constructor(private repository: RepositoryService) { 

  }    

  GetAll() {
      return axios.get(this.repository.apiUrl + this.modelPath);
  }
  Get(skip:number, limit:number) {
      return axios.get(this.repository.apiUrl + this.modelPath + `?skip=${skip} && limit=${limit}`);
  }
  GetById(id:string) {
      return this.repository.sendRequest("GET", this.repository.apiUrl + this.modelPath + '/' + id);
  }
  Create(model:Transaction) {
      return axios.post(this.repository.apiUrl + this.modelPath, model)
  }
  Edit(id:string, model:Transaction) {
      return axios.put(this.repository.apiUrl + this.modelPath + '/' + id, model)
  }
  Delete(id:string) {
      return axios.delete(this.repository.apiUrl + this.modelPath + '/' + id);
  }
  ToggleStatus(id:string) {
      return axios.patch(this.repository.apiUrl + this.modelPath + '/' + id + '/toggle-status');
  }
}
