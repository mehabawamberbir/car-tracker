import { TestBed } from '@angular/core/testing';

import { TransactionItemsService } from './transaction-items.service';

describe('TransactionItemsService', () => {
  let service: TransactionItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransactionItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
