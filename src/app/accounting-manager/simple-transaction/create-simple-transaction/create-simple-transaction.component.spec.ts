import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSimpleTransactionComponent } from './create-simple-transaction.component';

describe('CreateSimpleTransactionComponent', () => {
  let component: CreateSimpleTransactionComponent;
  let fixture: ComponentFixture<CreateSimpleTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSimpleTransactionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateSimpleTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
