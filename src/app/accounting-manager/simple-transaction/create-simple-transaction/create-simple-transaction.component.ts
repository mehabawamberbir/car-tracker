import { Component, OnInit } from '@angular/core';
import { Account, Journal, TransactionItem } from '../../accounting-manager.model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormArray, FormBuilder,FormGroup,Validators } from '@angular/forms';
import { JournalTransactionService } from '../../services/journal-transaction.service';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { AccountService } from '../../services/account.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';
import { NbCardModule } from '@nebular/theme';
import { SaveButtonComponent } from 'src/app/shared/templates/save-button.component';
import { JournalService } from '../../services/journal-service';
@Component({
  selector: 'app-create-journal-transaction',
  templateUrl: './create-simple-transaction.component.html',
  styleUrls: ['./create-simple-transaction.component.scss']
})
export class CreateSimpleTransactionComponent implements OnInit {

  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  currency_types: Lookup[];
  types: Lookup[];
  accounts: Account[];
  // journals: Journal[];
  journals: Lookup[];
  transactions: TransactionItem[];
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: JournalTransactionService,
    private lookupService:LookupService,
    private accountService:AccountService,
    private journalService:JournalService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
    id:[''],
    //transaction_type_id:['', [Validators.required]],
    transaction_date:[''],
    account_id:['', [Validators.required]],
    currency_id:['', [Validators.required]],
    journal_id:['', [Validators.required]],
    amount:0,
    reference_no:[''],
    description:[''],
    created_date:[''],
    created_by:[''],
    status:1,
    transaction_items:this.fb.array([
      
      ])
      });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CURRENCY).then((res)=>
    {
      this.currency_types = res.data;
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.TRANSACTION_TYPE).then((res)=>
    {
      this.types = res.data;
    });
  this.accountService.GetAll().then((res)=> {
    this.accounts = res.data;
  });
  // this.journalService.GetAll().then((res)=> {
  //   this.journals = res.data;
  // });
  this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.JOURNAL_TYPE).then((res)=> {
    this.journals = res.data;
  });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  // gettransactionItems() {
	// 	return this.model.get('transactions') as FormArray;
	// }
  // addTransactionItem(): FormGroup {
	// 	return this.fb.group({
  //     id:[''],
  //     transaction_id:[''],
  //     account_id:[''],
  //     debit:0,
  //     credit:0,
  //     description:[''],
  //     status:1 
	// 	});
	// }
  // addButtonClick(): void {
  //   (<FormArray>this.model.get('transactions')).push(this.addTransactionItem);
  // }
  // deleteTransactionItem(index: number) {
	// 	(<FormArray>this.model.get('transactions')).removeAt(index);
	// }
  ClearForm(){
    this.model = this.fb.group({
    //transaction_type_id:['', [Validators.required]],
    currency_id:['', [Validators.required]],
    transaction_date:[''],
    journal_id:['', [Validators.required]],
    account_id:['', [Validators.required]],
    amount:0,
    description:[''],
    reference_no:[''],
    created_date:[''],
    created_by:[''],
    status:1
    });
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }

}
