import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes, CanActivate } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IndexComponent } from './index/index.component';
import { ReportManagerModule } from './report-manager/report-manager.module';
import { EmployeeManagerModule } from './employee-manager/employee-manager.module';
import { LoginComponent } from './login/login.component';

import { AuthGuard } from './_helpers/auth.guard';
import { AccountingManagerModule } from './accounting-manager/accounting-manager.module';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: '', component: IndexComponent
    //  , canActivate: [AuthGuard]
  },
  {
    path: 'home', component: IndexComponent
    //  , canActivate: [AuthGuard]
  },
  {
    path: 'dashboard', component: DashboardComponent
    //  , canActivate: [AuthGuard]
  },
  {
    path: 'master',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./master-data/master-data.module').then(m => m.MasterDataModule),
    // data: {
    //   expectedRole: 'admin'
    // }
  },
  {
    path: 'contact-manager',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./contact-manager/contact-manager.module').then(m => m.ContactManagerModule),
  },
  {
    path: 'operation-manager',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./operation-manager/operation-manager.module').then(m => m.OperationManagerModule)
  },
  {
    path: 'invoice-manager',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./invoice-manager/invoice-manager.module').then(m => m.InvoiceManagerModule)
  },
  {
    path: 'product-manager',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./product-manager/product-manager.module').then(m => m.ProductManagerModule)
  },
  {
    path: 'report-manager',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./report-manager/report-manager.module').then(m => m.ReportManagerModule)
  },
  {
    path:'accounting-manager',
    loadChildren:() => import('./accounting-manager/accounting-manager.module').then(m=>AccountingManagerModule)
  },
  {
    path: 'employee-manager',
    canActivate: [AuthGuard],
    loadChildren: () => import('./employee-manager/employee-manager.module').then(m => m.EmployeeManagerModule)
  },
  {
    path: 'payroll-manager',
    canActivate: [AuthGuard],
    loadChildren: () => import('./employee-manager/employee-manager.module').then(m => m.EmployeeManagerModule)
  },
  {
    path: 'purchase-manager',
    loadChildren: () => import('./purchase-manager/purchase-manager.module').then(m=>m.PurchaseManagerModule)
  },
  { path: '**', redirectTo: 'login' }
];

const config: ExtraOptions = {
  useHash: false,
  onSameUrlNavigation :'reload',
  enableTracing: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
