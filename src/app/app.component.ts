import { Component, OnInit } from '@angular/core';
import { Platform } from '@angular/cdk/platform';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { filter, map } from 'rxjs/operators';

import { StorageService } from './_services/storage.service';
import { AuthService } from './_services/auth.service';
import { User } from './user-manager/model/user';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { MENU_ITEMS } from './menu-items';

@Component({
  selector: 'app-root',
  template: `
  <nb-layout>
    <nb-layout-header *ngIf="showHeader" fixed>  
      <div class="col-md-9">
      <div class="header-container">
        <div class="logo-container">
          <a class="logo" routerLink=''>
            <i class="fa fa-subway logo-icon" style="font-size: 18px; color:#bce3ba;font-weight:bolder;"> 
              <span class="app-title"> &nbsp; My Tracker </span>  
            </i>
          </a>          
          <a (click)="toggleSidebar()" href="#" class="sidebar-toggle" style="margin-left:20px">
            <nb-icon icon="menu-outline" status='control'></nb-icon>
          <!-- <nb-icon class="d-icon white-icon" icon='more-vertical-outline'></nb-icon> -->
          </a>
        </div>
      </div>

      </div>
      <div class="col-md-3" style="margin-bottom: -30px;">                   
        <div class="logout">
          <span style="font-size:11px;color:#b3cbb3;padding-right:9px;">{{username}} </span>
          <a href="#" style="font-size:15px;color:#b3cbb3;font-weight:bold;" (click)='logout()'>
                Logout <nb-icon style="font-size:13px;color:white;" icon='log-out-outline'>  </nb-icon> 
          </a> 
        </div>
      </div>
    </nb-layout-header>    
    

    <nb-sidebar responsive *ngIf="showHeader" class="menu-sidebar" tag="menu-sidebar"  class="sidebar">
      <nb-menu [items]="menuItems" tag="sidebar"></nb-menu>
    </nb-sidebar>
    <nb-layout-column class="colored-column-basic">        
      <router-outlet></router-outlet>
    </nb-layout-column>
    <nb-layout-footer fixed style="background-color:#377db9;font-size:10px;" class="footer-custom"> 
    <span style="font-size: 13px;">
    © 2022 tracker-system. All Rights Reserved.
    </span>
  </nb-layout-footer>
</nb-layout>
`,
styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  items = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  menuItems = MENU_ITEMS;
  userContextMenus = [
    {
        title: 'Profile',
        link: '/account/profile',
        icon: 'person-outline'
    },
    {
        title: 'Change Password',
        link: '/account/change-password',
        icon: 'lock-outline'
    },
    {
        title: 'Logout',
        link: '/account/logout',
        icon: 'unlock-outline'
    }
];

  title = 'tracker-system';
  isOnline: boolean;
  modalVersion: boolean;
  modalPwaEvent: any;
  modalPwaPlatform: string|undefined;

  private roles: string[] = [];
  isLoggedIn = false;
  showHeader = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username: string;
  
  constructor(private storageService: StorageService, 
    private authService: AuthService,
    private platform: Platform,
    private swUpdate: SwUpdate,
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService) {
    this.isOnline = false;
    this.showHeader = false;
    this.modalVersion = false;
    menuService.onItemClick().subscribe((e) => {
      if(e.tag == 'sidebar')
        if(this.platform.ANDROID || this.platform.IOS)
          this.toggleSidebar();      
    });
}

  public ngOnInit(): void {
    this.updateOnlineStatus();

    this.getCurrentUser();

    window.addEventListener('online',  this.updateOnlineStatus.bind(this));
    window.addEventListener('offline', this.updateOnlineStatus.bind(this));

    if (this.swUpdate.isEnabled) {
      this.swUpdate.versionUpdates.pipe(
        filter((evt: any): evt is VersionReadyEvent => evt.type === 'VERSION_READY'),
        map((evt: any) => {
          console.info(`currentVersion=[${evt.currentVersion} | latestVersion=[${evt.latestVersion}]`);
          this.modalVersion = true;
        }),
      );
    }
    this.loadModalPwa();
    //
    //
    this.isLoggedIn = this.authService.isAuthenticated();
    console.log("IsLogged In Root:" + this.isLoggedIn);
    if (this.isLoggedIn) {
      //const user = this.storageService.getUserKey();
      //this.roles = user.roles;
      // this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      // this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      //this.username = user.user_name;
      this.showHeader = true;
    }
  }
  
  logout(): void {
    this.authService.logout().subscribe({
      next: res => {
        console.log(res);
        this.storageService.clean();
      },
      error: err => {
        console.log(err);
      }
    });
    
    window.location.reload();
  }

  private updateOnlineStatus(): void {
    this.isOnline = window.navigator.onLine;
    console.info(`isOnline=[${this.isOnline}]`);
  }

  public updateVersion(): void {
    this.modalVersion = false;
    window.location.reload();
  }

  public closeVersion(): void {
    this.modalVersion = false;
  }
  private loadModalPwa(): void {
    if (this.platform.ANDROID) {
      window.addEventListener('beforeinstallprompt', (event: any) => {
        event.preventDefault();
        this.modalPwaEvent = event;
        this.modalPwaPlatform = 'ANDROID';
      });
    }

    if (this.platform.IOS && this.platform.SAFARI) {
      const isInStandaloneMode = ('standalone' in window.navigator) && ((<any>window.navigator)['standalone']);
      if (!isInStandaloneMode) {
        this.modalPwaPlatform = 'IOS';
      }
    }
    if(this.platform.isBrowser){
      this.modalPwaPlatform = 'ANDROID';
    }
  }
  public addToHomeScreen(): void {
    this.modalPwaEvent.prompt();
    this.modalPwaPlatform = undefined;
  }

  public closePwa(): void {
    this.modalPwaPlatform = undefined;
  }

  public getCurrentUser(){
    var item = this.storageService.getCurrUser(); 
    this.username = JSON.parse(item || '{}');   
    var roles = this.storageService.getCurrUserRoles();
    return item;
    //if(item  == '{}') return '';
    // var roles = JSON.parse(item || '{}');
    // console.log("roles:" + roles);
    // const currentUser = this.authService.currentUserValue;
    // this.username = currentUser.email;
  }

  
  menuClicked(){
    if(this.platform.ANDROID || this.platform.IOS){      
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
    }
  }
  toggleSidebar() {
    this.sidebarService.toggle(true, 'menu-sidebar');
    //this.layoutService.changeLayoutSize();
      return false;
    }

}