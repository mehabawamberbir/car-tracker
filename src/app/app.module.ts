import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DEFAULT_THEME, NbActionsModule, NbButtonModule, NbCardModule, NbContextMenuModule, NbDialogModule, NbIconModule, NbLayoutModule,NbSidebarModule, NbMenuModule, NbSearchModule, NbSelectModule, NbThemeModule, NbToastrModule, NbUserModule, NbWindowModule } from '@nebular/theme';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { IndexComponent } from './index/index.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from './core/core.module';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NbEvaIconsModule } from '@nebular/eva-icons';

import { LoginComponent } from './login/login.component';
//import { httpInterceptorProviders } from 'src/_helpers/http.interceptor';
import { FormsModule } from '@angular/forms';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { CONSTANT_CONFIG } from './config/constants';
import { AuthGuard } from './_helpers/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';

export function tokenGetter() {
  return localStorage.getItem(CONSTANT_CONFIG.API_TOKEN);
}

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoginComponent,
    DashboardComponent,
  ], 
  imports: [
    HttpClientModule,
    CoreModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NbSidebarModule.forRoot(),
    NbContextMenuModule,
    NbSelectModule,
    NbIconModule,
    NbMenuModule.forRoot(),
    NbThemeModule.forRoot({name: 'default'},[ DEFAULT_THEME],),  
    NbLayoutModule,
    NbActionsModule,
    NbSearchModule,
    NbUserModule,
    NbDialogModule.forRoot(),
    NbToastrModule.forRoot(),
    NbCardModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbMenuModule.forRoot(),
    NbDialogModule.forChild(),
    NbWindowModule.forChild(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        // allowedDomains: [],
        // disallowedRoutes: ["http://example.com/examplebadroute/"],
      },
    }),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: httpTranslateLoader,
          deps: [HttpClient]
      }
    }),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    NoopAnimationsModule,
  ],
  exports:[],  
  providers: [
    //httpInterceptorProviders,
    AuthGuard,
    JwtHelperService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}