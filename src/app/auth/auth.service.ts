import { Injectable } from '@angular/core';
import { JwtHelperService, JWT_OPTIONS  } from '@auth0/angular-jwt';
import { CONSTANT_CONFIG } from '../config/constants';


@Injectable()
export class AuthService {
  private jwtHelper = new JwtHelperService();
  constructor() {

  }
  // ...
  public isAuthenticated(): boolean {
    const token = localStorage.getItem(CONSTANT_CONFIG.API_TOKEN) || '{}';
    // Check whether the token is expired and return
    // true or false
    return !this.jwtHelper.isTokenExpired(token);
  }
}