import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommnManagerRoutingModule } from './commn-manager-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CommnManagerRoutingModule
  ]
})
export class CommnManagerModule { }
