import { environment } from "src/environments/environment"

export  default {
    appName: "tracker-system",
    appTitle: "tracker-system",
    dateFormat: "yyyy-MM-dd",
}
export const CONSTANT_CONFIG = {
    SAMPLE: "SAMPLE",
    // API_URL : "http://localhost:3000/",
    // API_URL : "http://localhost:8000/api/",
    // AUTH_API : "https://tracker.senufze.com/api/",
    // AUTH_API : environment.production ? "https://tracker.senufze.com/api/" : "http://localhost:8000/api/",
    // API_URL : environment.production ? "https://tracker.senufze.com/api/" : "http://localhost:8000/api/",
    API_URL : "https://tracker.senufze.com/api/",
    AUTH_API : "https://tracker.senufze.com/api/",
    API_TOKEN : 'access_token',
    CURR_USER : 'curr_user',
    ROLES : 'user_roles',
    //z
    // Lookup types
    ACTOR_AGENT: "AGENT",
    ACTOR_CONSIGNEE: "CONSIGNEE",
    ACTOR_TRANSPORTER: "TRANSPORTER",
    ACTOR_FF: "FF",
    ACTOR_LOADING: "LOADING",
    DATE_FORMAT : "yyyy-MM-dd"
    //
    //
}
