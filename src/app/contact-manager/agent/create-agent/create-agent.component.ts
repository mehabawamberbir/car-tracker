import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgentService } from 'src/app/contact-manager/services/agent.service';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-create-agent',
  templateUrl: './create-agent.component.html',
  styleUrls: ['./create-agent.component.scss']
})
export class CreateAgentComponent implements OnInit {
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: AgentService,
    ) { }

  ngOnInit(): void {
    this.added = false;
    this.error = false;
    this.submitted = false;
    this.model = this.fb.group({
      name: ['', [Validators.required]],
      code: ['', [Validators.required]],
      id : [''],
      telephone : [''],
      email : [''],
      address : [''],
      address2 : [''],
      bp : [''],
      city : [''],
      country : ['Ethiopia'],
      terms : [''],
      start_date : [''],
      end_date : [''],
      usd_rate : [1],
      status : 1,
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  ClearForm(){
    this.model = this.fb.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      status: 1,
    });
  }

  create() {
    var isFormValid = true;//this.model.valid;
    // validation not completed
    if (isFormValid) {
      this.modelService.Create(this.model.value)
      
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }

}