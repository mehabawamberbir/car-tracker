import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexAgentComponent } from './index-agent.component';

describe('IndexAgentComponent', () => {
  let component: IndexAgentComponent;
  let fixture: ComponentFixture<IndexAgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexAgentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
