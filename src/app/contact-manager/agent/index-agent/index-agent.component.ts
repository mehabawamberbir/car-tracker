import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { AgentService } from 'src/app/contact-manager/services/agent.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Agent } from '../../contact-models';

@Component({
  selector: 'app-index-agent',
  templateUrl: './index-agent.component.html',
  styleUrls: ['./index-agent.component.scss']
})
export class IndexAgentComponent implements OnInit {
  models: any;
  url:string;
  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: AgentService
  ) { }

  ngOnInit(): void {
    this.LoadData();
  }
  
  LoadData(){
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath + '?';
    this.modelService.GetAll()
    .then((res) => {
      this.models = res.data;
      this.searchService.searchData = res.data;
    })
  }

  newData(data:any) {
    this.models = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.models = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.models = data
  }

  edit(obj:Agent) {
    
    this.router.navigate(['/contact-manager/agents/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.modelService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
}