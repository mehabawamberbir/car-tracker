import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ConsigneeService } from 'src/app/contact-manager/services/consignee.service';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-edit-consignee',
  templateUrl: './edit-consignee.component.html',
  styleUrls: ['./edit-consignee.component.scss']
})
export class EditConsigneeComponent implements OnInit {
  id : string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: ConsigneeService,
    ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id") !;

    this.added = false;
    this.error = false;
    this.submitted = false;
    this.model = this.fb.group({
      name: ['', [Validators.required]],
      code: ['', [Validators.required]],
      id : [''],
      telephone : [''],
      email : [''],
      address : [''],
      address2 : [''],
      bp : [''],
      city : [''],
      country : ['Ethiopia'],
      terms : [''],
      usd_rate : [1],
      status : 1,
    });

    this.populateData(this.id);
  }
  
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        id : this.id,
        name:  data.name,
        code:  data.code,
        telephone : data.telephone,
        email : data.email,
        address : data.address,
        address2 : data.address2,
        bp : data.bp,
        city : data.city,
        country : data.country,
        terms : data.terms,
        usd_rate : data.usd_rate,
        status : data.status,
      });
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  ClearForm(){
    this.model = this.fb.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      status: 1,
    });
  }


  update() {
    var isFormValid = true;//this.model.valid;
    // validation not completed
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }

}