import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexConsigneeComponent } from './index-consignee.component';

describe('IndexConsigneeComponent', () => {
  let component: IndexConsigneeComponent;
  let fixture: ComponentFixture<IndexConsigneeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexConsigneeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexConsigneeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
