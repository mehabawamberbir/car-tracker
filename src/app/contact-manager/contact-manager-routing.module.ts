import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateAgentComponent } from './agent/create-agent/create-agent.component';
import { EditAgentComponent } from './agent/edit-agent/edit-agent.component';
import { IndexAgentComponent } from './agent/index-agent/index-agent.component';
import { CreateConsigneeComponent } from './consignee/create-consignee/create-consignee.component';
import { EditConsigneeComponent } from './consignee/edit-consignee/edit-consignee.component';
import { IndexConsigneeComponent } from './consignee/index-consignee/index-consignee.component';
import { CreateCustomerComponent } from './customer/create-customer/create-customer.component';
import { EditCustomerComponent } from './customer/edit-customer/edit-customer.component';
import { IndexCustomerComponent } from './customer/index-customer/index-customer.component';
import { IndexContactManagerComponent } from './index-contact-manager/index-contact-manager.component';
import { CreateVendorComponent } from './vendor/create-vendor/create-vendor.component';
import { EditVendorComponent } from './vendor/edit-vendor/edit-vendor.component';
import { IndexVendorComponent } from './vendor/index-vendor/index-vendor.component';


const routes: Routes = [
  {
    path: '', component : IndexContactManagerComponent,  
  },
  { path: 'agents', component: IndexAgentComponent },
  { path: 'agents/create', component: CreateAgentComponent },
  { path: 'agents/edit/:id', component: EditAgentComponent },
  { path: 'consignees', component: IndexConsigneeComponent },
  { path: 'consignees/create', component: CreateConsigneeComponent },
  { path: 'consignees/edit/:id', component: EditConsigneeComponent },
  { path: 'freight-forwarders', component: IndexCustomerComponent },
  { path: 'customers', component: IndexCustomerComponent },
  { path: 'customers/create', component: CreateCustomerComponent },
  { path: 'customers/edit/:id', component: EditCustomerComponent },
  { path: 'vendors', component: IndexVendorComponent },
  { path: 'vendors/create', component: CreateVendorComponent },
  { path: 'vendors/edit/:id', component: EditVendorComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactManagerRoutingModule { }
