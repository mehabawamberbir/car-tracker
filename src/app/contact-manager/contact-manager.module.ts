import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactManagerRoutingModule } from './contact-manager-routing.module';
import { IndexContactManagerComponent } from './index-contact-manager/index-contact-manager.component';
import { IndexVendorComponent } from './vendor/index-vendor/index-vendor.component';
import { CreateVendorComponent } from './vendor/create-vendor/create-vendor.component';
import { EditVendorComponent } from './vendor/edit-vendor/edit-vendor.component';
import { IndexCustomerComponent } from './customer/index-customer/index-customer.component';
import { CreateCustomerComponent } from './customer/create-customer/create-customer.component';
import { EditCustomerComponent } from './customer/edit-customer/edit-customer.component';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
import { NbCardModule, NbIconModule, NbMenuModule, NbSelectModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';
import { IndexConsigneeComponent } from './consignee/index-consignee/index-consignee.component';
import { CreateConsigneeComponent } from './consignee/create-consignee/create-consignee.component';
import { EditConsigneeComponent } from './consignee/edit-consignee/edit-consignee.component';
import { IndexAgentComponent } from './agent/index-agent/index-agent.component';
import { CreateAgentComponent } from './agent/create-agent/create-agent.component';
import { EditAgentComponent } from './agent/edit-agent/edit-agent.component';


@NgModule({
  declarations: [
    IndexContactManagerComponent,
    IndexVendorComponent,
    CreateVendorComponent,
    EditVendorComponent,
    IndexCustomerComponent,
    CreateCustomerComponent,
    EditCustomerComponent,
    IndexConsigneeComponent,
    CreateConsigneeComponent,
    EditConsigneeComponent,
    IndexAgentComponent,
    CreateAgentComponent,
    EditAgentComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,    
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    ReactiveFormsModule,
    NbMenuModule,
    ContactManagerRoutingModule
  ]
})
export class ContactManagerModule { }
