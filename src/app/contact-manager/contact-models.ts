export class Customer{
    id: string;
    name: string;
    code: string;
    telephone: string;
    email: string;
    address: string;
    address2: string;
    bp: string;
    city: string;
    country: string;
    account_id: string;
    terms: string;
    usd_rate: number;
    group_id: string;
    status: boolean;
}

export class Vendor{
    id: string;
    name: string;
    code: string;
    telephone: string;
    email: string;
    address: string;
    address2: string;
    bp: string;
    city: string;
    country: string;
    balance: number;
    account_id: string;
    terms: string;
    usd_rate: number;
    group_id: string;
    status: boolean;
}

export class Actor{
    id: string;
    code: string;
    name: string;
    telephone: string;
    email: string;
    address: string;
    actor_type: string;
    status: boolean;
}

export class Agent{
    id: string;
    name : string;
    code : string;
    telephone : string;
    email : string;
    address : string;
    address2 : string;
    bp : string;
    city : string;
    country : string;
    terms : string;
    start_date : Date;
    end_date : Date;
    usd_rate : number;
    status: boolean;
}

export class Consignee{
    id: string;
    name : string;
    code : string;
    telephone : string;
    email : string;
    address : string;
    address2 : string;
    bp : string;
    city : string;
    country : string;
    terms : string;
    usd_rate : number;
    status: boolean;
}