import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from 'src/app/contact-manager/services/customer.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.scss']
})
export class CreateCustomerComponent implements OnInit {
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  groups : Lookup[];
  accountTypes : Lookup[];

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: CustomerService,
    private lookupService: LookupService,
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
      name: ['', [Validators.required]],
      code: ['', [Validators.required]],
      id : [''],
      telephone : [''],
      email : [''],
      address : [''],
      address2 : [''],
      bp : [''],
      city : [''],
      country : ['Ethiopia'],
      account_id : [null],
      terms : [''],
      usd_rate : [1],
      group_id : [null],
      status : 1,
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CUSTOMER_GROUP)
    .then((res:any) => 
    {
      this.groups = res.data;
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.ACCOUNT_TYPE)
    .then((res:any) => 
    {
      this.accountTypes = res.data;
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  ClearForm(){
    this.model = this.fb.group({
      group_id: ['', Validators.required],
      name: ['', Validators.required],
      code: ['', Validators.required],
      status: 1,
    });
  }

  create() {
    var isFormValid = true;//this.model.valid;
    // validation not completed
    if (isFormValid) {
      this.modelService.Create(this.model.value)
      
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }

}