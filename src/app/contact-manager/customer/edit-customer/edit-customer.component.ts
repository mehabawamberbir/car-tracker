import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from 'src/app/contact-manager/services/customer.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.scss']
})
export class EditCustomerComponent implements OnInit {
  id : string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  groups : Lookup[];
  accountTypes : Lookup[];

  constructor(    
    private router: Router,
    private fb: FormBuilder,  
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: CustomerService,
    private lookupService: LookupService,
    ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id") !;
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
      name: ['', [Validators.required]],
      code: ['', [Validators.required]],
      id : [''],
      telephone : [''],
      email : [''],
      address : [''],
      address2 : [''],
      bp : [''],
      city : [''],
      country : ['Ethiopia'],
      account_id : [null],
      terms : [''],
      usd_rate : [1],
      group_id : [null],
      status : 1,
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CUSTOMER_GROUP)
    .then((res:any) => 
    {
      this.groups = res.data;
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.ACCOUNT_TYPE)
    .then((res:any) => 
    {
      this.accountTypes = res.data;
    });

    this.populateData(this.id);
  }
  
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        id : this.id,
        name:  data.name,
        code:  data.code,
        telephone : data.telephone,
        email : data.email,
        address : data.address,
        address2 : data.address2,
        bp : data.bp,
        city : data.city,
        country : data.country,
        account_id : data.account_id,
        terms : data.terms,
        usd_rate : data.usd_rate,
        group_id : data.group_id,
        status : data.status,
      });
    });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  update() {
    var isFormValid = true;//this.model.valid;
    // validation not completed
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }

}