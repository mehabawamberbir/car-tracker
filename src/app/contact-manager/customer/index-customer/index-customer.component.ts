import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { CustomerService } from 'src/app/contact-manager/services/customer.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Customer } from '../../contact-models';

@Component({
  selector: 'app-index-customer',
  templateUrl: './index-customer.component.html',
  styleUrls: ['./index-customer.component.scss']
})
export class IndexCustomerComponent implements OnInit {
  customers: any;
  url:string;
  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: CustomerService
  ) { }

  ngOnInit(): void {
    //this.url = this.actorService.pageUrl;
    this.LoadData();
  }
  
  LoadData(){
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath + '?';
    this.modelService.GetAll()
    .then((res) => {
      this.customers = res.data;
      this.searchService.searchData = res.data;
    })
  }

  newData(data:any) {
    this.customers = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.customers = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.customers = data
  }

  edit(obj:Customer) {
    
    this.router.navigate(['/contact-manager/customers/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.modelService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
}