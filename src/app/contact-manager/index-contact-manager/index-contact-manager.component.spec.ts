import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexContactManagerComponent } from './index-contact-manager.component';

describe('IndexContactManagerComponent', () => {
  let component: IndexContactManagerComponent;
  let fixture: ComponentFixture<IndexContactManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexContactManagerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexContactManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
