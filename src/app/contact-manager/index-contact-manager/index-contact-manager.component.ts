import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { CONSTANT_CONFIG } from 'src/app/config/constants';

@Component({
  selector: 'app-index-contact-manager',
  templateUrl: './index-contact-manager.component.html',
  styleUrls: ['./index-contact-manager.component.scss']
})
export class IndexContactManagerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items: NbMenuItem[] = [
    {
      title: "Freight Forwarders",
      link: '/contact-manager/freight-forwarders',
      icon: 'checkmark'
    },
    {
      title: "Consignees",
      link: '/contact-manager/consignees',
      icon: 'checkmark'
    },
    {
      title: "Agents",
      link: '/contact-manager/agents',
      icon: 'checkmark'
    },
    {
      title: "Customers",
      link: '/contact-manager/customers',
      icon: 'checkmark'
    },
    {
      title: "Vendors",
      link: '/contact-manager/vendors',
      icon: 'checkmark'
    }
   ];
}
