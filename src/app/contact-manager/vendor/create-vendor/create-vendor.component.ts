import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VendorService } from '../../services/vendor.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { AccountService } from 'src/app/accounting-manager/services/account.service';
import { Account } from 'src/app/accounting-manager/accounting-manager.model';

@Component({
  selector: 'app-create-vendor',
  templateUrl: './create-vendor.component.html',
  styleUrls: ['./create-vendor.component.scss']
})
export class CreateVendorComponent implements OnInit {
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  groups : Lookup[];
  accounts : Account[];
  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: VendorService,
    private lookupService: LookupService,
    private accountService:AccountService
  ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
      name: ['', [Validators.required]],
      code: ['', [Validators.required]],
      id : [''],
      telephone : [''],
      email : [''],
      address : [''],
      address2 : [''],
      bp : [''],
      city : [''],
      country : [''],
      balance : 0,
      account_id : [''],
      terms : [''],
      usd_rate : 1,
      group_id : [''],
      status : [''],
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.VENDOR_GROUP)
    .then((res:any) => 
    {
      this.groups = res.data;
    });

    this.accountService.GetAll()
    .then((res:any) => 
    {
      this.accounts = res.data;
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  ClearForm(){
    this.model = this.fb.group({
      group_id: ['', Validators.required],
      name: ['', Validators.required],
      code: ['', Validators.required],
      //status: 1,
    });
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }

}
