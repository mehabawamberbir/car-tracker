import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VendorService } from '../../services/vendor.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { ActivatedRoute } from '@angular/router';
import { Account } from 'src/app/accounting-manager/accounting-manager.model';
import { AccountService } from 'src/app/accounting-manager/services/account.service';
@Component({
  selector: 'app-edit-vendor',
  templateUrl: './edit-vendor.component.html',
  styleUrls: ['./edit-vendor.component.scss']
})
export class EditVendorComponent implements OnInit {
  id:string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  groups : Lookup[];
  accounts : Account[];
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: VendorService,
    private lookupService: LookupService,
    private accountService:AccountService
  ) { }

  ngOnInit(): void {
    
    this.id = this.route.snapshot.paramMap.get("id") !;
    this.added = false;
    this.error = false;
    this.submitted = false;

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.VENDOR_GROUP)
    .then((res:any) => 
    {
      this.groups = res.data;
    });    

    this.accountService.GetAll()
    .then((res:any) => 
    {
      this.accounts = res.data;
    });

    this.populateData(this.id);
  }
  
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        id : this.id,
        name:  data.name,
        code:  data.code,
        telephone : data.telephone,
        email : data.email,
        address : data.address,
        address2 : data.address2,
        bp : data.bp,
        city : data.city,
        country : data.country,
        account_id : data.account_id,
        terms : data.terms,
        usd_rate : data.usd_rate,
        balance : data.balance,
        group_id : data.group_id,
        status : data.status,
      });
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }  

  update() {
    var isFormValid = true;//this.model.valid;
    // validation not completed
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }

}
