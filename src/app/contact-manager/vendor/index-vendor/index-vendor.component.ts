import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { VendorService } from 'src/app/contact-manager/services/vendor.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Vendor } from '../../contact-models';

@Component({
  selector: 'app-index-vendor',
  templateUrl: './index-vendor.component.html',
  styleUrls: ['./index-vendor.component.scss']
})
export class IndexVendorComponent implements OnInit {
  vendors: any;
  url:string;
  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private vendorService: VendorService
  ) { }

  ngOnInit(): void {
    //this.url = this.actorService.pageUrl;
    this.LoadData();
  }
  
  LoadData(){
    this.url = this.repositoryService.apiUrl + "contact-manager/vendors?";
    this.vendorService.GetAll()
    .then((res) => {
      this.vendors = res.data;
      this.searchService.searchData = res.data;
    })
  }

  newData(data:any) {
    this.vendors = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.vendors = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.vendors = data
  }

  edit(obj:Vendor) {
    
    this.router.navigate(['/contact-manager/vendors/edit/' + obj.id]);
  }
  print(obj:Vendor) {    
    this.router.navigate(['/report-manager/invoice-report/'+ obj.id]);
  }
  toggleStatus(id:string) {
    this.vendorService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.vendorService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
}