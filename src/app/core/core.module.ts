import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { NbEvaIconsModule } from '@nebular/eva-icons';

import { RouterModule } from "@angular/router";
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LayoutNoMenuComponent } from './layout-no-menu/layout-no-menu.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import {
  NbActionsModule,
  NbLayoutModule,
  NbMenuModule,
  NbSearchModule,
  NbSidebarModule,
  NbUserModule,
  NbContextMenuModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbThemeModule,
  NbTooltipModule,
  NbBadgeModule,
} from '@nebular/theme';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LayoutComponent } from './layout/layout.component';

@NgModule({
  declarations: [
    LayoutNoMenuComponent,
    HeaderComponent,
    FooterComponent,
    LayoutComponent
  ],
  imports: [
    NbLayoutModule,
    NbEvaIconsModule,
    NbIconModule,
    NbBadgeModule,
    NbUserModule,
    NbSidebarModule,
    CommonModule,
    RouterModule,     
    HttpClientModule,    
    ReactiveFormsModule,  
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  exports: [HeaderComponent,LayoutComponent, LayoutNoMenuComponent],
  providers: [DatePipe]
})
export class CoreModule { }
export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

