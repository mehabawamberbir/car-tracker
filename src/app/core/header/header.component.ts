import { Component, OnInit } from '@angular/core';
import { NbSidebarService } from '@nebular/theme';
import { DatePipe } from '@angular/common';
//import { AuthService } from 'src/app/user-manager/services/auth.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  today!: string | null;
  selectedLanguage:string='English'
  constructor(private sidebarService: NbSidebarService,
     private datePipe: DatePipe, 
     //public authService: AuthService, 
     private route: Router,
     public translateService:TranslateService) { 
    this.translateService.addLangs(['English','ትግርኛ','ኣማርኛ']);
    this.translateService.setDefaultLang('English');
  }


  ngOnInit(): void {
    this.today = this.datePipe.transform(Date.now(), 'MMMM dd, y')
  }


  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }
  userPictureOnly: boolean = true;
  user={name:'Kal Belay',picture:'../../../../assets/images/tek.jpg'}
  userMenu=[ { title: 'Settings', icon:'star' }, { title: 'Logout' } ];

    ViewProfile() {
        this.route.navigateByUrl('/account/profile');
    }

    ChangePasswordProfile() {
        // this.route.navigate(['/account/change-password', this.authService.userName]);
    }

    LogOut() {
        this.route.navigateByUrl('/account/logout');
    }


}
