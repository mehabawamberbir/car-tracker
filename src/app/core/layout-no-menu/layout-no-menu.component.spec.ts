import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutNoMenuComponent } from './layout-no-menu.component';

describe('LayoutNoMenuComponent', () => {
  let component: LayoutNoMenuComponent;
  let fixture: ComponentFixture<LayoutNoMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutNoMenuComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LayoutNoMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
