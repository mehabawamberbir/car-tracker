import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout-no-menu',
  template: `
  <nb-layout windowMode>
      <nb-layout-header fixed class="header">
        <app-header></app-header>
      </nb-layout-header>

      <nb-layout-column>
        <ng-content select="router-outlet"></ng-content>
        <ng-content select="nb-card"></ng-content>
      </nb-layout-column>

      <nb-layout-footer fixed>
        <app-footer></app-footer>
      </nb-layout-footer>
    </nb-layout>
  `,
  styleUrls: ['./layout-no-menu.component.scss']
})
export class LayoutNoMenuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
