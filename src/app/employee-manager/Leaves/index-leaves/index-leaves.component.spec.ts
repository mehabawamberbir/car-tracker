import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexLeavesComponent } from './index-leaves.component';

describe('IndexLeavesComponent', () => {
  let component: IndexLeavesComponent;
  let fixture: ComponentFixture<IndexLeavesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexLeavesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexLeavesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
