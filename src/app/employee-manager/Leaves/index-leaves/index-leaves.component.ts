import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { Employees } from '../../emplyee-models';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Leave } from '../../emplyee-models';
import { LeavesService } from '../../services/leaves.service';

@Component({
  selector: 'app-index-leaves',
  templateUrl: './index-leaves.component.html',
  styleUrls: ['./index-leaves.component.scss']
})
export class IndexLeavesComponent implements OnInit {
  url: string;
  model: Leave[];
  constructor(private modelService: LeavesService,
              private router: Router,
              private notificationService: NotificationService,
              private sortService: SortService,
              private searchService: SearchService,
              private repositoryService: RepositoryService,
              private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.loadData();
  }
loadData(){
  this.url = this.repositoryService.apiUrl + this.modelService.modelPath + "?";
  this.modelService.GetAll().then((res)=>
    {
      this.model = res.data;
    });
}
newData(data:any) {
  this.model = data;
  this.searchService.searchData = data;
  this.sortService.sortData = data;
}
searchOutput(data: any) {
  this.model = data;
  this.searchService.searchData = data;
  this.sortService.sortData = data;
}
sortedData(data: any) {
  this.model = data
}

edit(obj: Leave) {
  
  this.router.navigate(['employee-manager/leaves/edit/' + obj.id]);
}
print(obj: Leave) {    
  this.router.navigate(['/report-manager/invoice-report/'+ obj.id]);
}
toggleStatus(id: string) {
  this.modelService.ToggleStatus(id)
  .then((res) => 
  {
    if(res.status == 200)
    {
      this.notificationService.showToastr("success", "Status Updated successfully!");
      this.loadData();
    } 
    else
    {
      this.notificationService.showToastr("danger", "Some error occured!")
    }
  })
  .catch((err) => {
    this.notificationService.showToastr("danger", "Some error occured!")
  })
  .finally(() => {
  });
}

delete(id:string) {
  this.modelService.Delete(id)
  .then((res) => 
  {
    if(res.status == 200)
    {
      this.notificationService.showToastr("success", "Record Deleted successfully!");
      this.loadData();
    } 
    else
    {
      this.notificationService.showToastr("danger", "Some error occured!")
    }
  })
  .catch((err) => {
    this.notificationService.showToastr("danger", "Some error occured!")
  })
  .finally(() => {
  });
}
}
