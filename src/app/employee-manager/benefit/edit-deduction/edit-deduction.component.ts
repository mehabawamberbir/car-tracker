import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '../../services/employee.service';
import { Employees } from '../../emplyee-models';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { DeductionService } from '../../services/deduction.service';


@Component({
  selector: 'app-edit-deduction',
  templateUrl: './edit-deduction.component.html',
  styleUrls: ['./edit-deduction.component.scss']
})
export class EditDeductionComponent implements OnInit {

  id: string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false;
  model: FormGroup;
  employees:Employees[];
  pay_periods:Lookup[];
  today:Date = new Date();
  constructor(private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: DeductionService,
    private employeeService: EmployeeService,
    private lookupService:LookupService) { }

  ngOnInit(): void {
    console.log(this.today);
    this.submitted = false;
    this.added = false;
    this.error = false;
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.employeeService.GetAll().then((res)=>
    {
      this.employees = res.data;
    });
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.PAY_PERIOD).then((res)=>
    {
      this.pay_periods = res.data;
    });


    this.model = this.fb.group({
      id:[''],
      employee_id:['', [Validators.required]],
      reg_date:[this.today, [Validators.required]],
      amount:[0, [Validators.required]],
      description:['', [Validators.required]],
      pay_period_id:['', [Validators.required]],
      status:1
    });
    if(this.id){
        this.populateData(this.id);
    }
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        id:this.id,
        employee_id:data.employee_id,
        reg_date:data.reg_date,
        amount:data.amount,
        description:data.description,
        pay_period_id:data.pay_period_id,
        status:data.status,
      });
    });
  }
  update() {
    if (this.model.valid) {
      this.modelService.Edit(this.id, this.model.value).then((res) => {
        this.notificationService.showToastr("success", "Record updated successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
    } else {
      this.submitted = true
    }
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value).then((res) => {
        this.ngOnInit()
        this.notificationService.showToastr("success", "Record created successfully!")
      }, err => {
       this.notificationService.showToastr("danger", "Unknown Error occurred!");
        console.log(err);
      });
    } else {
      this.submitted = true
    }
  }

}
