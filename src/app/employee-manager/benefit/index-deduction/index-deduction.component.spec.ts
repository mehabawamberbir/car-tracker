import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexDeductionComponent } from './index-deduction.component';

describe('IndexDeductionComponent', () => {
  let component: IndexDeductionComponent;
  let fixture: ComponentFixture<IndexDeductionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexDeductionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexDeductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
