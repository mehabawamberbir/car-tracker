import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexBenefitComponent } from './index-benefit.component';

describe('IndexBenefitComponent', () => {
  let component: IndexBenefitComponent;
  let fixture: ComponentFixture<IndexBenefitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexBenefitComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexBenefitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
