import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditDeductionComponent } from './benefit/edit-deduction/edit-deduction.component';
import { IndexDeductionComponent } from './benefit/index-deduction/index-deduction.component';
import { EditBenefitComponent } from './deduction/edit-benefit/edit-benefit.component';
import { IndexBenefitComponent } from './deduction/index-benefit/index-benefit.component';
import { CreateEmployeeComponent } from './employee/create-employee/create-employee.component';
import { EditEmployeeComponent } from './employee/edit-employee/edit-employee.component';
import { IndexEmployeeComponent } from './employee/index-employee/index-employee.component';
import { IndexEmployeeManagerComponent } from './index-employee-manager/index-employee-manager.component';
import { EditLeavesComponent } from './Leaves/edit-leaves/edit-leaves.component';
import { IndexLeavesComponent } from './Leaves/index-leaves/index-leaves.component';

const routes: Routes = [
  {path:'', component:IndexEmployeeManagerComponent,},
  {path:'employees', component:IndexEmployeeComponent},
  {path:'employees/create', component:CreateEmployeeComponent},
  {path:'employees/edit/:id', component:EditEmployeeComponent},

  {path:'deductions', component:IndexDeductionComponent},
  {path:'deductions/create', component:EditDeductionComponent},
  {path:'deductions/edit/:id', component:EditDeductionComponent},

  {path:'benefits', component:IndexBenefitComponent},
  {path:'benefits/create', component:EditBenefitComponent},
  {path:'benefits/edit/:id', component:EditBenefitComponent},

  {path:'leaves', component:IndexLeavesComponent},
  {path:'leaves/create', component:EditLeavesComponent},
  {path:'leaves/edit/:id', component:EditLeavesComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeManagerRoutingModule { }
