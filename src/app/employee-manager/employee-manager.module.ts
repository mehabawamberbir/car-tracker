import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeManagerRoutingModule } from './employee-manager-routing.module';
import { IndexEmployeeComponent } from './employee/index-employee/index-employee.component';
import { CreateEmployeeComponent } from './employee/create-employee/create-employee.component';
import { EditEmployeeComponent } from './employee/edit-employee/edit-employee.component';
import { NbCardModule, NbIconModule, NbMenuModule, NbSelectModule } from '@nebular/theme';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
import { ReactiveFormsModule } from '@angular/forms';
import { IndexEmployeeManagerComponent } from './index-employee-manager/index-employee-manager.component';
import { IndexBenefitComponent } from './deduction/index-benefit/index-benefit.component';
import { EditBenefitComponent } from './deduction/edit-benefit/edit-benefit.component';
import { IndexDeductionComponent } from './benefit/index-deduction/index-deduction.component';
import { EditDeductionComponent } from './benefit/edit-deduction/edit-deduction.component';
import { IndexLeavesComponent } from './Leaves/index-leaves/index-leaves.component';
import { EditLeavesComponent } from './Leaves/edit-leaves/edit-leaves.component';

@NgModule({
  declarations: [
    IndexEmployeeComponent,
    CreateEmployeeComponent,
    EditEmployeeComponent,
    IndexEmployeeManagerComponent,
    IndexBenefitComponent,
    EditBenefitComponent,
    IndexDeductionComponent,
    EditDeductionComponent,
    IndexLeavesComponent,
    EditLeavesComponent,
  ],
  imports: [
    CommonModule,
    EmployeeManagerRoutingModule,
    SharedModule,
    CoreModule,    
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    ReactiveFormsModule,
    NbMenuModule,
    
  ]
})
export class EmployeeManagerModule { }
