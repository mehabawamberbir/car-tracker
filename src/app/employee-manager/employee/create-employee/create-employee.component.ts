import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '../../services/employee.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.scss']
})
export class CreateEmployeeComponent implements OnInit {
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  groups : Lookup[];
  jobTiltles: Lookup[];
  departments: Lookup[];
  statuses : Lookup[];
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: EmployeeService,
    private lookupService: LookupService,) { }

  ngOnInit(): void {
    this.added = false;
    this.error = false;
    this.submitted = false;
    this.model = this.fb.group({
      id:[''],
      name: ['', [Validators.required]],
      job_title_id:['', [Validators.required]],
      department_id: ['', [Validators.required]],
      telephone: [''],
      gender: ['Male'],
      picture: [''],
      group_id: [''],
      home_tel: [''],
      work_tel: [''],
      work_email: [''],
      other_email: [''],
      address: [''],
      //jobSpecification:[''],
      status_id: [''],
      contract_details: [''],
      start_date: [''],
      end_date: [''],
      end_reason: ['']
    });
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.EMP_GROUP).then((res)=>{
    this.groups = res.data;
    });
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.JOB_TITLE).then((res)=>{
    this.jobTiltles = res.data;
    });
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.EMP_STATUS).then((res)=>{
    this.statuses = res.data;
    });
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.DEPARTMENT).then((res)=>
    {
      this.departments = res.data;
    })
  }
  ClearForm(){
    this.model = this.fb.group({
      department_id: ['', Validators.required],
      name: ['', Validators.required],
      job_title_id: ['', Validators.required],
      //status: 1,
    });
  }
  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
}