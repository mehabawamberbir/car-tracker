import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '../../services/employee.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {
  id:string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  groups : Lookup[];
  jobTiltles: Lookup[];
  departments: Lookup[];
  statuses : Lookup[];
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: EmployeeService,
    private lookupService: LookupService,
    private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.added = false;
    this.error = false;
    this.submitted = false;

    this.id = this.route.snapshot.paramMap.get("id")!;
    this.model = this.fb.group({
      id:[''],
      name: ['', [Validators.required]],
      job_title_id:['', [Validators.required]],
      department_id: ['', [Validators.required]],
      telephone: [''],
      gender: ['Male'],
      picture: [''],
      group_id: [''],
      home_tel: [''],
      work_tel: [''],
      work_email: [''],
      other_email: [''],
      address: [''],
      //jobSpecification:[''],
      status_id: [''],
      contract_details: [''],
      start_date: [''],
      end_date: [''],
      end_reason: ['']
    });
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.EMP_GROUP).then((res)=>{
    this.groups = res.data;
    });
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.JOB_TITLE).then((res)=>{
    this.jobTiltles = res.data;
    });
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.EMP_STATUS).then((res)=>{
    this.statuses = res.data;
    });
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.DEPARTMENT).then((res)=>
    {
      this.departments = res.data;
    });
    this.populateData(this.id);
  }
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        id:this.id,
        name: data.name,
        job_title_id:data.job_title_id,
        department_id: data.department_id,
        telephone: data.telephone,
        gender: data.gender,
        picture: data.picture,
        group_id: data.group_id,
        home_tel: data.home_tel,
        work_tel: data.work_tel,
        work_email: data.work_email,
        other_email: data.other_email,
        address: data.address,
        //jobSpecification:[''],
        status_id: data.status_id,
        contract_details: data.contract_details,
        start_date: data.start_date,
        end_date: data.end_date,
        end_reason: data.end_reason
      });
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  

  update() {
    var isFormValid = true;
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
  }


