import { Component, OnInit } from '@angular/core';
import { Employees } from '../../emplyee-models';
import { EmployeeService } from '../../services/employee.service';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
@Component({
  selector: 'app-index-employee',
  templateUrl: './index-employee.component.html',
  styleUrls: ['./index-employee.component.scss']
})
export class IndexEmployeeComponent implements OnInit {
  url: string;
  model: Employees[];
  constructor(private modelService: EmployeeService,
              private router: Router,
              private notificationService: NotificationService,
              private sortService: SortService,
              private searchService: SearchService,
              private repositoryService: RepositoryService) { }

  ngOnInit(): void {
    this.loadData();
  }
loadData(){
  this.url = this.repositoryService.apiUrl + "employee-manager/employees?";
  this.modelService.GetAll().then((res)=>
    {
      this.model = res.data;
    });
}
newData(data:any) {
  this.model = data;
  this.searchService.searchData = data;
  this.sortService.sortData = data;
}
searchOutput(data: any) {
  this.model = data;
  this.searchService.searchData = data;
  this.sortService.sortData = data;
}
sortedData(data: any) {
  this.model = data
}

edit(obj: Employees) {
  
  this.router.navigate(['employee-manager/employees/edit/' + obj.id]);
}
// print(obj: Employees) {    
//   this.router.navigate(['/report-manager/invoice-report/'+ obj.id]);
// }
toggleStatus(id: string) {
  this.modelService.ToggleStatus(id)
  .then((res) => 
  {
    if(res.status == 200)
    {
      this.notificationService.showToastr("success", "Status Updated successfully!");
      this.loadData();
    } 
    else
    {
      this.notificationService.showToastr("danger", "Some error occured!")
    }
  })
  .catch((err) => {
    this.notificationService.showToastr("danger", "Some error occured!")
  })
  .finally(() => {
  });
}

delete(id:string) {
  this.modelService.Delete(id)
  .then((res) => 
  {
    if(res.status == 200)
    {
      this.notificationService.showToastr("success", "Record Deleted successfully!");
      this.loadData();
    } 
    else
    {
      this.notificationService.showToastr("danger", "Some error occured!")
    }
  })
  .catch((err) => {
    this.notificationService.showToastr("danger", "Some error occured!")
  })
  .finally(() => {
  });
}
}
