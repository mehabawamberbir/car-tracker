import { Lookup } from "../master-data/master.model"

export class Employees{
    id: string
    name: string
    job_title_id: string
    job_title:Lookup
    department_id: string
    department:Lookup
    telephone: string
    gender: string
    picture: string
    group_id: string
    group:Lookup
    home_tel: string
    work_tel: string
    work_email: string
    other_email:string
    address: string
    //jobSpecification: string
    status_id: string
    emp_status: Lookup
    contract_details:string
    start_date: Date
    gross_salary:number
    end_date:Date
    end_reason: string
}

export class Deduction{
    id:string;
    employee_id:string;
    employee:Employees;
    reg_date:Date;
    amount:number;
    description:string;
    pay_period_id:string;
    pay_period:Lookup;
    created_at:Date;
    status:boolean;
}
export class Benefit{
    id:string;
    employee_id:string;
    employee:Employees;
    reg_date:Date;
    amount:number;
    description:string;
    pay_period_id:string;
    pay_period:Lookup;
    created_at:Date;
    status:boolean;
}

export class Leave{
    id:string;
    employee_id:string;
    employee:Employees;
    leave_type_id:string;
    leave_type:Lookup;
    start_date:Date;
    end_date:Date;
    remark:string;
    status:boolean;
}