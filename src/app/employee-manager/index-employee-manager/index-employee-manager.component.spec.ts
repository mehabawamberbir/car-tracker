import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexEmployeeManagerComponent } from './index-employee-manager.component';

describe('IndexEmployeeManagerComponent', () => {
  let component: IndexEmployeeManagerComponent;
  let fixture: ComponentFixture<IndexEmployeeManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexEmployeeManagerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexEmployeeManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
