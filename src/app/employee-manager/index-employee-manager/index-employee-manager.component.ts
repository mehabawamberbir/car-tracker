import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { CONSTANT_CONFIG } from 'src/app/config/constants';
@Component({
  selector: 'app-index-employee-manager',
  templateUrl: './index-employee-manager.component.html',
  styleUrls: ['./index-employee-manager.component.scss']
})
export class IndexEmployeeManagerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items : NbMenuItem[] = [
    {
    title : 'Employee',
    link : 'employees',
    icon : 'checkmark'
    },
    {
      title: 'Deduction',
      link: 'deductions',
      icon: 'checkmark'
    },
    {
      title: 'Benefit',
      link: 'benefits',
      icon: 'checkmark'
    },
    {
      title: 'Leaves',
      link: 'leaves',
      icon: 'checkmark'
    }
  ]
}
