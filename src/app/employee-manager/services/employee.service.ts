import { Injectable } from '@angular/core';
import axios from 'axios';
import { Employees } from '../emplyee-models';
import { RepositoryService } from 'src/app/shared/services/repository.service';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  model: Employees;
  modelPath: string = 'employee-manager/employees';
  constructor(private repository:RepositoryService) { }
 
  GetAll() {
    return axios.get(this.repository.apiUrl + this.modelPath + '-index');
  }
  Get(skip:number, limit:number) {
    return axios.get(this.repository.apiUrl + this.modelPath + `?skip=${skip} && limit=${limit}`);
}
  GetById(id:string) {
    return this.repository.sendRequest("GET", this.repository.apiUrl + this.modelPath + '/' + id);
  }
  Create(model: Employees) {
    return axios.post(this.repository.apiUrl + this.modelPath, model)
  }
  Edit(id:string, model: Employees) {
    return axios.put(this.repository.apiUrl + this.modelPath + '/' + id, model)
  }
  Delete(id:string) {
    return axios.delete(this.repository.apiUrl + this.modelPath + '/' + id);
  }
  ToggleStatus(id:string) {
    return axios.patch(this.repository.apiUrl + this.modelPath + '/' + id + '/toggle-status');
  }
}
