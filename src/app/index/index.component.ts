import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { StorageService } from '../_services/storage.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  userRoles : Array<string>= ['admin'];
  items = [
    { name:"master", title : "Master Data", link:"/master", icon: "settings", icon_type:"nb-icon", roles:['admin','super-admin']},
    { name:"school-manager", title : "School Manager", link:"/school-manager", icon: "fa-graduation-cap", icon_type:"fa-icon", roles:['admin','super-admin']},
    { name:"result-manager", title : "Gradebook", link:"/result-manager", icon: "fa-book", icon_type:"fa-icon", roles:['admin','super-admin','student','parent','teacher']},
    { name:"timetable-manager", title : "Timetable Manager", link:"/timetable-manager", icon: "fa-calendar-check-o", icon_type:"fa-icon", roles:['admin','super-admin']},
    { name:"admission-manager", title : "Admission Manager", link:"/admission-manager", icon: "fa-user-plus", icon_type:"fa-icon", roles:['admin','super-admin']},   
    { name:"student-manager", title : "Student Manager", link:"/student-manager", icon: "fa-users", icon_type:"fa-icon", roles:['admin','super-admin']},    
    { name:"user-manager", title : "User Manager", link:"/user-manager", icon: "fa-users", icon_type:"fa-icon", roles:['admin','super-admin']},
    { name:"commn", title : "Communication", link:"/commn", icon: "fa-commenting", icon_type:"fa-icon", roles:"*"},
    { name:"report-manager", title : "Report Manager", link:"/report-manager", icon: "fa-file", icon_type:"fa-icon", roles:['admin','super-admin','teacher','manager']} 
  ];
  constructor(private authService : AuthService,
    private storageService : StorageService) { }

  ngOnInit(): void 
  {
    let user = this.storageService.getCurrUser();

  }
  userHasRole(item:any)
  {
    let roles :Array<string> = item.roles;
    console.log("Roles:" + roles);
    for(let i=0; i< roles.length; i++){
      var res = this.userRoles.find(t => roles.indexOf(t) > -1 || roles.indexOf('*') > -1);
      if(res) return true;
    }
    return false;
  }
}
