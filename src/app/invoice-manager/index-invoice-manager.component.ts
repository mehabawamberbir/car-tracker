import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { CONSTANT_CONFIG } from 'src/app/config/constants';

@Component({
  selector: 'app-index-invoice-manager',
  templateUrl: './index-invoice-manager.component.html',
  styleUrls: ['./index-invoice-manager.component.scss']
})
export class IndexInvocieManagerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items: NbMenuItem[] = [
    {
      title: "Transit Invoices",
      link: '/invoice-manager/invoices/transit',
      icon: 'checkmark'
    },
    {
      title: "Complementary Invoices",
      link: '/invoice-manager/invoices/complementary',
      icon: 'checkmark'
    },
    {
      title: "Invoices",
      link: '/invoice-manager/invoices/index',
      icon: 'checkmark'
    },
    {
      title: "Proforma",
      link: '/invoice-manager/invoices/estimate',
      icon: 'checkmark'
    },
    {
      title: "Receive Payment",
      link: '/invoice-manager/receive-payments',
      icon: 'checkmark'
    }
   ];
}