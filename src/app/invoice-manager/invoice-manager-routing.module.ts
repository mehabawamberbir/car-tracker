import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { AuthGuard } from '../_helpers/auth.guard';

import { IndexInvoiceComponent } from './invoice/index-invoice/index-invoice.component';
import { CreateInvoiceComponent } from './invoice/create-invoice/create-invoice.component';
import { EditInvoiceComponent } from './invoice/edit-invoice/edit-invoice.component';
import { IndexInvocieManagerComponent } from './index-invoice-manager.component';
import { IndexReceivePaymentComponent } from './receive-payment/index-receive-payment/index-receive-payment.component';
import { CreateReceivePaymentComponent } from './receive-payment/create-receive-payment/create-receive-payment.component';
import { EditReceivePaymentComponent } from './receive-payment/edit-receive-payment/edit-receive-payment.component';
import { ViewInvoiceComponent } from './invoice/view-invoice/view-invoice.component';

const routes: Routes = [
  
    { path: '', component: IndexInvocieManagerComponent, },
    { path: 'invoices/:invoice_type', component: IndexInvoiceComponent },
    { path: 'invoices/create', component: CreateInvoiceComponent },
    { path: 'invoices/:invoice_type/create', component: CreateInvoiceComponent },
    { path: 'invoices/edit/:id', component: EditInvoiceComponent },
    { path: 'invoices/view/:id', component: ViewInvoiceComponent },

    { path: 'receive-payments', component: IndexReceivePaymentComponent },
    { path: 'receive-payments/create', component: CreateReceivePaymentComponent },
    { path: 'receive-payments/edit/:id', component: EditReceivePaymentComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceManagerRoutingModule { }
