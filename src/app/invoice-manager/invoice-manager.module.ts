import { CoreModule } from '../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { InvoiceManagerRoutingModule } from './invoice-manager-routing.module';
import { NbCardModule, NbAlertModule, NbButtonModule, NbIconModule, NbInputModule, NbSelectModule, NbTreeGridModule, NbDialogModule, NbToggleModule, NbTooltipModule, NbMenuModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CreateInvoiceComponent } from './invoice/create-invoice/create-invoice.component';
import { EditInvoiceComponent } from './invoice/edit-invoice/edit-invoice.component';
import { IndexInvoiceComponent } from './invoice/index-invoice/index-invoice.component';
import { IndexInvocieManagerComponent } from './index-invoice-manager.component';
import { IndexReceivePaymentComponent } from './receive-payment/index-receive-payment/index-receive-payment.component';
import { EditReceivePaymentComponent } from './receive-payment/edit-receive-payment/edit-receive-payment.component';
import { CreateReceivePaymentComponent } from './receive-payment/create-receive-payment/create-receive-payment.component';
import { ViewInvoiceComponent } from './invoice/view-invoice/view-invoice.component';

@NgModule({
  declarations: [
    IndexInvocieManagerComponent,
    CreateInvoiceComponent,
    EditInvoiceComponent,
    IndexInvoiceComponent,
    IndexReceivePaymentComponent,
    EditReceivePaymentComponent,
    CreateReceivePaymentComponent,
    ViewInvoiceComponent
  ],
  imports: [
    CommonModule,
    InvoiceManagerRoutingModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbAlertModule,
    NbInputModule,
    NbSelectModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SmartTableModule,
    NbTreeGridModule,
    SharedModule,
    NbDialogModule.forChild(),
    NbToggleModule,
    NbTooltipModule,
    CoreModule,
    NbMenuModule,
  ],
  providers: [
  ]
})
export class InvoiceManagerModule { }
