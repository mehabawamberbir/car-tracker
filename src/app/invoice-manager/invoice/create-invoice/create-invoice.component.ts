import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InvoiceService } from 'src/app/invoice-manager/services/invoice.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { CustomerService } from 'src/app/contact-manager/services/customer.service';


import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';
import { Operation } from 'src/app/operation-manager/models/operation-model';
import { Consignee, Customer } from 'src/app/contact-manager/contact-models';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { ConsigneeService } from 'src/app/contact-manager/services/consignee.service';
import { ActivatedRoute, Route } from '@angular/router';

@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.scss']
})
export class CreateInvoiceComponent implements OnInit {
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  invoice_type:string;
  invoiceTypes:Lookup[];
  operations:Operation[];
  consignees:Consignee[];
  customers : Customer[];
  currencies : Lookup[];

  constructor(
    private fb: FormBuilder,
    private route : ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: InvoiceService,
    private lookupService: LookupService,
    private consigneeService: ConsigneeService,
    private operationService: OperationService,
    private customerService: CustomerService,
    ) { }

  ngOnInit(): void {
    this.invoice_type = this.route.snapshot.paramMap.get("invoice_type")!;
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
      type: [this.invoice_type, [Validators.required]],
      invoice_no: ['', [Validators.required]],
      operation_id: ['', [Validators.required]],
      customer_id: ['', [Validators.required]],
      invoice_date: ['', [Validators.required]],
      currency_id: ['', [Validators.required]],
      amount: [0, [Validators.required]],
      consignee_id: ['',[Validators.required]],
      remarks: [''],
      status:1
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CURRENCY).then((res) => {
      this.currencies = res.data;
    }) 

    this.operationService.GetAll().then((res) => {
      this.operations = res.data;
    })

    this.consigneeService.GetAll().then((res) => {
      this.consignees = res.data;
    })

    this.customerService.GetAll().then((res) => {
      this.customers = res.data;
    })
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value).then((res) => {
        this.ngOnInit()
        this.notificationService.showToastr("success", "Record created successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Unknown Error occurred!")
      })
    } else {
      this.submitted = true;
      console.log("Invalid Model");
    }
  }

}