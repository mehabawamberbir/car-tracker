import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InvoiceService } from 'src/app/invoice-manager/services/invoice.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { CustomerService } from 'src/app/contact-manager/services/customer.service';

import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';
import { Operation } from 'src/app/operation-manager/models/operation-model';
import { Consignee, Customer } from 'src/app/contact-manager/contact-models';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { ConsigneeService } from 'src/app/contact-manager/services/consignee.service';
import { LookupService } from 'src/app/master-data/service/lookup.service';

@Component({
  selector: 'app-edit-invoice',
  templateUrl: './edit-invoice.component.html',
  styleUrls: ['./edit-invoice.component.scss']
})
export class EditInvoiceComponent implements OnInit {

  submitted: boolean = false;
  id:string;
  model: FormGroup;
  invoice_type: string;
  invoiceTypes : Lookup[];
  operations : Operation[];
  consignees : Consignee[];
  customers : Customer[];
  currencies : Lookup[];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: InvoiceService,
    private customerService: CustomerService,
    private operationService: OperationService,
    private consigneeService: ConsigneeService,
    private lookupService: LookupService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.submitted = false;

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.INVOICE_TYPE).then((res) => {
      this.invoiceTypes = res.data;
    })   

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CURRENCY).then((res) => {
      this.currencies = res.data;
    }) 

    this.operationService.GetAll().then((res) => {
      this.operations = res.data;
    })

    this.consigneeService.GetAll().then((res) => {
      this.consignees = res.data;
    })

    this.customerService.GetAll().then((res) => {
      this.customers = res.data;
    })

    this.model = this.fb.group({
      id: [],
      type: ['', [Validators.required]],
      invoice_no: ['', [Validators.required]],
      operation_id: ['', [Validators.required]],
      customer_id: ['', [Validators.required]],
      consignee_id: ['', [Validators.required]],
      invoice_date: [''],
      currency_id: [''],
      amount: ['', [Validators.required]],
      remarks: [''],
      status:1
    });
    this.populateData(this.id);
  }
  
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.invoice_type = data.type;
      

    console.log(data.word);

      this.model.setValue({
        id : this.id,
        type : data.type,
        invoice_no: data.invoice_no,
        operation_id: data.operation_id,
        customer_id: data.customer_id,
        consignee_id: data.consignee_id,
        invoice_date: data.invoice_date,
        currency_id: data.currency_id,
        amount: data.amount,
        remarks: data.remarks,
        status : data.status
      });
    });
  }

  update() {
    if (this.model.valid) {
      this.modelService.Edit(this.id, this.model.value).then((res) => {
        this.notificationService.showToastr("success", "Record updated successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
    } else {
      this.submitted = true
    }
  }

}
