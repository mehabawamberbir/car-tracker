import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { InvoiceService } from '../../services/invoice.service';
import { Invoice } from '../../models/invoice.models';
import { INVOICE_STATUS } from 'src/app/shared/InvoiceStatus';

@Component({
  selector: 'app-invoice',
  templateUrl: './index-invoice.component.html',
  styleUrls: ['./index-invoice.component.scss']
})
export class IndexInvoiceComponent implements OnInit {
  invoices: any;
  url:string;
  invoice_type:string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: InvoiceService
  ) 
  { 
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };
  }

  ngOnInit(): void {
    var type = this.route.snapshot.paramMap.get("invoice_type");
    this.invoice_type = type == null ? "invoice" : this.route.snapshot.paramMap.get("invoice_type")!;
    this.url = this.repositoryService.apiUrl + "invoice-manager/invoices?type=" + this.invoice_type + "&&";
  }
  
  getAll() : void {
        
    this.invoice_type = this.route.snapshot.paramMap.get("id")!;
    this.modelService.GetAll(this.invoice_type).then(res => 
      {
        this.invoices = res.data;
      },
      error => { console.log(error);}
      );
  }

  refreshList(): void {
    this.getAll();
  }

  newData(data:any) {
    this.invoices = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.invoices = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    
    this.invoices = data;
    
  }
  print(obj:Invoice) {
    this.modelService.model = obj;
    this.router.navigate(['/report-manager/invoice-report/'+ obj.id]);
  }
  edit(obj:Invoice) {
    this.modelService.model = obj;
    this.router.navigate(['/invoice-manager/invoices/edit/' + obj.id]);
  }
  
  view(obj:Invoice) {
    this.modelService.model = obj;
    this.router.navigate(['/invoice-manager/invoices/view/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id).then(res => {
      this.notificationService.showToastr("success", "Status updated successfully!")
    }, err => {
      this.notificationService.showToastr("danger", "Failed to update the status!")
    })
  }
  delete(id:string) {
    this.modelService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!")
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
  
  GetInvoiceStatus(status:number){
    return INVOICE_STATUS.find(t => t.id == status)?.name;
  }
}