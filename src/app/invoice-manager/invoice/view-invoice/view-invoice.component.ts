import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InvoiceService } from 'src/app/invoice-manager/services/invoice.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { CustomerService } from 'src/app/contact-manager/services/customer.service';

import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';
import { Expense, Operation } from 'src/app/operation-manager/models/operation-model';
import { Consignee, Customer } from 'src/app/contact-manager/contact-models';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { ConsigneeService } from 'src/app/contact-manager/services/consignee.service';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { Invoice } from '../../models/invoice.models';
import { INVOICE_STATUS } from 'src/app/shared/InvoiceStatus';
import { ExpenseService } from 'src/app/operation-manager/services/expense.service';

@Component({
  selector: 'app-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.scss']
})
export class ViewInvoiceComponent implements OnInit {

  submitted: boolean = false;
  id:string;
  model: FormGroup;
  obj:Invoice;
  invoice_type: string;
  invoiceTypes : Lookup[];
  operations : Operation[];
  consignees : Consignee[];
  customers : Customer[];
  currencies : Lookup[];
  invoiceItems : Expense[];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: InvoiceService,
    private expenseService:ExpenseService,
    private customerService: CustomerService,
    private operationService: OperationService,
    private consigneeService: ConsigneeService,
    private lookupService: LookupService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.submitted = false;    

    this.model = this.fb.group({
      id: [],
      type: ['', [Validators.required]],
      invoice_no: ['', [Validators.required]],
      operation_id: ['', [Validators.required]],
      customer_id: ['', [Validators.required]],
      consignee_id: ['', [Validators.required]],
      invoice_date: [''],
      currency_id: [''],
      amount: ['', [Validators.required]],
      remarks: [''],
      status:1
    });
    this.populateData(this.id);
    
  }
  
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.invoice_type = data.type;
      this.obj = data;

    this.expenseService.Query('operation_id', data.operation_id).then(res => {
      this.invoiceItems = res.data;
    });

      this.model.setValue({
        id : this.id,
        type : data.type,
        invoice_no: data.invoice_no,
        operation_id: data.operation_id,
        customer_id: data.customer_id,
        consignee_id: data.consignee_id,
        invoice_date: data.invoice_date,
        currency_id: data.currency_id,
        amount: data.amount,
        remarks: data.remarks,
        status : data.status
      });
    });
  }

  update() {
    if (this.model.valid) {
      this.modelService.Edit(this.id, this.model.value).then((res) => {
        this.notificationService.showToastr("success", "Record updated successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
    } else {
      this.submitted = true
    }
  }

  
  
  GetInvoiceStatus(status:number){
    return INVOICE_STATUS.find(t => t.id == status)?.name;
  }
  GetColorByStatus(status:number){
    return INVOICE_STATUS.find(t => t.id == status)?.color;
  }

  Sum(val1: number, val2:number){
    return val1*val2;
  }

}
