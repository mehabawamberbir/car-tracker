import { Consignee, Customer } from "src/app/contact-manager/contact-models";
import { Lookup } from "src/app/master-data/master.model";
import { Operation } from "src/app/operation-manager/models/operation-model";

export class Invoice{
    id:string;
    type:string;
    invoice_no:string;
    operation_id:string;
    operation:Operation;
    customer_id:string;
    customer?:Customer;
    invoice_date:string;
    invoice_due_date:string;
    currency_id:string;
    currency:Lookup;
    amount:number;
    consignee_id:string;
    consignee?:Consignee;
    remarks:string;
    status: number;    
}
export class ReceivePayment{
    id:string;
    customer_id:string;
    reference_no:string;
    payment_date:string;
    currency_id:string;
    amount:number;
    payment_method_id:string;
    account_id:string;
    remarks:string;
    status: boolean;
}