import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateReceivePaymentComponent } from './create-receive-payment.component';

describe('CreateReceivePaymentComponent', () => {
  let component: CreateReceivePaymentComponent;
  let fixture: ComponentFixture<CreateReceivePaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateReceivePaymentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateReceivePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
