import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/contact-manager/services/customer.service';
import { Customer } from 'src/app/contact-manager/contact-models';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormGroup, FormBuilder, Validators,FormControl } from '@angular/forms';
import { ReceivePayment } from '../../models/invoice.models';
import { ReceivePaymentService } from '../../services/receive_payment.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { ReactiveFormsModule } from '@angular/forms';
import { Account } from 'src/app/accounting-manager/accounting-manager.model';
import { AccountService } from 'src/app/accounting-manager/services/account.service';
@Component({
  selector: 'app-create-receive-payment',
  templateUrl: './create-receive-payment.component.html',
  styleUrls: ['./create-receive-payment.component.scss']
})
export class CreateReceivePaymentComponent implements OnInit {

  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  customers:Customer[];
  currencies:Lookup[];
  payment_methods:Lookup[];
  accounts:Account[];
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: ReceivePaymentService,
    private customerService:CustomerService,
    private lookUpService:LookupService,
    private accountService:AccountService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
      id:[''],
      customer_id:['',],
      currency_id:[''],
      date:['', [Validators.required]], 
      amount:[0, [Validators.required]],
      payment_method_id:['', [Validators.required]],
      account_id:['', [Validators.required]],
      reference:[''],
      remarks:[''],
      status:1,
    });

  this.customerService.GetAll().then((res)=> {
    this.customers = res.data;
  });
  this.accountService.GetAll().then((res)=> {
    this.accounts = res.data;
  });
  this.lookUpService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CURRENCY).then((res)=>
  {
    this.currencies = res.data;
  });
  this.lookUpService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.PAY_METHOD).then((res)=>
  {
    this.payment_methods = res.data;
  });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  ClearForm(){
    this.model = this.fb.group({
      id:[''],
      vendor_id:['',],
      currency_id:[''],
      date:['', [Validators.required]], 
      amount:[0, [Validators.required]],
      payment_method_id:['', [Validators.required]],
      account_id:['', [Validators.required]],
      reference:[''],
      remarks:[''],
      status:1,
      });
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }


}
