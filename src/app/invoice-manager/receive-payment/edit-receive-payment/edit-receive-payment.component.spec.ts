import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditReceivePaymentComponent } from './edit-receive-payment.component';

describe('EditReceivePaymentComponent', () => {
  let component: EditReceivePaymentComponent;
  let fixture: ComponentFixture<EditReceivePaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditReceivePaymentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditReceivePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
