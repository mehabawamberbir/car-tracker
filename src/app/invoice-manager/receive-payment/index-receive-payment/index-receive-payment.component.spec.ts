import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexReceivePaymentComponent } from './index-receive-payment.component';

describe('IndexReceivePaymentComponent', () => {
  let component: IndexReceivePaymentComponent;
  let fixture: ComponentFixture<IndexReceivePaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexReceivePaymentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexReceivePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
