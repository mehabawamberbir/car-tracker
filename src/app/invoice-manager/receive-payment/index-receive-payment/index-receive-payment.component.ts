import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { ReceivePaymentService } from '../../services/receive_payment.service';
import { ReceivePayment } from '../../models/invoice.models';
import { INVOICE_STATUS } from 'src/app/shared/InvoiceStatus';
@Component({
  selector: 'app-index-receive-payment',
  templateUrl: './index-receive-payment.component.html',
  styleUrls: ['./index-receive-payment.component.scss']
})
export class IndexReceivePaymentComponent implements OnInit {

  url:string;
  receive_payments:any;

  constructor(private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: ReceivePaymentService) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath  + "?";
    this.LoadData();
  }
  LoadData(){
    this.modelService.GetAll()
    .then((res) => {
      this.receive_payments = res.data;
      this.searchService.searchData = res.data;
    });
  }
  edit(obj:ReceivePayment) {
    
    this.router.navigate(['/purchase-manager/receive-payments/edit/' + obj.id]);
  }

  newData(data:any) {
    this.receive_payments = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.receive_payments = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.receive_payments = data
  }

  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.modelService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  GetInvoiceStatus(status:number){
    return INVOICE_STATUS.find(t => t.id == status)?.name;
  }

}
