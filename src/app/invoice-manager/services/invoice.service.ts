import { Injectable } from '@angular/core';
import { RepositoryService } from 'src/app/shared/services/repository.service';

import  axios  from 'axios';
import { Invoice } from '../models/invoice.models';

@Injectable({
    providedIn: 'root'
})

export class InvoiceService {
    model : Invoice;
    modelPath : string = "invoice-manager/invoices";
    constructor(private repository: RepositoryService) { 

    }    
    GetAll(invoice_type:string) {
        return axios.get(this.repository.apiUrl + this.modelPath + '-index/' + invoice_type);
    }
    GetAllPaginated(invoice_type :string, skip:number, limit:number) {
        return axios.get(this.repository.apiUrl + this.modelPath + `?type=${invoice_type} && skip=${skip} && limit=${limit}`);
    }
    Get(skip:number, limit:number) {
        return axios.get(this.repository.apiUrl + this.modelPath + `?skip=${skip} && limit=${limit}`);
    }
    Query(attribute:string, value:string) {
        return axios.get(this.repository.apiUrl + this.modelPath + '/query/' + attribute + '/' + value);
    }
    GetById(id:string) {
        return this.repository.sendRequest("GET", this.repository.apiUrl + this.modelPath + '/' + id);
    }
    Create(model:Invoice) {
        return axios.post(this.repository.apiUrl + this.modelPath, model)
    }
    Edit(id:string, model:Invoice) {
        return axios.put(this.repository.apiUrl + this.modelPath + '/' + id, model)
    }
    Delete(id:string) {
        return axios.delete(this.repository.apiUrl + this.modelPath + '/' + id);
    }
    ToggleStatus(id:string) {
        return axios.patch(this.repository.apiUrl + this.modelPath + '/' + id + '/toggle-status');
    }
}