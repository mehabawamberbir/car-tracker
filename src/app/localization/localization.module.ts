import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalizeComponent } from './localize/localize.component';



@NgModule({
  declarations: [
    LocalizeComponent
  ],
  imports: [
    CommonModule
  ]
})
export class LocalizationModule { }
