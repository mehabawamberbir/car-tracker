import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../shared/services/notification.service';
import { AuthService } from '../_services/auth.service';
import { StorageService } from '../_services/storage.service';
import { UserService } from '../_services/user.service';
import { Router,CanActivate,ActivatedRouteSnapshot,RouterStateSnapshot, ActivatedRoute } from '@angular/router';

const AUTH_API = CONSTANT_CONFIG.AUTH_API + 'login';

import  axios  from 'axios';
import { CONSTANT_CONFIG } from '../config/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: any = {
    username: null,
    password: null
  };
  isLoggedIn = false;  
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  constructor(private authService: AuthService, 
    private notificationService: NotificationService,
    private storageService: StorageService,
    private route: ActivatedRoute,
    private router: Router) { }
  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      //this.roles = this.storageService.getUser().roles;
    }
  }
  onSubmit(): void {
    const { username, password } = this.form;
    this.notificationService.limit = 1;
    this.notificationService.showToastr("info", "Authenticating ...");  

    this.authService.login(username, password).then((res) => 
    {
      if(res.status == 200)
      {        
        this.notificationService.showToastr("primary", "Redirecting ...");   
        this.reloadPage();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Login Failed. Username or Password invalid!!")  
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Login Failed. Username or Password invalid!!")
    })
    .finally(() => {
      //console.log('Login completed');
    });
  }
  reloadPage(): void {
    this.notificationService.showToastr("info", "Redirecting ...");  
    // get return url from route parameters or default to '/'
    //const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.router.navigate(['/home'])
        .then(() => {
          window.location.reload();
        });
  }
}