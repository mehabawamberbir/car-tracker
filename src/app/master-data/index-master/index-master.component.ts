import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-index-master',
  templateUrl: './index-master.component.html',
  styleUrls: ['./index-master.component.scss']
})
export class IndexMasterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  items: NbMenuItem[] = [
    {
      title: "Lookups",
      link: '/master/lookups',
      icon: 'checkmark'
    },
    {
      title: "Lookup Types",
      link: '/master/lookup-types',
      icon: 'checkmark'
    }
   ];
}