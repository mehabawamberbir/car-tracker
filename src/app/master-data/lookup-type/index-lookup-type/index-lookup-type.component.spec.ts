import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexLookupTypeComponent } from './index-lookup-type.component';

describe('IndexLookupTypeComponent', () => {
  let component: IndexLookupTypeComponent;
  let fixture: ComponentFixture<IndexLookupTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexLookupTypeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexLookupTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
