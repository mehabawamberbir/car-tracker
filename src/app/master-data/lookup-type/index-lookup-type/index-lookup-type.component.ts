import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/shared/services/search.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SortService } from 'src/app/shared/services/sort.service';
import { PaginationComponent } from 'src/app/shared/templates/pagination/pagination.component';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { LookupTypeService } from '../../service/lookup-type.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { LookupType } from '../../master.model';

@Component({
  selector: 'app-index-lookup-type',
  templateUrl: './index-lookup-type.component.html',
  styleUrls: ['./index-lookup-type.component.scss']
})
export class IndexLookupTypeComponent implements OnInit {
  lookupTypes : LookupType[]; 
  url:string; 
  id:string;
  submitted: boolean = false;
  disabled: boolean = false;
  createMode: boolean = false;
  editMode: boolean = false;
  LookupType: FormGroup;

  constructor(
    private notificationService: NotificationService,
    private lookupTypeService: LookupTypeService,
    private sortService: SortService,
    private searchService: SearchService,
    private repositoryService: RepositoryService,
    private fb: FormBuilder,
    private pagination: PaginationComponent) {

  }

  ngOnInit(): void {
    this.createMode = false;
    this.editMode = false;
    this.submitted = false;
    this.disabled = false;
    this.url = this.repositoryService.apiUrl + "lookups";
    
    this.LoadData();
  }
  LoadData(){
    this.lookupTypeService.GetAll().then((res) => {
      this.lookupTypes = res.data;
      this.searchService.searchData = res.data;
    })
  }
  ClearForm(){
    this.LookupType = this.fb.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      local_name: [''],
      order: 1,
      status:1
    })
  }
  createClicked() {
    this.createMode = true;
    this.editMode = false;
    
    this.ClearForm();
  }

  editClicked(lookupType:LookupType) {
    this.createMode = false;
    this.editMode = true;
    this.id = lookupType.id;
    this.LookupType = this.fb.group({
      id: [lookupType.id],
      name: [lookupType.name, Validators.required],
      code: [lookupType.code, Validators.required],
      local_name: [lookupType.local_name],
      order: [lookupType.order],
      status : [lookupType.status]
    })
  }

  closeModes() {
    this.createMode = this.editMode = this.submitted = false;
  }
  create() {
    if (this.LookupType.valid) {
        this.lookupTypeService.Create(this.LookupType.value)
        .then((res) => 
      {
        if(res.status == 201)
        {
          this.notificationService.showToastr("success", "Record Created successfully!");
          this.ClearForm();
          this.LoadData();
        } 
        else
        {
          this.notificationService.showToastr("danger", "Some error occured!")
        }
      })
      .catch((err) => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
      .finally(() => {
      });
    }
    else {
      this.submitted = true;
      this.notificationService.showToastr("danger", "INVALID REQUEST!")
    }
  }

  edit() {
    if (this.LookupType.valid) {
      this.lookupTypeService.Edit(this.id, this.LookupType.value)
      .then((res) => 
      {
        if(res.status == 200)
        {
          this.notificationService.showToastr("success", "Record Updated successfully!");
          this.editMode = false;
          this.LoadData();
        } 
        else
        {
          this.notificationService.showToastr("danger", "Some error occured!")
        }
      })
      .catch((err) => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
      .finally(() => {
      });
    }
    else {
      this.submitted = true
    }
  }

  newData(data:any) {
    this.lookupTypes = data
    this.searchService.searchData = data
    this.sortService.sortData = data
  }
  searchOutput(data:any) {
    this.lookupTypes = data
    this.sortService.sortData = data
  }
  sortedData(data:any) {
    this.lookupTypes = data
  }

  toggleStatus(id:string) {
    this.lookupTypeService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.lookupTypeService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
}
