import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexLookupComponent } from './index-lookup.component';

describe('IndexLookupComponent', () => {
  let component: IndexLookupComponent;
  let fixture: ComponentFixture<IndexLookupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexLookupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
