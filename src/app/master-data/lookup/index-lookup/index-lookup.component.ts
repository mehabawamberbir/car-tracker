import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/shared/services/search.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SortService } from 'src/app/shared/services/sort.service';
import { PaginationComponent } from 'src/app/shared/templates/pagination/pagination.component';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { LookupService } from '../../service/lookup.service';
import { LookupTypeService } from '../../service/lookup-type.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Lookup, LookupType } from '../../master.model';

@Component({
  selector: 'app-index-lookup',
  templateUrl: './index-lookup.component.html',
  styleUrls: ['./index-lookup.component.scss']
})
export class IndexLookupComponent implements OnInit {
  lookupTypes : LookupType[]; 
  lookups: Lookup[];
  url:string; 
  id:string;
  submitted: boolean = false;
  disabled: boolean = false;
  createMode: boolean = false;
  editMode: boolean = false;
  Lookup: FormGroup;

  constructor(
    private notificationService: NotificationService,
    private lookupService: LookupService,
    private lookupTypeService: LookupTypeService,
    private sortService: SortService,
    private searchService: SearchService,
    private repositoryService: RepositoryService,
    private fb: FormBuilder,
    private pagination: PaginationComponent) {

  }

  ngOnInit(): void {
    this.createMode = false;
    this.editMode = false;
    this.submitted = false;
    this.disabled = false;
    this.url = this.repositoryService.apiUrl + "lookups?";
    
    this.LoadData();
    this.lookupTypeService.GetAll().then(res => {
      this.lookupTypes = res.data
    })
  }
  
  LoadData(){
    
    this.lookupService.GetAll()
    .then((res) => {
      this.lookups = res.data;
      this.searchService.searchData = res.data;
    })
  }
  ClearForm(){
    this.Lookup = this.fb.group({
      lookup_type_id: ['', Validators.required],
      name: ['', Validators.required],
      code: ['', Validators.required],
      local_name: [''],
      order: 1,
      status: 1,
    });
  }
  createClicked() {
    this.createMode = true;
    this.editMode = false;
    this.Lookup = this.fb.group({
      lookup_type_id: ['', Validators.required],
      name: ['', Validators.required],
      code: ['', Validators.required],
      local_name: [''],
      order: 1,
      status: 1,
    });
  }

  editClicked(lookup:Lookup) {
    this.createMode = false;
    this.editMode = true;
    this.id = lookup.id;
    this.Lookup = this.fb.group({
      id: [lookup.id],
      lookup_type_id: [lookup.lookup_type_id, Validators.required],
      name: [lookup.name, Validators.required],
      code: [lookup.code, Validators.required],
      local_name: [lookup.local_name],
      order: [lookup.order],
      status : [lookup.status]
    })
  }

  closeModes() {
    this.createMode = this.editMode = this.submitted = false;
  }
  create() {
    if (this.Lookup.valid) {
      this.lookupService.Create(this.Lookup.value)
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    }
    else {
      this.submitted = true
    }
  }
  edit() {
    if (this.Lookup.valid) {
      this.lookupService.Edit(this.id, this.Lookup.value)
      .then((res) => 
      {
        if(res.status == 200)
        {
          this.notificationService.showToastr("success", "Record Updated successfully!");
          this.editMode = false;
          this.LoadData();
        } 
        else
        {
          this.notificationService.showToastr("danger", "Some error occured!")
        }
      })
      .catch((err) => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
      .finally(() => {
      });
    }
    else {
      this.submitted = true
    }
  }

  newData(data:any) {
    this.lookups = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.lookups = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.lookups = data
  }

  toggleStatus(id:string) {
    this.lookupService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.lookupService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }

  getLookupTypeName(lookUp:Lookup)
  {
    if(lookUp == null || lookUp.lookup_type == null) return "";
    return lookUp.lookup_type.name;
  }
}
