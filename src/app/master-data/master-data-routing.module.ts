import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexMasterComponent } from './index-master/index-master.component';
import { IndexLookupTypeComponent } from './lookup-type/index-lookup-type/index-lookup-type.component';
import { IndexLookupComponent } from './lookup/index-lookup/index-lookup.component';

const routes: Routes = [
  {
    path: '', component : IndexMasterComponent,  
  },
  { path: 'lookups', component: IndexLookupComponent },
  { path: 'lookup-types', component: IndexLookupTypeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterDataRoutingModule { }
