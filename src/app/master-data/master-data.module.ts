import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { MasterDataRoutingModule } from './master-data-routing.module';
import { IndexMasterComponent } from './index-master/index-master.component';
import { IndexLookupComponent } from './lookup/index-lookup/index-lookup.component';
import { IndexLookupTypeComponent } from './lookup-type/index-lookup-type/index-lookup-type.component';
import { CoreModule } from '../core/core.module';
import { NbCardModule, NbIconModule, NbMenuModule, NbSelectModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    IndexMasterComponent,
    IndexLookupComponent,
    IndexLookupTypeComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,    
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    MasterDataRoutingModule,
    ReactiveFormsModule,
    NbMenuModule,
  ],
  providers :[],  
})
export class MasterDataModule { }
