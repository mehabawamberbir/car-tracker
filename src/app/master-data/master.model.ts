export class LookupType{    
    id: string;
    name: string;
    local_name: string;
    code: string;
    order: number;
    status: boolean;
}
export class Lookup{
    id: string;
    lookup_type_id: string;
    lookup_type : LookupType;
    name: string;
    local_name: string;
    code: string;
    order: number;
    is_default: boolean;
    status:boolean;
}