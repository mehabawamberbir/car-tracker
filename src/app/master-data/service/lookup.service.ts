import { Injectable } from '@angular/core';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Lookup } from '../master.model';

import  axios  from 'axios';

@Injectable({
    providedIn: 'root'
})

export class LookupService {
    model : Lookup;
    modelPath : string = "lookups";
    constructor(private repository: RepositoryService) { 

    }    

    GetAll() {
        return axios.get(this.repository.apiUrl + this.modelPath + '-index');
    }
    Get(skip:number, limit:number) {
        return axios.get(this.repository.apiUrl + this.modelPath + `?skip=${skip} && limit=${limit}`);
    }
    GetById(id:string) {
        return axios.get(this.repository.apiUrl + this.modelPath + '/' + id);
    }
    Create(model:Lookup) {
        return axios.post(this.repository.apiUrl + this.modelPath, model)
    }
    Edit(id:string, model:Lookup) {
        return axios.put(this.repository.apiUrl + this.modelPath + '/' + id, model)
    }
    Delete(id:string) {
        return axios.delete(this.repository.apiUrl + this.modelPath + '/' + id);
    }
    ToggleStatus(id:string) {
        return axios.patch(this.repository.apiUrl + this.modelPath + '/' + id + '/toggle-status');
    }
    
    getLookupByLookupTypeCode(code:string) {
        return axios.get(this.repository.apiUrl + this.modelPath + '/'+ code +'/lookups-by-type');
    }
}