import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
    {
    title:"Dashboard",
    icon:"bar-chart-outline",
    link:"/dashboard",
    fragment : 'sidebar'
    },
    {
        title:"Contact Manager",
        icon:"book-open",
        link:"/contact-manager",
        fragment : 'sidebar'
    },
    {
        title:"Operation Manager",
        icon:"car",
        link:"/operation-manager",
        fragment : 'sidebar'
    },
    {
        title:"Sales Manager",
        icon:"file-add",
        link:"/invoice-manager",
        fragment : 'sidebar'
    },
    {
        title:"Purchase Manager",
        icon:"file-add",
        link:"/purchase-manager",
        fragment : 'sidebar'
    },
    {
        title:"Accounting Manager",
        icon:"trending-up-outline",
        link:"/accounting-manager",
        fragment : 'sidebar'
    },
    {
        title:"Payroll Manager",
        icon:"credit-card",
        link:"/employee-manager",
        fragment : 'sidebar'
    },
    {
        title:"Product Manager",
        icon:"pricetags",
        link:"/product-manager",
        fragment : 'sidebar'
    },
    {
        title:"Report Manager",
        icon:"file-text",
        link:"/report-manager",
        fragment : 'sidebar'
    },
    // {
    //     title:"User Manager",
    //     icon:"shield-outline",
    //     link:"/user-manager",
    //     fragment : 'sidebar'
    // },
    
    {
        title: 'Master Data',
        icon: 'settings',
        link:"/master",
        fragment : 'sidebar'
    },
]