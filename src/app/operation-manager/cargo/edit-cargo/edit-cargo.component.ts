import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Operation } from '../../models/operation-model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Product } from 'src/app/product-manager/models/product-model';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { CargoService } from '../../services/cargo.service';


@Component({
  selector: 'app-edit-cargo',
  templateUrl: './edit-cargo.component.html',
  styleUrls: ['./edit-cargo.component.scss']
})
export class EditCargoComponent implements OnInit {
  id: string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false;
  model: FormGroup;
  operation_no:string;
  operation_id:string;
  operations : Operation[];
  locations:Lookup[];

  constructor(private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: CargoService,
    private operationService: OperationService,
    private lookupService:LookupService) { }

  ngOnInit(): void {
    this.submitted = false;
    this.added = false;
    this.error = false;
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.operationService.GetAll().then((res)=>
    {
      this.operations = res.data;
    });
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONT_LOCATION).then((res)=>
    {
      this.locations = res.data;
    });


    this.model = this.fb.group({
      id:[''],
      quantity:0,
      description:[''],
      weight:0,
      way_bill_no:[''],
      truck_no:[''],
      gate_pass:[''],
      sydonia_ref:[''],
      location_id:[''],
      operation_id:this.operation_id,
      status:1
    });
    if(this.id){
        this.populateData(this.id);
    }
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.operation_no = data.operation.operation_no;
      this.model.setValue({
        id:this.id,
        quantity:data.quantity,
        description:data.description,
        weight:data.weight,
        way_bill_no:data.way_bill_no,
        truck_no:data.truck_no,
        gate_pass:data.gate_pass,
        sydonia_ref:data.sydonia_ref,
        location_id:data.location_id,
        operation_id:data.operation_id,
        status:1
      });
    });
  }
  update() {
    if (this.model.valid) {
      this.modelService.Edit(this.id, this.model.value).then((res) => {
        this.notificationService.showToastr("success", "Record updated successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
    } else {
      this.submitted = true
    }
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value).then((res) => {
        this.ngOnInit()
        this.notificationService.showToastr("success", "Record created successfully!")
      }, err => {
       this.notificationService.showToastr("danger", "Unknown Error occurred!");
        console.log(err);
      });
    } else {
      this.submitted = true
    }
  }

}
