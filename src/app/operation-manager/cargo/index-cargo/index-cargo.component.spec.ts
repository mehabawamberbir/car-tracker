import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexCargoComponent } from './index-cargo.component';

describe('IndexCargoComponent', () => {
  let component: IndexCargoComponent;
  let fixture: ComponentFixture<IndexCargoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexCargoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexCargoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
