import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { CargoService } from '../../services/cargo.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Cargo } from '../../models/operation-model';
import { Lookup } from 'src/app/master-data/master.model';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { LookupService } from 'src/app/master-data/service/lookup.service';

@Component({
  selector: 'app-index-cargo',
  templateUrl: './index-cargo.component.html',
  styleUrls: ['./index-cargo.component.scss']
})
export class IndexCargoComponent implements OnInit {

  cargos: Cargo[];
  locations:Lookup[];
  url:string;
  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: CargoService
  ) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath + "?";
    this.getAll();
  }
  
  getAll() : void {
    this.modelService.GetAll().then(res => 
      {
        this.cargos = res.data;
      },
      error => { console.log(error);}
      );
  }

  refreshList(): void {
    this.getAll();
  }

  newData(data : any) {
    this.cargos = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data : any) {
    this.cargos = data;
    this.sortService.sortData = data;
  }
  sortedData(data : any) {
    
    this.cargos = data;
    
  }
  edit(obj:Cargo) {
    this.modelService.model = obj;
    this.router.navigate(['/operation-manager/cargos/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id).then(res => {
      this.notificationService.showToastr("success", "Status updated successfully!")
    }, err => {
      this.notificationService.showToastr("danger", "Failed to update the status!")
    })
  }
  delete(id:string) {
    this.modelService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!")
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }

}
