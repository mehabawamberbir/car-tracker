import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContainerService } from 'src/app/operation-manager/services/container.service';
import { ActorService } from 'src/app/contact-manager/services/actor.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';

import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';
import { Operation } from '../../models/operation-model';
import { Agent } from 'src/app/contact-manager/contact-models';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { AgentService } from 'src/app/contact-manager/services/agent.service';
import { DatePipe } from '@angular/common';
import {formatDate} from '@angular/common';
import { CONSTANT_CONFIG } from 'src/app/config/constants';

@Component({
  selector: 'app-create-container',
  templateUrl: './create-container.component.html',
  styleUrls: ['./create-container.component.scss']
})
export class CreateContainerComponent implements OnInit {
  isModal : boolean = false;
  operation_id : string;
  operation_no:string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  containerTypes : Lookup[];
  operations : Operation[];
  locations : Lookup[];

  today = new Date();
  currDate = formatDate(this.today, CONSTANT_CONFIG.DATE_FORMAT, 'en');

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: ContainerService,
    private lookupService: LookupService,
    private actorService: ActorService,
    private operationService: OperationService,
    private agentService : AgentService,
    private datePipe: DatePipe
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false;

    console.log("Today:" + this.currDate);
    this.model = this.fb.group({
      operation_id :this.operation_id,
      container_type_id : [''],
      container_no : [''],
      seal_no : [''],
      voyage_no : [''],
      description : [''],
      quantity : 1,
      gross_weight : [''],
      port_out_date : this.currDate,
      empty_date : [''],
      return_date : this.currDate,
      truck_no : [''],
      refund : 0,
      deposit : 0,
      gate_pass_no : [''],
      way_bill_no : [''],
      declaration_no : [''],
      declaration_no4 : [''],
      declaration_date : [''],
      destination : [''],
      certification_no : [''],
      vgm_form : [''],
      location_id : [''],
    })

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONTAINER_TYPE).then((res) => {
      this.containerTypes = res.data;
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONT_LOCATION).then((res) => {
      this.locations = res.data;
    });  

    this.operationService.GetAll().then((res) => {
      this.operations = res.data;
    });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value).then((res) => {
        this.ngOnInit()
        this.notificationService.showToastr("success", "Record created successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Unknown Error occurred!")
      })
    } else {
      this.submitted = true
    }
  }

}