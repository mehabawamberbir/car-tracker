import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContainerService } from 'src/app/operation-manager/services/container.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';

import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';
import { Operation } from '../../models/operation-model';
import { Agent } from 'src/app/contact-manager/contact-models';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { AgentService } from 'src/app/contact-manager/services/agent.service';

@Component({
  selector: 'app-edit-container',
  templateUrl: './edit-container.component.html',
  styleUrls: ['./edit-container.component.scss']
})
export class EditContainerComponent implements OnInit {

  submitted: boolean = false;
  id:string | null = null;
  isModal : boolean = false;
  operation_id:string;
  operation_no:string;
  model: FormGroup;
  groups : Lookup[];
  operations : Operation[];
  containerTypes : Lookup[];
  locations : Lookup[];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: ContainerService,
    private operationService: OperationService,
    private lookupService: LookupService,
    private agentService : AgentService) { }

  ngOnInit(): void {
    if(this.id == null)
      this.id = this.route.snapshot.paramMap.get("id");
    this.submitted = false;

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CUSTOMER_GROUP).then((res) => {
      this.groups = res.data;
    })
    
    

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONTAINER_TYPE).then((res) => {
      this.containerTypes = res.data;
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONT_LOCATION).then((res) => {
      this.locations = res.data;
    })  

    this.model = this.fb.group({
      id: this.id,
      operation_id :this.operation_id,
      container_type_id : [''],
      container_no : [''],
      seal_no : [''],
      voyage_no : [''],
      description : [''],
      quantity : [''],
      gross_weight : [''],
      port_out_date : [''],
      empty_date : [''],
      return_date : [''],
      truck_no : [''],
      refund : [''],
      deposit : [''],
      gate_pass_no : [''],
      way_bill_no : [''],
      declaration_no : [''],
      declaration_no4 : [''],
      declaration_date : [''],
      destination : [''],
      certification_no : [''],
      vgm_form : [''],
      location_id : [''],
    });
    if(this.id != null)
      this.populateData(this.id);
  }
  
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.operation_no = data.operation.operation_no;      
      this.model.setValue({
        id : this.id,
        operation_id : data.operation_id,
        container_type_id : data.container_type_id,
        container_no : data.container_no,
        seal_no : data.seal_no,
        voyage_no : data.voyage_no,
        description : data.description,
        quantity : data.quantity,
        gross_weight : data.gross_weight,
        port_out_date : data.port_out_date,
        empty_date : data.empty_date,
        return_date : data.return_date,
        truck_no : data.truck_no,
        refund : data.refund,
        deposit : data.deposit,
        gate_pass_no : data.gate_pass_no,
        way_bill_no : data.way_bill_no,
        declaration_no : data.declaration_no,
        declaration_no4 : data.declaration_no4,
        declaration_date : data.declaration_date,
        destination : data.destination,
        certification_no : data.certification_no,
        vgm_form : data.vgm_form,
        location_id : data.location_id,
      });
    });
  }

  update() {
    if (this.model.valid && this.id != null) {
      this.modelService.Edit(this.id, this.model.value).then((res) => {
        this.notificationService.showToastr("success", "Record updated successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
    } else {
      this.submitted = true
    }
  }

}
