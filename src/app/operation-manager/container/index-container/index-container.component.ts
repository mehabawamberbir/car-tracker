import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { ContainerService } from 'src/app/operation-manager/services/container.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Container } from '../../models/operation-model';

@Component({
  selector: 'app-index-container',
  templateUrl: './index-container.component.html',
  styleUrls: ['./index-container.component.scss']
})
export class IndexContainerComponent implements OnInit {
  containers: Container[];
  url:string;
  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: ContainerService
  ) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + "operation-manager/containers?";
    //this.getAll();
  }
  
  getAll() : void {
    this.modelService.GetAll().then(res => 
      {
        this.containers = res.data;
      },
      error => { console.log(error);}
      );
  }

  refreshList(): void {
    this.getAll();
  }

  newData(data : any) {
    this.containers = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data : any) {
    this.containers = data;
    this.sortService.sortData = data;
  }
  sortedData(data : any) {
    
    this.containers = data;
    
  }
  edit(obj:Container) {
    this.modelService.model = obj;
    this.router.navigate(['/operation-manager/containers/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id).then(res => {
      this.notificationService.showToastr("success", "Status updated successfully!")
    }, err => {
      this.notificationService.showToastr("danger", "Failed to update the status!")
    })
  }
  delete(id:string) {
    this.modelService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!");      
      this.getAll();
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
}