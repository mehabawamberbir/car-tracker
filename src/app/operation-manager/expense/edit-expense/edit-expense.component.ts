import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Operation } from '../../models/operation-model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Product } from 'src/app/product-manager/models/product-model';
import { ProductService } from 'src/app/product-manager/services/product.service';
import { Expense } from '../../models/operation-model';
import { ExpenseService } from '../../services/expense.service';

@Component({
  selector: 'app-edit-expense',
  templateUrl: './edit-expense.component.html',
  styleUrls: ['./edit-expense.component.scss']
})
export class EditExpenseComponent implements OnInit {
  id: string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false;
  operation_id:string;
  operation_no:string;
  model: FormGroup;
  operations : Operation[];
  products: Product[];

  constructor(private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: ExpenseService,
    private operationService: OperationService,
    private productService:ProductService) { }

  ngOnInit(): void {
    this.submitted = false;
    this.added = false;
    this.error = false;
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.operationService.GetAll().then((res)=>
    {
      this.operations = res.data;
    });
    this.productService.GetAll().then((res)=>
    {
      this.products = res.data;
    });


    this.model = this.fb.group({
      id: [''],
      operation_id: this.operation_id,
      product_id: ['', [Validators.required]],
      reference_no: [''],
      quantity:0,
      unit_cost:0,
      amount: 0,
      status: 1,
    });
    if(this.id){
        this.populateData(this.id);
    }
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.operation_no = data.operation.operation_no;
      this.model.setValue({
        id: this.id,
        operation_id: data.operation_id,
        product_id: data.product_id,
        reference_no: data.reference_no,
        quantity:data.quantity,
        unit_cost:data.unit_cost,
        amount: data.amount,
        status: data.status,
      });
    });
  }
  update() {
    if (this.model.valid) {
      this.modelService.Edit(this.id, this.model.value).then((res) => {
        this.notificationService.showToastr("success", "Record updated successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
    } else {
      this.submitted = true
    }
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value).then((res) => {
        this.ngOnInit()
        this.notificationService.showToastr("success", "Record created successfully!")
      }, err => {
       this.notificationService.showToastr("danger", "Unknown Error occurred!");
        console.log(err);
      });
    } else {
      this.submitted = true
    }
  }

}
