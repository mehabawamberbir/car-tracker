import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexExpenseComponent } from './index-expense.component';

describe('IndexExpenseComponent', () => {
  let component: IndexExpenseComponent;
  let fixture: ComponentFixture<IndexExpenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexExpenseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
