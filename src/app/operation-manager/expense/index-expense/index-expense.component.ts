import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { ExpenseService } from '../../services/expense.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Expense } from '../../models/operation-model';

@Component({
  selector: 'app-index-expense',
  templateUrl: './index-expense.component.html',
  styleUrls: ['./index-expense.component.scss']
})
export class IndexExpenseComponent implements OnInit {

  expenses: Expense[];
  url:string;
  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: ExpenseService
  ) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath + "?";
    this.getAll();
  }
  
  getAll() : void {
    this.modelService.GetAll().then(res => 
      {
        this.expenses = res.data;
      },
      error => { console.log(error);}
      );
  }

  refreshList(): void {
    this.getAll();
  }

  newData(data : any) {
    this.expenses = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data : any) {
    this.expenses = data;
    this.sortService.sortData = data;
  }
  sortedData(data : any) {
    
    this.expenses = data;
    
  }
  edit(obj:Expense) {
    this.modelService.model = obj;
    this.router.navigate(['/operation-manager/expenses/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id).then(res => {
      this.notificationService.showToastr("success", "Status updated successfully!")
    }, err => {
      this.notificationService.showToastr("danger", "Failed to update the status!")
    })
  }
  delete(id:string) {
    this.modelService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!")
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }

}
