import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Operation } from '../../models/operation-model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { GatePass } from '../../models/operation-model';
import { GatePassService } from '../../services/gate-pass.service';
@Component({
  selector: 'app-edit-gate-pass',
  templateUrl: './edit-gate-pass.component.html',
  styleUrls: ['./edit-gate-pass.component.scss']
})
export class EditGatePassComponent implements OnInit {
  id: string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false;
  operation_id:string;
  operation_no:string;
  model: FormGroup;
  operations : Operation[];

  constructor(private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: GatePassService,
    private operationService: OperationService) { }

  ngOnInit(): void {
    this.submitted = false;
    this.added = false;
    this.error = false;
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.operationService.GetAll().then((res)=>
    {
      this.operations = res.data;
    });
    this.model = this.fb.group({
      id: [''],
      operation_id: this.operation_id,
      truck_no: [''],
      destination: [''],      
      status: 1,
    });
    if(this.id){
        this.populateData(this.id);
    }
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.operation_no = data.operation.operation_no;
      this.model.setValue({
        id: this.id,
        operation_id: data.operation_id,
        truck_no:data.truck_no,
        destination:data.destination,
        status: data.status,
      });
    });
  }
  update() {
    if (this.model.valid) {
      this.modelService.Edit(this.id, this.model.value).then((res) => {
        this.notificationService.showToastr("success", "Record updated successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
    } else {
      this.submitted = true
    }
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value).then((res) => {
        this.ngOnInit()
        this.notificationService.showToastr("success", "Record created successfully!")
      }, err => {
       this.notificationService.showToastr("danger", "Unknown Error occurred!");
        console.log(err);
      });
    } else {
      this.submitted = true
    }
  }


}
