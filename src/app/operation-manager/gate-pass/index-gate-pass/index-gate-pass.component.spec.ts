import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexGatePassComponent } from './index-gate-pass.component';

describe('IndexGatePassComponent', () => {
  let component: IndexGatePassComponent;
  let fixture: ComponentFixture<IndexGatePassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexGatePassComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexGatePassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
