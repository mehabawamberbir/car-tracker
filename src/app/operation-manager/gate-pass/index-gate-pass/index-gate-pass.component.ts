import { Component, OnInit } from '@angular/core';
import { GatePass } from '../../models/operation-model';
import { GatePassService } from '../../services/gate-pass.service';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
@Component({
  selector: 'app-index-gate-pass',
  templateUrl: './index-gate-pass.component.html',
  styleUrls: ['./index-gate-pass.component.scss']
})
export class IndexGatePassComponent implements OnInit {
  gatePasses: GatePass[];
  url:string;
  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: GatePassService
  ) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath + "?";
    this.getAll();
  }
  
  getAll() : void {
    this.modelService.GetAll().then(res => 
      {
        this.gatePasses = res.data;
      },
      error => { console.log(error);}
      );
  }

  refreshList(): void {
    this.getAll();
  }

  newData(data : any) {
    this.gatePasses = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data : any) {
    this.gatePasses = data;
    this.sortService.sortData = data;
  }
  sortedData(data : any) {
    
    this.gatePasses = data;
    
  }
  edit(obj:GatePass) {
    this.modelService.model = obj;
    this.router.navigate(['/operation-manager/gate-passes/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id).then(res => {
      this.notificationService.showToastr("success", "Status updated successfully!")
    }, err => {
      this.notificationService.showToastr("danger", "Failed to update the status!")
    })
  }
  delete(id:string) {
    this.modelService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!")
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }

}
