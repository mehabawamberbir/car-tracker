import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGoodRemovalComponent } from './edit-good-removal.component';

describe('EditGoodRemovalComponent', () => {
  let component: EditGoodRemovalComponent;
  let fixture: ComponentFixture<EditGoodRemovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditGoodRemovalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditGoodRemovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
