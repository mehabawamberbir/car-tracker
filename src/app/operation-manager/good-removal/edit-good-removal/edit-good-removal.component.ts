import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Operation } from '../../models/operation-model';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { GoodRemoval } from '../../models/operation-model';
import { GoodRemovalService } from '../../services/good-removal.service';
@Component({
  selector: 'app-edit-good-removal',
  templateUrl: './edit-good-removal.component.html',
  styleUrls: ['./edit-good-removal.component.scss']
})
export class EditGoodRemovalComponent implements OnInit {
  id: string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false;
  operation_id:string;
  operation_no:string;
  model: FormGroup;
  operations : Operation[];

  constructor(private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: GoodRemovalService,
    private operationService: OperationService) { }

  ngOnInit(): void {
    this.submitted = false;
    this.added = false;
    this.error = false;
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.operationService.GetAll().then((res)=>
    {
      this.operations = res.data;
    });
    this.model = this.fb.group({
      id: [''],
      operation_id: this.operation_id,
      good_removal_date: [''],   
      status: 1,
    });
    if(this.id){
        this.populateData(this.id);
    }
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.operation_no = data.operation.operation_no;
      this.model.setValue({
        id: this.id,
        operation_id: data.operation_id,
        good_removal_date:data.good_removal_date,
        status: data.status,
      });
    });
  }
  update() {
    if (this.model.valid) {
      this.modelService.Edit(this.id, this.model.value).then((res) => {
        this.notificationService.showToastr("success", "Record updated successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
    } else {
      this.submitted = true
    }
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value).then((res) => {
        this.ngOnInit()
        this.notificationService.showToastr("success", "Record created successfully!")
      }, err => {
       this.notificationService.showToastr("danger", "Unknown Error occurred!");
        console.log(err);
      });
    } else {
      this.submitted = true
    }
  }

}
