import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexGoodRemovalComponent } from './index-good-removal.component';

describe('IndexGoodRemovalComponent', () => {
  let component: IndexGoodRemovalComponent;
  let fixture: ComponentFixture<IndexGoodRemovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexGoodRemovalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexGoodRemovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
