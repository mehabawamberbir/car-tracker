import { Component, OnInit } from '@angular/core';
import { GoodRemoval } from '../../models/operation-model';
import { GoodRemovalService } from '../../services/good-removal.service';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
@Component({
  selector: 'app-index-good-removal',
  templateUrl: './index-good-removal.component.html',
  styleUrls: ['./index-good-removal.component.scss']
})
export class IndexGoodRemovalComponent implements OnInit {
  goodremovals:GoodRemoval[];
  url:string;
  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: GoodRemovalService
  ) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath + "?";
    this.getAll();
  }
  
  getAll() : void {
    this.modelService.GetAll().then(res => 
      {
        this.goodremovals = res.data;
      },
      error => { console.log(error);}
      );
  }

  refreshList(): void {
    this.getAll();
  }

  newData(data : any) {
    this.goodremovals = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data : any) {
    this.goodremovals = data;
    this.sortService.sortData = data;
  }
  sortedData(data : any) {
    
    this.goodremovals = data;
    
  }
  edit(obj:GoodRemoval) {
    this.modelService.model = obj;
    this.router.navigate(['/operation-manager/good-removals/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id).then(res => {
      this.notificationService.showToastr("success", "Status updated successfully!")
    }, err => {
      this.notificationService.showToastr("danger", "Failed to update the status!")
    })
  }
  delete(id:string) {
    this.modelService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!")
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }

}
