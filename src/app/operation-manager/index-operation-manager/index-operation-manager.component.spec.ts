import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexOperationManagerComponent } from './index-operation-manager.component';

describe('IndexOperationManagerComponent', () => {
  let component: IndexOperationManagerComponent;
  let fixture: ComponentFixture<IndexOperationManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexOperationManagerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexOperationManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
