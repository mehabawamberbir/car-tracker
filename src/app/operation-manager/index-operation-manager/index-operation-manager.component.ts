import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { CONSTANT_CONFIG } from 'src/app/config/constants';

@Component({
  selector: 'app-index-operation-manager',
  templateUrl: './index-operation-manager.component.html',
  styleUrls: ['./index-operation-manager.component.scss']
})
export class IndexOperationManagerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items: NbMenuItem[] = [
    {
      title: "Operations",
      link: '/operation-manager/operations',
      icon: 'checkmark'
    },
    {
      title: "Containers",
      link: '/operation-manager/containers',
      icon: 'checkmark'
    },
    {
      title: "Waybills",
      link: '/operation-manager/waybills',
      icon: 'checkmark'
    },
    {
      title: "Trucks",
      link: '/operation-manager/trucks',
      icon: 'checkmark'
    },
    {
      title: "Cargos",
      link: '/operation-manager/cargos',
      icon: 'checkmark'
    },
    {
      title: "Expenses",
      link: '/operation-manager/expenses',
      icon: 'checkmark'
    },
    {
      title: "Gate Passes",
      link: '/operation-manager/gate-passes',
      icon: 'checkmark'
    },
    {
      title: "Good Removals",
      link: '/operation-manager/good-removals',
      icon: 'checkmark'
    }
   ];
}
