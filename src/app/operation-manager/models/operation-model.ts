// import { Agent } from "http";
import { Consignee, Customer, Agent } from "src/app/contact-manager/contact-models";
import { Lookup } from "src/app/master-data/master.model";
import { Product } from "src/app/product-manager/models/product-model";

interface option {
    id:string;
    name:string;
  }
  
export class Operation{
    id: string;
    category_id: string;
    category:Lookup;
    operation_no: string;
    reference_no: string;
    consignee_id: string;
    consignee:Consignee;
    freight_forwarder_id: string;
    customer:Customer;
    reception_date: string;
    registration_date: string;
    booking_no:string;
    atd_operation:string;
    esl_operation:string;
    bol: string;
    quantity: number;
    gross_weight: number;
    loading_type_id: string;
    total_vehicle:number;
    total_cbm:number;
    partial_shipment:string;
    container_status:string;
    destination: string;
    agent_id: string;
    agent:Agent;
    arrival_date: string;
    description: string;
    attachement_file:Blob;
    status: number;
}
  
export class OperationProcess{
    id: string;
    operation_id : string;
    declaration_no : string;
    declaration_date : Date;
    vessel_name : string;
    voyage_no : string;
    partial_shipment : string;
    container_status : string;
    stuffing_date : Date;
    gross_weight : string;
    net_weight : string;
    description : string;
    declaration_no_eth : string;
    assigned_no_declarant : string;
    assigned_no_importer : string;
    border_office : string;
    clearance_office : string;
    fumigation : boolean;
    weight_inspection : boolean;
    inspection : boolean;
    craft_paper : boolean;
}

export class Container{
  id: string;
  operation_id:string;
  operation:Operation;
  container_type_id:string;
  container_type: Lookup;
  container_no: string;
  seal_no: string;
  voyage_no: string;
  arrival_date?: Date;
  description:string;
  quantity:number;
  gross_weight:number;
  port_out_date:Date;
  empty_date:Date;
  return_date:Date;
  days:number;
  vessel_no:string;
  truck_no:string;
  refund:number;
  deposit:string;
  gate_pass_no:string;
  way_bill_no:string;
  declaration_no:string;
  declaration_no4:string;
  declaration_date:Date;
  destination:string;
  certification_no:string;
  vgm_form:string;
  location_id:string;
  location:Lookup;
  status: boolean;
}

export class Waybill{
  id: string;
  operation_id:string;
  operation:Operation;
  operation_no: string;
  truck_no:string;
  departure_date: string;
  destination: string;
  driver_name:string;
  driver_tel:string;
  transporter:string;
  rate_agree:string;
  clearance_office:string;
  loading_request:string;
  eta:string;
  remarks:string;
  status: boolean;
}

export class Expense{
  id:string;
  operation_id:string;
  operation:Operation;
  product:Product;
  product_id:string;
  reference_no:string;
  quantity:number;
  unit_cost:number;
  amount:number;
  status: boolean;
}

export class Truck{
  id:string;
  chasis_no:string;
  quantity:number;
  description:string;
  weight:number;
  operation_id:string;
  operation:Operation;
  way_bill_no:string;
  truck_no:string;
  gate_pass:string;
  sydonia_ref:string;
  location_id:string;
  location:Lookup;
  status:boolean;
}
export class Cargo{
  id:string;
  quantity:number;
  description:string;
  weight:number;
  operation_id:string;
  operation:Operation;
  way_bill_no:string;
  truck_no:string;
  gate_pass:string;
  sydonia_ref:string;
  location_id:string;
  location:Lookup;
  status:boolean;
}

export class GatePass{
  id:string;
  operation_id:string;
  operation:Operation;
  truck_no:string;
  destination:string;
  status:boolean;
}
export class GoodRemoval{
id:string;
operation_id:string;
operation:Operation;
good_removal_date: Date;
status:boolean;

}