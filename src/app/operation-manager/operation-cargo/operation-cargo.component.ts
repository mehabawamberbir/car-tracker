import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbWindowService } from '@nebular/theme';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { EditCargoComponent } from '../cargo/edit-cargo/edit-cargo.component';
import { Cargo } from '../models/operation-model';
import { CargoService } from '../services/cargo.service';

@Component({
  selector: 'operation-cargo',
  templateUrl: './operation-cargo.component.html',
  styleUrls: ['./operation-cargo.component.scss']
})
export class OperationCargoComponent implements OnInit {
  @Input() id : string;
  @Input() operation_no:string;
  container_id:string;

  model: FormGroup;
  types : Lookup[];
  trucks: Cargo[];

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private lookupService: LookupService,
    private cargoService: CargoService,
    private windowService : NbWindowService
    ) { }

  ngOnInit(): void {
    
    this.populateData();

    this.GetAll();
    this.model = this.fb.group({
      operation_id: this.id,
    });
    
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONTAINER_TYPE).then((res) => {
      this.types = res.data;
    });

  }

  populateData() {


  }

  GetAll(){
    this.cargoService.Query('operation_id',this.id).then((res) => {
      this.trucks = res.data;
    });
  }
  
  deleteItem(id:string) {
    this.cargoService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!");
      this.GetAll();
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
  createClicked(){
    this.windowService.open(EditCargoComponent, { title: 'Create Cargo', 
     context : { isModal : true, operation_id : this.id, operation_no : this.operation_no }});
  }  

  editClicked(obj:Cargo) {
    this.windowService.open(EditCargoComponent,  { title: 'Edit Cargo', context:{ isModal : true, operation_id : this.id, id: obj.id}});
  }

}