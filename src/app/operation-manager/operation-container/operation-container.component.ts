import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbDialogService, NbWindowService } from '@nebular/theme';
import { AgentService } from 'src/app/contact-manager/services/agent.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Product } from 'src/app/product-manager/models/product-model';
import { ProductService } from 'src/app/product-manager/services/product.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { OPERATION_STATUS } from 'src/app/shared/OperationStatus';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { CreateContainerComponent } from '../container/create-container/create-container.component';
import { EditContainerComponent } from '../container/edit-container/edit-container.component';
import { Container, Expense } from '../models/operation-model';
import { ContainerService } from '../services/container.service';
import { ExpenseService } from '../services/expense.service';
@Component({
  selector: 'operation-container',
  templateUrl: './operation-container.component.html',
  styleUrls: ['./operation-container.component.scss']
})
export class OperationContainerComponent implements OnInit {
  @Input() id : string;
  @Input() operation_no:string;
  container_id:string;

  model: FormGroup;
  types : Lookup[];
  containers: Container[];

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private operationService: OperationService,
    private lookupService: LookupService,
    private containerService:ContainerService,
    private dialogService: NbDialogService,
    private windowService : NbWindowService
    ) { }

  ngOnInit(): void {
    
    this.populateData();

    this.GetAll();
    this.model = this.fb.group({
      operation_id: this.id,
      container_type_id:0,
      container_no: [''],
      seal_no: [''],
      way_bill_no: [''],
      amount: 0,
    });    
    
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONTAINER_TYPE).then((res) => {
      this.types = res.data;
    });

  }

  populateData() {


  }

  GetAll(){
    this.containerService.Query('operation_id',this.id).then((res) => {
      this.containers = res.data;
    });
  }
  
  deleteItem(id:string) {
    this.containerService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!");
      this.GetAll();
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
  createClicked(){
    this.windowService.open(CreateContainerComponent, { title: 'Create Container', 
     context : { isModal : true, operation_id : this.id, operation_no : this.operation_no }});
  }  

  editClicked(obj:Container) {
    this.windowService.open(EditContainerComponent,  { title: 'Edit Container', context:{ isModal : true, operation_id : this.id, id: obj.id}});
  }

}