import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbWindowService } from '@nebular/theme';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { EditExpenseComponent } from '../expense/edit-expense/edit-expense.component';
import { Expense } from '../models/operation-model';
import { ExpenseService } from '../services/expense.service';

@Component({
  selector: 'operation-expense',
  templateUrl: './operation-expense.component.html',
  styleUrls: ['./operation-expense.component.scss']
})
export class OperationExpenseComponent implements OnInit {
  @Input() id : string;
  @Input() operation_no:string;
  container_id:string;

  model: FormGroup;
  types : Lookup[];
  expenses: Expense[];

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private lookupService: LookupService,
    private expenseService: ExpenseService,
    private windowService : NbWindowService
    ) { }

  ngOnInit(): void {
    
    this.populateData();

    this.GetAll();
    this.model = this.fb.group({
      operation_id: this.id,
    });
    
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONTAINER_TYPE).then((res) => {
      this.types = res.data;
    });

  }

  populateData() {


  }

  GetAll(){
    this.expenseService.Query('operation_id',this.id).then((res) => {
      this.expenses = res.data;
    });
  }
  
  deleteItem(id:string) {
    this.expenseService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!");
      this.GetAll();
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
  createClicked(){
    this.windowService.open(EditExpenseComponent, { title: 'Create Expense', 
     context : { isModal : true, operation_id : this.id, operation_no : this.operation_no }});
  }  

  editClicked(obj:Expense) {
    this.windowService.open(EditExpenseComponent,  { title: 'Edit Expense', context:{ isModal : true, operation_id : this.id, id: obj.id}});
  }

}