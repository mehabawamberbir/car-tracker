import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbWindowService } from '@nebular/theme';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { EditGatePassComponent } from '../gate-pass/edit-gate-pass/edit-gate-pass.component';
import { GatePass } from '../models/operation-model';
import { GatePassService } from '../services/gate-pass.service';

@Component({
  selector: 'operation-gate-pass',
  templateUrl: './operation-gate-pass.component.html',
  styleUrls: ['./operation-gate-pass.component.scss']
})
export class OperationGatePassComponent implements OnInit {
  @Input() id : string;
  @Input() operation_no:string;
  container_id:string;

  model: FormGroup;
  types : Lookup[];
  gatePasses: GatePass[];

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private lookupService: LookupService,
    private gatePassService: GatePassService,
    private windowService : NbWindowService
    ) { }

  ngOnInit(): void {
    
    this.populateData();

    this.GetAll();
    this.model = this.fb.group({
      operation_id: this.id,
    });
    
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONTAINER_TYPE).then((res) => {
      this.types = res.data;
    });

  }

  populateData() {


  }

  GetAll(){
    this.gatePassService.Query('operation_id',this.id).then((res) => {
      this.gatePasses = res.data;
    });
  }
  
  deleteItem(id:string) {
    this.gatePassService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!");
      this.GetAll();
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
  createClicked(){
    this.windowService.open(EditGatePassComponent, { title: 'Create GatePass', 
     context : { isModal : true, operation_id : this.id, operation_no : this.operation_no }});
  }  

  editClicked(obj:GatePass) {
    this.windowService.open(EditGatePassComponent,  { title: 'Edit GatePass', context:{ isModal : true, operation_id : this.id, id: obj.id}});
  }

}