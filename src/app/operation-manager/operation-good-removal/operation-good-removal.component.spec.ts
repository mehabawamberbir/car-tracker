import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationGoodRemovalComponent } from './operation-good-removal.component';

describe('OperationGoodRemovalComponent', () => {
  let component: OperationGoodRemovalComponent;
  let fixture: ComponentFixture<OperationGoodRemovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OperationGoodRemovalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OperationGoodRemovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
