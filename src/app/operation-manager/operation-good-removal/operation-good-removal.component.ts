import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbWindowService } from '@nebular/theme';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { EditGoodRemovalComponent } from '../good-removal/edit-good-removal/edit-good-removal.component';
import { GoodRemoval } from '../models/operation-model';
import { GoodRemovalService } from '../services/good-removal.service';
@Component({
  selector: 'operation-good-removal',
  templateUrl: './operation-good-removal.component.html',
  styleUrls: ['./operation-good-removal.component.scss']
})
export class OperationGoodRemovalComponent implements OnInit {
  @Input() id : string;
  @Input() operation_no:string;
  container_id:string;

  model: FormGroup;
  types : Lookup[];
  goodRemovals: GoodRemoval[];

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private lookupService: LookupService,
    private goodRemovalService: GoodRemovalService,
    private windowService : NbWindowService
    ) { }

  ngOnInit(): void {
    
    this.populateData();

    this.GetAll();
    this.model = this.fb.group({
      operation_id: this.id,
    });
    
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONTAINER_TYPE).then((res) => {
      this.types = res.data;
    });

  }

  populateData() {


  }

  GetAll(){
    this.goodRemovalService.Query('operation_id',this.id).then((res) => {
      this.goodRemovals = res.data;
    });
  }
  
  deleteItem(id:string) {
    this.goodRemovalService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!");
      this.GetAll();
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
  createClicked(){
    this.windowService.open(EditGoodRemovalComponent, { title: 'Create GoodRemoval', 
     context : { isModal : true, operation_id : this.id, operation_no : this.operation_no }});
  }  

  editClicked(obj:GoodRemoval) {
    this.windowService.open(EditGoodRemovalComponent,  { title: 'Edit GoodRemoval', context:{ isModal : true, operation_id : this.id, id: obj.id}});
  }


}
