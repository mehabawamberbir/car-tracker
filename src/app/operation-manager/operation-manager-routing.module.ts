import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { AuthGuard } from '../_helpers/auth.guard';
import { IndexOperationComponent } from './operation/index-operation/index-operation.component';
import { CreateOperationComponent } from './operation/create-operation/create-operation.component';
import { EditOperationComponent } from './operation/edit-operation/edit-operation.component';

import { IndexContainerComponent } from './container/index-container/index-container.component';
import { CreateContainerComponent } from './container/create-container/create-container.component';
import { EditContainerComponent } from './container/edit-container/edit-container.component';

import { IndexWaybillComponent } from './waybill/index-waybill/index-waybill.component';
import { CreateWaybillComponent } from './waybill/create-waybill/create-waybill.component';
import { EditWaybillComponent } from './waybill/edit-waybill/edit-waybill.component';
import { IndexOperationManagerComponent } from './index-operation-manager/index-operation-manager.component';
import { IndexCargoComponent } from './cargo/index-cargo/index-cargo.component';
import { EditCargoComponent } from './cargo/edit-cargo/edit-cargo.component';
import { IndexTruckComponent } from './truck/index-truck/index-truck.component';
import { EditTruckComponent } from './truck/edit-truck/edit-truck.component';
import { IndexExpenseComponent } from './expense/index-expense/index-expense.component';
import { EditExpenseComponent } from './expense/edit-expense/edit-expense.component';
import { IndexGatePassComponent } from './gate-pass/index-gate-pass/index-gate-pass.component';
import { EditGatePassComponent } from './gate-pass/edit-gate-pass/edit-gate-pass.component';
import { IndexGoodRemovalComponent } from './good-removal/index-good-removal/index-good-removal.component';
import { EditGoodRemovalComponent } from './good-removal/edit-good-removal/edit-good-removal.component';

const routes: Routes = [

  { path: '', component: IndexOperationManagerComponent },

  { path: 'operations', component: IndexOperationComponent },
  { path: 'operations/create', component: CreateOperationComponent },
  { path: 'operations/create/:category_id', component: CreateOperationComponent },
  { path: 'operations/edit/:id', component: EditOperationComponent },

  { path: 'containers', component: IndexContainerComponent },
  { path: 'containers/create', component: CreateContainerComponent },
  { path: 'containers/edit/:id', component: EditContainerComponent },

  { path: 'waybills', component: IndexWaybillComponent },
  { path: 'waybills/create', component: CreateWaybillComponent },
  { path: 'waybills/edit/:id', component: EditWaybillComponent },

  { path: 'cargos', component: IndexCargoComponent },
  { path: 'cargos/create', component: EditCargoComponent},
  { path: 'cargos/edit/:id', component: EditCargoComponent },

  { path: 'trucks', component: IndexTruckComponent },
  { path: 'trucks/create', component: EditTruckComponent},
  { path: 'trucks/edit/:id', component: EditTruckComponent },

  { path: 'expenses', component: IndexExpenseComponent },
  { path: 'expenses/create', component: EditExpenseComponent },
  { path: 'expenses/edit/:id', component: EditExpenseComponent },

  { path: 'gate-passes', component: IndexGatePassComponent },
  { path: 'gate-passes/create', component: EditGatePassComponent },
  { path: 'gate-passes/edit/:id', component: EditGatePassComponent },

  { path: 'good-removals', component: IndexGoodRemovalComponent },
  { path: 'good-removals/create', component: EditGoodRemovalComponent },
  { path: 'good-removals/edit/:id', component: EditGoodRemovalComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperationManagerRoutingModule { }
