import { CoreModule } from '../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
//import { DropDownTreeModule } from '@syncfusion/ej2-angular-dropdowns';

import { OperationManagerRoutingModule } from './operation-manager-routing.module';
import { NbCardModule, NbAlertModule, NbButtonModule, NbIconModule, NbInputModule, NbSelectModule, NbTreeGridModule, NbDialogModule, NbToggleModule, NbTooltipModule, NbMenuModule, NbTabsetModule, NbStepperModule, NbLayoutModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CreateOperationComponent } from './operation/create-operation/create-operation.component';
import { EditOperationComponent } from './operation/edit-operation/edit-operation.component';
import { CreateContainerComponent } from './container/create-container/create-container.component';
import { EditContainerComponent } from './container/edit-container/edit-container.component';
import { IndexContainerComponent } from './container/index-container/index-container.component';
import { CreateWaybillComponent } from './waybill/create-waybill/create-waybill.component';
import { EditWaybillComponent } from './waybill/edit-waybill/edit-waybill.component';
import { IndexWaybillComponent } from './waybill/index-waybill/index-waybill.component';
import { OperationTabComponent } from './operation/operation-tab/operation-tab.component';
import { IndexOperationManagerComponent } from './index-operation-manager/index-operation-manager.component';
import { IndexOperationComponent } from './operation/index-operation/index-operation.component';
import { OperationStepOneComponent } from './operation-step-one/operation-step-one.component';
import { OperationStepTwoComponent } from './operation-step-two/operation-step-two.component';
import { OperationStepThreeComponent } from './operation-step-three/operation-step-three.component';
import { OperationStepFiveComponent } from './operation-step-five/operation-step-five.component';
import { OperationStepSixComponent } from './operation-step-six/operation-step-six.component';
import { OperationStepFourComponent } from './operation-step-four/operation-step-four.component';
import { OperationContainerComponent } from './operation-container/operation-container.component';
import { IndexTruckComponent } from './truck/index-truck/index-truck.component';
import { EditTruckComponent } from './truck/edit-truck/edit-truck.component';
import { IndexCargoComponent } from './cargo/index-cargo/index-cargo.component';
import { EditCargoComponent } from './cargo/edit-cargo/edit-cargo.component';
import { IndexExpenseComponent } from './expense/index-expense/index-expense.component';
import { EditExpenseComponent } from './expense/edit-expense/edit-expense.component';
import { IndexGatePassComponent } from './gate-pass/index-gate-pass/index-gate-pass.component';
import { EditGatePassComponent } from './gate-pass/edit-gate-pass/edit-gate-pass.component';
import { IndexGoodRemovalComponent } from './good-removal/index-good-removal/index-good-removal.component';
import { EditGoodRemovalComponent } from './good-removal/edit-good-removal/edit-good-removal.component';
import { OperationCargoComponent } from './operation-cargo/operation-cargo.component';
import { OperationTruckComponent } from './operation-truck/operation-truck.component';
import { OperationExpenseComponent } from './operation-expense/operation-expense.component';
import { OperationGatePassComponent } from './operation-gate-pass/operation-gate-pass.component';
import { OperationWaybillComponent } from './operation-waybill/operation-waybill.component';
import { OperationGoodRemovalComponent } from './operation-good-removal/operation-good-removal.component';

@NgModule({
  declarations: [
    IndexOperationManagerComponent,
    IndexOperationComponent,
    CreateOperationComponent,
    EditOperationComponent,
    CreateContainerComponent,
    EditContainerComponent,
    IndexContainerComponent,
    CreateWaybillComponent,
    EditWaybillComponent,
    IndexWaybillComponent,
    OperationTabComponent,
    IndexOperationManagerComponent,
    OperationStepOneComponent,
    OperationStepTwoComponent,
    OperationStepThreeComponent,
    OperationStepFourComponent,
    OperationStepFiveComponent,
    OperationStepSixComponent,
    OperationContainerComponent,
    IndexTruckComponent,
    EditTruckComponent,
    IndexCargoComponent,
    EditCargoComponent,
    IndexExpenseComponent,
    EditExpenseComponent,
    IndexGatePassComponent,
    EditGatePassComponent,
    IndexGoodRemovalComponent,
    EditGoodRemovalComponent,
    OperationCargoComponent,
    OperationTruckComponent,
    OperationExpenseComponent,
    OperationGatePassComponent,
    OperationWaybillComponent,
    OperationGoodRemovalComponent,
  ],
  imports: [
    CommonModule,
    OperationManagerRoutingModule,
    //DropDownTreeModule,
    NbButtonModule,
    NbIconModule,
    NbAlertModule,
    NbInputModule,
    NbSelectModule,
    NbCardModule,
    NbIconModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SmartTableModule,
    NbTreeGridModule,
    NbTabsetModule,
    NbDialogModule.forChild(),
    NbToggleModule,
    NbTooltipModule,
    SharedModule,
    CoreModule,
    NbMenuModule,
    NbStepperModule,
    NbLayoutModule,
    //ILocalizationModule
  ],
  providers: []
})
export class OperationManagerModule { }
