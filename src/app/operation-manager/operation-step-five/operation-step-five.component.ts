import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgentService } from 'src/app/contact-manager/services/agent.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Product } from 'src/app/product-manager/models/product-model';
import { ProductService } from 'src/app/product-manager/services/product.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { OPERATION_STATUS } from 'src/app/shared/OperationStatus';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Expense } from '../models/operation-model';
import { ExpenseService } from '../services/expense.service';
@Component({
  selector: 'operation-step-five',
  templateUrl: './operation-step-five.component.html',
  styleUrls: ['./operation-step-five.component.scss']
})
export class OperationStepFiveComponent implements OnInit {
  @Input() id : string;
  expense_id:string;
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  createExpenseMode = false;
  editExpenseMode = false;

  fifthForm: FormGroup;
  expenseModel : FormGroup;
  categories : Lookup[];
  agents : Lookup[];
  types : Lookup[];
  loadingTypes : Lookup[];
  expenses: Expense[];
  products : Product[];
  currentStep = 5;
  nextStep = this.currentStep + 1;
  //steps = OPERATION_STATUS.filter(t => t.id <= this.nextStep && t.id > 1);
  steps = OPERATION_STATUS;

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: OperationService,
    private lookupService: LookupService,
    private agentService : AgentService,
    private productService:ProductService,
    private expenseService:ExpenseService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false;
    
    this.populateData();

    this.GetAllExpenses();
    this.expenseModel = this.fb.group({
      product_id: [''],
      reference_no: [''],
      amount: 0,
    });
    
    this.productService.type = 'all';
    this.productService.GetAll().then((res) => {
      this.products = res.data;
    });

    this.fifthForm = this.fb.group({
      status: this.currentStep
    });
    
    this.populateData();

  }

  populateData() {

    this.modelService.GetById(this.id).subscribe(data => {
      this.fifthForm.setValue({
        status : data.status,
      });
    });

  }

  GetAllExpenses(){
    this.expenseService.Query('operation_id',this.id).then((res) => {
      this.expenses = res.data;
    });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  
  deleteExpenseItem(id:string) {
    this.expenseService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!");
      this.GetAllExpenses();
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
clearForm(){
  
  this.createExpenseMode = true;
  this.editExpenseMode = false;

  this.expenseModel = this.fb.group({
    operation_id:[this.id,Validators.required],
    product_id: ['', Validators.required],
    reference_no: ['', Validators.required],
    amount: 0,
  });
}
  createExpenseClicked(){
    this.clearForm();
  }  

  editExpenseClicked(expense:Expense) {
    this.createExpenseMode = false;
    this.editExpenseMode = true;
    this.expense_id = expense.id;
    this.expenseModel = this.fb.group({
      id: [expense.id],
      operation_id: [expense.operation_id, Validators.required],
      product_id: [expense.product_id, Validators.required],
      reference_no: [expense.reference_no],
      amount: [expense.amount, Validators.required]
    })
  }

  createExpense()
  {
    if (this.expenseModel.valid) 
    {
      this.expenseService.Create(this.expenseModel.value).then((res) => {
        //this.ngOnInit();
        this.expenseModel.markAsDirty();
        this.notificationService.showToastr("success", "Record created successfully!");
        this.expenseModel.reset();
        this.clearForm();
        this.GetAllExpenses();
      }, err => {
        this.notificationService.showToastr("danger", "Exception: Unknown Error occurred!");
      });
    } 
    else 
    {
      this.submitted = true;
      this.notificationService.showToastr("danger", "Exception: Invalid Data!");
    }
  }
  editExpense(){
    if (this.expenseModel.valid) 
    {
      this.expenseService.Edit(this.expense_id, this.expenseModel.value).then((res) => {
        //this.ngOnInit();
        this.expenseModel.markAsDirty();
        this.notificationService.showToastr("success", "Record created successfully!");
        
        this.editExpenseMode = false;
        this.GetAllExpenses();
      }, err => {
        this.notificationService.showToastr("danger", "Exception: Unknown Error occurred!");
      });
    } 
    else 
    {
      this.submitted = true;
      this.notificationService.showToastr("danger", "Exception: Invalid Data!");
    }
  }

  closeExpenseModes() {
    this.createExpenseMode = this.editExpenseMode = this.submitted = false;
  }
  
  
  
  NextStep(){
  
    this.fifthForm.markAsDirty();
    let nextStep = this.nextStep;
    let statusControl = this.fifthForm.get('status');
    if(statusControl != null)
      nextStep = statusControl.value;

      if(nextStep != this.currentStep){
        this.modelService.ToggleStatus(this.id, nextStep).then((res) => {
          window.location.reload();
          this.notificationService.showToastr("success", "Operation Status Updated successfully!");
        }, err => {
          this.notificationService.showToastr("danger", "Unknown Error occurred!")
        });
      }
  }

}