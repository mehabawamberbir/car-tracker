import { Component, OnInit,Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OPERATION_STATUS } from 'src/app/shared/OperationStatus';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { OperationService } from '../services/operation.service';

@Component({
  selector: 'operation-step-four',
  templateUrl: './operation-step-four.component.html',
  styleUrls: ['./operation-step-four.component.scss']
})
export class OperationStepFourComponent implements OnInit {
  @Input() id : string;
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;
  selectedIndex: number;
  model: FormGroup;
  currentStatus : number = 4;
  currentStep = 4;
  nextStep = 5;
  fourthsteps = OPERATION_STATUS;
  
  fourthForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: OperationService
  ) { }

  ngOnInit(): void {

    this.fourthForm = this.fb.group({
      status: this.currentStatus,
    });

    this.GetOperationDetail();
  }

  GetOperationDetail(){
    this.modelService.GetById(this.id).subscribe(data => {
      this.currentStatus = data.status;    
      this.fourthForm.setValue({        
        status : this.currentStatus,
      });

    });
  }

  NextStep(){
  
    this.fourthForm.markAsDirty();
    let nextStep = this.nextStep;
    let statusControl = this.fourthForm.get('status');
    if(statusControl != null)
      nextStep = statusControl.value;

      if(nextStep != this.currentStep){
        this.modelService.ToggleStatus(this.id, nextStep).then((res) => {
          window.location.reload();
          this.notificationService.showToastr("success", "Operation Status Updated successfully!");
        }, err => {
          this.notificationService.showToastr("danger", "Unknown Error occurred!")
        });
      }
  }

}
