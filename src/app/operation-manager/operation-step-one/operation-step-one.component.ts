import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Consignee, Customer } from 'src/app/contact-manager/contact-models';
import { AgentService } from 'src/app/contact-manager/services/agent.service';
import { ConsigneeService } from 'src/app/contact-manager/services/consignee.service';
import { CustomerService } from 'src/app/contact-manager/services/customer.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Product } from 'src/app/product-manager/models/product-model';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
@Component({
  selector: 'operation-step-one',
  templateUrl: './operation-step-one.component.html',
  styleUrls: ['./operation-step-one.component.scss']
})
export class OperationStepOneComponent implements OnInit {
  @Input() categoryId : string;
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  categories : Lookup[];
  agents : Lookup[];
  loadingTypes : Lookup[];
  products : Product[];
  consignees : Consignee[];
  customers : Customer[];
  firstForm: FormGroup;
  showTotalVehicleCBM : boolean;
  showContainerNo : boolean;
  secondForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: OperationService,
    private lookupService: LookupService,
    private agentService : AgentService,
    private customerService: CustomerService,
    private consigneeService:ConsigneeService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false;


    this.showTotalVehicleCBM = false;
    this.showContainerNo = false;

    this.firstForm = this.fb.group({
      category_id: this.categoryId,
      operation_no: [''],
      reference_no: [''],
      consignee_id: [''],
      freight_forwarder_id: [''],
      reception_date: [''],
      registration_date: [''],
      booking_no: [''],
      atd_operation:[''],
      esl_operation:[''],
      bol: [''],
      quantity: 1,
      gross_weight: 0,
      partial_shipment : [''],
      container_status: [''],
      loading_type_id: [''],
      total_vehicle:0,
      total_cbm:0,
      total_containers_20:0,
      total_containers_40:0,
      destination: [''],
      agent_id: [''],
      arrival_date: [''],
      description:[''],
      attachement_file:[''],
      status: 2
    });


    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.OPERATION_CATEGORY).then((res) => {
      this.categories = res.data;
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.LOADING_TYPE).then((res) => {
      this.loadingTypes = res.data;
    });

    this.agentService.GetAll().then((res) => {
      this.agents = res.data;
    });

    this.customerService.GetAll().then((res) => {
      this.customers = res.data;
    });

    this.consigneeService.GetAll().then((res) => {
      this.consignees = res.data;
    });

    this.populateData();
  }

  populateData() {

    this.secondForm.setValue({
      category_id: this.categoryId,
      operation_no: [''],
      reference_no: [''],
      consignee_id: [''],
      freight_forwarder_id: [''],
      reception_date: [''],
      registration_date: [''],
      booking_no: [''],
      atd_operation:[''],
      esl_operation:[''],
      bol: [''],
      quantity: 1,
      gross_weight: 0,
      partial_shipment : [''],
      container_status: [''],
      loading_type_id: [''],
      total_vehicle:0,
      total_cbm:0,
      total_containers_20:0,
      total_containers_40:0,
      destination: [''],
      agent_id: [''],
      arrival_date: [''],
      description:[''],
      attachement_file:[''],
      status: 2
    });

  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  
  SaveChangesStepOne() {
    if (this.firstForm.valid) 
    {
      this.modelService.Create(this.firstForm.value).then((res) => {
        this.firstForm.markAsDirty();
        this.notificationService.showToastr("success", "Record created successfully!");
      }, err => {
        this.notificationService.showToastr("danger", "Unknown Error occurred!")
      });
    } 
    else 
    {
      this.submitted = true;
      
      this.notificationService.showToastr("danger", "Exception, Invalid Form Input!")
    }
  }
  LoadingTypeChanged(e:any){
    //console.log("Changed");
    let value = e.target.value;
    let loadType = e.target.options[e.target.options.selectedIndex].text;    
    
    this.showTotalVehicleCBM = loadType == 'Car Carrier' || loadType == 'Self Drive' ||
                               loadType == 'Truck/Lowbed' || loadType == 'Self Drive';
    this.showContainerNo = loadType == 'With Container' || loadType == 'Without Container';
  }

  onFileSelect(event:any){
    // if (event.target.files.length > 0) {
    //   const file = event.target.files[0];
    //   if(this.firstForm.get('attachement_file') != null){        
    //     this.firstForm.get('attachement_file')?.setValue(file);
    //   }
    // }
  }

}