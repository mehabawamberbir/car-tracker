import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Invoice } from 'src/app/invoice-manager/models/invoice.models';
import { InvoiceService } from 'src/app/invoice-manager/services/invoice.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Product } from 'src/app/product-manager/models/product-model';
import { INVOICE_STATUS } from 'src/app/shared/InvoiceStatus';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { OPERATION_STATUS } from 'src/app/shared/OperationStatus';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'operation-step-six',
  templateUrl: './operation-step-six.component.html',
  styleUrls: ['./operation-step-six.component.scss']
})
export class OperationStepSixComponent implements OnInit {
  @Input() id:string;
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  createInvoiceMode = false;
  editInvoiceMode = false;
  
  invoiceModel : FormGroup;
  categories : Lookup[];
  actors : Lookup[];
  types : Lookup[];
  invoices: Invoice[];
  products : Product[];
  currentStep = 5;
  nextStep = this.currentStep + 1;
  //steps = OPERATION_STATUS.filter(t => t.id <= this.nextStep && t.id > 1);
  steps = OPERATION_STATUS;

  sixthForm: FormGroup;

  constructor(
    private router:Router,
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: OperationService,
    private invoiceService : InvoiceService,
    private lookupService: LookupService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false;

    this.sixthForm = this.fb.group({
      operation_id: [this.id],
      status:[this.currentStep]
    });

    console.log("id:" + this.id);

    this.invoiceService.Query('operation_id',this.id).then((res) => {
      this.invoices = res.data;
    });

  }

  closeSuccessAlert() {
    this.added = false
  }

  closeErrorAlert() {
    this.error = false
  }

  editInvoiceItem(item:Invoice){

  }

  
  deleteInvoiceItem(id:string) {
    this.invoiceService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!");
      this.invoiceService.Query('operation_id',this.id).then((res) => {
        this.invoices = res.data;
      });
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }

  createInvoiceClicked(){
    this.createInvoiceMode = true;
    this.editInvoiceMode = false;
    this.invoiceModel = this.fb.group({
      
    });
  }  
  createInvoice(){

  }
  editInvoice(){

  }

  closeInvoiceModes() {
    this.createInvoiceMode = this.editInvoiceMode = this.submitted = false;
  }

  
  
  GetInvoiceStatus(status:number){
    return INVOICE_STATUS.find(t => t.id == status)?.name;
  }

  print(obj:Invoice) {
    this.router.navigate(['/report-manager/invoice-report/'+ obj.id]);
  }
  
  NextStep(){
  
    this.sixthForm.markAsDirty();
    let nextStep = this.nextStep;
    let statusControl = this.sixthForm.get('status');
    if(statusControl != null)
      nextStep = statusControl.value;

      if(nextStep != this.currentStep){
        this.modelService.ToggleStatus(this.id, nextStep).then((res) => {
          window.location.reload();
          this.notificationService.showToastr("success", "Operation Status Updated successfully!");
        }, err => {
          this.notificationService.showToastr("danger", "Unknown Error occurred!")
        });
      }
  }

}