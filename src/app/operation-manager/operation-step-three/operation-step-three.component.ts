import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Product } from 'src/app/product-manager/models/product-model';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { OPERATION_STATUS } from 'src/app/shared/OperationStatus';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { OperationProcessService } from '../services/operation-process.service';
@Component({
  selector: 'operation-step-three',
  templateUrl: './operation-step-three.component.html',
  styleUrls: ['./operation-step-three.component.scss']
})
export class OperationStepThreeComponent implements OnInit {
  @Input() id : string;
  @Input() operation_no:string;
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  categories : Lookup[];
  actors : Lookup[];
  types : Lookup[];
  products : Product[];
  currentStep = 3;
  nextStep = 4;
  //steps = OPERATION_STATUS.filter(t => t.id <= this.nextStep && t.id > 1);
  steps = OPERATION_STATUS;
  thirdForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: OperationService,
    private operationProcessService:OperationProcessService,
    private lookupService: LookupService
    ) { }

  ngOnInit(): void {
    this.added = false;
    this.error = false;
    this.submitted = false;

    this.thirdForm = this.fb.group({
      operation_id : [''],
      operation_no : [''],
      declaration_no : [''],
      declaration_date : [''],
      vessel_name : [''],
      voyage_no : [''],
      partial_shipment : [''],
      container_status : [''],
      stuffing_date : [''],
      gross_weight : [''],
      net_weight : [''],
      description : [''],
      declaration_no_eth : [''],
      assigned_no_declarant : [''],
      assigned_no_importer : [''],
      border_office : [''],
      clearance_office : [''],
      fumigation : [''],
      weight_inspection : [''],
      inspection : [''],
      craft_paper : [''],
      status: 3
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.OPERATION_CATEGORY).then((res) => {
      this.categories = res.data;
      this.types=res.data;
    })

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.OPERATION_CATEGORY).then((res) => {
      this.actors = res.data;
    });

    this.populateData(this.id);
  }
  
  
  populateData(id:string) {
    let operationProcess = this.operationProcessService.Query('operation_id',this.id);
    operationProcess.then(res => {
      if(res.data != null && res.data.length > 0)
      {        
        let data = res.data[0];
        this.thirdForm.setValue({
          operation_id : data.operation_id,
          declaration_no : data.declaration_no,
          operation_no : data.operation.operation_no,
          declaration_date : data.declaration_date,
          vessel_name : data.vessel_name,
          voyage_no : data.voyage_no,
          partial_shipment : data.partial_shipment,
          container_status : data.container_status,
          stuffing_date : data.stuffing_date,
          gross_weight : data.gross_weight,
          net_weight : data.net_weight,
          description : data.description,
          declaration_no_eth : data.declaration_no_eth,
          assigned_no_declarant : data.assigned_no_declarant,
          assigned_no_importer : data.assigned_no_importer,
          border_office : data.border_office,
          clearance_office : data.clearance_office,
          fumigation : data.fumigation,
          weight_inspection : data.weight_inspection,
          inspection : data.inspection,
          craft_paper : data.craft_paper,
          status : data.operation.status,
        });
      }
      else
      {        
            this.modelService.GetById(this.id).subscribe(data => {
                this.thirdForm.setValue({
                  operation_id : data.id,
                  operation_no : data.operation_no,
                  declaration_no : '',
                  declaration_date : '',
                  vessel_name : '',
                  voyage_no : '',
                  partial_shipment : '',
                  container_status : '',
                  stuffing_date : '',
                  gross_weight : 0,
                  net_weight : 0,
                  description : '',
                  declaration_no_eth : '',
                  assigned_no_declarant : '',
                  assigned_no_importer : '',
                  border_office : '',
                  clearance_office : '',
                  fumigation : 0,
                  weight_inspection : 0,
                  inspection : 0,
                  craft_paper : 0,
                  status : data.status,
                });
            });   
      }
    });

  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  
  SaveChangesStepThree() {
    
    if (this.thirdForm.valid) {
      this.operationProcessService.Create(this.thirdForm.value).then((res) => {
        this.ngOnInit();
        this.thirdForm.markAsDirty();
        this.notificationService.showToastr("success", "Record Updated successfully!");
        window.location.reload();
      }, err => {
        this.notificationService.showToastr("danger", "Unknown Error occurred!")
      })
    } else {
      this.submitted = true
    }
  }

  
  
  
  
  
  
  NextStep(){
  
    this.thirdForm.markAsDirty();
    let nextStep = this.nextStep;
    let statusControl = this.thirdForm.get('status');
    if(statusControl != null)
      nextStep = statusControl.value;

      if(nextStep != this.currentStep){
        this.modelService.ToggleStatus(this.id, nextStep).then((res) => {
          window.location.reload();
          this.notificationService.showToastr("success", "Operation Status Updated successfully!");
        }, err => {
          this.notificationService.showToastr("danger", "Unknown Error occurred!")
        });
      }
  }


}