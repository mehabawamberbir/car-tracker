import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Consignee, Customer } from 'src/app/contact-manager/contact-models';
import { AgentService } from 'src/app/contact-manager/services/agent.service';
import { ConsigneeService } from 'src/app/contact-manager/services/consignee.service';
import { CustomerService } from 'src/app/contact-manager/services/customer.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Product } from 'src/app/product-manager/models/product-model';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { OPERATION_STATUS } from 'src/app/shared/OperationStatus';
import { NotificationService } from 'src/app/shared/services/notification.service';
@Component({
  selector: 'operation-step-two',
  templateUrl: './operation-step-two.component.html',
  styleUrls: ['./operation-step-two.component.scss']
})
export class OperationStepTwoComponent implements OnInit {
  @Input() id : string;

  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  categories : Lookup[];
  agents : Lookup[];
  loadingTypes : Lookup[];
  products : Product[];
  consignees : Consignee[];
  customers : Customer[];
  showTotalVehicleCBM : boolean;
  showContainerNo : boolean;
  secondForm: FormGroup;
  currentStep = 2;
  nextStep = this.currentStep + 1;
  //steps = OPERATION_STATUS.filter(t => t.id <= this.nextStep && t.id > 1);
  steps = OPERATION_STATUS;

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: OperationService,
    private lookupService: LookupService,
    private agentService : AgentService,
    private customerService: CustomerService,
    private consigneeService:ConsigneeService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false;

    this.showTotalVehicleCBM = false;
    this.showContainerNo = false;
    


    this.populateData(this.id);

    this.secondForm = this.fb.group({
      id:[''],
      category_id: [''],
      operation_no: [''],
      reference_no: [''],
      consignee_id: [''],
      freight_forwarder_id: [''],
      reception_date: [''],
      registration_date: [''],
      booking_no: [''],
      atd_operation:[''],
      esl_operation:[''],
      bol: [''],
      quantity: 1,
      gross_weight: 0,
      partial_shipment: [''],
      container_status: [''],
      loading_type_id: [''],
      total_vehicle:0,
      total_cbm:0,
      total_containers_20:0,
      total_containers_40:0,
      destination: [''],
      agent_id: [''],
      arrival_date: [''],
      description:[''],
      status: 3
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.OPERATION_CATEGORY).then((res) => {
      this.categories = res.data;
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.LOADING_TYPE).then((res) => {
      this.loadingTypes = res.data;
    });

    this.agentService.GetAll().then((res) => {
      this.agents = res.data;
    });

    this.customerService.GetAll().then((res) => {
      this.customers = res.data;
    });

    this.consigneeService.GetAll().then((res) => {
      this.consignees = res.data;
    });

  }
  
  
  populateData(id:string) {

    this.modelService.GetById(this.id).subscribe(data => {
      this.secondForm.setValue({
        id : this.id,
        category_id : data.category_id,
        operation_no : data.operation_no,
        reference_no : data.reference_no,
        consignee_id : data.consignee_id,
        freight_forwarder_id : data.freight_forwarder_id,
        reception_date : data.reception_date,
        registration_date : data.registration_date,
        atd_operation : data.atd_operation,
        esl_operation : data.esl_operation,
        bol : data.bol,
        booking_no : data.booking_no,
        quantity : data.quantity,
        partial_shipment:data.partial_shipment,
        container_status:data.container_status,
        gross_weight : data.gross_weight,
        loading_type_id : data.loading_type_id,
        total_vehicle:data.total_vehicle,
        total_cbm:data.total_cbm,
        total_containers_20:data.total_containers_20,
        total_containers_40:data.total_containers_40,
        destination : data.destination,
        agent_id : data.agent_id,
        arrival_date : data.arrival_date,
        description : data.description,
        status : data.status,
      });
      var loadType = data.loading_type.name;
      // console.log(loadType);
      this.showTotalVehicleCBM = loadType == 'Car Carrier' || loadType == 'Self Drive' ||
                                 loadType == 'Truck/Lowbed' || loadType == 'Self Drive';
      this.showContainerNo = loadType == 'With Container' || loadType == 'Without Container';
    });

  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  
  SaveChangesStepTwo() {
    
    if (this.secondForm.valid) {
      this.modelService.Edit(this.id, this.secondForm.value).then((res) => {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      }, err => {
        this.notificationService.showToastr("danger", "Unknown Error occurred!")
      })
    } else {
      this.submitted = true
    }
  }
  
  NextStep(){
  
    this.secondForm.markAsDirty();
    let nextStep = this.nextStep;
    let statusControl = this.secondForm.get('status');
    if(statusControl != null)
      nextStep = statusControl.value;

    if(nextStep != this.currentStep){
      this.modelService.ToggleStatus(this.id, nextStep).then((res) => {
        window.location.reload();
        this.notificationService.showToastr("success", "Operation Status Updated successfully!");
      }, err => {
        this.notificationService.showToastr("danger", "Unknown Error occurred!")
      });
    }
  }

  
  LoadingTypeChanged(e:any){
    //console.log("Changed");
    let value = e.target.value;
    let loadType = e.target.options[e.target.options.selectedIndex].text;    
    
    this.showTotalVehicleCBM = loadType == 'Car Carrier' || loadType == 'Self Drive' ||
                               loadType == 'Truck/Lowbed' || loadType == 'Self Drive';
    this.showContainerNo = loadType == 'With Container' || loadType == 'Without Container';
  }

}
