import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbWindowService } from '@nebular/theme';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Truck } from '../models/operation-model';
import { TruckService } from '../services/truck.service';
import { EditTruckComponent } from '../truck/edit-truck/edit-truck.component';

@Component({
  selector: 'operation-truck',
  templateUrl: './operation-truck.component.html',
  styleUrls: ['./operation-truck.component.scss']
})
export class OperationTruckComponent implements OnInit {
  @Input() id : string;
  @Input() operation_no:string;
  container_id:string;

  model: FormGroup;
  types : Lookup[];
  trucks: Truck[];

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private lookupService: LookupService,
    private truckService: TruckService,
    private windowService : NbWindowService
    ) { }

  ngOnInit(): void {
    
    this.populateData();

    this.GetAll();
    this.model = this.fb.group({
      operation_id: this.id,
    });
    
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONTAINER_TYPE).then((res) => {
      this.types = res.data;
    });

  }

  populateData() {


  }

  GetAll(){
    this.truckService.Query('operation_id',this.id).then((res) => {
      this.trucks = res.data;
    });
  }
  
  deleteItem(id:string) {
    this.truckService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!");
      this.GetAll();
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
  createClicked(){
    this.windowService.open(EditTruckComponent, { title: 'Create Truck', 
     context : { isModal : true, operation_id : this.id, operation_no : this.operation_no }});
  }  

  editClicked(obj:Truck) {
    this.windowService.open(EditTruckComponent,  { title: 'Edit Truck', context:{ isModal : true, operation_id : this.id, id: obj.id}});
  }

}