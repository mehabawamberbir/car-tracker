import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbWindowService } from '@nebular/theme';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Waybill } from '../models/operation-model';
import { WaybillService } from '../services/waybill.service';
import { EditWaybillComponent } from '../waybill/edit-waybill/edit-waybill.component';

@Component({
  selector: 'operation-waybill',
  templateUrl: './operation-waybill.component.html',
  styleUrls: ['./operation-waybill.component.scss']
})
export class OperationWaybillComponent implements OnInit {
  @Input() id : string;
  @Input() operation_no:string;
  container_id:string;

  model: FormGroup;
  types : Lookup[];
  waybills: Waybill[];

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private lookupService: LookupService,
    private waybillService: WaybillService,
    private windowService : NbWindowService
    ) { }

  ngOnInit(): void {
    
    this.populateData();

    this.GetAll();
    this.model = this.fb.group({
      operation_id: this.id,
    });
    
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CONTAINER_TYPE).then((res) => {
      this.types = res.data;
    });

  }

  populateData() {


  }

  GetAll(){
    this.waybillService.Query('operation_id',this.id).then((res) => {
      this.waybills = res.data;
    });
  }
  
  deleteItem(id:string) {
    this.waybillService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!");
      this.GetAll();
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
  createClicked(){
    this.windowService.open(EditWaybillComponent, { title: 'Create Waybill', 
     context : { isModal : true, operation_id : this.id, operation_no : this.operation_no }});
  }  

  editClicked(obj:Waybill) {
    this.windowService.open(EditWaybillComponent,  { title: 'Edit Waybill', context:{ isModal : true, operation_id : this.id, id: obj.id}});
  }

}