import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Product } from 'src/app/product-manager/models/product-model';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Expense } from '../../models/operation-model';
@Component({
  selector: 'app-create-operation',
  templateUrl: './create-operation.component.html',
  styleUrls: ['./create-operation.component.scss']
})
export class CreateOperationComponent implements OnInit {
  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  categories : Lookup[];
  actors : Lookup[];
  types : Lookup[];

  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  fourthForm: FormGroup;
  fifthForm: FormGroup;
  sixthForm: FormGroup;
  seventhForm: FormGroup;
  categoryId : string;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: OperationService,
    private lookupService: LookupService
    ) { }

  ngOnInit(): void {
    this.added = false;
    this.error = false;
    this.submitted = false;
    
    this.categoryId = this.route.snapshot.paramMap.get("category_id")!;
    this.fourthForm = this.fb.group({
      fourthCtrl: [4],
    });
    
  }
  onFourthSubmit(){

  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

}