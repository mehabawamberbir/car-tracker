import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { Product } from 'src/app/product-manager/models/product-model';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { OPERATION_STATUS } from 'src/app/shared/OperationStatus';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Expense } from '../../models/operation-model';
@Component({
  selector: 'app-edit-operation',
  templateUrl: './edit-operation.component.html',
  styleUrls: ['./edit-operation.component.scss']
})
export class EditOperationComponent implements OnInit {
  added: boolean = false;
  operation_no:string;
  error: boolean = false;
  submitted: boolean = false;
  id : string;
  selectedIndex: number;
  model: FormGroup;
  categories : Lookup[];
  actors : Lookup[];
  types : Lookup[];
  currentStatus : number = 2;
  currentStep = 2;
  nextStep = 5;
  fourthsteps = OPERATION_STATUS;
  
  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  fourthForm: FormGroup;
  fifthForm: FormGroup;
  sixthForm: FormGroup;
  seventhForm: FormGroup;

  private _selectedActivityIndex: number; 
  @Input() get selectedActivityIndex() 
  { return this._selectedActivityIndex; } 
  set selectedActivityIndex(val) { this._selectedActivityIndex = val; }

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: OperationService,
    private lookupService: LookupService
    ) { }

  ngOnInit(): void {
    this.added = false;
    this.error = false;
    this.submitted = false;
        
    this.id = this.route.snapshot.paramMap.get("id")!;
    

    

    this.modelService.GetById(this.id).subscribe(data => {
      this.currentStatus = data.status;
      this.selectedActivityIndex = this.currentStatus - 1; 
      this.operation_no = data.operation_no;
    });

  }
  
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  stepCompleted(step:number){
    if(this.selectedActivityIndex >= step){
      return 'true';
    }
    return 'false;'
  }

}