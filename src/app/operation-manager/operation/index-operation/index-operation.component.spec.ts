import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexOperationComponent } from './index-operation.component';

describe('IndexOperationComponent', () => {
  let component: IndexOperationComponent;
  let fixture: ComponentFixture<IndexOperationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexOperationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
