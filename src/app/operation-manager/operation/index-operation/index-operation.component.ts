import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Operation } from '../../models/operation-model';
import { OPERATION_STATUS } from 'src/app/shared/OperationStatus';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';

@Component({
  selector: 'app-index-operation',
  templateUrl: './index-operation.component.html',
  styleUrls: ['./index-operation.component.scss']
})
export class IndexOperationComponent implements OnInit {
  operations: Operation[];
  url:string;
  categories : Lookup[];

  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: OperationService,
    private lookupService:LookupService
  ) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + "operation-manager/operations";

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.OPERATION_CATEGORY).then((res) => {
      this.categories = res.data;
    });
  }
  
  getAll() : void {

    

    this.modelService.GetAll().then(res => 
      {
        // this.operations = res.data;
        var newArray = res.data.filter(function (t:Operation)
        {
          return t.category != null && t.category.name.toUpperCase() == "Import Unimodal";
        }
        );  
      this.operations = newArray;
      },
      error => { console.log(error);}
      );
  }

  refreshList(): void {
    this.getAll();
  }

  newData(data:any) {
    this.operations = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.operations = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    
    this.operations = data;
    
  }
  edit(obj:Operation) {
    this.modelService.model = obj;
    this.router.navigate(['/operation-manager/operations/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    // this.modelService.ToggleStatus(id).then(res => {
    //   this.notificationService.showToastr("success", "Status updated successfully!")
    // }, err => {
    //   this.notificationService.showToastr("danger", "Failed to update the status!")
    // })
  }
  delete(id:string) {
    this.modelService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!")
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
  GetOperationStatus(status:number){
    return OPERATION_STATUS.find(t => t.id == status)?.name;
  }

  changeTab(event:any){
    //console.log(event.tabTitle);
    //this.operations = this.operations.filter(t => t.category != null && t.category.name == event.tabTitle);
    // var newArray = this.operations.filter(function (t)
    //     {
    //       return t.category != null && t.category.name == event.tabTitle;
    //     }
    // );  
    // this.operations = newArray;  
  }  
}