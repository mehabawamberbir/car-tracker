import { Injectable } from '@angular/core';
import { Cargo } from '../models/operation-model';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import axios from 'axios';
@Injectable({
  providedIn: 'root'
})
export class CargoService {

  model : Cargo;
  modelPath : string = "operation-manager/cargos";
  constructor(private repository: RepositoryService) { 

  }    

  GetAll() {
      return axios.get(this.repository.apiUrl + this.modelPath + '-index');
  }
  Get(skip:number, limit:number) {
      return axios.get(this.repository.apiUrl + this.modelPath + `?skip=${skip} && limit=${limit}`);
  }
  Query(attribute:string, value:string) {
      return axios.get(this.repository.apiUrl + this.modelPath + '/query/' + attribute + '/' + value);
  }
  GetById(id:string) {
      return this.repository.sendRequest("GET", this.repository.apiUrl + this.modelPath + '/' + id);
  }
  Create(model:Cargo) {
      return axios.post(this.repository.apiUrl + this.modelPath, model)
  }
  Edit(id:string, model:Cargo) {
      return axios.put(this.repository.apiUrl + this.modelPath + '/' + id, model)
  }
  Delete(id:string) {
      return axios.delete(this.repository.apiUrl + this.modelPath + '/' + id);
  }
  ToggleStatus(id:string) {
      return axios.patch(this.repository.apiUrl + this.modelPath + '/' + id + '/toggle-status');
  }
  GetAllCargos(){
      return axios.get(this.repository.apiUrl + 'cargos')
  }
  GetCargoById(id:string){
      return this.repository.sendRequest("GET", this.repository.apiUrl + "cargos" + '/' + id)
  }
}

