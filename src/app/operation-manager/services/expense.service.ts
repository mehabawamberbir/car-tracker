import { Injectable } from '@angular/core';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Expense } from '../models/operation-model';

import  axios  from 'axios';

@Injectable({
    providedIn: 'root'
})

export class ExpenseService {
    model : Expense;
    modelPath : string = "operation-manager/expenses";
    constructor(private repository: RepositoryService) { 

    }    

    GetAll() {
        return axios.get(this.repository.apiUrl + this.modelPath + '-index');
    }
    Get(skip:number, limit:number) {
        return axios.get(this.repository.apiUrl + this.modelPath + `?skip=${skip} && limit=${limit}`);
    }
    Query(attribute:string, value:string) {
        return axios.get(this.repository.apiUrl + this.modelPath + '/query/' + attribute + '/' + value);
    }
    GetById(id:string) {
        return this.repository.sendRequest("GET", this.repository.apiUrl + this.modelPath + '/' + id);
    }
    Create(model:Expense) {
        return axios.post(this.repository.apiUrl + this.modelPath, model)
    }
    Edit(id:string, model:Expense) {
        return axios.put(this.repository.apiUrl + this.modelPath + '/' + id, model)
    }
    Delete(id:string) {
        return axios.delete(this.repository.apiUrl + this.modelPath + '/' + id);
    }
    ToggleStatus(id:string) {
        return axios.patch(this.repository.apiUrl + this.modelPath + '/' + id + '/toggle-status');
    }
    GetAllExpenses(){
        return axios.get(this.repository.apiUrl + 'expenses')
    }
    GetExpenseById(id:string){
        return this.repository.sendRequest("GET", this.repository.apiUrl + "expenses" + '/' + id)
    }
}