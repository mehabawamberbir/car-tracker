import { Injectable } from '@angular/core';
import axios from 'axios';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { GoodRemoval } from '../models/operation-model';
@Injectable({
  providedIn: 'root'
})
export class GoodRemovalService {
  model : GoodRemoval;
  modelPath : string = "operation-manager/good-removals";
  constructor(private repository: RepositoryService) { 

  }    

  GetAll() {
      return axios.get(this.repository.apiUrl + this.modelPath + '-index');
  }
  Get(skip:number, limit:number) {
      return axios.get(this.repository.apiUrl + this.modelPath + `?skip=${skip} && limit=${limit}`);
  }
  Query(attribute:string, value:string) {
      return axios.get(this.repository.apiUrl + this.modelPath + '/query/' + attribute + '/' + value);
  }
  GetById(id:string) {
      return this.repository.sendRequest("GET", this.repository.apiUrl + this.modelPath + '/' + id);
  }
  Create(model:GoodRemoval) {
      return axios.post(this.repository.apiUrl + this.modelPath, model)
  }
  Edit(id:string, model:GoodRemoval) {
      return axios.put(this.repository.apiUrl + this.modelPath + '/' + id, model)
  }
  Delete(id:string) {
      return axios.delete(this.repository.apiUrl + this.modelPath + '/' + id);
  }
  ToggleStatus(id:string) {
      return axios.patch(this.repository.apiUrl + this.modelPath + '/' + id + '/toggle-status');
  }
  GetAllGatePasses(){
      return axios.get(this.repository.apiUrl + 'good-removals')
  }
  GetGatePassesById(id:string){
      return this.repository.sendRequest("GET", this.repository.apiUrl + "good-removals" + '/' + id)
  }
}
