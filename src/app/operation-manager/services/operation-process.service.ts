import { Injectable } from '@angular/core';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Operation, OperationProcess } from '../models/operation-model';

import  axios  from 'axios';

@Injectable({
    providedIn: 'root'
})

export class OperationProcessService {
    model : OperationProcess;
    modelPath : string = "operation-manager/operation-processes";
    constructor(private repository: RepositoryService) { 

    }    

    GetAll() {
        return axios.get(this.repository.apiUrl + this.modelPath + '-index');
    }
    Get(skip:number, limit:number) {
        return axios.get(this.repository.apiUrl + this.modelPath + `?skip=${skip} && limit=${limit}`);
    }
    GetById(id:string) {
        return this.repository.sendRequest("GET", this.repository.apiUrl + this.modelPath + '/' + id);
    }
    GetById2(id:string) {
        return axios.get(this.repository.apiUrl + this.modelPath + '/' + id);
    }
    Query(attribute:string, value:string) {
        return axios.get(this.repository.apiUrl + this.modelPath + '/query/' + attribute + '/' + value);
    }
    Create(model:Operation) {
        return axios.post(this.repository.apiUrl + this.modelPath, model)
    }
    Edit(id:string, model:Operation) {
        return axios.put(this.repository.apiUrl + this.modelPath + '/' + id, model)
    }
    Delete(id:string) {
        return axios.delete(this.repository.apiUrl + this.modelPath + '/' + id);
    }
}