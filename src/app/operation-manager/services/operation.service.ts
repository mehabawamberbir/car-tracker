import { Injectable } from '@angular/core';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { Operation } from '../models/operation-model';

import  axios  from 'axios';

@Injectable({
    providedIn: 'root'
})

export class OperationService {
    model : Operation;
    modelPath : string = "operation-manager/operations";
    categoryId: string;
    constructor(private repository: RepositoryService) { 

    }    

    GetAll() {
        return axios.get(this.repository.apiUrl + this.modelPath + '-index');
    }
    Get(skip:number, limit:number) {
        return axios.get(this.repository.apiUrl + this.modelPath + `?type=${this.categoryId}&&skip=${skip} && limit=${limit}`);
    }
    GetById(id:string) {
        return this.repository.sendRequest("GET", this.repository.apiUrl + this.modelPath + '/' + id);
    }
    GetById2(id:string) {
        return axios.get(this.repository.apiUrl + this.modelPath + '/' + id);
    }
    Create(model:any) {
        return axios.post(this.repository.apiUrl + this.modelPath, model)
    }
    Edit(id:string, model:any) {
        return axios.put(this.repository.apiUrl + this.modelPath + '/' + id, model)
    }
    Delete(id:string) {
        return axios.delete(this.repository.apiUrl + this.modelPath + '/' + id);
    }
    ToggleStatus(id:string, status:number) {
        return axios.patch(this.repository.apiUrl + this.modelPath + '/' + id + '/toggle-status/' + status);
    }
}