import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexTruckComponent } from './index-truck.component';

describe('IndexTruckComponent', () => {
  let component: IndexTruckComponent;
  let fixture: ComponentFixture<IndexTruckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexTruckComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexTruckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
