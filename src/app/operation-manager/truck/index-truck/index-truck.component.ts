import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { TruckService } from '../../services/truck.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Truck } from '../../models/operation-model';
import { Lookup } from 'src/app/master-data/master.model';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { LookupService } from 'src/app/master-data/service/lookup.service';

@Component({
  selector: 'app-index-truck',
  templateUrl: './index-truck.component.html',
  styleUrls: ['./index-truck.component.scss']
})
export class IndexTruckComponent implements OnInit {

  trucks: Truck[];
  locations:Lookup[];
  url:string;
  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: TruckService
  ) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath + "?";
    this.getAll();
  }
  
  getAll() : void {
    this.modelService.GetAll().then(res => 
      {
        this.trucks = res.data;
      },
      error => { console.log(error);}
      );
  }

  refreshList(): void {
    this.getAll();
  }

  newData(data : any) {
    this.trucks = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data : any) {
    this.trucks = data;
    this.sortService.sortData = data;
  }
  sortedData(data : any) {
    
    this.trucks = data;
    
  }
  edit(obj:Truck) {
    this.modelService.model = obj;
    this.router.navigate(['/operation-manager/trucks/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id).then(res => {
      this.notificationService.showToastr("success", "Status updated successfully!")
    }, err => {
      this.notificationService.showToastr("danger", "Failed to update the status!")
    })
  }
  delete(id:string) {
    this.modelService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!")
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }

}
