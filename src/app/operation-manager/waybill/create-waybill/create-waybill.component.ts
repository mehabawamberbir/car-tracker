import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WaybillService } from 'src/app/operation-manager/services/waybill.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';

import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Operation } from '../../models/operation-model';
import { Consignee, Customer } from 'src/app/contact-manager/contact-models';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { ConsigneeService } from 'src/app/contact-manager/services/consignee.service';
import { CustomerService } from 'src/app/contact-manager/services/customer.service';

@Component({
  selector: 'app-create-waybill',
  templateUrl: './create-waybill.component.html',
  styleUrls: ['./create-waybill.component.scss']
})
export class CreateWaybillComponent implements OnInit {
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  operations: Operation[];
  consignees : Consignee[];
  freightForwarders : Customer[];

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: WaybillService,
    private lookupService: LookupService,
    private consigneeService: ConsigneeService,
    private customerService : CustomerService,
    private operationService: OperationService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
      operation_id: ['', [Validators.required]],
      truck_no: [''],
      departure_date: [''],
      destination: [''],
      driver_name: [''],
      driver_tel: [''],
      vessel: [''],
      transporter: [''],
      rate_agree: ['', [Validators.required]],
      clearance_office: [''],
      remarks: [''],
      location: ['']
    })

    this.operationService.GetAll().then((res) => {
      this.operations = res.data;
    })

    this.consigneeService.GetAll().then((res) => {
      this.consignees = res.data;
    })
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value).then((res) => {
        this.ngOnInit()
        this.notificationService.showToastr("success", "Record created successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Unknown Error occurred!")
      })
    } 
    else {
      this.submitted = true
    }
  }

}