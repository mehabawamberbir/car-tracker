import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WaybillService } from 'src/app/operation-manager/services/waybill.service';

import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { Lookup } from 'src/app/master-data/master.model';
import { Operation } from '../../models/operation-model';
import { Consignee, Customer } from 'src/app/contact-manager/contact-models';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { LookupService } from 'src/app/master-data/service/lookup.service';

@Component({
  selector: 'app-edit-waybill',
  templateUrl: './edit-waybill.component.html',
  styleUrls: ['./edit-waybill.component.scss']
})
export class EditWaybillComponent implements OnInit {

  submitted: boolean = false;
  id:string;
  model: FormGroup;
  groups : Lookup[];
  operations: Operation[];
  consignees : Consignee[];
  freightForwarders : Customer[];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: WaybillService,
    private lookupService: LookupService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.submitted = false;
    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CUSTOMER_GROUP).then((res) => {
      this.groups = res.data;
    })

    this.model = this.fb.group({
      id: [],
      operation_id: ['', [Validators.required]],
      truck_no: [''],
      departure_date: [''],
      destination: [''],
      driver_name: [''],
      driver_tel: [''],
      vessel: [''],
      transporter: [''],
      rate_agree: ['', [Validators.required]],
      clearance_office: [''],
      remarks: [''],
      location: ['']
    });
    this.populateData(this.id);
  }
  
  populateData(id:string) {
    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        id : this.id,
        operation_id : data.operation_id,
        truck_no : data.truck_no,
        departure_date : data.departure_date,
        destination : data.destination,
        driver_name : data.driver_name,
        driver_tel : data.driver_tel,
        vessel : data.vessel,
        transporter : data.transporter,
        rate_agree : data.rate_agree,
        clearance_office : data.clearance_office,
        remarks : data.remarks,
        location : data.location
      });
    });
  }

  update() {
    if (this.model.valid) {
      this.modelService.Edit(this.id, this.model.value).then((res) => {
        this.notificationService.showToastr("success", "Record updated successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Some error occured!")
      })
    } else {
      this.submitted = true
    }
  }

}
