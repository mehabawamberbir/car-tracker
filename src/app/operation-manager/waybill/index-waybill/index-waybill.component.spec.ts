import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexWaybillComponent } from './index-waybill.component';

describe('IndexWaybillComponent', () => {
  let component: IndexWaybillComponent;
  let fixture: ComponentFixture<IndexWaybillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexWaybillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexWaybillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
