import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { WaybillService } from 'src/app/operation-manager/services/waybill.service';

import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Waybill } from '../../models/operation-model';

@Component({
  selector: 'app-index-waybill',
  templateUrl: './index-waybill.component.html',
  styleUrls: ['./index-waybill.component.scss']
})
export class IndexWaybillComponent implements OnInit {
  waybills: any;
  url:string;
  constructor(
    private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private waybillService: WaybillService
  ) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + "operation-manager/waybills?";
    //this.getAll();
  }
  
  getAll() : void {
    this.waybillService.GetAll().then(res => 
      {
        this.waybills = res.data;
      },
      error => { console.log(error);}
      );
  }

  refreshList(): void {
    this.getAll();
  }

  newData(data:any) {
    this.waybills = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.waybills = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    
    this.waybills = data;
    
  }
  edit(obj:Waybill) {
    this.waybillService.model = obj;
    this.router.navigate(['/operation-manager/waybills/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.waybillService.ToggleStatus(id).then(res => {
      this.notificationService.showToastr("success", "Status updated successfully!")
    }, err => {
      this.notificationService.showToastr("danger", "Failed to update the status!")
    })
  }
  delete(id:string) {
    this.waybillService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!")
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
}