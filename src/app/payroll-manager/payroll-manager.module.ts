import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayrollManagerRoutingModule } from './payroll-manager-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PayrollManagerRoutingModule
  ]
})
export class PayrollManagerModule { }
