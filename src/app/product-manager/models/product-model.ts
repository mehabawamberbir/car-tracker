export class Product{
    id:string;
    is_sales_product:boolean;
    product_type_id:string;
    name:string;
    description:string;
    unit_price:string;
    is_billable:boolean;
    is_sellable:boolean;
    invoice_account_id:string;
    income_account_id:string;
    purchasable:boolean;
    exp_account_id:string;
    view_at_invoice_detail:boolean;
    trackable:boolean;
    disp_at_operation_exp:boolean;
    status:boolean;
}