import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductManagerComponent } from './product-manager.component';
// import { AuthGuard } from '../_helpers/auth.guard';

import { IndexProductComponent } from './product/index-product/index-product.component';
import { CreateProductComponent } from './product/create-product/create-product.component';
import { EditProductComponent } from './product/edit-product/edit-product.component';

const routes: Routes = [
  { path: '', component:ProductManagerComponent },

  { path: 'products/:type', component: IndexProductComponent },
  { path: 'products/:type/create', component: CreateProductComponent },
  { path: 'products/edit/:id', component: EditProductComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductManagerRoutingModule { }
