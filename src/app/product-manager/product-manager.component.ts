import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { CONSTANT_CONFIG } from 'src/app/config/constants';

@Component({
  selector: 'app-product-manager',
  templateUrl: './product-manager.component.html',
  styleUrls: ['./product-manager.component.scss']
})
export class ProductManagerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items: NbMenuItem[] = [
    {
      title: "Sellable Products",
      link: '/product-manager/products/sellable',
      icon: 'checkmark'
    },
    {
      title: "Purchase Products",
      link: '/product-manager/products/purchase',
      icon: 'checkmark'
    },
   ];
}
