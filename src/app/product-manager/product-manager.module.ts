import { CoreModule } from '../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ProductManagerRoutingModule } from './product-manager-routing.module';
import { ProductManagerComponent } from './product-manager.component';
import { NbCardModule, NbAlertModule, NbButtonModule, NbIconModule, NbInputModule, NbSelectModule, NbTreeGridModule, NbDialogModule, NbToggleModule, NbTooltipModule, NbMenuModule, NbTabsetModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CreateProductComponent } from './product/create-product/create-product.component';
import { EditProductComponent } from './product/edit-product/edit-product.component';
import { IndexProductComponent } from './product/index-product/index-product.component';

@NgModule({
  declarations: [
    ProductManagerComponent,
    CreateProductComponent,
    EditProductComponent,
    IndexProductComponent,
  ],
  imports: [
    CommonModule,
    ProductManagerRoutingModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbAlertModule,
    NbInputModule,
    NbSelectModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SmartTableModule,
    NbTreeGridModule,
    NbTabsetModule,
    SharedModule,
    NbDialogModule.forChild(),
    NbToggleModule,
    NbTooltipModule,
    CoreModule,
    NbMenuModule,
  ],
  providers: [    
  ]
})
export class ProductManagerModule { }
