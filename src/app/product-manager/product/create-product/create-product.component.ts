import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from 'src/app/accounting-manager/services/account.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { ProductService } from 'src/app/product-manager/services/product.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  productTypes: Lookup[];
  accounts : Lookup[];
  type = "sellable";

  constructor(
    private fb: FormBuilder,
    private route:ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: ProductService,
    private lookupService: LookupService,
    private accountService:AccountService
    ) { }

  ngOnInit(): void {

    this.type = this.route.snapshot.paramMap.get("type")!;

    this.added = false;
    this.error = false;
    this.submitted = false;
    this.model = this.fb.group({
      product_type_id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      description: [''],
      unit_price: 0,
      is_billable: 0,
      invoice_account_id: [''],
      is_sellable: 1,
      income_account_id: [''],
      purchasable: 0,
      exp_account_id: [''],
      view_at_invoice_detail: 0,
      trackable: 0,
      disp_at_operation_exp: 0,
      status:1
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.PRODUCT_TYPE).then((res) => {
      this.productTypes = res.data;
    });

    this.accountService.GetAll().then((res) => {
      this.accounts = res.data;
    });

  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }
  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value).then((res) => {
        this.ngOnInit()
        this.notificationService.showToastr("success", "Record created successfully!")
      }, err => {
        this.notificationService.showToastr("danger", "Unknown Error occurred!")
      })
    } else {
      this.submitted = true
    }
  }

}