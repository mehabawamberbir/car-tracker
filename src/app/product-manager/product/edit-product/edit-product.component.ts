import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from 'src/app/accounting-manager/services/account.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  id : string;
  added: boolean = false
  error: boolean = false
  submitted: boolean = false
  model: FormGroup;
  type:string;
  productTypes: Lookup[];
  accounts : Lookup[];

  constructor(    
    private router: Router,
    private fb: FormBuilder,  
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private modelService: ProductService,
    private lookupService: LookupService,
    private accountService:AccountService
    ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id") !;
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
      product_type_id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      description: [''],
      unit_price: 0,
      is_billable: 0,
      invoice_account_id: [''],
      is_sellable: 0,
      income_account_id: [''],
      purchasable: 0,
      exp_account_id: [''],
      view_at_invoice_detail: 0,
      trackable: 0,
      disp_at_operation_exp: 0,
      status : 1,
    });

    this.lookupService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.PRODUCT_TYPE).then((res) => {
      this.productTypes = res.data;
    });

    this.accountService.GetAll().then((res) => {
      this.accounts = res.data;
    });

    this.populateData(this.id);
  }
  
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        product_type_id: data.product_type_id,
        name: data.name,
        description: data.description,
        unit_price: data.unit_price,
        is_billable: data.is_billable,
        invoice_account_id: data.invoice_account_id,
        is_sellable: data.is_sellable,
        income_account_id: data.income_account_id,
        purchasable: data.purchasable,
        exp_account_id: data.exp_account_id,
        view_at_invoice_detail: data.view_at_invoice_detail,
        trackable: data.trackable,
        disp_at_operation_exp: data.disp_at_operation_exp,
        status : data.status,
      });
      this.type = data.sellable ? "sellable" : "purchase";
    });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  update() {
    var isFormValid = true;//this.model.valid;
    // validation not completed
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }

}