import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { ProductService } from 'src/app/product-manager/services/product.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Product } from '../../models/product-model';

@Component({
  selector: 'app-index-product',
  templateUrl: './index-product.component.html',
  styleUrls: ['./index-product.component.scss']
})
export class IndexProductComponent implements OnInit {
  url:string;
  models:any;
  type = "sellable";
  constructor(
    private router: Router,
    private route:ActivatedRoute,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: ProductService
  ) { }

  ngOnInit(): void {
    this.type = this.route.snapshot.paramMap.get("type")!;
    this.url = this.repositoryService.apiUrl + 'product-manager/products-'+ this.type + '?';
    this.modelService.type = this.type;
  }
  
  getAll() : void {
    this.modelService.GetAll().then(res => 
      {
        this.models = res.data;
      },
      error => { console.log(error);}
      );
  }

  refreshList(): void {
    this.getAll();
  }

  newData(data:any) {
    this.models = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.models = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    
    this.models = data;
    
  }
  edit(obj:Product) {
    this.modelService.model = obj;
    this.router.navigate(['/product-manager/products/edit/' + obj.id]);
  }
  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id).then(res => {
      this.notificationService.showToastr("success", "Status updated successfully!")
    }, err => {
      this.notificationService.showToastr("danger", "Failed to update the status!")
    })
  }
  delete(id:string) {
    this.modelService.Delete(id).then((res) => {
      this.notificationService.showToastr("success", "Record deleted successfully!")
    }, error => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
  }
}