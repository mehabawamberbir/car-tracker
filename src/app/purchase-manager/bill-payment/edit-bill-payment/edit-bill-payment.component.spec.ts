import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBillPaymentComponent } from './edit-bill-payment.component';

describe('EditBillPaymentComponent', () => {
  let component: EditBillPaymentComponent;
  let fixture: ComponentFixture<EditBillPaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditBillPaymentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditBillPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
