import { Component, OnInit } from '@angular/core';
import { VendorService } from 'src/app/contact-manager/services/vendor.service';
import { Vendor } from 'src/app/contact-manager/contact-models';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormGroup, FormBuilder, Validators,FormControl } from '@angular/forms';
import { BillPayment } from '../../purchase-manager-models';
import { BillPaymentService } from '../../services/bill-payment.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { ReactiveFormsModule } from '@angular/forms';
import { Account } from 'src/app/accounting-manager/accounting-manager.model';
import { AccountService } from 'src/app/accounting-manager/services/account.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-bill-payment',
  templateUrl: './edit-bill-payment.component.html',
  styleUrls: ['./edit-bill-payment.component.scss']
})
export class EditBillPaymentComponent implements OnInit {

  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;
  id:string;
  model: FormGroup;
  vendors:Vendor[];
  currencies:Lookup[];
  payment_methods:Lookup[];
  accounts:Account[];
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: BillPaymentService,
    private vendorService:VendorService,
    private lookUpService:LookupService,
    private accountService:AccountService,
    private route:ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false

    this.id = this.route.snapshot.paramMap.get("id")!;
    this.model = this.fb.group({
      id:[''],
      vendor_id:['',],
      currency_id:[''],
      date:['', [Validators.required]], 
      amount:[0, [Validators.required]],
      payment_method_id:['', [Validators.required]],
      account_id:['', [Validators.required]],
      reference:[''],
      remarks:[''],
      status:1,
    });

  this.vendorService.GetAll().then((res)=> {
    this.vendors = res.data;
  });
  this.accountService.GetAll().then((res)=> {
    this.accounts = res.data;
  });
  this.lookUpService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CURRENCY).then((res)=>
  {
    this.currencies = res.data;
  });
  this.lookUpService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.PAY_METHOD).then((res)=>
  {
    this.payment_methods = res.data;
  });

  this.populateData(this.id);
  }
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        id:this.id,
        vendor_id:data.vendor_id,
        currency_id:data.currency_id,
        date:data.date, 
        amount:data.amount,
        payment_method_id:data.payment_method_id,
        account_id:data.account_id,
        reference:data.reference,
        remarks:data.remarks,
        status:data.status,
      });
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  

  update() {
    var isFormValid = true;
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
  }


