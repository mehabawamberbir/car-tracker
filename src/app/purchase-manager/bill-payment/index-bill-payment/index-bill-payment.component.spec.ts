import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexBillPaymentComponent } from './index-bill-payment.component';

describe('IndexBillPaymentComponent', () => {
  let component: IndexBillPaymentComponent;
  let fixture: ComponentFixture<IndexBillPaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexBillPaymentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexBillPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
