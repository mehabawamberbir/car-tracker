import { Component, OnInit } from '@angular/core';
import { VendorService } from 'src/app/contact-manager/services/vendor.service';
import { Vendor } from 'src/app/contact-manager/contact-models';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormGroup, FormBuilder, Validators,FormControl } from '@angular/forms';
import { Bill } from '../../purchase-manager-models';
import { BillService } from '../../services/bill.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'app-create-bill',
  templateUrl: './create-bill.component.html',
  styleUrls: ['./create-bill.component.scss']
})
export class CreateBillComponent implements OnInit {

  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  vendors:Vendor[];
  currencies:Lookup[];
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: BillService,
    private vendorService:VendorService,
    private lookUpService:LookupService
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.model = this.fb.group({
      id:[''],
      vendor_id:['', [Validators.required]],
      invoice_no:['', [Validators.required]],
      date:['', [Validators.required]],
      due_date:['', [Validators.required]],
      currency_id:['',[Validators.required]],
      amount:0,
      remarks:[''],
      status:1,
    });

  this.vendorService.GetAll().then((res)=> {
    this.vendors = res.data;
  });
  this.lookUpService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CURRENCY).then((res)=>
  {
    this.currencies = res.data;
  });
  }
  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  ClearForm(){
    this.model = this.fb.group({
      id:[''],
      vendor_id:['', [Validators.required]],
      invoice_no:['', [Validators.required]],
      date:['', [Validators.required]],
      due_date:['', [Validators.required]],
      currency_id:['',[Validators.required]],
      remarks:[''],
      status:1,
      amount:0,
      });
  }

  create() {
    if (this.model.valid) {
      this.modelService.Create(this.model.value)
      
      .then((res) => 
    {
      if(res.status == 201)
      {
        this.notificationService.showToastr("success", "Record Created successfully!");
        this.ClearForm();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
}
