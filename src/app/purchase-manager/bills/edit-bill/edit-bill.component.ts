import { Component, OnInit } from '@angular/core';
import { VendorService } from 'src/app/contact-manager/services/vendor.service';
import { Vendor } from 'src/app/contact-manager/contact-models';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { FormGroup, FormBuilder, Validators,FormControl } from '@angular/forms';
import { Bill } from '../../purchase-manager-models';
import { BillService } from '../../services/bill.service';
import { Lookup } from 'src/app/master-data/master.model';
import { LookupService } from 'src/app/master-data/service/lookup.service';
import { LOOKUPTYPE_CONFIG } from 'src/app/shared/LookupTypeConfig';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-bill',
  templateUrl: './edit-bill.component.html',
  styleUrls: ['./edit-bill.component.scss']
})
export class EditBillComponent implements OnInit {

  added: boolean = false;
  error: boolean = false;
  submitted: boolean = false;

  model: FormGroup;
  vendors:Vendor[];
  currencies:Lookup[];
  id:string;
  constructor(private fb: FormBuilder,
    private notificationService: NotificationService,
    private modelService: BillService,
    private vendorService:VendorService,
    private lookUpService:LookupService,
    private route:ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.added = false
    this.error = false
    this.submitted = false
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.model = this.fb.group({
      id:[''],
      vendor_id:['', [Validators.required]],
      invoice_no:['', [Validators.required]],
      date:['', [Validators.required]],
      due_date:['', [Validators.required]],
      currency_id:['',[Validators.required]],
      amount:0,
      remarks:[''],
      status:1,
    });

  this.vendorService.GetAll().then((res)=> {
    this.vendors = res.data;
  });
  this.lookUpService.getLookupByLookupTypeCode(LOOKUPTYPE_CONFIG.CURRENCY).then((res)=>
  {
    this.currencies = res.data;
  });

  this.populateData(this.id);
  }
  populateData(id:string) {

    this.modelService.GetById(id).subscribe(data => {
      this.model.setValue({
        id:this.id,
        vendor_id:data.vendor_id,
        invoice_no:data.invoice_no,
        date:data.date,
        due_date:data.due_date,
        currency_id:data.currency_id,
        amount:data.amount,
        remarks:data.remarks,
        status:data.status,
      });
    });
  }

  closeSuccessAlert() {
    this.added = false
  }
  closeErrorAlert() {
    this.error = false
  }

  

  update() {
    var isFormValid = true;
    if (isFormValid) {
      this.modelService.Edit(this.id, this.model.value)      
      .then((res) => 
    {
      if(res.status == 200)
      {
        this.ngOnInit();
        this.notificationService.showToastr("success", "Record Updated successfully!");
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
    } 
    else {
      this.submitted = false;
      this.notificationService.showToastr("danger", "Invalid Form!");
    }
  }
  

}
