import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexBillComponent } from './index-bill.component';

describe('IndexBillComponent', () => {
  let component: IndexBillComponent;
  let fixture: ComponentFixture<IndexBillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexBillComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
