
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { SortService } from 'src/app/shared/services/sort.service';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { BillService } from '../../services/bill.service';
import { Bill } from '../../purchase-manager-models';
import { INVOICE_STATUS } from 'src/app/shared/InvoiceStatus';
@Component({
  selector: 'app-index-bill',
  templateUrl: './index-bill.component.html',
  styleUrls: ['./index-bill.component.scss']
})
export class IndexBillComponent implements OnInit {
  url:string;
  bills:any;

  constructor(private router: Router,
    private searchService: SearchService,
    private sortService: SortService,
    private repositoryService: RepositoryService,
    private notificationService: NotificationService,
    private modelService: BillService) { }

  ngOnInit(): void {
    this.url = this.repositoryService.apiUrl + this.modelService.modelPath  + "?";
    this.LoadData();
  }
  LoadData(){
    this.modelService.GetAll()
    .then((res) => {
      this.bills = res.data;
      this.searchService.searchData = res.data;
    });
  }
  edit(obj:Bill) {
    
    this.router.navigate(['/purchase-manager/bills/edit/' + obj.id]);
  }

  newData(data:any) {
    this.bills = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  searchOutput(data:any) {
    this.bills = data;
    this.searchService.searchData = data;
    this.sortService.sortData = data;
  }
  sortedData(data:any) {
    this.bills = data
  }

  toggleStatus(id:string) {
    this.modelService.ToggleStatus(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Status Updated successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  
  delete(id:string) {
    this.modelService.Delete(id)
    .then((res) => 
    {
      if(res.status == 200)
      {
        this.notificationService.showToastr("success", "Record Deleted successfully!");
        this.LoadData();
      } 
      else
      {
        this.notificationService.showToastr("danger", "Some error occured!")
      }
    })
    .catch((err) => {
      this.notificationService.showToastr("danger", "Some error occured!")
    })
    .finally(() => {
    });
  }
  GetInvoiceStatus(status:number){
    return INVOICE_STATUS.find(t => t.id == status)?.name;
  }

}
