import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexPurchaseManagerComponent } from './index-purchase-manager.component';

describe('IndexPurchaseManagerComponent', () => {
  let component: IndexPurchaseManagerComponent;
  let fixture: ComponentFixture<IndexPurchaseManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexPurchaseManagerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexPurchaseManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
