import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { CONSTANT_CONFIG } from 'src/app/config/constants';
@Component({
  selector: 'app-index-purchase-manager',
  templateUrl: './index-purchase-manager.component.html',
  styleUrls: ['./index-purchase-manager.component.scss']
})
export class IndexPurchaseManagerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
 
  items : NbMenuItem[] = [
  {
    title:'Bills',
    link:'bills',
    icon:'checkmark'
  },
  {
    title:'Bill Payment',
    link:'bill-payments',
    icon:'checkmark'
  }
  ];
}
