export class Bill{
    id:string;
    vendor_id:string;
    invoice_no:string;
    date:Date;
    due_date:Date;
    currency_id:string;
    amount:number;
    remarks:string;
    status:boolean;
}

export class BillPayment{
    id:string;
    vendor_id:string;
    currency_id:string;
    date:Date;
    amount:number;
    payment_method_id:string;
    account_id:string;
    reference:string;
    remarks:string;
    status:boolean;
}