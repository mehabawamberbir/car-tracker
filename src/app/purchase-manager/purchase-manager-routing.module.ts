import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditButtonComponent } from '../shared/templates/edit-button.compoent';
import { CreateBillPaymentComponent } from './bill-payment/create-bill-payment/create-bill-payment.component';
import { EditBillPaymentComponent } from './bill-payment/edit-bill-payment/edit-bill-payment.component';
import { IndexBillPaymentComponent } from './bill-payment/index-bill-payment/index-bill-payment.component';
import { CreateBillComponent } from './bills/create-bill/create-bill.component';
import { EditBillComponent } from './bills/edit-bill/edit-bill.component';
import { IndexBillComponent } from './bills/index-bill/index-bill.component';
import { IndexPurchaseManagerComponent } from './index-purchase-manager/index-purchase-manager.component';

const routes: Routes = [
  {path:'', component:IndexPurchaseManagerComponent},

  {path:'bills', component:IndexBillComponent},
  {path:'bills/create', component:CreateBillComponent},
  {path:'bills/edit/:id', component:EditBillComponent},

  {path:'bill-payments', component:IndexBillPaymentComponent},
  {path:'bill-payments/create', component:CreateBillPaymentComponent},
  {path:'bill-payments/edit/:id', component:EditBillPaymentComponent},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseManagerRoutingModule { }
