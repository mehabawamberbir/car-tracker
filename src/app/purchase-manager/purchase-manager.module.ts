import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbAlertModule, NbButtonModule, NbIconModule, NbInputModule, NbSelectModule, NbTreeGridModule, NbDialogModule, NbToggleModule, NbTooltipModule, NbMenuModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';

import { PurchaseManagerRoutingModule } from './purchase-manager-routing.module';
import { IndexBillComponent } from './bills/index-bill/index-bill.component';
import { CreateBillComponent } from './bills/create-bill/create-bill.component';
import { IndexPurchaseManagerComponent } from './index-purchase-manager/index-purchase-manager.component';
import { EditBillComponent } from './bills/edit-bill/edit-bill.component';
import { CreateBillPaymentComponent } from './bill-payment/create-bill-payment/create-bill-payment.component';
import { EditBillPaymentComponent } from './bill-payment/edit-bill-payment/edit-bill-payment.component';
import { IndexBillPaymentComponent } from './bill-payment/index-bill-payment/index-bill-payment.component';


@NgModule({
  declarations: [
    IndexBillComponent,
    CreateBillComponent,
    IndexPurchaseManagerComponent,
    EditBillComponent,
    CreateBillPaymentComponent,
    EditBillPaymentComponent,
    IndexBillPaymentComponent
  ],
  imports: [
    CommonModule,
    PurchaseManagerRoutingModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbAlertModule,
    NbInputModule,
    NbSelectModule,
    ReactiveFormsModule,
    FormsModule,
    NbTreeGridModule,
    SharedModule,
    NbDialogModule,
    NbToggleModule,
    NbTooltipModule,
    CoreModule,
    NbMenuModule,
  ]
})
export class PurchaseManagerModule { }
