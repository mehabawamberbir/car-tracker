import { Injectable } from '@angular/core';
import { RepositoryService } from 'src/app/shared/services/repository.service';
import { BillPayment } from '../purchase-manager-models';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class BillPaymentService {
  model : BillPayment;
  modelPath : string = "purchase-manager/bill-payments";
  constructor(private repository: RepositoryService) { 

  }    

  GetAll() {
      return axios.get(this.repository.apiUrl + this.modelPath);
  }
  Get(skip:number, limit:number) {
      return axios.get(this.repository.apiUrl + this.modelPath + `?skip=${skip} && limit=${limit}`);
  }
  GetById(id:string) {
      return this.repository.sendRequest("GET", this.repository.apiUrl + this.modelPath + '/' + id);
  }
  Create(model:BillPayment) {
      return axios.post(this.repository.apiUrl + this.modelPath, model)
  }
  Edit(id:string, model:BillPayment) {
      return axios.put(this.repository.apiUrl + this.modelPath + '/' + id, model)
  }
  Delete(id:string) {
      return axios.delete(this.repository.apiUrl + this.modelPath + '/' + id);
  }
  ToggleStatus(id:string) {
      return axios.patch(this.repository.apiUrl + this.modelPath + '/' + id + '/toggle-status');
  }
}
