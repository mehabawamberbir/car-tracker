import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexReportManagerComponent } from './index-report-manager.component';

describe('IndexReportManagerComponent', () => {
  let component: IndexReportManagerComponent;
  let fixture: ComponentFixture<IndexReportManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexReportManagerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexReportManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
