import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-index-report-manager',
  templateUrl: './index-report-manager.component.html',
  styleUrls: ['./index-report-manager.component.scss']
})
export class IndexReportManagerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  items: NbMenuItem[] = [
    {
      title: "Freight Forwarders",
      link: '/contact-manager/freight-forwarders',
      icon: 'checkmark'
    },
    {
      title: "Consignees",
      link: '/contact-manager/consignees',
      icon: 'checkmark'
    },
    {
      title: "Agents",
      link: '/contact-manager/agents',
      icon: 'checkmark'
    },
    {
      title: "Customers",
      link: '/contact-manager/customers',
      icon: 'checkmark'
    },
    {
      title: "Vendors",
      link: '/contact-manager/vendors',
      icon: 'checkmark'
    }
   ];
}
