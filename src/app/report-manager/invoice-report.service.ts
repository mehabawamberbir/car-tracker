import { Injectable } from '@angular/core';
import { RepositoryService } from 'src/app/shared/services/repository.service';

import  axios  from 'axios';
import { InvoiceReportDetail } from './report-model';

@Injectable({
    providedIn: 'root'
})

export class InvoiceReportService {
    model : InvoiceReportDetail;
    modelPath : string = "report-manager/invoice-details";
    constructor(private repository: RepositoryService) { 

    }    

    GetAll() {
        return axios.get(this.repository.apiUrl + this.modelPath + '-index');
    }
    Get(skip:number, limit:number) {
        return axios.get(this.repository.apiUrl + this.modelPath + `?skip=${skip} && limit=${limit}`);
    }
    GetById(id:string) {
        return this.repository.sendRequest("GET", this.repository.apiUrl + this.modelPath + '/' + id);
    }
    GetInvoiceReportDetail(id:string){       
        return this.repository.sendRequest("GET", "http://localhost:3000/invoice-detail/" + id);
    }
}