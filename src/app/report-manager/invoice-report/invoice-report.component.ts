import { Component, OnInit } from '@angular/core';
import { InvoiceReportService } from '../invoice-report.service';
import { Invoice } from 'src/app/invoice-manager/models/invoice.models';
import { InvoiceReportDetail } from '../report-model';
import { ActivatedRoute } from '@angular/router';
import { InvoiceReportDetailItem } from '../report-model';
import { NbToastrService, NbWindowRef, NbWindowService } from "@nebular/theme";
import { printHelper } from '../print-helper';
import { InvoiceService } from 'src/app/invoice-manager/services/invoice.service';
import { Operation, OperationProcess } from 'src/app/operation-manager/models/operation-model';
import { OperationProcessService } from 'src/app/operation-manager/services/operation-process.service';
import { INVOICE_STATUS } from 'src/app/shared/InvoiceStatus';
import { Consignee } from 'src/app/contact-manager/contact-models';

@Component({
  selector: 'app-invoice-report',
  templateUrl: './invoice-report.component.html',
  styleUrls: ['./invoice-report.component.scss']
})

export class InvoiceReportComponent implements OnInit {
  data: Invoice = new Invoice;
  operationProcess: OperationProcess;
  id: string;
  totalAmount = 0;
  invoiceItems: InvoiceReportDetailItem[];
  constructor(private modelService: InvoiceReportService,
               private route: ActivatedRoute,
               private invoiceService : InvoiceService,
               private operationProcessService:OperationProcessService
               ) { }
  
  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id")!;

    this.invoiceService.GetById(this.id).subscribe((data:Invoice)=>{
    this.data = data;
    this.invoiceItems = [];
    this.invoiceItems.forEach(element => {
      this.totalAmount += element.amount;
    });

    // this.operationProcessService.Query('operation_id',this.data.operation_id).then((res) => {
    //   if(res.data.length > 0 )
    //     this.operationProcess = res.data[0];
    // });
  });
}
  
GetInvoiceStatus(status:number){
  return INVOICE_STATUS.find(t => t.id == status)?.name;
}

convertUSDRate(amount:number){
  return this.totalAmount*175;
}

PrintElement()
{  
  var printContents = document.getElementById('invoice-report-div')?.innerHTML;
  var originalContents = document.body.innerHTML;        
  document.body.innerHTML = printContents!;
  window.print();
  document.body.innerHTML = originalContents;
}

}
