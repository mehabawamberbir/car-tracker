import { Component, OnInit } from '@angular/core';
import { printHelper } from '../print-helper';
import { ActivatedRoute } from '@angular/router';
import { Operation, OperationProcess, Container } from 'src/app/operation-manager/models/operation-model';
import { OperationProcessService } from 'src/app/operation-manager/services/operation-process.service';
import { OperationService } from 'src/app/operation-manager/services/operation.service';
import { ContainerService } from 'src/app/operation-manager/services/container.service';
import { DatePipe } from '@angular/common';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-operation-report',
  templateUrl: './operation-report.component.html',
  styleUrls: ['./operation-report.component.scss'],
})
export class OperationReportComponent implements OnInit {
id: string;
data: Operation;
data1: OperationProcess;
data2: any;
today = Date.now();
totalAmount: Container [];
  constructor(private OperationService:OperationService,
              private operationProcessService:OperationProcessService,
              private route:ActivatedRoute,
              private containerService:ContainerService,
              private datePipe:DatePipe
              ) {
                //this.today = this.datePipe.transform(this.today, )
              }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.OperationService.GetById(this.id).subscribe((data:Operation)=>
    {
      this.data = data;
    });
    this.operationProcessService.GetById(this.id).subscribe((data:OperationProcess)=>
    {
      this.data1 = data;
    }
    );
  
    this.containerService.GetById(this.id).subscribe((data:Container)=>
    {
      this.data2 = data;
    }
    );
  }
  PrintElement(){
  
    var printContents = document.getElementById('invoice-report-div')?.innerHTML;
    var originalContents = document.body.innerHTML;        
    document.body.innerHTML = printContents!;
    window.print();
    document.body.innerHTML = originalContents;
  }

}
