export function printHelper(divId: string) {
    const printContents = document.getElementById(divId)?.innerHTML;
    const pageContent = `<!DOCTYPE html><html><head></head><body onload="window.print()">${printContents}</html>`;
    let popupWindow: Window;
    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
      let popupWindow1 = window.open(
        '',
        '_blank',
        'width=800,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no'
      );
      popupWindow1?.window.focus();
      popupWindow1?.document.write(pageContent);
        popupWindow1?.document.close();
    if(popupWindow1 != null)
      popupWindow1.onbeforeunload = event => {
        popupWindow1?.close();
      };
      if(popupWindow1 != null)
      popupWindow1.onabort = event => {
        popupWindow1?.document.close();
        popupWindow1?.close();
      };
    } 
    else {
      let popupWindow1 = window.open('', '_blank', 'width=800,height=600');
      popupWindow1?.document.open();
      popupWindow1?.document.write(pageContent);
      popupWindow1?.document.close();
    }
    
  }