import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexReportManagerComponent } from './index-report-manager/index-report-manager.component';
import { InvoiceReportComponent } from './invoice-report/invoice-report.component';
import { OperationReportComponent } from './operation-report/operation-report.component';

const routes: Routes = [
  {
    path: '', component : IndexReportManagerComponent,  
  },
  { 
    path: 'invoice-report/:id', component:InvoiceReportComponent 
  },
  { 
    path: 'operation-report/:id', component:OperationReportComponent 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportManagerRoutingModule { }
