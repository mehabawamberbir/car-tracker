import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportManagerRoutingModule } from './report-manager-routing.module';
import { InvoiceReportComponent } from './invoice-report/invoice-report.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule, NbIconModule, NbMenuModule } from '@nebular/theme';
import { IndexReportManagerComponent } from './index-report-manager/index-report-manager.component';
import { CoreModule } from '../core/core.module';
import { OperationReportComponent } from './operation-report/operation-report.component';
@NgModule({
  declarations: [
    InvoiceReportComponent,
    IndexReportManagerComponent,
    OperationReportComponent
  ],
  imports: [
    CommonModule,
    ReportManagerRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbMenuModule,
    NbIconModule,
    CoreModule
  ]
})
export class ReportManagerModule { }
