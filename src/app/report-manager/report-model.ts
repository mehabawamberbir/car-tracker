export class InvoiceReportDetail{
    id:string;
    consignee_name:string;
    reference_no:string;
    description:string;
    bol:string;
    operation_no:string;
    reg_no:string;
    freight_forwarder:string;
    shipper:string;
    invoice_no:string;
    invoice_date:string;    
    declaration_no:string;
    status:string;
    invoice_items:InvoiceReportDetailItem[]

}
export class InvoiceReportDetailItem{
    description:string;
    reference_no:string;
    amount:number;
}
