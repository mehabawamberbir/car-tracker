import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesManagerRoutingModule } from './sales-manager-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SalesManagerRoutingModule
  ]
})
export class SalesManagerModule { }
