export const INVOICE_STATUS = [
    {
        "id" : 1, name: "Open", color:"#f39c12"
    },
    {
        "id" : 2, name: "Posted", color:"#f39c12"
    },
    {
        "id" : 3, name: "Paid", color:"#f39c12"
    }
];