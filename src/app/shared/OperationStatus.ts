export const OPERATION_STATUS = [
    {
        "id" : 1, name: "New Operation"
    },
    {
        "id" : 2, name: "Vessel Not Arrived"
    },
    {
        "id" : 3, name: "Under Process"
    },
    {
        "id" : 4, name: "Waiting Truck"
    },
    {
        "id" : 5, name: "Dispatched"
    },
    {
        "id" : 6, name: "Invoiced"
    },
    {
        "id" : 7, name: "Terminated"
    },
];