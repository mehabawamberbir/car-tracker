export class SearchModel<T> {

    constructor(
      public list: Array<T>,
      public currentPage: number,
      public maxPage: number,
      public searchKeyWord: string,
      public pagingSize: number,
      public sortingColumn: string,
      public sortingDirection: SortingDirection,
      public totalItems: number,
      public filters: { [key: string]: string },
      public tags: string[],
      public objectFilters?: { [key: string]: any }) { }
  }
  
  export enum SortingDirection {
    Ascending = 1,
    Descending = 2
  }
  