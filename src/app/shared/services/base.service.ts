import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class BaseService {

    constructor() { }

    handleError(error: Response) {
        if (error.headers) {
        var applicationError = error.headers.get('Application-Error');

        // either application-error in header or model error in body
        if (applicationError) {
            return new Error(applicationError);
        }

        }
        if (error.status) {
            return new Error("" + error.status);
        }
        // var modelStateErrors: string = "";
        // for now just concatenate the error descriptions, alternative we could simply pass the entire error response upstream
        // for (var key in error['error']['errors']) {
        // if (error['error']['errors'][key]) modelStateErrors += error['error']['errors'][key] + '\n';
        // }

        // modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
        return new Error('Server error');
        //return throwError(modelStateErrors || 'Server error');
    }
}
