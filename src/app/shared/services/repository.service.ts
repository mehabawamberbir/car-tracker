import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, EMPTY, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { BaseService } from './base.service';
import { CONSTANT_CONFIG } from 'src/app/config/constants';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RepositoryService {
    public apiUrl: string = CONSTANT_CONFIG.API_URL;
    constructor(private router: Router, public httpClient: HttpClient, private baseService: BaseService) {
    }

    public sendRequest(method: string, url: string, options?: any): Observable<any> {
        // let header = new HttpHeaders();
        // header.set('Access-Control-Allow-Origin', '*');
        if (options != null && !options.hasOwnProperty('observe')) {
            options['observe'] = 'response'
        }

        return this.httpClient.request(method, url, options).pipe(
            // catchError((err: Response) => {
            //     if (err.status == 409) {
            //         return throwError(err["error"] ? err["error"] : "Network Error");
            //     }
            //     return this.baseService.handleError(err);

            // }
            // )
            catchError(err => {
                throw 'error in source. Details: ' + err;
              })            
            );


    }
}
