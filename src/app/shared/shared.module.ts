import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from './top-bar/top-bar.component';
import { AddButtonComponent } from './templates/add-button/add-button.component';
import { NbCardModule, NbIconModule, NbMenuItem, NbToggleModule, NbTooltipModule } from '@nebular/theme';
import { CardComponent } from './templates/card/card.component';
import { FormsModule } from '@angular/forms';
import { SearchInputComponent } from './templates/search-input/search-input.component';
import { EditButtonComponent } from './templates/edit-button.compoent';
import { DeleteButtonComponent } from './templates/delete.button.component';
import { ToggleButtonComponent } from './templates/toggle-button.component';
import { SaveButtonComponent } from './templates/save-button.component';
import { BackButtonComponent } from './templates/back-button.component';
import { PaginationComponent } from './templates/pagination/pagination.component';
import { PrintButtonComponent } from './templates/print-button.component';
import { CustomSelectComponent } from './templates/custom-select/custom-select.component';
import { ViewButtonComponent } from './templates/view-button.compoent';

@NgModule({
  declarations: [
    TopBarComponent,
    AddButtonComponent,
    EditButtonComponent,
    DeleteButtonComponent,
    ToggleButtonComponent,
    CardComponent,
    SearchInputComponent,    
    SaveButtonComponent,
    BackButtonComponent,
    PaginationComponent,
    PrintButtonComponent,
    CustomSelectComponent,
    ViewButtonComponent
  ],
  imports: [
    CommonModule,
    NbCardModule,
    NbIconModule,
    NbTooltipModule,
    NbToggleModule,
    FormsModule
  ],
  exports: [TopBarComponent,AddButtonComponent,EditButtonComponent,DeleteButtonComponent,
    ToggleButtonComponent,SaveButtonComponent,BackButtonComponent,PaginationComponent,
    SearchInputComponent,PrintButtonComponent, ViewButtonComponent]
})
export class SharedModule { }
