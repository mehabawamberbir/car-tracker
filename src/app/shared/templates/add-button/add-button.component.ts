import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-button',
  template: `
<button class="btn btn-primary">
    <nb-icon icon='plus-circle-outline' size='giant' status='control'></nb-icon> 
    <ng-content></ng-content>
</button>
  `,
  styles: [
  ]
})
export class AddButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
