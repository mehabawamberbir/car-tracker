import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-back-button',
  template: `
    <i class="fa fa-arrow-left backbtn mr-3 ml-3" nbTooltip="Back To Previous Page" nbTooltipPlacement='left' 
                            nbTooltipStatus='info'> Back To List<ng-content></ng-content></i> 
  `,
  styles: [`
  .backbtn{color:#36f} .backbtn:hover{cursor:pointer;}`
  ]
})
export class BackButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}