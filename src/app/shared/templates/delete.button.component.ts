import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { Component, OnInit,TemplateRef,EventEmitter,Output,Input} from '@angular/core';

@Component({
  selector: 'app-delete-button',
  template: `
  <ng-template #dialog let-data let-ref="dialogRef">
      <nb-card>
        <nb-card-header>Delete Item</nb-card-header>
        <nb-card-body><div>{{ data }}</div><div  class='ml-3 mt-2 mx-auto'>This action is irreversible.</div></nb-card-body>
        <nb-card-footer>
          <div class="row ">
            <div style='width:50%' class='mx-auto d-flex justify-content-between'>
            <button class='btn btn-danger' (click)="Yes()">Yes</button>
             <button class='btn btn-success ml-2' (click)="No()">No</button>
            </div>
          </div>
         
        </nb-card-footer>
      </nb-card>
    </ng-template>
     <i class="fa fa-trash delete-icon" (click)="open(dialog)" nbTooltip='Delete this item' nbTooltipPlacement='right'
                            nbTooltipStatus='danger'></i>
  `,
  styles: [`
  
.delete-icon{
    color: #ff0000;
    font-size: 22px;
    margin-right:1rem;
}
.delete-icon:hover{
    cursor: pointer;
}`
  ]
})
export class DeleteButtonComponent implements OnInit {
  private dialogref:any
  constructor(private dialogService: NbDialogService,) {
  }
  @Input() itemId :string;
  @Output() yes=new EventEmitter();
  @Output() no=new EventEmitter();
  Yes(){
this.yes.emit(this.itemId);
this.dialogref.close();
  }
  No(){
    this.no.emit({message:'do not delete item',id:this.itemId});
    this.dialogref.close();
  }
  open(dialog: TemplateRef<any>) {
  this.dialogref= this.dialogService.open(dialog, { context: 'Are you Sure you want to delete this item ?' });
  }
ngOnInit(){

}
}