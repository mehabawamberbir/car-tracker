import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-button',
  template: `
   <i class="fa fa-pencil edit-icon" nbTooltip="Edit this item" nbTooltipPlacement='left' 
                            nbTooltipStatus='info'></i>
  `,
  styles: [`
  .edit-icon{
    color: #0000ff;
    margin-right: 1rem;
    font-size:22px;
}
.edit-icon:hover{
    cursor: pointer;
}`
  ]
})
export class EditButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}