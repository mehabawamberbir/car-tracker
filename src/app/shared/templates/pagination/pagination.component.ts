import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import { RepositoryService } from '../../services/repository.service';
import { Injectable } from '@angular/core';
import  axios  from 'axios';

@Component({
    selector: 'app-pagination',
    template: `
    <div class=" d-flex justify-content-space-around bg-green">
     
   <div class='mt-3'>
   <ul class="pagination">
    <li class="page-item" [ngClass]='{disabled:activeLink==1}' (click)='backPage()'><a class="page-link"><i class='fa fa-angle-left'></i> Previous</a></li>
    <li class="page-item" *ngFor="let page of pages"  (click)='navigatePage(page)' [ngClass]="{active:activeLink==page}"><a class="page-link">{{page}}</a></li>
    <li class="page-item" [ngClass]='{disabled:activeLink==pages.length}' (click)='nextPage()'><a class="page-link">Next <i class='fa fa-angle-right'></i></a></li>
  </ul>
   </div>
  <div class='mr-5'>
     <select [(ngModel)]='entry' (change)='getdata()' class='mt-4 ml-5 select'>
         <option value='5'>5</option>
         <option value='10'>10</option>
         <option value='50'>50</option>
         <option value='100'>100</option>
         <option value='500'>500</option>
     </select>
  </div>
  <div class='mt-4 ml-5'>
      <small>Showing {{shown}} of {{total}} items</small>
  </div>
  </div>
    `,  
styles: [`.fidel-search{
     width:23vw;
  };.select{
  
  };a{
      text-decoration:none !important;
  } i{color:#36f}`]
})

@Injectable({
    providedIn: 'root',
  })
export class PaginationComponent implements OnInit {
    @Input() url:string;
    @Input() dtoName : any;
    @Output()
    newData = new EventEmitter();
    entry = 10;
    total = 0;
    shown = 0;
    pages: any[] = [];
    // pages = [];
    activeLink = 1;
    constructor(
        private notificationService: NotificationService,
        private repositoryService: RepositoryService) { }

    ngOnInit() {
        this.getdata();
        if(this.url.indexOf("?") < 0)
            this.url = this.url + "?";
    }
    navigatePage(page:number) {
        this.activeLink = page;
        let skip = (this.activeLink - 1) * this.entry;
        axios.get(this.url + `limit=${this.entry}&&skip=${skip}`).then(res => {
                var data = res.data;
                this.newData.emit(data[this.dtoName]);
                this.shown = Number((data[this.dtoName])['length'])
            })
    }
    backPage() {
        if (this.activeLink <= 1) return;
        this.activeLink--;
        let skip = (this.activeLink - 1) * this.entry;
        axios.get(this.url +
            `limit=${this.entry}&&skip=${skip}`).then(res => {
                var data = res.data;
                this.newData.emit(data[this.dtoName]);
                this.shown = Number((data[this.dtoName])['length'])
            })
    }
    nextPage() {
        if (this.activeLink >= this.pages.length) return;
        this.activeLink++;
        let skip = (this.activeLink - 1) * this.entry;
        axios.get(this.url +
            `limit=${this.entry}&&skip=${skip}`).then(res => {
                var data = res.data;
                this.newData.emit(data[this.dtoName]);
                this.shown = Number((data[this.dtoName])['length'])
            })
    }
    getdata() {
        let skip = 0;
        axios.get(this.url +
            `limit=${this.entry}&&skip=${skip}`).then(res => {
                var data = res.data;
                this.total = data['total'];
                console.log("Total:" + this.total);
                if(data[this.dtoName] == null) data[this.dtoName] = [];
                this.shown = Number((data[this.dtoName])['length'])
                let maxPage = Math.ceil(this.total / this.entry);
                this.pages = [];
                for (let i = 1; i <= maxPage; i++) {
                    this.pages.push(i);
                }
                this.activeLink = 1;
                this.newData.emit(data[this.dtoName]);
            }, err => {
                this.notificationService.showToastr('danger', 'failed to fech items')
            })
    }
}