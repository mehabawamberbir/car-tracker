import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-print-button',
  template: `
  <i class="fa fa-print" style="margin-right: 1rem;font-size:22px;cursor:pointer;" nbTooltip="Print this item" nbTooltipPlacement='left' 
                          nbTooltipStatus='info'></i>
  `,
  styles: []
})
export class PrintButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}