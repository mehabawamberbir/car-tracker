import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-save-button',
  template: `
    <button class='btn btn-success btn-md' [disabled]='disabled' type='submit'> <i class='fa fa-check mr-1'></i> <ng-content></ng-content></button>
  `,
  styles: [
  ]
})
export class SaveButtonComponent implements OnInit {

  constructor() { }
@Input() disabled:any;
  ngOnInit(): void {
  }
}