import { SearchService } from 'src/app/shared/services/search.service';
import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormControl } from "@angular/forms";
@Component({
    selector: 'app-search-input',
    template: `
  <div class="float-right mb-2">
   <div class="input-group app-search">
                <input id='search' [(ngModel)]="searchString" (input)='search()' placeholder=" search here" class="form-control" style="padding: 0px !important;">
                <div class="input-group-prepend" style="padding: 0px !important;">
                    <span class="input-group-text">
                        <nb-icon icon='search-outline'></nb-icon>
                    </span>
                </div>
            </div>
</div>
  `,
    styles: [`.app-search
    {
     /* width:100vw; */
  }`]
})
export class SearchInputComponent implements OnInit {
    @Input() data : Array <any> = []; 
    @Input() fields : any = [];
    @Output()
    result = new EventEmitter();
    searchString : string;
    constructor(private searchService: SearchService) { }
    search() {
        if (this.searchString) {
            let search = this.searchString.toLowerCase();
            let filteredData = this.searchService.searchData.filter((row: any) => {
                return this.fields.find((field:string) => {
                    if(row[field] == null) {
                        console.log("Field NULL:" + field);
                        return false;
                    }
                    return search == "" || row[field].toString().toLowerCase().indexOf(search) !== -1;
                });
            });
            this.result.emit(filteredData);
        }
        else {

            this.result.emit(this.searchService.searchData);
        }
    }
    ngOnInit() {
        // this.searchService.searchData=this.data;

    }
}