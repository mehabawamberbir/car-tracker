import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-toggle-button',
    template: `
        <nb-toggle id="{{type}}{{itemId}}" (change)='toggleStatus()' [(checked)]='checked' status="basic"></nb-toggle>
  `,
    styles: [
    ]
})
export class ToggleButtonComponent implements OnInit {

    @Input() itemId:string;
    @Input() checked:any;
    @Input() type:string;
    @Output() toggle = new EventEmitter();
    constructor() { }
    ngOnInit(): void {

    }
    toggleStatus() {
        this.toggle.emit(this.itemId)
    }
}