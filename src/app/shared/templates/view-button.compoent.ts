import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-button',
  template: `
   <i class="fa fa-chevron-circle-down view-icon" nbTooltip="View Detail" nbTooltipPlacement='left' nbTooltipStatus='info'></i>
  `,
  styles: [`
  .view-icon{
    color: #21507e;
    margin-right: 1rem;
    font-size:22px;
}
.view-icon:hover{
    cursor: pointer;
}`
  ]
})
export class ViewButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}