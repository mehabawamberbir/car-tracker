export class UserVm {
    public firstName: string;
    public middleName: string;
    public lastName: string;
    public email: string;
    public phoneNumber: string;
    public disabled: string;
    public id: string;
    public twoFactorEnabled: string;
    public emailConfirmed: string;
    public locked: string;
    public userName: string;
    public isManagable: string;
    public fullName: string;
    public username: string;
  }
  
  export class UserProfile {
    public firstName?: string;
    public middleName?: string;
    public lastName?: string;
    public userName?: string;
    public email?: string;
    public enabled?: string;
    public phoneNumber?: string;
    public twoFactorEnabled?: boolean;
    public lockoutEnabled?: boolean;
    public emailConfirmed?: boolean;
    public role?: string;
    public isManagable ?: boolean = this.userName != "admin";
  }
  