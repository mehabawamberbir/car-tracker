-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2022 at 10:00 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `transit_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_account_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `reconcileable` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `account_type_id`, `class_id`, `name`, `code`, `description`, `sub_account_id`, `status`, `reconcileable`, `created_at`, `updated_at`) VALUES
('ec22b909-2c9e-11ed-b575-00090ffe0001', 'c9d6c9a5-2c9e-11ed-b575-00090ffe0001', '81a9c6c6-7e00-4f8a-82b9-8c33a8faa14b', 'Test acc', 'Test acc', 'Test acc', NULL, 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `account_types`
--

CREATE TABLE `account_types` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coa_type_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `account_types`
--

INSERT INTO `account_types` (`id`, `coa_type_id`, `code`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
('c9d6c9a5-2c9e-11ed-b575-00090ffe0001', 'eb830dbc-88fd-4ef6-b77b-31685e9bea99', 'Test1', 'Test', 'test', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usd_rate` double(18,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`id`, `name`, `code`, `telephone`, `email`, `address`, `address2`, `bp`, `city`, `country`, `terms`, `start_date`, `end_date`, `remarks`, `usd_rate`, `status`, `created_at`, `updated_at`) VALUES
('5ab84210-5f98-4e54-92ae-4faeb226ca53', 'Agent Test1', 'agt1', '', '', '', '', '', '', 'Ethiopia', '', '2022-09-05', '2022-09-16', NULL, 1.00, 1, '2022-09-04 22:00:25', '2022-09-04 22:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `cargos`
--

CREATE TABLE `cargos` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chasis_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` double(18,2) NOT NULL DEFAULT 0.00,
  `weight` double(18,2) NOT NULL DEFAULT 0.00,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `way_bill_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gate_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sydonia_ref` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `consignees`
--

CREATE TABLE `consignees` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usd_rate` double(18,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consignees`
--

INSERT INTO `consignees` (`id`, `name`, `code`, `telephone`, `email`, `address`, `address2`, `bp`, `city`, `country`, `terms`, `usd_rate`, `status`, `created_at`, `updated_at`) VALUES
('eb0af234-e436-4033-bcd5-cdac50fdabbc', 'Consigne Test', 'CT1', '', '', '', '', '', '', 'Ethiopia', '', 1.00, 1, '2022-09-04 21:54:35', '2022-09-04 21:54:35');

-- --------------------------------------------------------

--
-- Table structure for table `containers`
--

CREATE TABLE `containers` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chasis_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` double(18,2) NOT NULL DEFAULT 0.00,
  `weight` double(18,2) NOT NULL DEFAULT 0.00,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `way_bill_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gate_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sydonia_ref` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usd_rate` double(18,2) DEFAULT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `code`, `telephone`, `email`, `address`, `address2`, `bp`, `city`, `country`, `account_id`, `terms`, `usd_rate`, `group_id`, `status`, `created_at`, `updated_at`) VALUES
('1c3f1c3b-75c3-4ae5-9086-c83721af7a51', 'Kal AND Henok', 'KAH', '', '', '', '', '', '', 'Ethiopia', NULL, '', 1.00, 'b33f9011-2924-4b02-bb47-a5c470405958', 1, '2022-09-04 21:54:09', '2022-09-04 21:54:09');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(18,2) NOT NULL DEFAULT 0.00,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lookups`
--

CREATE TABLE `lookups` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lookup_type_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `local_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lookups`
--

INSERT INTO `lookups` (`id`, `lookup_type_id`, `name`, `code`, `order`, `local_name`, `status`, `created_at`, `updated_at`) VALUES
('27717007-4dae-4910-9748-a483d5e92975', '14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Import Unimodal', 'IUM', 3, '', 1, '2022-09-04 21:15:21', '2022-09-04 21:16:25'),
('453e7ae7-50de-4799-9473-961e3a2b3cb3', '3ca40e34-2083-4ab7-88e1-ec7193beea70', '1-Class', '1', 1, '', 1, '2022-09-04 22:12:15', '2022-09-04 22:12:15'),
('4e2ccec3-5927-4603-9ef4-6550c5600d89', 'ca1526a2-eba1-4d71-ab03-407716284065', 'Default', 'DEF', 1, '', 1, '2022-09-04 21:18:57', '2022-09-04 21:18:57'),
('66fa5273-ddf7-41d1-99f8-4a8b74c4e814', '30ec5c8e-f61f-4fa6-a2f4-2399a1a30fba', 'Cargo', 'CARGO', 1, '', 1, '2022-09-04 22:32:16', '2022-09-04 22:32:16'),
('81a9c6c6-7e00-4f8a-82b9-8c33a8faa14b', '14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Transport', 'TRN', 4, '', 1, '2022-09-04 21:15:39', '2022-09-04 21:16:34'),
('b33f9011-2924-4b02-bb47-a5c470405958', 'abef8b9f-7d44-4032-aa6d-f6e7748079ae', 'Default', 'DEF', 1, '', 1, '2022-09-04 21:18:46', '2022-09-04 21:18:46'),
('b74061d6-a541-4da8-ad66-ceb07cfbe60f', '1aa7bfe2-6345-485a-be5a-9d336fa5baa1', 'Asset', '1', 1, '', 1, '2022-09-04 22:12:02', '2022-09-04 22:12:02'),
('e5fb653f-fbfe-4f83-8538-afd86c127788', '14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Import Multimodal', 'IMM', 1, '', 1, '2022-09-04 21:14:53', '2022-09-04 21:14:53'),
('eb830dbc-88fd-4ef6-b77b-31685e9bea99', '14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Import Local', 'ILO', 5, '', 1, '2022-09-04 21:16:04', '2022-09-04 21:16:44'),
('fdc45465-b741-49b2-b5d0-7278c8ee703f', '14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Export', 'EXP', 2, '', 1, '2022-09-04 21:15:00', '2022-09-04 21:16:17');

-- --------------------------------------------------------

--
-- Table structure for table `lookup_types`
--

CREATE TABLE `lookup_types` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `local_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lookup_types`
--

INSERT INTO `lookup_types` (`id`, `name`, `code`, `order`, `local_name`, `status`, `created_at`, `updated_at`) VALUES
('14507b9b-0249-4ecd-a4d1-5d918c0ef0da', 'Operation Category', 'OPCAT', 1, '', 1, '2022-09-04 21:13:01', '2022-09-04 21:13:01'),
('1aa7bfe2-6345-485a-be5a-9d336fa5baa1', 'COA Type', 'COA_TYPE', 1, '', 1, '2022-09-04 22:11:42', '2022-09-04 22:11:42'),
('30ec5c8e-f61f-4fa6-a2f4-2399a1a30fba', 'Loading Type', 'LOAD_TYPE', 1, '', 1, '2022-09-04 21:17:20', '2022-09-04 21:17:20'),
('3ca40e34-2083-4ab7-88e1-ec7193beea70', 'Account Class', 'ACC_CLASS', 1, '', 1, '2022-09-04 21:43:51', '2022-09-04 21:43:51'),
('abef8b9f-7d44-4032-aa6d-f6e7748079ae', 'Customer Group', 'CG', 1, '', 1, '2022-09-04 21:18:15', '2022-09-04 21:18:15'),
('ca1526a2-eba1-4d71-ab03-407716284065', 'Vendor Group', 'VG', 1, '', 1, '2022-09-04 21:18:24', '2022-09-04 21:18:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2022_08_27_085300_create_table_initial', 1),
(2, '2022_09_04_204310_phase2', 2);

-- --------------------------------------------------------

--
-- Table structure for table `operations`
--

CREATE TABLE `operations` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operation_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consignee_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `freight_forwarder_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reception_date` date DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  `atd_operation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `esl_operation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bol` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `gross_weight` double(18,2) NOT NULL DEFAULT 0.00,
  `loading_type_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partial_shipment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `container_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrival_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_sales_product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_type_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_billable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_sellable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_account_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchasable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exp_account_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_at_invoice_detail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trackable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disp_at_operation_exp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
('1QWSD', 'Admin', '2022-09-04 20:08:41', '2022-09-04 20:08:41'),
('23DF', 'User', '2022-09-04 20:08:41', '2022-09-04 20:08:41');

-- --------------------------------------------------------

--
-- Table structure for table `transporters`
--

CREATE TABLE `transporters` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trucks`
--

CREATE TABLE `trucks` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chasis_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` double(18,2) NOT NULL DEFAULT 0.00,
  `weight` double(18,2) NOT NULL DEFAULT 0.00,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `way_bill_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gate_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sydonia_ref` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
('S001', 'First User', 'user1@gmail.com', NULL, '$2y$10$kK/E4gUTzQMOH1aPXhPnReqr339SKWQp.YqBSvQC6CiKVInQFCmzS', NULL, '2022-09-04 20:08:41', '2022-09-04 20:08:41'),
('S002', 'Admin User', 'admin@gmail.com', NULL, '$2y$10$EFt33Epgf8cem/jC.g.1EuBVcAENotbclrU/ZDPP.LeYNifCZQi2e', NULL, '2022-09-04 20:08:40', '2022-09-04 20:08:40');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usd_rate` double(18,2) DEFAULT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `name`, `code`, `telephone`, `email`, `address`, `address2`, `bp`, `city`, `country`, `account_id`, `terms`, `balance`, `usd_rate`, `group_id`, `status`, `created_at`, `updated_at`) VALUES
('af78883f-6a28-4c8a-896d-b6cfcfd7d7e6', 'Vendor Test1', 'VT1', '098', 'test@gmail.com', '', '', '', '', '', 'ec22b909-2c9e-11ed-b575-00090ffe0001', '', '0', 1.00, '4e2ccec3-5927-4603-9ef4-6550c5600d89', 1, '2022-09-04 22:25:09', '2022-09-04 22:25:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accounts_account_type_id_foreign` (`account_type_id`),
  ADD KEY `accounts_class_id_foreign` (`class_id`);

--
-- Indexes for table `account_types`
--
ALTER TABLE `account_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_types_coa_type_id_foreign` (`coa_type_id`);

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cargos_location_id_foreign` (`location_id`);

--
-- Indexes for table `consignees`
--
ALTER TABLE `consignees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `containers`
--
ALTER TABLE `containers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `containers_location_id_foreign` (`location_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customers_account_id_foreign` (`account_id`),
  ADD KEY `customers_group_id_foreign` (`group_id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_product_id_foreign` (`product_id`);

--
-- Indexes for table `lookups`
--
ALTER TABLE `lookups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lookups_lookup_type_id_foreign` (`lookup_type_id`);

--
-- Indexes for table `lookup_types`
--
ALTER TABLE `lookup_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operations`
--
ALTER TABLE `operations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operations_category_id_foreign` (`category_id`),
  ADD KEY `operations_consignee_id_foreign` (`consignee_id`),
  ADD KEY `operations_freight_forwarder_id_foreign` (`freight_forwarder_id`),
  ADD KEY `operations_loading_type_id_foreign` (`loading_type_id`),
  ADD KEY `operations_agent_id_foreign` (`agent_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_product_type_id_foreign` (`product_type_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transporters`
--
ALTER TABLE `transporters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trucks`
--
ALTER TABLE `trucks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trucks_location_id_foreign` (`location_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_roles_user_id_foreign` (`user_id`),
  ADD KEY `user_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendors_group_id_foreign` (`group_id`),
  ADD KEY `vendors_account_id_foreign` (`account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_account_type_id_foreign` FOREIGN KEY (`account_type_id`) REFERENCES `account_types` (`id`),
  ADD CONSTRAINT `accounts_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `account_types`
--
ALTER TABLE `account_types`
  ADD CONSTRAINT `account_types_coa_type_id_foreign` FOREIGN KEY (`coa_type_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `cargos`
--
ALTER TABLE `cargos`
  ADD CONSTRAINT `cargos_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `containers`
--
ALTER TABLE `containers`
  ADD CONSTRAINT `containers_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `customers_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `lookups`
--
ALTER TABLE `lookups`
  ADD CONSTRAINT `lookups_lookup_type_id_foreign` FOREIGN KEY (`lookup_type_id`) REFERENCES `lookup_types` (`id`);

--
-- Constraints for table `operations`
--
ALTER TABLE `operations`
  ADD CONSTRAINT `operations_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`),
  ADD CONSTRAINT `operations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `lookups` (`id`),
  ADD CONSTRAINT `operations_consignee_id_foreign` FOREIGN KEY (`consignee_id`) REFERENCES `consignees` (`id`),
  ADD CONSTRAINT `operations_freight_forwarder_id_foreign` FOREIGN KEY (`freight_forwarder_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `operations_loading_type_id_foreign` FOREIGN KEY (`loading_type_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_product_type_id_foreign` FOREIGN KEY (`product_type_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `trucks`
--
ALTER TABLE `trucks`
  ADD CONSTRAINT `trucks_location_id_foreign` FOREIGN KEY (`location_id`) REFERENCES `lookups` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `vendors`
--
ALTER TABLE `vendors`
  ADD CONSTRAINT `vendors_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `vendors_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `lookups` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
